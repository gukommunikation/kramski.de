#####################
## configures felogin
#####################
plugin.tx_felogin_pi1.templateFile = {$path.private}/Templates/felogin.html
plugin.tx_felogin_pi1.welcomeHeader_stdWrap.wrap = <h1>|</h1>
plugin.tx_felogin_pi1.successHeader_stdWrap.wrap = <h1>|</h1>
plugin.tx_felogin_pi1.logoutHeader_stdWrap.wrap = <h1>|</h1>
plugin.tx_felogin_pi1.errorHeader_stdWrap.wrap = <h1>|</h1>
plugin.tx_felogin_pi1.forgotHeader_stdWrap.wrap = <h1>|</h1>
plugin.tx_felogin_pi1.changePasswordHeader_stdWrap.wrap = <h1>|</h1>
plugin.tx_felogin_pi1.welcomeMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.successMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.logoutMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.errorMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.forgotMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.forgotErrorMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.forgotResetMessageEmailSentMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.changePasswordNotValidMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.changePasswordTooShortMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.changePasswordNotEqualMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.changePasswordMessage_stdWrap.wrap = <p>|</p>
plugin.tx_felogin_pi1.changePasswordDoneMessage_stdWrap.wrap = <p>|</p>

#################
## indexed search
#################
plugin.tx_indexedsearch.templateFile = {$path.private}/Templates/indexedsearch.html
config.index_metatags = 0
page.config.index_metatags = 0

############
## downloads
############
plugin.tx_rtgfiles_pi1.template = {$path.private}/Templates/downloadlist.html
plugin.tx_rtgfiles_pi1 {
  iconsUploadPath = {$path.public}/Images/downloadicons/
  types {
    pdf {
      title = PDF Dokument
      image = pdf.png
    } 
    doc {
      title = MS Word Dokument
      image = doc.png
    } 
    docx {
      title = MS Word Dokument
      image = docx.png
    } 
    xls {
      title = MS Excel Dokument
      image = xls.png
    } 
    xlsx {
      title = MS Excel Dokument
      image = xlsx.png
    } 
    rtf {
      title = Rich Text Format
      image = doc.png
    } 
    jpg {
      title = Bild
      image = doc.png
    } 
    gif {
      title = Grafik
      image = doc.png
    } 
    png {
      title = Bild
      image = doc.png
    } 
    bmp {
      title = Bild
      image = doc.png
    }
    zip {
      title = ZIP komprimierter Ordner
      image = zip.png
    }
    txt {
      title = Plain text 
      image = doc.png
    }
    exe {
      title = Ausführende exe Datei
      image = zip.png
    }    
    html {
      title = Externer Link
      image = link.png
    }    
  }
}
