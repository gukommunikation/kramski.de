<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2007 Robert Gonda <robert.gonda@gmail.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/


/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *
 */

if( !defined ('PATH_typo3conf') ) die ( 'Could not access this script directly!' );

//require_once( t3lib_extMgm::extPath( 'lang', 'lang.php' ) );
//require_once( t3lib_extMgm::extPath( 'cms', 'tslib/class.tslib_content.php' ) );
//require_once( PATH_t3lib . 'class.t3lib_page.php' );


/**
 * Plugin 'Documents' for the 'rtg_files' extension.
 *
 * @author	Robert Gonda <robert.gonda@gmail.com>
 * @package	TYPO3
 * @subpackage	tx_rtgfiles
 * @author_company	BSP Magnetica s.r.o.
 */
class tx_rtgfiles_download {

	var $prefixId = 'tx_rtgfiles_pi1';
	var $GPvars;
	var $filesPath = 'uploads/tx_rtgfiles/';
	var $file;

	/**
	 * Creates an instance of this class
	 *
	 * @return	void
	 */
	public function __construct() {

		$feUserObj = tslib_eidtools::initFeUser(); // Initialize FE user object
		tslib_eidtools::connectDB();

		$this->GPvars = t3lib_div::_GP( $this->prefixId );
		$this->GPvars['uid'] = intval( $this->GPvars['uid'] );

		if( $this->GPvars['uid'] < 1 ) {
			die( 'ERROR FILE DOWNLOAD!' );
		}
	}

	/**
	 * The main method of the PlugIn
	 *
	 * @return	The		content that is displayed on the website
	 */
	public function main() {

		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery( 'uid,file,url,clicks', 'tx_rtgfiles_files', 'uid = '.$this->GPvars['uid'].' AND deleted=0 AND hidden=0' );
		if( $res ) {
			if( $this->file = $GLOBALS['TYPO3_DB']->sql_fetch_assoc( $res ) ) {
				if( file_exists( $this->filesPath.$this->file['file'] ) ) {

					// Update clicks
					$values = array( 'clicks' => intval( $this->file['clicks'] ) + 1 );
					$GLOBALS['TYPO3_DB']->exec_UPDATEquery( 'tx_rtgfiles_files', 'uid = '.$this->GPvars['uid'], $values );

					// Redirect to external URL
					if( $this->file['url'] != '' ) {
						header( 'Location: '.$this->file['url'] );
						exit;
					}

					// Set MIME type by file extension
					$ext = strtolower( substr( ( $t = strrchr( $data['file'], '.' ) ) !== false ? $t : '' ,1 ) );
					switch( $ext ) {
						case 'pdf': $ctype = 'application/pdf'; break;
						case 'exe': $ctype = 'application/octet-stream'; break;
						case 'zip': $ctype = 'application/zip'; break;
						case 'doc': $ctype = 'application/msword'; break;
						case 'xls': $ctype = 'application/vnd.ms-excel'; break;
						case 'ppt': $ctype = 'application/vnd.ms-powerpoint'; break;
						case 'gif': $ctype = 'image/gif'; break;
						case 'png': $ctype = 'image/png'; break;
						case 'jpeg':
						case 'jpe':
						case 'jpg': $ctype = 'image/jpg'; break;
						case 'svg': $ctype = 'image/svg+xml'; break;
						case 'flv': $ctype = 'video/x-flv'; break;
						case 'mpg': $ctype = 'application/octet-stream'; break;
						case 'swf': $ctype = 'application/x-shockwave-flash'; break;
						default:    $ctype = 'application/force-download';
					}

					// Header data
					header( 'Pragma: public' );
					header( 'Expires: 0' );
					header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
					// header( 'Cache-Control: private', false );
					header( 'Content-Type: '.$ctype );
					// header( 'Content-Disposition: inline; filename="'.basename( $this->file['file'] ).'"' );

					// File attachment
					header( "Content-Disposition: attachment; filename=".basename( $this->file['file'] ).";" );
					header( "Content-Transfer-Encoding: binary" );
					header( "Content-Length: ".filesize( $this->filesPath.$this->file['file'] ) );
					readfile( $this->filesPath.$this->file['file'] );
					exit;
				}
			}
		}
		die( 'ERROR FILE DOWNLOAD!!' );
	}
}

// Make instance:
$SOBE = t3lib_div::makeInstance( 'tx_rtgfiles_download' );
$SOBE->main();

?>