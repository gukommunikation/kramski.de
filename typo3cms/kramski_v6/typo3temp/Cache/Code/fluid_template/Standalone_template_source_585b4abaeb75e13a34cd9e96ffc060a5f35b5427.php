<?php
class FluidCache_Standalone_template_source_585b4abaeb75e13a34cd9e96ffc060a5f35b5427 extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;

return '
			
	<dl class="powermail_all">
		
			
					



<dt class="powermail_all_label powermail_all_label_errormarkerisempty_05">
	first name / name
</dt>
<dd class="powermail_all_value powermail_all_value_errormarkerisempty_05">
	
			Pentti Vataja
		
</dd>
				
		
			
					



<dt class="powermail_all_label powermail_all_label_errormarkerisempty_03">
	company
</dt>
<dd class="powermail_all_value powermail_all_value_errormarkerisempty_03">
	
			Enternet Oy
		
</dd>
				
		
			
					



<dt class="powermail_all_label powermail_all_label_errormarkerisempty_02">
	e-mail address
</dt>
<dd class="powermail_all_value powermail_all_value_errormarkerisempty_02">
	
			info@endcryptor.com
		
</dd>
				
		
			
					



<dt class="powermail_all_label powermail_all_label_errormarkerisempty_01">
	message
</dt>
<dd class="powermail_all_value powermail_all_value_errormarkerisempty_01">
	
			Hi!<br />
<br />
Dear reader, please forward this message to IT Department or to the person in management who is responsible for IT development.<br />
<br />
Can your organization protect email communication from unauthorized viewing? Encryption is the only way to ensure that only the intended recipient sees the message. Encryption also ensures to the receiver that the email came from the claimed sender in unaltered form.<br />
<br />
We offer our email encryption solution - EndCryptor - modern and easy to use email client that uses the best standards and latest techniques. It has been patented in the USA and is being developed in Finland.<br />
<br />
The product has security properties that competitors like PGP and SMIME cannot offer - e.g. even if a hacker gets all the current secret key material he/she can\'t decrypt old encrypted emails. EndCryptor also recovers automatically from the attack.<br />
<br />
The emails are encrypted on sender\'s computer and decrypted on receiver\'s computer - this end-to-end encryption means that an unencrypted email or an encryption key is never on a third party computer.<br />
<br />
Ensure that your organization can protect the email communication!<br />
<br />
Best regards,<br />
<br />
Pentti Vataja<br />
Managing Director<br />
Enternet Oy<br />
Finland<br />
Tel: +358 50 550 0131<br />
info@endcryptor.com<br />
www.endcryptor.com <br />

		
</dd>
				
		
	</dl>

	<div class="clear"></div>

		';
}


}
#1477566997    3012      