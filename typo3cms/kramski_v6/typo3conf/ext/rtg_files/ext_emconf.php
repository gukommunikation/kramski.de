<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "rtg_files".
 *
 * Auto generated 15-09-2014 13:20
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Documents download',
	'description' => 'Documents/files and URLs archive, list, searching, download files, download statistics.',
	'category' => 'plugin',
	'shy' => 0,
	'version' => '1.5.4',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 1,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Robert Gonda',
	'author_email' => 'robert.gonda@gmail.com',
	'author_company' => 'BSP Magnetica s.r.o.',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
			'php' => '5.0.0-0.0.0',
			'typo3' => '4.1.0-6.1.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:37:{s:9:"ChangeLog";s:4:"2557";s:12:"ext_icon.gif";s:4:"48c9";s:17:"ext_localconf.php";s:4:"b427";s:14:"ext_tables.php";s:4:"3686";s:14:"ext_tables.sql";s:4:"83ce";s:12:"flexform.xml";s:4:"802a";s:13:"locallang.xml";s:4:"caf4";s:16:"locallang_db.xml";s:4:"0123";s:10:"README.txt";s:4:"9fa9";s:7:"tca.php";s:4:"d9ee";s:21:"tx_rtgfiles_files.gif";s:4:"0a6c";s:24:"tx_rtgfiles_files__f.gif";s:4:"887d";s:24:"tx_rtgfiles_files__h.gif";s:4:"f852";s:25:"tx_rtgfiles_files__ht.gif";s:4:"7d92";s:24:"tx_rtgfiles_files__t.gif";s:4:"b893";s:24:"tx_rtgfiles_files__x.gif";s:4:"51ce";s:23:"tx_rtgfiles_systems.gif";s:4:"48c9";s:26:"tx_rtgfiles_systems__f.gif";s:4:"299d";s:26:"tx_rtgfiles_systems__h.gif";s:4:"3421";s:27:"tx_rtgfiles_systems__ht.gif";s:4:"2ebc";s:26:"tx_rtgfiles_systems__t.gif";s:4:"06c3";s:26:"tx_rtgfiles_systems__x.gif";s:4:"3736";s:14:"doc/manual.sxw";s:4:"5a08";s:19:"doc/wizard_form.dat";s:4:"fe1f";s:20:"doc/wizard_form.html";s:4:"8a2e";s:34:"lib/class.tx_rtgfiles_download.php";s:4:"6538";s:14:"pi1/ce_wiz.gif";s:4:"02b6";s:29:"pi1/class.tx_rtgfiles_pi1.php";s:4:"9eee";s:37:"pi1/class.tx_rtgfiles_pi1_wizicon.php";s:4:"b206";s:13:"pi1/clear.gif";s:4:"cc11";s:19:"pi1/iconfolders.gif";s:4:"a271";s:22:"pi1/iconfoldersact.gif";s:4:"fe80";s:17:"pi1/locallang.xml";s:4:"81a5";s:17:"pi1/template.html";s:4:"b058";s:19:"pi1/template_2.html";s:4:"8516";s:24:"pi1/static/editorcfg.txt";s:4:"c099";s:20:"pi1/static/setup.txt";s:4:"6597";}',
);

?>