/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function datacookieopen() {
    var url      = window.location.href;

    var TextHeadline = "";
    var TextInfo = "";
    var ButtonAccept = "";
    var ButtonDatenschutz = "";
    var UrlDatenschutz = "";
    var germanTextHeadline = 'Kramski informiert:';
    var germanTextInfo = "Mit der weiteren Nutzung unseres Angebots erklären Sie sich mit der Verwendung von Cookies einverstanden.";
    var germanButtonAccept = "Ich bin einverstanden";
    var germanButtonDatenschutz = "Datenschutzseite";
	var germanUrlDatenschutz = "/datenschutz.html";
    var englishTextHeadline = 'Kramski informs:';
    var englishTextInfo = "By continuing to use our website, you agree to the use of cookies.";
    var englishButtonAccept = "I agree";
    var englishButtonDatenschutz = "Privacy Policy";
	var englishUrlDatenschutz = "/en/data-protection.html";
    
    if (url.indexOf("/en") >= 0) {
        ButtonDatenschutz = englishButtonDatenschutz;
        ButtonAccept = englishButtonAccept;
        TextHeadline = englishTextHeadline;
        TextInfo = englishTextInfo;
		UrlDatenschutz = englishUrlDatenschutz;
		
    } else {
        ButtonDatenschutz = germanButtonDatenschutz;
        ButtonAccept = germanButtonAccept;
        TextHeadline = germanTextHeadline;
        TextInfo = germanTextInfo;
		UrlDatenschutz = germanUrlDatenschutz;
    }
    var cookie   = '<div id="datacookie">';
    cookie      += '       <div class="topborder">';
    cookie      += '           <div class="cookielement">';
    cookie      += '                        <div class="cookietext">';
    cookie      += '                            <span class="headline">'+TextHeadline+'</span> '+TextInfo;
    cookie      += '                        </div>';
    cookie      += '                <a class="link button" href="'+UrlDatenschutz+'">'+ButtonDatenschutz+'</a>';
    cookie      += '                        <div class="yes button" onClick="datacookieclose()">'+ButtonAccept+'</div>';                          
    cookie      += '            </div>';
    cookie      += '        </div>';
    cookie      += '    </div>';
//    cookie = decodeURIComponent(escape(cookie));
    
    $("body").append(cookie);
    $("#datacookie").animate({ bottom: "+0px"}, 1000, function() {});
}

function datacookieclose() {
	document.cookie = 'hideDataCookie=1;path=/';
    var height = $("#datacookie").height();
    $("#datacookie").animate({ bottom: "-"+height+"px"}, 1000, function() {$("#datacookie").remove()});
}

$(document).ready(function() {
	if(document.cookie.indexOf('hideDataCookie=1') == -1) {
		datacookieopen();
	}
        showtestarea(); 
});

/*Anpassungen für Bewerbungsformular*/
function showtestarea() {

    var url = window.location.href;

    
    if (url === "https://www.kramski.de/aktuelles/artikel-lesen/article/besucherrekord-fuer-kramski-bei-ausbildungsmesse-in-pforzheim.html") {
        addVideo();
    }
    
    if(url.indexOf("?test=true") > -1) // This doesn't work, any suggestions?
    {
        
         console.log("testarea is enabled");
    } else {
        $( "a[href='https://www.kramski.de/bewerbungsformular.html']").hide();
    }
    
//    addVideo();
}

function addVideo() {
    var video = "<div class='youtubevideo' style='display: none; margin-top:30px;'>";
        video += '<iframe width="100%" height="315" src="https://www.youtube.com/embed/AW-0JdXOTcw" frameborder="0" allowfullscreen></iframe>'
        video +=    "</div>";
    
    $(".news-single-content").append(video);
}
function loadPHP() {
    
}
function checkdatabase() {
            $(this).load('../Php/bewerbungcheck.php');
            console.log("ajax ready");
           console.log("Starte Übertragung");
            // Loading GIF im Div Containter "Kommentar" anzeigen
//            $('.kommentar').append('<img src="images/ajax-loader.gif" alt="loading..." id="loading" />');

            var name = "$('#name').val()";
            var email = "$('#email').val()";
            var text = "$('#text').val()";
            $.ajax({
               type: 'POST',
               url: '../Php/bewerbungcheck.php',
               data: 'name=' + name + '&email=' + email + '&text=' + text,
               
               success: function(){
                    console.log("Eintrag erfolgreich");
               },
               error: function(){
                   console.log("Datenübertragung fehlgeschlagen");
               }
               
            });
            return false;    

}