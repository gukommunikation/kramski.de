page = PAGE
page {
  typeNum = 0
  meta {
    viewport = width=device-width, initial-scale=1, maximum-scale=1
##       viewport = width=1000
  }
  shortcutIcon = {$path.public}/Images/favico.ico

  bodyTagCObject = CASE
  bodyTagCObject {
     key.field = backend_layout
     key.ifEmpty.data = levelfield:-2, backend_layout_next_level, slide
     default = TEXT
     default.value = standard
     1 = TEXT
     1.value = standard
     6 = TEXT
     6.value = folgeseite
     stdWrap.wrap = <body class ="|">
  }

  headerData.400 = TEXT
  headerData.400.value = <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://{$siteurl}/{$path.public}/Images/apple-touch-icons/apple-touch-icon-144x144-precomposed.png" />
  headerData.410 = TEXT
  headerData.410.value = <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://{$siteurl}/{$path.public}/Images/apple-touch-icons/apple-touch-icon-114x114-precomposed.png" />
  headerData.420 = TEXT
  headerData.420.value = <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://{$siteurl}/{$path.public}/Images/apple-touch-icons/apple-touch-icon-72x72-precomposed.png" />
  headerData.430 = TEXT
  headerData.430.value = <link rel="apple-touch-icon-precomposed" href="http://{$siteurl}/{$path.public}/Images/apple-touch-icons/apple-touch-icon-precomposed.png" />

  headerData.440 = TEXT
  headerData.440.value = <meta property="og:image" content="http://{$siteurl}/{$path.public}/Images/OpenGraph.png" />
  headerData.450 = TEXT
  headerData.450.value = <meta property="og:title" content="{$sitename}" />
  headerData.460 = TEXT
  headerData.460.value = <meta property="og:site_name" content="{$sitename}" />
  headerData.470 = TEXT
  headerData.470.value = <meta property="og:description" content="{$sitedesc}" />

  10 = FLUIDTEMPLATE
  10.file.cObject = CASE
  10.file.cObject {
    key.data = levelfield:-1, backend_layout_next_level, slide
    key.override.field = backend_layout
    default = TEXT
    default.value = {$path.private}/Templates/standard.html
    1 = TEXT
    1.value = {$path.private}/Templates/standard.html
    6 = TEXT
    6.value = {$path.private}/Templates/folgeseite.html
  }
  10.partialRootPath = {$path.private}/Partials/
  10.layoutRootPath = {$path.private}/Layouts/

  10.variables {

    belayout = TEXT
    belayout.data = levelfield:-1,backend_layout_next_level,slide
    belayout.override.field = backend_layout

    header < styles.content.get
    header.select.where = colPos = 0

    content < styles.content.get
    content.select.where = colPos = 1

    border < styles.content.get
    border.select.where = colPos = 2

    logo < styles.content.get
    logo.select.where = colPos = 3
    logo.select.pidInList = 45

    sitemap < styles.content.get
    sitemap.select.where = colPos = 4
    sitemap.select.pidInList = 45

    contact < styles.content.get
    contact.select.where = colPos = 5
    contact.select.pidInList = 45

    sondermeldung < styles.content.get
    sondermeldung.select.where = colPos = 6

    metanav = HMENU
    metanav.special = directory
    metanav.special.value = 11
    metanav.wrap = <div id="metanav"><ul class="menu">|</ul><a href="https://www.facebook.com/pages/Kramski-GmbH/160494003969843" target="_blank"><div class="facebook"></div></a></div>
    metanav.1 = TMENU
    metanav.1 {
      expAll = 0
      NO.stdWrap.htmlSpecialChars = 1
      NO.allWrap = <li class="NO">|</li>
      ACT = 1
      ACT.stdWrap.htmlSpecialChars = 1
      ACT.allWrap = <li class="ACT">|</li>
      CUR = 1
      CUR.stdWrap.htmlSpecialChars = 1
      CUR.allWrap =  <li class="ACT">|</li>
    }

    mainmenu = HMENU
    mainmenu.special = directory
    mainmenu.special.value = 1
    mainmenu.wrap = <div id="mainmenu"><ul class="menu">|</ul></div>
    mainmenu.1 = TMENU
    mainmenu.1 {
      expAll = 0
      NO.stdWrap.htmlSpecialChars = 1
      NO.allWrap = <li class="NO">|</li>
      ACT = 1
      ACT.stdWrap.htmlSpecialChars = 1
      ACT.allWrap = <li class="ACT">|</li>
      CUR < .ACT
    }

    submenu = HMENU
    submenu.entryLevel = 1
    submenu.wrap = <div id="submenu"><ul class="menu">|</ul></div>
    submenu.1 = TMENU
    submenu.1 {
      expAll = 0
      NO.stdWrap.htmlSpecialChars = 1
      NO.allWrap = <li class="NO">|</li>
      ACT = 1
      ACT.stdWrap.htmlSpecialChars = 1
      ACT.allWrap = <li class="ACT">|</li>
      CUR < .ACT
    }

    parentnav = HMENU
    parentnav.special = rootline
    parentnav.special.range = 1|1
    parentnav.stdWrap.wrap = <div id="parentnav"><ul class="menu"> | </ul></div>
    parentnav.1 = TMENU
    parentnav.1 {
      expAll = 0
      NO.stdWrap.htmlSpecialChars = 1
      NO.allWrap = <li>|</li>
      ACT < .NO
      CUR < .NO
    }

    leftmenu = COA
    leftmenu.wrap = <div id="leftmenu"><ul class="menu level1">|</ul></div>
    leftmenu.1 = HMENU
    leftmenu.1.entryLevel = 2
    leftmenu.1.1 = TMENU
    leftmenu.1.1 {
      expAll = 0
      NO = 1
      NO.wrapItemAndSub= <li class="NO">|</li>
      ACT < .NO
      ACT.wrapItemAndSub = <li class="ACT">|</li>
      CUR = 1
      CUR.wrapItemAndSub = <li class="CUR">|</li>
    }
    leftmenu.1.2 < .1
    leftmenu.1.2.wrap =  <ul class="submenu menu level2">|</ul>
    leftmenu.1.2 = TMENU
    leftmenu.1.2 {
      expAll = 0
      NO = 1
      NO.wrapItemAndSub = <li class="NO">|</li>
      ACT < .NO
      ACT.wrapItemAndSub = <li class="ACT">|</li>
      CUR = 1
      CUR.wrapItemAndSub = <li class="CUR">|</li>
    }
    breadcrumb = HMENU
    breadcrumb.special = rootline
    breadcrumb.special.range = 0|5
    breadcrumb.entryLevel = 0
    breadcrumb.wrap =  <div id="breadcrumb">|</div>
    breadcrumb.1 = TMENU
    breadcrumb.1 {
      noBlur = 1
      NO {
        allWrap = |&#124; |*| |&#124; |*| |
        stdWrap.htmlSpecialChars = 1
      }
    }
    ## output mobilemenu
    mobilemenu = COA
    mobilemenu.wrap = <div id="mobilemenu">|</div>
    mobilemenu.1 = HMENU
    mobilemenu.1.entryLevel = 0
    mobilemenu.1.wrap = <div class="mobilemenu-wrap"><ul class="menu level1">|</ul></div>
    mobilemenu.1.1 = TMENU
    mobilemenu.1.1 {
      expAll = 1
      NO = 1
      NO.wrapItemAndSub = <li class="NO"><span class="menuarrow"></span>|</li>
      ACT < .NO
      ACT.wrapItemAndSub = <li class="ACT"><span class="menuarrow"></span>|</li>
      CUR = 1
      CUR.wrapItemAndSub = <li class="CUR"><span class="menuarrow"></span>|</li>
      ACTIFSUB < .NO
      ACTIFSUB = 1
      ACTIFSUB.wrapItemAndSub = <li class="ACT"><span class="menuarrow"></span>|</li>
    }
    mobilemenu.1.2 < .1
    mobilemenu.1.2.wrap =  <ul class="submenu menu level2">|</ul>
    mobilemenu.1.2 = TMENU
    mobilemenu.1.2 {
      expAll = 1
      NO = 1
      NO.wrapItemAndSub = <li class="NO">|</li>
      ACT < .NO
      ACT.wrapItemAndSub = <li class="ACT">|</li>
      CUR = 1
      CUR.wrapItemAndSub = <li class="CUR">|</li>
      ACTIFSUB < .NO
      ACTIFSUB = 1
      ACTIFSUB.wrapItemAndSub = <li class="ACT">|</li>
    }
    mobilemenu.2 = HMENU
    mobilemenu.2.special = directory
    mobilemenu.2.special.value = 51
    mobilemenu.2.wrap = <div id="metanav-mobile"><ul class="menu">|</ul></div>
    mobilemenu.2.1 = TMENU
    mobilemenu.2.1 {
      expAll = 0
      NO.stdWrap.htmlSpecialChars = 1
      NO.allWrap = <li class="NO">|</li>
      ACT = 1
      ACT.stdWrap.htmlSpecialChars = 1
      ACT.allWrap = <li class="ACT">|</li>
      CUR = 1
      CUR.stdWrap.htmlSpecialChars = 1
      CUR.allWrap =  <li class="ACT">|</li>
    }

  }
}


temp.languagede = COA
temp.languagede.wrap = <div class="language-wrap">|</div><div class="world"></div>
temp.languagede {
  10 = HMENU
    10 {
      special = language
      special.value = 1,0
      special.normalWhenNoLanguage = 0
      1 = TMENU
      1 {
        NO = 1
        NO.linkWrap = |
        NO.stdWrap.cObject = TEXT
        NO.stdWrap.cObject {
          value = <div class="lang-mob-item">&nbsp;/&nbsp;ENGLISH</div> ||  <div class="lang-mob-item act">DEUTSCH</div>
        }
        USERDEF1 = 1
        USERDEF1 < .NO
        USERDEF1 {
          doNotLinkIt = 1
          stdWrap.typolink.parameter = 55
        }
    }
    }
}
temp.languageen = COA
temp.languageen.wrap = <div class="language-wrap">|</div><div class="world"></div>
temp.languageen {
  10 = HMENU
    10 {
      special = language
      special.value =1,0
      special.normalWhenNoLanguage = 0
      1 = TMENU
      1 {
        NO = 1
        NO.linkWrap = |
        NO.stdWrap.cObject = TEXT
        NO.stdWrap.cObject {
          value =  <div class="lang-mob-item act">ENGLISH</div> || <div class="lang-mob-item">DEUTSCH&nbsp;/&nbsp;</div>
        }
        USERDEF1 = 1
        USERDEF1 < .NO
        USERDEF1 {
          doNotLinkIt = 1
          stdWrap.typolink.parameter = 55
        }
      }
  }
}
page.10.variables.language < temp.languagede
[globalVar = GP:L = 1]
page.10.variables.language < temp.languageen
[global]

temp.languagemobilede = COA
temp.languagemobilede {
  10 = HMENU
    10 {
      special = language
      special.value = 0,1
      special.normalWhenNoLanguage = 0
      1 = TMENU
      1 {
        NO = 1
        NO.linkWrap = |
        NO.stdWrap.cObject = TEXT
        NO.stdWrap.cObject {
          value = <div class="lang-mob-item act">DE</div> ||  <div class="lang-mob-item">EN</div>
        }
        USERDEF1 = 1
        USERDEF1 < .NO
        USERDEF1 {
          doNotLinkIt = 1
          stdWrap.typolink.parameter = 55
        }
      }
  }
}

temp.languagemobileen = COA
temp.languagemobileen {
  10 = HMENU
    10 {
      special = language
      special.value = 0,1
      special.normalWhenNoLanguage = 0
      1 = TMENU
      1 {
        NO = 1
        NO.linkWrap = |
        NO.stdWrap.cObject = TEXT
        NO.stdWrap.cObject {
          value =  <div class="lang-mob-item">DE</div> || <div class="lang-mob-item act">EN</div>
        }
        USERDEF1 = 1
        USERDEF1 < .NO
        USERDEF1 {
          doNotLinkIt = 1
          stdWrap.typolink.parameter = 55
        }
      }
  }
}
page.10.variables.languagemobile < temp.languagemobilede
[globalVar = GP:L = 1]
page.10.variables.languagemobile < temp.languagemobileen
[global]


temp.thirdlevelmenude = COA
temp.thirdlevelmenude {
  5 = TEXT
  5.value = <li class="select">Direkt zu ...</li>
}
temp.thirdlevelmenuen = COA
temp.thirdlevelmenuen {
  5 = TEXT
  5.value = <li class="select">Directly to ...</li>
}
page.10.variables.thirdlevelmenu < temp.thirdlevelmenude
[globalVar = GP:L = 1]
page.10.variables.thirdlevelmenu < temp.thirdlevelmenuen
[global]

page.10.variables.mobilemenubutton = TEXT
page.10.variables.mobilemenubutton.value = Men&uuml;
[globalVar = GP:L = 1]
page.10.variables.mobilemenubutton.value = Menu
[global]

config.noPageTitle = 2
page.headerData.5 = TEXT
page.headerData.5.field = subtitle // title
page.headerData.5.wrap = <title>{$sitename}:&nbsp;|</title>

tt_content.image.20.rendering.singleNoCaption.singleStdWrap.wrap.override = <div class="csc-textpic-image###CLASSES###"><div class="csc-textpic-image-inner">|</div></div>
tt_content.image.20.rendering.noCaption.singleStdWrap.wrap.override = <div class="csc-textpic-image###CLASSES###"><div class="csc-textpic-image-inner">|</div></div>
tt_content.image.20.rendering.singleCaption.singleStdWrap.wrap.override = <div class="csc-textpic-image###CLASSES###"><div class="csc-textpic-image-inner">|</div>###CAPTION###</div>
tt_content.image.20.rendering.splitCaption.singleStdWrap.wrap.override = <div class="csc-textpic-image###CLASSES###"><div class="csc-textpic-image-inner">|</div>###CAPTION###</div>
tt_content.image.20.rendering.globalCaption.allStdWrap.wrap.override = <div class="csc-textpic-imagewrap"><div class="csc-textpic-image-inner">|</div>###CAPTION###</div>

## manipulate img tags for lazy loading
## tt_content.image.20.1.params.cObject = COA
## tt_content.image.20.1.params.cObject.10 = TEXT
## tt_content.image.20.1.params.cObject.10.value = class="lazy" data-original="uploads/pics/
## tt_content.image.20.1.params.cObject.20 = TEXT
## tt_content.image.20.1.params.cObject.20.field = image
## tt_content.image.20.1.params.cObject.20.listNum.stdWrap.data = register : IMAGE_NUM_CURRENT
## tt_content.image.20.1.params.cObject.30 = TEXT
## tt_content.image.20.1.params.cObject.30.value = " src=""