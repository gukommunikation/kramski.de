
/* taken from powermail, thanks */

/* plugin for resize of grid in single container */
Ext.namespace('Ext.ux.plugin');
Ext.ux.plugin.FitToParent = Ext.extend(Object, {
    constructor : function(parent) {
        this.parent = parent;
    },
    init : function(c) {
        c.on('render', function(c) {
            c.fitToElement = Ext.get(this.parent
                    || c.getPositionEl().dom.parentNode);
            if (!c.doLayout) {
                this.fitSizeToParent();
                Ext.EventManager.onWindowResize(this.fitSizeToParent, this);
            }
        }, this, {
            single : true
        });
        if (c.doLayout) {
            c.monitorResize = true;
            c.doLayout = c.doLayout.createInterceptor(this.fitSizeToParent);
        }
    },
    fitSizeToParent : function() {
        // Uses the dimension of the current viewport, but removes the document header
        // and an additional margin of 40 pixels (e.g. Safari needs this addition)

        this.fitToElement.setHeight(Ext.getBody().getHeight() - this.fitToElement.getTop() - 40);
        var pos = this.getPosition(true), size = this.fitToElement.getViewSize();
        this.setSize(size.width - pos[0], size.height - pos[1]);

    }
});
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Markus Blaschke (TEQneers GmbH & Co. KG) <blaschke@teqneers.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
Ext.ns('TQSeo');

TQSeo = {

    /**
     * Highlight text in grid
     *
     * @copyright	Stefan Gehrig (TEQneers GmbH & Co. KG) <gehrig@teqneers.de>
     */
    highlightText: function(node, search, cls) {
        search		= search.toUpperCase();
        var skip	= 0;
        if (node.nodeType == 3) {
            var pos = node.data.toUpperCase().indexOf(search);
            if (pos >= 0) {
                var spannode		= document.createElement('span');
                spannode.className	= cls || 'tqseo-search-highlight';
                var middlebit		= node.splitText(pos);
                var endbit			= middlebit.splitText(search.length);
                var middleclone		= middlebit.cloneNode(true);
                spannode.appendChild(middleclone);
                middlebit.parentNode.replaceChild(spannode, middlebit);
                skip = 1;
            }
        } else if (node.nodeType == 1 && node.childNodes && !/(script|style)/i.test(node.tagName)) {
            for (var i = 0; i < node.childNodes.length; ++i) {
                i += TQSeo.highlightText(node.childNodes[i], search);
            }
        }
        return skip;
    },

    /**
     * Check if highlight text is available
     *
     * @copyright	Markus Blaschke (TEQneers GmbH & Co. KG) <blaschke@teqneers.de>
     */
    highlightTextExists: function(value, search) {
        search		= search.toUpperCase();
        var skip	= 0;

        var pos = value.toUpperCase().indexOf(search);
        if (pos >= 0) {
            return true;
        }

        return false;
    }
}
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Markus Blaschke (TEQneers GmbH & Co. KG) <blaschke@teqneers.de>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

Ext.ns('TQSeo.overview');

Ext.onReady(function(){
    Ext.QuickTips.init();
    Ext.state.Manager.setProvider(new Ext.state.CookieProvider());

    TQSeo.overview.grid.init();
});

TQSeo.overview.grid = {

    _cellEditMode: false,
    _fullCellHighlight: true,

    gridDs: false,
    grid: false,

    filterReload: function() {

        TQSeo.overview.conf.criteriaFulltext = Ext.getCmp('searchFulltext').getValue();
        TQSeo.overview.conf.sysLanguage = Ext.getCmp('sysLanguage').getValue();
        TQSeo.overview.conf.depth = Ext.getCmp('listDepth').getValue();

        this.gridDs.reload();
    },

    storeReload: function() {
        TQSeo.overview.conf.criteriaFulltext = Ext.getCmp('searchFulltext').getValue();
        TQSeo.overview.conf.sysLanguage = Ext.getCmp('sysLanguage').getValue();
        TQSeo.overview.conf.depth = Ext.getCmp('listDepth').getValue();

        this.gridDs.reload();
    },

    init: function() {
        // Init
        var me = this;

        /****************************************************
         * settings
         ****************************************************/
        switch( TQSeo.overview.conf.listType ) {
            case 'metadata':
                this._cellEditMode = true;
                break;

            case 'geo':
                this._cellEditMode = true;
                break;

            case 'searchengines':
                this._fullCellHighlight = false;
                this._cellEditMode = true;
                break;

            case 'url':
                this._fullCellHighlight = false;
                this._cellEditMode = true;
                break;

            case 'pagetitle':
                this._cellEditMode = true;
                this._fullCellHighlight = false;
                break;

            case 'pagetitlesim':
                this._fullCellHighlight = false;
                break;

            default:
                // Not defined
                return;
                break;
        }

        /****************************************************
         * grid storage
         ****************************************************/
        this.gridDs = this._createGridDs();

        /****************************************************
         * column model
         ****************************************************/
        var columnModel = this._createGridColumnModel();


        /****************************************************
         * grid panel
         ****************************************************/
        var grid = new Ext.grid.GridPanel({
            layout: 'fit',
            renderTo: TQSeo.overview.conf.renderTo,
            store: this.gridDs,
            loadMask: true,
            plugins: [new Ext.ux.plugin.FitToParent()],
            columns: columnModel,
            stripeRows: true,
            height: 350,
            width: '98%',
            frame: true,
            border: true,
            disableSelection: false,
            title: TQSeo.overview.conf.lang.title,
            viewConfig: {
                forceFit: true,
                listeners: {
                    refresh: function(view) {
                        if (!this._fullCellHighlight && !Ext.isEmpty(TQSeo.overview.conf.criteriaFulltext)) {
                            view.el.select('.x-grid3-body .x-grid3-cell').each(function(el) {
                                TQSeo.highlightText(el.dom, TQSeo.overview.conf.criteriaFulltext);
                            });
                        }
                    }
                }
            },
            tbar: [
                TQSeo.overview.conf.lang.labelSearchFulltext,
                {
                    xtype: 'textfield',
                    id: 'searchFulltext',
                    fieldLabel: TQSeo.overview.conf.lang.labelSearchFulltext,
                    emptyText : TQSeo.overview.conf.lang.emptySearchFulltext,
                    listeners: {
                        specialkey: function(f,e){
                            if (e.getKey() == e.ENTER) {
                                me.filterReload(this);
                            }
                        }
                    }
                },
                '->',
                TQSeo.overview.conf.lang.labelSearchPageLanguage,
                {
                    xtype: 'combo',
                    id: 'sysLanguage',
                    fieldLabel: TQSeo.overview.conf.lang.labelSearchPageLanguage,
                    emptyText : TQSeo.overview.conf.lang.emptySearchPageLanguage,
                    listeners: {
                        select: function(f,e){
                            me.filterReload(this);
                        }
                    },
                    forceSelection: true,
                    editable: false,
                    mode: 'local',
                    triggerAction: 'all',
                    store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: [
                            'id',
                            'label',
                            'flag'
                        ],
                        data: TQSeo.overview.conf.dataLanguage
                    }),
                    valueField: 'id',
                    displayField: 'label',
                    tpl: '<tpl for="."><div class="x-combo-list-item">{flag}{label}</div></tpl>',
                    value: TQSeo.overview.conf.sysLanguage
                }
            ],
            bbar: [
                TQSeo.overview.conf.lang.labelDepth,
                {
                    xtype: 'combo',
                    id: 'listDepth',
                    listeners: {
                        select: function(f,e){
                            me.storeReload(this);
                        }
                    },
                    forceSelection: true,
                    editable: false,
                    mode: 'local',
                    triggerAction: 'all',
                    value : TQSeo.overview.conf.depth,
                    store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: [
                            'id',
                            'label'
                        ],
                        data: [
                            [1, 1],
                            [2, 2],
                            [3, 3],
                            [4, 4],
                            [5, 5]
                        ]
                    }),
                    valueField: 'id',
                    displayField: 'label'
                }
            ]
        });
        this.grid = grid;

        var editWindow = false;

        if( this._cellEditMode ) {
            grid.addClass('tqseo-grid-editable');

            grid.on('cellclick', function(grid, rowIndex, colIndex, e) {
                var record = grid.getStore().getAt(rowIndex);
                var fieldName = grid.getColumnModel().getDataIndex(colIndex);
                var fieldId = grid.getColumnModel().getColumnId(colIndex);
                var col = grid.getColumnModel().getColumnById(fieldId);
                var data = record.get(fieldName);

                var title = record.get('title');

                if( col.tqSeoOnClick ) {
                    col.tqSeoOnClick(record, fieldName, fieldId, col, data);
                }

                if( col.tqSeoEditor ) {
                    // Init editor field
                    var field = col.tqSeoEditor;
                    field.itemId = 'form-field';

                    if( !field.width)	field.width = 375;

                    switch( field.xtype ) {
                        case 'textarea':
                            if( !field.height)	field.height = 150;
                            field.value = data;
                            break;

                        case 'checkbox':
                            if( data == '0' || data == '' ) {
                                field.checked = false;
                            } else {
                                field.checked = true;
                            }
                            break;

                        default:
                            field.value = data;
                            break;
                    }

                    // Init editor window
                    var editWindow = new Ext.Window({
                        xtype: 'form',
                        width: 400,
                        height: 'auto',
                        modal: true,
                        title: title+': '+col.header,
                        items: [ field ],
                        buttons: [
                            {
                                text: 'Save',
                                itemId: 'form-button-save',
                                disabled: true,
                                handler: function(cmp, e) {
                                    grid.loadMask.show();

                                    var pid = record.get('uid');
                                    var fieldValue = editWindow.getComponent('form-field').getValue();

                                    var callbackFinish = function(response) {
                                        var response = Ext.decode(response.responseText);

                                        if( response && response.error ) {
                                            TYPO3.Flashmessage.display(TYPO3.Severity.error, '', Ext.util.Format.htmlEncode(response.error) );
                                        }

                                        grid.getStore().load();
                                    };

                                    Ext.Ajax.request({
                                        url: TQSeo.overview.conf.ajaxController + '&cmd=updatePageField',
                                        params: {
                                            pid             : Ext.encode(pid),
                                            field           : Ext.encode(fieldName),
                                            value           : Ext.encode(fieldValue),
                                            sysLanguage     : Ext.encode( TQSeo.overview.conf.sysLanguage ),
                                            sessionToken    : Ext.encode( TQSeo.overview.conf.sessionToken )
                                        },
                                        success: callbackFinish,
                                        failure: callbackFinish
                                    });

                                    editWindow.destroy();
                                }
                            },{
                                text: 'Cancel',
                                handler: function(cmp, e) {
                                    editWindow.destroy();
                                }
                            }
                        ]
                    });
                    editWindow.show();

                    var formField		= editWindow.getComponent('form-field');
                    var formButtonSave	= editWindow.getFooterToolbar().getComponent('form-button-save');

                    // add listeners
                    formField.on('valid', function() {
                        formButtonSave.setDisabled(false);
                    });

                    formField.on('invalid', function() {
                        formButtonSave.setDisabled(true);
                    });


                }
            });
        }

    },


    _createGridDs: function() {
        var me = this;

        var gridDsColumns = [
            {name: 'uid', type: 'int'},
            {name: 'title', type: 'string'},
            {name: '_depth', type: 'int'},
            {name: '_overlay', type: 'array'}
        ];

        switch( TQSeo.overview.conf.listType ) {
            case 'metadata':
                gridDsColumns.push(
                    {name: 'keywords', type: 'string'},
                    {name: 'description', type: 'string'},
                    {name: 'abstract', type: 'string'},
                    {name: 'author', type: 'string'},
                    {name: 'author_email', type: 'string'},
                    {name: 'lastupdated', type: 'string'}
                );
                break;

            case 'geo':
                gridDsColumns.push(
                    {name: 'tx_tqseo_geo_lat', type: 'string'},
                    {name: 'tx_tqseo_geo_long', type: 'string'},
                    {name: 'tx_tqseo_geo_place', type: 'string'},
                    {name: 'tx_tqseo_geo_region', type: 'string'}
                );
                break;

            case 'searchengines':
                gridDsColumns.push(
                    {name: 'tx_tqseo_canonicalurl', type: 'string'},
                    {name: 'tx_tqseo_is_exclude', type: 'string'},
                    {name: 'tx_tqseo_priority', type: 'string'}
                );
                break;

            case 'url':
                gridDsColumns.push(
                    {name: 'alias', type: 'string'},
                    {name: 'url_scheme', type: 'string'}
                );

                if( TQSeo.overview.conf.realurlAvailable ) {
                    gridDsColumns.push(
                        {name: 'tx_realurl_pathsegment', type: 'string'},
                        {name: 'tx_realurl_pathoverride', type: 'string'},
                        {name: 'tx_realurl_exclude', type: 'string'}
                    );
                }
                break;

            case 'pagetitle':
                gridDsColumns.push(
                    {name: 'tx_tqseo_pagetitle', type: 'string'},
                    {name: 'tx_tqseo_pagetitle_rel', type: 'string'},
                    {name: 'tx_tqseo_pagetitle_prefix', type: 'string'},
                    {name: 'tx_tqseo_pagetitle_suffix', type: 'string'}
                );
                break;

            case 'pagetitlesim':
                gridDsColumns.push(
                    {name: 'title_simulated', type: 'string'}
                );
                break;
        }

        var gridDs = new Ext.data.Store({
            storeId: 'TQSeoOverviewRecordsStore',
            autoLoad: true,
            remoteSort: true,
            url: TQSeo.overview.conf.ajaxController + '&cmd=getList',
            reader: new Ext.data.JsonReader({
                    totalProperty: 'results',
                    root: 'rows'
                },
                gridDsColumns
            ),
            sortInfo: {
                field	 : 'uid',
                direction: 'DESC'
            },
            baseParams: {
                pid						: Ext.encode( TQSeo.overview.conf.pid ),
                pagerStart				: Ext.encode( 0 ),
                pagingSize				: Ext.encode( TQSeo.overview.conf.pagingSize ),
                sortField				: Ext.encode( TQSeo.overview.conf.sortField ),
                depth					: Ext.encode( TQSeo.overview.conf.depth ),
                listType				: Ext.encode( TQSeo.overview.conf.listType ),
                sessionToken			: Ext.encode( TQSeo.overview.conf.sessionToken ),
                sysLanguage             : Ext.encode( TQSeo.overview.conf.sysLanguage )
            },
            listeners: {
                beforeload: function() {
                    this.baseParams.pagingSize          = Ext.encode( TQSeo.overview.conf.pagingSize );
                    this.baseParams.depth               = Ext.encode( TQSeo.overview.conf.depth );
                    this.baseParams.criteriaFulltext    = Ext.encode( TQSeo.overview.conf.criteriaFulltext );
                    this.baseParams.sysLanguage         = Ext.encode( TQSeo.overview.conf.sysLanguage );
                    this.removeAll();
                }
            }
        });

        return gridDs;
    },


    _createGridColumnModel: function() {
        var me = this;

        var fieldRenderer = function(value, metaData, record, rowIndex, colIndex, store) {
            var fieldName     = me.grid.getColumnModel().getDataIndex(colIndex);
            var overlayStatus = record.get('_overlay')[fieldName];

            // check for overlay
            var html = me._fieldRenderer(value);

            if( overlayStatus == 2 ) {
                html = '<div class="tqseo-info-only-in-base">'+html+'</div>';
            } else if( overlayStatus == 1 ) {
                html = '<div class="tqseo-info-from-overlay">'+html+'</div>';
            } else {
                html = '<div class="tqseo-info-from-base">'+html+'</div>';
            }

            return html;
        };

        var fieldRendererRaw = function(value, metaData, record, rowIndex, colIndex, store) {
            return me._fieldRendererRaw(value);
        };

        var fieldRendererBoolean = function(value, metaData, record, rowIndex, colIndex, store) {
            if( value == 0 || value == '' ) {
                value = '<div class="tqseo-boolean">'+Ext.util.Format.htmlEncode(TQSeo.overview.conf.lang.boolean_no)+'</div>';
            } else {
                value = '<div class="tqseo-boolean"><strong>'+Ext.util.Format.htmlEncode(TQSeo.overview.conf.lang.boolean_yes)+'</strong></div>';
            }

            return me._fieldRendererCallback(value, '', false, false);
        }

        var columnModel = [{
            id       : 'uid',
            header   : TQSeo.overview.conf.lang.page_uid,
            width    : 'auto',
            sortable : false,
            dataIndex: 'uid',
            hidden	 : true
        }, {
            id       : 'title',
            header   : TQSeo.overview.conf.lang.page_title,
            width    : 200,
            sortable : false,
            dataIndex: 'title',
            renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                var qtip = value;

                if( record.data._depth ) {
                    value = new Array(record.data._depth).join('    ') + value;
                }

                return me._fieldRendererCallback(value, qtip, false, true);
            },
            tqSeoEditor	: {
                xtype: 'textfield',
                minLength: 3
            }
        }];

        switch( TQSeo.overview.conf.listType ) {
            case 'metadata':
                columnModel.push({
                    id			: 'keywords',
                    header		: TQSeo.overview.conf.lang.page_keywords,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'keywords',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textarea'
                    }
                },{
                    id			: 'description',
                    header		: TQSeo.overview.conf.lang.page_description,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'description',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textarea'
                    }
                },{
                    id			: 'abstract',
                    header		: TQSeo.overview.conf.lang.page_abstract,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'abstract',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textarea'
                    }
                },{
                    id			: 'author',
                    header		: TQSeo.overview.conf.lang.page_author,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'author',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id			: 'author_email',
                    header		: TQSeo.overview.conf.lang.page_author_email,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'author_email',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield',
                        vtype: 'email'
                    }
                },{
                    id			: 'lastupdated',
                    header		: TQSeo.overview.conf.lang.page_lastupdated,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'lastupdated',
                    renderer	: fieldRendererRaw,
                    tqSeoEditor	: {
                        xtype: 'datefield',
                        format: 'Y-m-d'
                    }
                });
                break;

            case 'geo':
                columnModel.push({
                    id			: 'tx_tqseo_geo_lat',
                    header		: TQSeo.overview.conf.lang.page_geo_lat,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'tx_tqseo_geo_lat',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id			: 'tx_tqseo_geo_long',
                    header		: TQSeo.overview.conf.lang.page_geo_long,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'tx_tqseo_geo_long',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id			: 'tx_tqseo_geo_place',
                    header		: TQSeo.overview.conf.lang.page_geo_place,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'tx_tqseo_geo_place',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id			: 'tx_tqseo_geo_region',
                    header		: TQSeo.overview.conf.lang.page_geo_region,
                    width		: 'auto',
                    sortable	: false,
                    dataIndex	: 'tx_tqseo_geo_region',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                });
                break;


            case 'searchengines':
                var fieldRendererSitemapPriority = function(value, metaData, record, rowIndex, colIndex, store) {
                    var qtip = value;

                    if( value == '0' ) {
                        value = '<span class="tqseo-default">'+Ext.util.Format.htmlEncode(TQSeo.overview.conf.lang.value_default)+'</span>';
                    } else {
                        value = Ext.util.Format.htmlEncode(value);
                    }

                    return me._fieldRendererCallback(value, qtip, false, false);
                }

                columnModel.push({
                    id       : 'tx_tqseo_canonicalurl',
                    header   : TQSeo.overview.conf.lang.page_searchengine_canonicalurl,
                    width    : 400,
                    sortable : false,
                    dataIndex: 'tx_tqseo_canonicalurl',
                    renderer : fieldRendererRaw,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id       : 'tx_tqseo_priority',
                    header   : TQSeo.overview.conf.lang.page_sitemap_priority,
                    width    : 150,
                    sortable : false,
                    dataIndex: 'tx_tqseo_priority',
                    renderer : fieldRendererSitemapPriority,
                    tqSeoEditor	: {
                        xtype: 'numberfield'
                    }
                },{
                    id       : 'tx_tqseo_is_exclude',
                    header   : TQSeo.overview.conf.lang.page_searchengine_is_exclude,
                    width    : 100,
                    sortable : false,
                    dataIndex: 'tx_tqseo_is_exclude',
                    renderer : fieldRendererBoolean,
                    tqSeoEditor	: {
                        xtype: 'combo',
                        forceSelection: true,
                        editable: false,
                        mode: 'local',
                        triggerAction: 'all',
                        store: new Ext.data.ArrayStore({
                            id: 0,
                            fields: [
                                'id',
                                'label'
                            ],
                            data: [
                                [0, TQSeo.overview.conf.lang.searchengine_is_exclude_disabled],
                                [1, TQSeo.overview.conf.lang.searchengine_is_exclude_enabled]
                            ]
                        }),
                        valueField: 'id',
                        displayField: 'label'
                    }
                });
                break;


            case 'url':
                var fieldRendererUrlScheme = function(value, metaData, record, rowIndex, colIndex, store) {
                    var ret = '';

                    value = parseInt(value);
                    switch(value) {
                        case 0:
                            ret = '<span class="tqseo-default">'+Ext.util.Format.htmlEncode( TQSeo.overview.conf.lang.page_url_scheme_default )+'</span>';
                            break;

                        case 1:
                            ret = '<strong class="tqseo-url-http">'+Ext.util.Format.htmlEncode( TQSeo.overview.conf.lang.page_url_scheme_http)+'</strong>';
                            break;

                        case 2:
                            ret = '<strong class="tqseo-url-https">'+Ext.util.Format.htmlEncode( TQSeo.overview.conf.lang.page_url_scheme_https)+'</strong>';
                            break;
                    }

                    return ret;
                }

                var fieldRendererUrlSimulate = function(value, metaData, record, rowIndex, colIndex, store) {
                    var qtip = Ext.util.Format.htmlEncode(TQSeo.overview.conf.lang.qtip_url_simulate);

                    return '<div class="tqseo-toolbar" ext:qtip="' + qtip +'">'+TQSeo.overview.conf.sprite.info+'</div>';
                }


                columnModel.push({
                    id       : 'url_scheme',
                    header   : TQSeo.overview.conf.lang.page_url_scheme,
                    width    : 100,
                    sortable : false,
                    dataIndex: 'url_scheme',
                    renderer : fieldRendererUrlScheme,
                    tqSeoEditor	: {
                        xtype: 'combo',
                        forceSelection: true,
                        editable: false,
                        mode: 'local',
                        triggerAction: 'all',
                        store: new Ext.data.ArrayStore({
                            id: 0,
                            fields: [
                                'id',
                                'label'
                            ],
                            data: [
                                [0, TQSeo.overview.conf.lang.page_url_scheme_default],
                                [1, TQSeo.overview.conf.lang.page_url_scheme_http],
                                [2, TQSeo.overview.conf.lang.page_url_scheme_https]
                            ]
                        }),
                        valueField: 'id',
                        displayField: 'label'
                    }
                },{
                    id       : 'alias',
                    header   : TQSeo.overview.conf.lang.page_url_alias,
                    width    : 200,
                    sortable : false,
                    dataIndex: 'alias',
                    renderer : fieldRendererRaw,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                });

                if( TQSeo.overview.conf.realurlAvailable ) {
                    columnModel.push({
                        id       : 'tx_realurl_pathsegment',
                        header   : TQSeo.overview.conf.lang.page_url_realurl_pathsegment,
                        width    : 200,
                        sortable : false,
                        dataIndex: 'tx_realurl_pathsegment',
                        renderer : fieldRendererRaw,
                        tqSeoEditor	: {
                            xtype: 'textfield'
                        }
                    },{
                        id       : 'tx_realurl_pathoverride',
                        header   : TQSeo.overview.conf.lang.page_url_realurl_pathoverride,
                        width    : 150,
                        sortable : false,
                        dataIndex: 'tx_realurl_pathoverride',
                        renderer : fieldRendererBoolean,
                        tqSeoEditor	: {
                            xtype: 'combo',
                            forceSelection: true,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            store: new Ext.data.ArrayStore({
                                id: 0,
                                fields: [
                                    'id',
                                    'label'
                                ],
                                data: [
                                    [0, TQSeo.overview.conf.lang.boolean_no],
                                    [1, TQSeo.overview.conf.lang.boolean_yes]
                                ]
                            }),
                            valueField: 'id',
                            displayField: 'label'
                        }
                    },{
                        id       : 'tx_realurl_exclude',
                        header   : TQSeo.overview.conf.lang.page_url_realurl_exclude,
                        width    : 150,
                        sortable : false,
                        dataIndex: 'tx_realurl_exclude',
                        renderer : fieldRendererBoolean,
                        tqSeoEditor	: {
                            xtype: 'combo',
                            forceSelection: true,
                            editable: false,
                            mode: 'local',
                            triggerAction: 'all',
                            store: new Ext.data.ArrayStore({
                                id: 0,
                                fields: [
                                    'id',
                                    'label'
                                ],
                                data: [
                                    [0, TQSeo.overview.conf.lang.boolean_no],
                                    [1, TQSeo.overview.conf.lang.boolean_yes]
                                ]
                            }),
                            valueField: 'id',
                            displayField: 'label'
                        }
                    },{
                        id       : 'url_simulated',
                        header   : '',
                        width    : 50,
                        sortable : false,
                        renderer : fieldRendererUrlSimulate,
                        tqSeoOnClick: function(record, fieldName, fieldId, col, data) {
                            me.grid.loadMask.show();

                            var callbackFinish = function(response) {
                                var response = Ext.decode(response.responseText);

                                me.grid.loadMask.hide();

                                if( response && response.error ) {
                                    TYPO3.Flashmessage.display(TYPO3.Severity.error, '', Ext.util.Format.htmlEncode(response.error) );
                                }

                                if( response && response.url ) {
                                    TYPO3.Flashmessage.display(TYPO3.Severity.information, '', Ext.util.Format.htmlEncode(response.url) );
                                }
                            };

                            Ext.Ajax.request({
                                url: TQSeo.overview.conf.ajaxController + '&cmd=generateSimulatedUrl',
                                params: {
                                    pid				: Ext.encode(record.get('uid')),
                                    sessionToken	: Ext.encode( TQSeo.overview.conf.sessionToken )
                                },
                                success: callbackFinish,
                                failure: callbackFinish
                            });

                        }
                    });
                }

                break;


            case 'pagetitle':
                var fieldRendererTitleSimulate = function(value, metaData, record, rowIndex, colIndex, store) {
                    var qtip = Ext.util.Format.htmlEncode(TQSeo.overview.conf.lang.qtip_pagetitle_simulate);

                    return '<div class="tqseo-toolbar" ext:qtip="' + qtip +'">'+TQSeo.overview.conf.sprite.info+'</div>';
                }

                columnModel.push({
                    id       : 'tx_tqseo_pagetitle_rel',
                    header   : TQSeo.overview.conf.lang.page_tx_tqseo_pagetitle_rel,
                    width    : 'auto',
                    sortable : false,
                    dataIndex: 'tx_tqseo_pagetitle_rel',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id       : 'tx_tqseo_pagetitle_prefix',
                    header   : TQSeo.overview.conf.lang.page_tx_tqseo_pagetitle_prefix,
                    width    : 'auto',
                    sortable : false,
                    dataIndex: 'tx_tqseo_pagetitle_prefix',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id       : 'tx_tqseo_pagetitle_suffix',
                    header   : TQSeo.overview.conf.lang.page_tx_tqseo_pagetitle_suffix,
                    width    : 'auto',
                    sortable : false,
                    dataIndex: 'tx_tqseo_pagetitle_suffix',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id       : 'tx_tqseo_pagetitle',
                    header   : TQSeo.overview.conf.lang.page_tx_tqseo_pagetitle,
                    width    : 'auto',
                    sortable : false,
                    dataIndex: 'tx_tqseo_pagetitle',
                    renderer	: fieldRenderer,
                    tqSeoEditor	: {
                        xtype: 'textfield'
                    }
                },{
                    id       : 'title_simulated',
                    header   : '',
                    width    : 50,
                    sortable : false,
                    renderer : fieldRendererTitleSimulate,
                    tqSeoOnClick: function(record, fieldName, fieldId, col, data) {
                        me.grid.loadMask.show();

                        var callbackFinish = function(response) {
                            var response = Ext.decode(response.responseText);

                            me.grid.loadMask.hide();

                            if( response && response.error ) {
                                TYPO3.Flashmessage.display(TYPO3.Severity.error, '', Ext.util.Format.htmlEncode(response.error) );
                            }

                            if( response && response.title ) {
                                TYPO3.Flashmessage.display(TYPO3.Severity.information, '', Ext.util.Format.htmlEncode(response.title) );
                            }
                        };

                        Ext.Ajax.request({
                            url: TQSeo.overview.conf.ajaxController + '&cmd=generateSimulatedTitle',
                            params: {
                                pid				: Ext.encode(record.get('uid')),
                                sessionToken	: Ext.encode( TQSeo.overview.conf.sessionToken )
                            },
                            success: callbackFinish,
                            failure: callbackFinish
                        });

                    }
                });
                break;

            case 'pagetitlesim':
                columnModel.push({
                    id       : 'title_simulated',
                    header   : TQSeo.overview.conf.lang.page_title_simulated,
                    width    : 400,
                    sortable : false,
                    dataIndex: 'title_simulated',
                    renderer : fieldRendererRaw
                });
                break;

        }


        // Add tooltip
        Ext.each(columnModel, function(item, index) {
            if( !item.tooltip ) {
                item.tooltip = item.header;
            }
        });

        return columnModel;
    },


    _fieldRenderer: function(value) {
        return this._fieldRendererCallback(value, value, 23, true);
    },

    _fieldRendererRaw: function(value) {
        return this._fieldRendererCallback(value, value, false, true);
    },

    _fieldRendererCallback: function(value, qtip, maxLength, escape) {
        var classes = '';
        var icon = '';

        if( this._cellEditMode ) {
            classes += 'tqseo-cell-editable ';
            icon = TQSeo.overview.conf.sprite.edit;
        }

        if(this._fullCellHighlight && !Ext.isEmpty(TQSeo.overview.conf.criteriaFulltext)) {
            if( TQSeo.highlightTextExists(value, TQSeo.overview.conf.criteriaFulltext) ) {
                classes += 'tqseo-cell-highlight ';
            }
        }

        if( maxLength && value != '' && value.length >= maxLength ) {
            value = value.substring(0, (maxLength-3) )+'...';
        }

        if(escape) {
            value = Ext.util.Format.htmlEncode(value);
            value = value.replace(/ /g, "&nbsp;");
            value += '&nbsp;';
        }



        if(escape) {
            qtip = Ext.util.Format.htmlEncode(qtip);
        }
        qtip = qtip.replace(/\n/g, "<br />");

        return '<div class="'+classes+'" ext:qtip="' + qtip +'">' + value +icon+'</div>';
    }


};
/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Javascript functions regarding the clickmenu
 * relies on the javascript library "prototype"
 */

/**
 * new clickmenu code to make an AJAX call and render the
 * AJAX result in a layer next to the mouse cursor
 */
var Clickmenu = {
	clickURL: 'alt_clickmenu.php',
	ajax: true, // \TYPO3\CMS\Backend\Template\DocumentTemplate::isCMLayers
	mousePos: { X: null, Y: null },
	delayClickMenuHide: false,

	/**
	 * main function, called from most clickmenu links
	 * @param	table		table from where info should be fetched
	 * @param	uid		the UID of the item
	 * @param	listFr		list Frame?
	 * @param	enDisItems	Items to disable / enable
	 * @param	backPath	TYPO3 backPath
	 * @param	addParams	additional params
	 * @return	nothing
	 */
	show: function(table, uid, listFr, enDisItems, backPath, addParams) {
		var params = 'table=' + encodeURIComponent(table) +
			'&uid=' + uid +
			'&listFr=' + listFr +
			'&enDisItems=' + enDisItems +
			'&backPath=' + backPath +
			'&addParams=' + addParams;
		this.callURL(params);
	},


	/**
	 * switch function that either makes an AJAX call
	 * or loads the request in the top frame
	 *
	 * @param	params	parameters added to the URL
	 * @return	nothing
	 */
	callURL: function(params) {
		if (this.ajax && Ajax.getTransport()) { // run with AJAX
			params += '&ajax=1';
			var call = new Ajax.Request(this.clickURL, {
				method: 'get',
				parameters: params,
				onComplete: function(xhr) {
					var response = xhr.responseXML;

					if (!response.getElementsByTagName('data')[0]) {
						var res = params.match(/&reloadListFrame=(0|1|2)(&|$)/);
						var reloadListFrame = parseInt(res[1], 0);
						if (reloadListFrame) {
							var doc = reloadListFrame != 2 ? top.content.list_frame : top.content;
							doc.location.reload(true);
						}
						return;
					}
					var menu  = response.getElementsByTagName('data')[0].getElementsByTagName('clickmenu')[0];
					var data  = menu.getElementsByTagName('htmltable')[0].firstChild.data;
					var level = menu.getElementsByTagName('cmlevel')[0].firstChild.data;
					this.populateData(data, level);

				}.bind(this)
			});
		}
	},


	/**
	 * fills the clickmenu with content and displays it correctly
	 * depending on the mouse position
	 * @param	data	the data that will be put in the menu
	 * @param	level	the depth of the clickmenu
	 */
	populateData: function(data, level) {
		if (!$('contentMenu0')) {
			this.addClickmenuItem();
		}
		level = parseInt(level, 10) || 0;
		var obj = $('contentMenu' + level);

		if (obj && (level === 0 || Element.visible('contentMenu' + (level-1)))) {
			obj.innerHTML = data;
			var x = this.mousePos.X;
			var y = this.mousePos.Y;
			var dimsWindow = document.viewport.getDimensions();
			dimsWindow.width = dimsWindow.width-20; // saving margin for scrollbars

			var dims = Element.getDimensions(obj); // dimensions for the clickmenu
			var offset = document.viewport.getScrollOffsets();
			var relative = { X: this.mousePos.X - offset.left, Y: this.mousePos.Y - offset.top };

			// adjusting the Y position of the layer to fit it into the window frame
			// if there is enough space above then put it upwards,
			// otherwise adjust it to the bottom of the window
			if (dimsWindow.height - dims.height < relative.Y) {
				if (relative.Y > dims.height) {
					y -= (dims.height - 10);
				} else {
					y += (dimsWindow.height - dims.height - relative.Y);
				}
			}
			// adjusting the X position like Y above, but align it to the left side of the viewport if it does not fit completely
			if (dimsWindow.width - dims.width < relative.X) {
				if (relative.X > dims.width) {
					x -= (dims.width - 10);
				} else if ((dimsWindow.width - dims.width - relative.X) < offset.left) {
					x = offset.left;
				} else {
					x += (dimsWindow.width - dims.width - relative.X);
				}
			}

			obj.style.left = x + 'px';
			obj.style.top  = y + 'px';
			Element.show(obj);
		}
		if (/MSIE5/.test(navigator.userAgent)) {
			this._toggleSelectorBoxes('hidden');
		}
	},


	/**
	 * event handler function that saves the actual position of the mouse
	 * in the Clickmenu object
	 * @param	event	the event object
	 */
	calcMousePosEvent: function(event) {
		if (!event) {
			event = window.event;
		}
		this.mousePos.X = Event.pointerX(event);
		this.mousePos.Y = Event.pointerY(event);
		this.mouseOutFromMenu('contentMenu0');
		this.mouseOutFromMenu('contentMenu1');
	},


	/**
	 * hides a visible menu if the mouse has moved outside
	 * of the object
	 * @param	obj	the object to hide
	 * @result	nothing
	 */
	mouseOutFromMenu: function(obj) {
		obj = $(obj);
		if (obj && Element.visible(obj) && !Position.within(obj, this.mousePos.X, this.mousePos.Y)) {
			this.hide(obj);
			if (/MSIE5/.test(navigator.userAgent) && obj.id === 'contentMenu0') {
				this._toggleSelectorBoxes('visible');
			}
		} else if (obj && Element.visible(obj)) {
			this.delayClickMenuHide = true;
		}
	},

	/**
	 * hides a clickmenu
	 *
	 * @param	obj	the clickmenu object to hide
	 * @result	nothing
	 */
	hide: function(obj) {
		this.delayClickMenuHide = false;
		window.setTimeout(function() {
			if (!Clickmenu.delayClickMenuHide) {
				Element.hide(obj);
			}
		}, 500);
	},

	/**
	 * hides all clickmenus
	 */
	hideAll: function() {
		this.hide('contentMenu0');
		this.hide('contentMenu1');
	},


	/**
	 * hides / displays all selector boxes in a page, fixes an IE 5 selector problem
	 * originally by Michiel van Leening
	 *
	 * @param	action 	hide (= "hidden") or show (= "visible")
	 * @result	nothing
	 */
	_toggleSelectorBoxes: function(action) {
		for (var i = 0; i < document.forms.length; i++) {
			for (var j = 0; j < document.forms[i].elements.length; j++) {
				if (document.forms[i].elements[j].type == 'select-one') {
					document.forms[i].elements[j].style.visibility = action;
				}
			}
		}
	},


	/**
	 * manipulates the DOM to add the divs needed for clickmenu at the bottom of the <body>-tag
	 *
	 * @return	nothing
	 */
	addClickmenuItem: function() {
		var code = '<div id="contentMenu0" style="display: block;"></div><div id="contentMenu1" style="display: block;"></div>';
		var insert = new Insertion.Bottom(document.getElementsByTagName('body')[0], code);
	}
}

// register mouse movement inside the document
Event.observe(document, 'mousemove', Clickmenu.calcMousePosEvent.bindAsEventListener(Clickmenu), true);


// @deprecated: Deprecated functions since 4.2, here for compatibility, remove in 4.4+
// ## BEGIN ##

// Still used in Core: typo3/alt_clickmenu.php::linkItem()
function showClickmenu_raw(url) {
	var parts = url.split('?');
	if (parts.length === 2) {
		Clickmenu.clickURL = parts[0];
		Clickmenu.callURL(parts[1]);
	} else {
		Clickmenu.callURL(url);
	}
}
function showClickmenu_noajax(url) {
	Clickmenu.ajax = false; showClickmenu_raw(url);
}
function setLayerObj(tableData, cmLevel) {
	Clickmenu.populateData(tableData, cmLevel);
}
function hideEmpty() {
	Clickmenu.hideAll();
	return false;
}
function hideSpecific(level) {
	if (level === 0 || level === 1) {
		Clickmenu.hide('contentMenu'+level);
	}
}
function showHideSelectorBoxes(action) {
	toggleSelectorBoxes(action);
}
// ## END ##

/*
 * This code has been copied from Project_CMS
 * Copyright (c) 2005 by Phillip Berndt (www.pberndt.com)
 *
 * Extended Textarea for IE and Firefox browsers
 * Features:
 *  - Possibility to place tabs in <textarea> elements using a simply <TAB> key
 *  - Auto-indenting of new lines
 *
 * License: GNU General Public License
 */

function checkBrowser() {
	browserName = navigator.appName;
	browserVer = parseInt(navigator.appVersion);

	ok = false;
	if (browserName == "Microsoft Internet Explorer" && browserVer >= 4) {
		ok = true;
	} else if (browserName == "Netscape" && browserVer >= 3) {
		ok = true;
	}

	return ok;
}

	// Automatically change all textarea elements
function changeTextareaElements() {
	if (!checkBrowser()) {
			// Stop unless we're using IE or Netscape (includes Mozilla family)
		return false;
	}

	document.textAreas = document.getElementsByTagName("textarea");

	for (i = 0; i < document.textAreas.length; i++) {
			// Only change if the class parameter contains "enable-tab"
		if (document.textAreas[i].className && document.textAreas[i].className.search(/(^| )enable-tab( |$)/) != -1) {
			document.textAreas[i].textAreaID = i;
			makeAdvancedTextArea(document.textAreas[i]);
		}
	}
}

	// Wait until the document is completely loaded.
	// Set a timeout instead of using the onLoad() event because it could be used by something else already.
window.setTimeout("changeTextareaElements();", 200);

	// Turn textarea elements into "better" ones. Actually this is just adding some lines of JavaScript...
function makeAdvancedTextArea(textArea) {
	if (textArea.tagName.toLowerCase() != "textarea") {
		return false;
	}

		// On attempt to leave the element:
		// Do not leave if this.dontLeave is true
	textArea.onblur = function(e) {
		if (!this.dontLeave) {
			return;
		}
		this.dontLeave = null;
		window.setTimeout("document.textAreas[" + this.textAreaID + "].restoreFocus()", 1);
		return false;
	}

		// Set the focus back to the element and move the cursor to the correct position.
	textArea.restoreFocus = function() {
		this.focus();

		if (this.caretPos) {
			this.caretPos.collapse(false);
			this.caretPos.select();
		}
	}

		// Determine the current cursor position
	textArea.getCursorPos = function() {
		if (this.selectionStart) {
			currPos = this.selectionStart;
		} else if (this.caretPos) {	// This is very tricky in IE :-(
			oldText = this.caretPos.text;
			finder = "--getcurrpos" + Math.round(Math.random() * 10000) + "--";
			this.caretPos.text += finder;
			currPos = this.value.indexOf(finder);

			this.caretPos.moveStart('character', -finder.length);
			this.caretPos.text = "";

			this.caretPos.scrollIntoView(true);
		} else {
			return;
		}

		return currPos;
	}

		// On tab, insert a tabulator. Otherwise, check if a slash (/) was pressed.
	textArea.onkeydown = function(e) {
		if (this.selectionStart == null &! this.createTextRange) {
			return;
		}
		if (!e) {
			e = window.event;
		}

			// Tabulator
		if (e.keyCode == 9) {
			this.dontLeave = true;
			this.textInsert(String.fromCharCode(9));
		}

			// Newline
		if (e.keyCode == 13) {
				// Get the cursor position
			currPos = this.getCursorPos();

				// Search the last line
			lastLine = "";
			for (i = currPos - 1; i >= 0; i--) {
				if(this.value.substring(i, i + 1) == '\n') {
					break;
				}
			}
			lastLine = this.value.substring(i + 1, currPos);

				// Search for whitespaces in the current line
			whiteSpace = "";
			for (i = 0; i < lastLine.length; i++) {
				if (lastLine.substring(i, i + 1) == '\t') {
					whiteSpace += "\t";
				} else if (lastLine.substring(i, i + 1) == ' ') {
					whiteSpace += " ";
				} else {
					break;
				}
			}

				// Another ugly IE hack
			if (navigator.appVersion.match(/MSIE/)) {
				whiteSpace = "\\n" + whiteSpace;
			}

				// Insert whitespaces
			window.setTimeout("document.textAreas["+this.textAreaID+"].textInsert(\""+whiteSpace+"\");", 1);
		}
	}

		// Save the current cursor position in IE
	textArea.onkeyup = textArea.onclick = textArea.onselect = function(e) {
		if (this.createTextRange) {
			this.caretPos = document.selection.createRange().duplicate();
		}
	}

		// Insert text at the current cursor position
	textArea.textInsert = function(insertText) {
		if (this.selectionStart != null) {
			var savedScrollTop = this.scrollTop;
			var begin = this.selectionStart;
			var end = this.selectionEnd;
			if (end > begin + 1) {
				this.value = this.value.substr(0, begin) + insertText + this.value.substr(end);
			} else {
				this.value = this.value.substr(0, begin) + insertText + this.value.substr(begin);
			}

			this.selectionStart = begin + insertText.length;
			this.selectionEnd = begin + insertText.length;
			this.scrollTop = savedScrollTop;
		} else if (this.caretPos) {
			this.caretPos.text = insertText;
			this.caretPos.scrollIntoView(true);
		} else {
			text.value += insertText;
		}

		this.focus();
	}
}
/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

Ext.ns('TYPO3', 'TYPO3.CSH.ExtDirect');

/**
 * Class to show tooltips for links that have the css t3-help-link
 * need the tags data-table and data-field (HTML5)
 */


TYPO3.ContextHelp = function() {

	/**
	 * Cache for CSH
	 * @type {Ext.util.MixedCollection}
	 */
	var cshHelp = new Ext.util.MixedCollection(true),
	tip;

	/**
	 * Shows the tooltip, triggered from mouseover event handler
	 *
	 */
	function showToolTipHelp() {
		var link = tip.triggerElement;
		if (!link) {
			return false;
		}
		var table = link.getAttribute('data-table');
		var field = link.getAttribute('data-field');
		var key = table + '.' + field;
		var response = cshHelp.key(key);
		tip.target = tip.triggerElement;
		if (response) {
			updateTip(response);
		} else {
				// If a table is defined, use ExtDirect call to get the tooltip's content
			if (table) {
				var description = '';
				if (typeof(top.TYPO3.LLL) !== 'undefined') {
					description = top.TYPO3.LLL.core.csh_tooltip_loading;
				} else if (opener && typeof(opener.top.TYPO3.LLL) !== 'undefined') {
					description = opener.top.TYPO3.LLL.core.csh_tooltip_loading;
				}

					// Clear old tooltip contents
				updateTip({
					description: description,
					cshLink: '',
					moreInfo: '',
					title: ''
				});
					// Load content
				TYPO3.CSH.ExtDirect.getTableContextHelp(table, function(response, options) {
					Ext.iterate(response, function(key, value){
						cshHelp.add(value);
						if (key === field) {
							updateTip(value);
								// Need to re-position because the height may have increased
							tip.show();
						}
					});
				}, this);

				// No table was given, use directly title and description
			} else {
				updateTip({
					description: link.getAttribute('data-description'),
					cshLink: '',
					moreInfo: '',
					title: link.getAttribute('data-title')
				});
			}
		}
	}

	/**
	 * Update tooltip message
	 *
	 * @param {Object} response
	 */
	function updateTip(response) {
		tip.body.dom.innerHTML = response.description;
		tip.cshLink = response.id;
		tip.moreInfo = response.moreInfo;
		if (tip.moreInfo) {
			tip.addClass('tipIsLinked');
		}
		tip.setTitle(response.title);
		tip.doAutoWidth();
	}

	return {
		/**
		 * Constructor
		 */
		init: function() {
			tip = new Ext.ToolTip({
				title: 'CSH', // needs a title for init because of the markup
				html: '',
					// The tooltip will appear above the label, if viewport allows
				anchor: 'bottom',
				minWidth: 160,
				maxWidth: 240,
				target: Ext.getBody(),
				delegate: 'span.t3-help-link',
				renderTo: Ext.getBody(),
				cls: 'typo3-csh-tooltip',
				shadow: false,
				dismissDelay: 0, // tooltip stays while mouse is over target
				autoHide: true,
				showDelay: 1000, // show after 1 second
				hideDelay: 300, // hide after 0.3 seconds
				closable: true,
				isMouseOver: false,
				listeners: {
					beforeshow: showToolTipHelp,
					render: function(tip) {
						tip.body.on({
							'click': {
								fn: function(event) {
									event.stopEvent();
									if (tip.moreInfo) {
										try {
											top.TYPO3.ContextHelpWindow.open(tip.cshLink);
										} catch(e) {
											// do nothing
										}
									}
									tip.hide();
								}
							}
						});
						tip.el.on({
							'mouseover': {
								fn: function() {
									if (tip.moreInfo) {
										tip.isMouseOver = true;
									}
								}
							},
							'mouseout': {
								fn: function() {
									if (tip.moreInfo) {
										tip.isMouseOver = false;
										tip.hide.defer(tip.hideDelay, tip, []);
									}
								}
							}
						});
					},
					hide: function(tip) {
						tip.setTitle('');
						tip.body.dom.innerHTML = '';
					},
					beforehide: function(tip) {
						return !tip.isMouseOver;
					},
					scope: this
				}
			});

			Ext.getBody().on({
				'keydown': {
					fn: function() {
						tip.hide();
					}
				},
				'click': {
					fn: function() {
						tip.hide();
					}
				}
			});

			/**
			 * Adds a sequence to Ext.TooltTip::showAt so as to increase vertical offset when anchor position is 'bottom'
			 * This positions the tip box closer to the target element when the anchor is on the bottom side of the box
			 * When anchor position is 'top' or 'bottom', the anchor is pushed slightly to the left in order to align with the help icon, if any
			 *
			 */
			Ext.ToolTip.prototype.showAt = Ext.ToolTip.prototype.showAt.createSequence(
				function() {
					var ap = this.getAnchorPosition().charAt(0);
					if (this.anchorToTarget && !this.trackMouse) {
						switch (ap) {
							case 'b':
								var xy = this.getPosition();
								this.setPagePosition(xy[0]-10, xy[1]+5);
								break;
							case 't':
								var xy = this.getPosition();
								this.setPagePosition(xy[0]-10, xy[1]);
								break;
						}
					}
				}
			);

		},

		/**
		 * Opens the help window, triggered from click event handler
		 *
		 * @param {Event} event
		 * @param {Node} link
		 */
		openHelpWindow: function(event, link) {
			var id = link.getAttribute('data-table') + '.' + link.getAttribute('data-field');
			event.stopEvent();
			top.TYPO3.ContextHelpWindow.open(id);
		}
	}
}();

/**
 * Calls the init on Ext.onReady
 */
Ext.onReady(TYPO3.ContextHelp.init, TYPO3.ContextHelp);

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Flashmessage rendered by ExtJS
 *
 *
 * @author Steffen Kamper <info@sk-typo3.de>
 */
Ext.ns('TYPO3');

/**
 * Object for named severities
 */
TYPO3.Severity = {
	notice: 0,
	information: 1,
	ok: 2,
	warning: 3,
	error: 4
};

/**
 * @class TYPO3.Flashmessage
 * Passive popup box singleton
 * @singleton
 *
 * Example (Information message):
 * TYPO3.Flashmessage.display(1, 'TYPO3 Backend - Version 4.4', 'Ready for take off', 3);
 */
TYPO3.Flashmessage = function() {
	var messageContainer;
	var severities = ['notice', 'information', 'ok', 'warning', 'error'];

	function createBox(severity, title, message) {
		return ['<div class="typo3-message message-', severity, '" style="width: 400px">',
				'<div class="t3-icon t3-icon-actions t3-icon-actions-message t3-icon-actions-message-close t3-icon-message-' + severity + '-close"></div>',
				'<div class="header-container">',
				'<div class="message-header">', title, '</div>',
				'</div>',
				'<div class="message-body">', message, '</div>',
				'</div>'].join('');
	}

	return {
		/**
		 * Shows popup
		 * @member TYPO3.Flashmessage
		 * @param int severity (0=notice, 1=information, 2=ok, 3=warning, 4=error)
		 * @param string title
		 * @param string message
		 * @param float duration in sec (default 5)
		 */
		display : function(severity, title, message, duration) {
			duration = duration || 5;
			if (!messageContainer) {
				messageContainer = Ext.DomHelper.insertFirst(document.body, {
					id   : 'msg-div',
					style: 'position:absolute;z-index:10000'
				}, true);
			}

			var box = Ext.DomHelper.append(messageContainer, {
				html: createBox(severities[severity], title, message)
			}, true);
			messageContainer.alignTo(document, 't-t');
			box.child('.t3-icon-actions-message-close').on('click',	function (e, t, o) {
				var node;
				node = new Ext.Element(Ext.get(t).findParent('div.typo3-message'));
				node.hide();
				Ext.removeNode(node.dom);
			}, box);
			box.slideIn('t').pause(duration).ghost('t', {remove: true});
		}
	};
}();
