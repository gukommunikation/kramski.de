<?php


///////////////////////////////////////////////////// uid: 12 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid12',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 12
' . 'tt_content.dce_dceuid12.10 >', 43);





///////////////////////////////////////////////////// uid: 11 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid11',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 11
' . 'tt_content.dce_dceuid11.10 >', 43);





///////////////////////////////////////////////////// uid: 9 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid9',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 9
' . 'tt_content.dce_dceuid9.10 >', 43);





///////////////////////////////////////////////////// uid: 6 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid6',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 6
' . 'tt_content.dce_dceuid6.10 >', 43);





///////////////////////////////////////////////////// uid: 5 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid5',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 5
' . 'tt_content.dce_dceuid5.10 >', 43);





///////////////////////////////////////////////////// uid: 4 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid4',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 4
' . 'tt_content.dce_dceuid4.10 >', 43);





///////////////////////////////////////////////////// uid: 3 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid3',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 3
' . 'tt_content.dce_dceuid3.10 >', 43);





///////////////////////////////////////////////////// uid: 2 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid2',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 2
' . 'tt_content.dce_dceuid2.10 >', 43);





///////////////////////////////////////////////////// uid: 1 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'ArminVieweg.dce',
	'dceuid1',
	array(
		'Dce' => 'show',
	),
	array(
		
	),
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide lib.stdheader for DCE with uid 1
' . 'tt_content.dce_dceuid1.10 >', 43);


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript('dce', 'setup', '
# Hide default wrapping for content elements for DCE with uid 1
' . 'tt_content.stdWrap.innerWrap.cObject.default.stdWrap.if.value := addToList(dce_dceuid1)', 43);


