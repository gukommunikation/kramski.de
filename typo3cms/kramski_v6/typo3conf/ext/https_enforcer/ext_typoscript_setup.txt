plugin.tx_httpsenforcer_pi1 {
	unsecure_typo3_root = {$https_enforcer.unsecure_typo3_root}
	secure_typo3_root = {$https_enforcer.secure_typo3_root}
	require_ssl = {$https_enforcer.require_ssl}
	disable_httpsenforcer_for_be_user = {$https_enforcer.disable_httpsenforcer_for_be_user}
	ssl_proxy = {$https_enforcer.ssl_proxy}
	always_allow_SSL = {$https_enforcer.always_allow_SSL}
	sslPort = {$https_enforcer.sslPort}
}
