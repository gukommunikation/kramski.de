plugin.tx_wtgallery_pi1 {
  list {
    image = IMAGE
    image {
      imageLinkWrap.typolink {
        parameter.cObject = IMG_RESOURCE
        parameter.cObject.file.import.field = picture
        parameter.cObject.file.maxW = 700
        parameter.cObject.file.maxH = 700
        ATagParams.dataWrap = rel="fancybox" class="fancybox"
      }
    }
  }
}
plugin.tx_wtgallery_pi1.single.image.file.width = 700
plugin.tx_wtgallery_pi1.list.image.file.height = 140c
plugin.tx_wtgallery_pi1.list.rows = 4
plugin.tx_wtgallery_pi1.list.columns = 5
plugin.tx_wtgallery_pi1.template.list = {$path.private}/Templates/wtgallery_list.html
plugin.tx_wtgallery_pi1.list.width = 140c
plugin.tx_wtgallery_pi1.main.DIVforRows = 0