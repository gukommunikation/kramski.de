<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}
// Fuegt Element im backend hinzu
// Titel, tt_content Eintrag, Content Type (kein Plugin)

$inherit = array(
	'inherit' => array (
		'exclude' => 0,
		'label' => 'Objekt links und rechts einrücken',
		'config' => array (
			'type' => 'check',
		)
	)
);
t3lib_extMgm::addTCAcolumns('tt_content', $inherit, 1);
t3lib_extMgm::addFieldsToPalette('tt_content', 'general', 'inherit', 'after:sys_language_uid');

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Content Elements');

?>