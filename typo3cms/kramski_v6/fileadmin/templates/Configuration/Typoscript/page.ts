#############################################
## setzt Zugriffe fuer neu angelegte Seiten ##
#############################################
TCEMAIN.permissions.everybody = show,edit,delete,new,editcontent
TCEMAIN.permissions.group = show,edit,delete,new,editcontent

################
## RTE Config ##
################
RTE.default.contentCSS = fileadmin/templates/Resources/Public/Css/rte.css
RTE.classes {
  bluebig {
    name = Schrift blau gross
    value = color: #00a1e4;
  }
  bluebigbold {
    name = Schrift blau gross fett
    value = color: #00a1e4; font-weight: bold;
  }
  bluemiddle {
    name = Schrift blau mittel
    value = color: #00a1e4;
  }
  bluemiddlebold {
    name = Schrift blau mittel fett
    value = color: #00a1e4; font-weight: bold;
  }
  bluesmall {
    name = Schrift blau klein
    value = color: #00a1e4;
  }
  bluesmallbold {
    name = Schrift blau klein fett
    value = color: #00a1e4; font-weight: bold;
  }
  bluesmallarial {
    name = Schrift blau klein Arial
    value = color: #00a1e4;
  }
  blackbig {
    name = Schrift schwarz gross
    value = color: #000;
  }
  blackbigbold {
    name = Schrift schwarz gross fett
    value = color: #000; font-weight: bold;
  }
  blackmiddle {
    name = Schrift schwarz mittel
    value = color: #000;
  }
  blackmiddlebold {
    name = Schrift schwarz mittel fett
    value = color: #000; font-weight: bold;
  }
  blacksmall {
    name = Schrift schwarz klein
    value = color: #000;
  }
  blacksmallbold {
    name = Schrift schwarz klein fett
    value = color: #000; font-weight: bold;
  }  
  whitebig {
    name = Schrift weiss gross
    value = color: #000;
  }
  whitebigbold {
    name = Schrift weiss gross fett
    value = color: #000; font-weight: bold;
  }
  whitemiddle {
    name = Schrift weiss mittel
    value = color: #000;
  }
  whitemiddlebold {
    name = Schrift weiss mittel fett
    value = color: #000; font-weight: bold;
  }
  whitesmall {
    name = Schrift weiss klein
    value = color: #000;
  }
  whitesmallbold {
    name = Schrift weiss klein fett
    value = color: #000; font-weight: bold;
  }
  greybackground {
    name = Grauer Hintergrund
    value = background: #f2f2f2;
  }
  fontbluebg {
    name = Weisse Schrift auf blau
    value = background: #4dbdec; color: white;
  }
}
RTE.default.buttons.textstyle.tags.span.allowedClasses := addToList(fontbluebg, bluebig,bluebigbold,bluemiddle,bluemiddlebold,bluesmall,bluesmallbold,bluesmallarial,blackbig,blackbigbold,blackmiddle,blackmiddlebold,blacksmall,blacksmallbold,whitebig,whitebigbold,whitemiddle,whitemiddlebold,whitesmall,whitesmallbold)
RTE.default.proc.allowedClasses := addToList(fontbluebg, greybackground,bluebig,bluebigbold,bluemiddle,bluemiddlebold,bluesmall,bluesmallbold,bluesmallarial,blackbig,blackbigbold,blackmiddle,blackmiddlebold,blacksmall,blacksmallbold,whitebig,whitebigbold,whitemiddle,whitemiddlebold,whitesmall,whitesmallbold)
RTE.default.buttons.textstyle.tags.span.allowedClasses ( 
fontbluebg, bluebig,bluebigbold,bluemiddle,bluemiddlebold,bluesmall,bluesmallbold,bluesmallarial,blackbig,blackbigbold,blackmiddle,blackmiddlebold,blacksmall,blacksmallbold,whitebig,whitebigbold,whitemiddle,whitemiddlebold,whitesmall,whitesmallbold
)
RTE.default.buttons.blockstyle.tags.span.allowedClasses ( 
greybackground
)

RTE.default.proc {
  dontConvBRtoParagraph = 1
  allowTags (
        a, abbr, acronym, address, blockquote, b, br, caption, center, cite, code, div, em, font, h1, h2, h3, h4, h5, h6, hr, i, img, li, link, ol, p, pre, q, sdfield, strike, strong, sub, sup, table, thead, tbody, tfoot, td, th, tr, tt, u, ul, span
        )        
        entryHTMLparser_db = 1
}
RTE.default.proc.entryHTMLparser_db {
  allowTags < RTE.default.proc.allowTags
  htmlSpecialChars = 0
}
RTE.default.proc.exitHTMLparser_db {
  tags.b.remap = strong
  tags.i.remap = em
  keepNonMatchedTags = 1
  htmlSpecialChars = 0
}
RTE.default.hideButtons = class,formatblock,subscript,superscript,table,about,toggleborders,tableproperties,rowproperties,rowinsertabove,rowinsertunder,rowdelete,rowsplit,columninstertbefore,columninsertafter,columndelete,columnsplit,cellproperties,cellinsertbefore,cellinsertafter,celldelete,cellsplit,cellemerge,fontstyle, fontsize,insertparagraphbefore,insertparagraphafter,justifyfull,strikethrough,emoticon,textindicator,textcolor, bgcolor,underline,line,findreplace,spellcheck,inserttag,lefttoright, righttoleft,language,definitionlist,undo,redo,outdent,indent,copy,cut,paste,textindicator,toggleterms,definition,chMode,big,citation,code
RTE.default.showButtons = bold, italic, link, orderedlist, unorderedlist, image, removeformat,textstylelabel,textstyle, insertcharacter,left,center,right,blockstylelabel,blockstyle,pastetoggle 
RTE.default {
  enableWordClean = 1 
  removeComments = 1
  keepButtonGroupTogether = 1
  showStatusBar = 1
}
RTE.default.buttons.pastetoggle.setActiveOnRteOpen = 1
RTE.classesAnchor.externalLink.image >
RTE.classesAnchor.externalLinkInNewWindow.image >
RTE.classesAnchor.internalLink.image >
RTE.classesAnchor.internalLinkInNewWindow.image >
RTE.classesAnchor.download.image >
RTE.classesAnchor.mail.image >
RTE.classesAnchor {
  externalLink.titleText = Öffnet diese Seite.
  externalLinkInNewWindow.titleText = Öffnet diese Seite in neuem Fenster.
  internalLink.titleText = Öffnet diese Seite.
  internalLinkInNewWindow.titleText = Öffnet diese Seite in neuem Fenster.
  download.titleText = Öffnet die Datei.
  mail.titleText = Eine E-Mail an diese Adresse senden.
  externalLink.altText = Öffnet diese Seite.
  externalLinkInNewWindow.altText = Öffnet diese Seite in neuem Fenster.
  internalLink.altText = Öffnet diese Seite.
  internalLinkInNewWindow.altText = Öffnet diese Seite in neuem Fenster.
  download.altText = Öffnet die Datei.
  mail.altText = Eine E-Mail an diese Adresse senden.
}
FE < RTE.default
RTE.default.disableColorPicker = 1
RTE.default.FE.proc < RTE.default.proc
RTE.default.FE < RTE.default

##########################
## Linkvalidator Config ##
##########################
mod.linkvalidator {
       searchFields {
               pages = media,url
               tt_content = bodytext,header_link,records,image_link
               tt_news =  bodytext,links,ext_url
       }
       linktypes = db,file,external
       checkhidden = 0
       mail {
               fromname = Kramski GmbH
               fromemail = snotest@gkmbsport.de
               replytoname = GKMB GmbH  
               replytoemail = snotest@gkmbsport.de
               subject = TYPO3 Linkvalidator report
       }
}
##############################
## BE Config for tt_content ##
##############################
TCEFORM.tt_content {
  header_layout  {
    altLabels.0 = Überschrift blau mittel
    altLabels.2 = Überschrift blau mittel in Navigation
    altLabels.3 = Überschrift schwarz klein
    removeItems = 1,4,5
  }
  sys_language_uid.disabled = 0
  header_position.disabled = 1
  header_link.disabled = 0
  date.disabled = 1
  subheader.disabled = 0
  section_frame {
    removeItems = 1,2,3,4,5,6,10,11,12,20,21
    disabled = 1
  }
  text_size.disabled = 1
  text_color.disabled = 1
  text_properties.disabled = 1
  image_compression.disabled = 1
  image_effects.disabled = 0
  image_frames.disabled = 1
  imagecaption_position.disabled = 1
  altText.disabled = 1
  titleText.disabled = 1
  longdescURL.disabled = 1
  table_border.disabled = 1
  table_bgColor.disabled = 1
  table_cellspacing.disabled = 1
  table_cellpadding.disabled = 1
  layout.altLabels.3 = Tabelle mit Einzelueberschrift Spalteninhalt rechtsbuendig
  layout.altLabels.2 = Tabelle mit Einzelueberschrift Spalteninhalt linksbuendig
  layout.altLabels.1 = Tabelle mit einer Ueberschrift Spalteninhalt rechtsbuendig
  layout.altLabels.0 = Tabelle mit einer Ueberschrift Spalteninhalt linksbuendig
  layout.addItems.4 = Tabelle mit Überschriften in der ersten Spalte
  layout.addItems.5 = Tabelle transparent
  imagewidth.disabled = 1
  imageheight.disabled = 1
  image_noRows.disabled = 1
  spaceAfter.disabled = 0
  spaceBefore.disabled = 0
  imagecols.removeItems = 4,5,6,7,8
  imageorient.removeItems = 0,1,3,4,5,6,7,8,9,10
  imagecols.label.default = 1
  imageborder.disabled = 0
  image_zoom.disabled = 0
  cols.removeItems = 0,6,7,8,9,10
  CType.removeItems = rte,script,splash,swfobject,qtobject,multimedia,header,bullets,media,sr_language_menu_pi1,caddie_pi1,search,uploads
}

#########################
## BE Config for pages ##
#########################
TCEFORM.pages {
  layout.disabled = 1
  lastUpdated.disabled = 1
  keywords.disabled = 0
  newUntil.disabled = 1
  description.disabled = 0
  no_search.disabled = 1
  subtitle.disabled = 1
  nav_title.disabled = 1
  author.disabled = 1
  author_email.disabled = 1
  php_tree_stop.disabled = 1
  fe_login_mode.disabled = 1
  module.disabled = 0
  tx_rlmptmplselector_ca_tmpl.disabled = 1
  tx_rlmptmplselector_main_tmpl.disabled = 1
  tx_imagecycle_mode.disabled = 1
  tx_imagecycle_damimages.disabled = 1
  tx_imagecycle_damcategories.disabled = 1
  tx_imagecycle_images.disabled = 1
  tx_imagecycle_hrefs.disabled = 1
  tx_imagecycle_captions.disabled = 1
  tx_imagecycle_stoprecursion.disabled = 1
  tx_imagecycle_effect.disabled = 1
  is_siteroot.disabled = 1
  media.disabled = 0
  abstract.disabled = 1
  extendToSubpages.disabled = 1
  url_scheme.disabled = 1
}
############################
## BE Config for fe_users ##
############################
TCEFORM.fe_users {
  company.disabled = 1
  title.disabled = 1
  cnum.disabled = 1
  middle_name.disabled = 1
  status.disabled = 1
  date_of_birth.disabled = 1
  zone.disabled = 1
  static_info_country.disabled = 1
  language.disabled = 1
  by_invitation.disabled = 1
  image.disabled = 1
  lockToDomain.disabled = 1  
}
###########################
## BE Config for tt_news ##
###########################
TCEFORM.tt_news {
  imagealttext.disabled = 0
  imagetitletext.disabled = 0
  related.disabled = 1
  author.disabled = 1
  author_email.disabled = 1
  news_files.disabled = 1
  links.disabled = 1
  keywords.disabled = 0
  tx_imagecycle_activate.disabled = 1  
  tx_imagecycle_duration.disabled = 1
  archivedate.disabled = 1
  fe_group.disabled = 1
  no_auto_pb.disabled = 1
  sys_language_uid.disabled = 0
  editlock.disabled = 1
  page.disabled = 0
}  
##########################
## BE Config for banner ##
##########################
TCEFORM.tx_macinabanners_banners {
  sys_language_uid.disabled = 1
  parameters.disabled = 1
  maxw.disabled = 1
  alttext.disabled = 1
  border_top.disabled = 1
  border_right.disabled = 1
  border_bottom.disabled = 1
  border_left.disabled = 1
  placement.disabled = 1
  fe_group.disabled = 1
}

##################################
## id storagepage of gridlayout ##
##################################
TCEFORM.pages.backend_layout.PAGE_TSCONFIG_ID = 3
TCEFORM.pages.backend_layout_next_level.PAGE_TSCONFIG_ID = 3

##################################
## hide no backend layout label ##
##################################
TCEFORM.pages.backend_layout_next_level.removeItems = -1
TCEFORM.pages.backend_layout.removeItems = -1

###################################
## give empty label a definition ##
###################################
TCEFORM.pages.backend_layout.altLabels.0 = Vordefiniertes Standard Layout

#################
## admin panel ##
#################
admPanel.enable.all = 1
admPanel.hide = 0

########################
## gridelement config ##
########################
TCEFORM.tt_content.tx_gridelements_backend_layout {
    removeChildrenFromList = 1
}

####################################################
## setzt Anzahl der Spalten im image object auf 1 ##
####################################################
mod.wizards.newContentElement.wizardItems.common.elements.image.tt_content_defValues.imagecols = 1


############################################################
## unbenutzte Contentelemente aus Dropdownliste entfernen ##
############################################################
TCEFORM.tt_content.CType.removeItems =

TCAdefaults.pages.l18n_cfg = 2