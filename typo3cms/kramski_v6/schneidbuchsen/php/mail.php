<?php
define('DEBUG', false);
define('error_reporting', off);

if( (isset($_POST['Name'])&&$_POST['Name']!="") &&
	(isset($_POST['Telefon'])&&$_POST['Telefon']!="") && 
	(isset($_POST['Email'])&&$_POST['Email']!="") && 
	(isset($_POST['Firma'])&&$_POST['Firma']!="") && 
	(isset($_POST['Nachricht'])&&$_POST['Nachricht']!="")
	){ 
	
	$to = 'vertrieb@kramski.com';
	
	if((isset($_POST['totalPrice'])&&$_POST['totalPrice']!="")){
		$subject = 'Kramski Schneidbuchsen Bestellung';
		$message = '
        <html>
            <head>
                <title>Kontaktaufnahme Kramski Schneidbuchsen</title>
            </head>
            <body>
                <p><b>Name:</b> '.$_POST['Name'].'</p>
                <p><b>Firma:</b> '.$_POST['Firma'].'</p>
                <p><b>E-Mail:</b> '.$_POST['Email'].'</p>
                <p><b>Telefon:</b> '.$_POST['Telefon'].'</p>
                <p><b>Nachricht:</b><br/> '.$_POST['Nachricht'].'</p>
				<p><h4>Warenkorb:</h4></p>
				<p><b>Gesamtpreis: </b>'.$_POST['totalPrice'].'</p>
				<p>'.$_POST['products'].'</p>
		</body>
        </html>'; 		
	} else {
		$subject = 'Kramski Schneidbuchsen Anfrage';
		$message = '
			<html>
				<head>
					<title>Kontaktaufnahme Kramski Schneidbuchsen</title>
				</head>
				<body>
					<p><b>Name:</b> '.$_POST['Name'].'</p>
					<p><b>Firma:</b> '.$_POST['Firma'].'</p>
					<p><b>E-Mail:</b> '.$_POST['Email'].'</p>
					<p><b>Telefon:</b> '.$_POST['Telefon'].'</p>
					<p><b>Nachricht:</b> '.$_POST['Nachricht'].'</p>
				</body>
			</html>'; 
	}
	
	$headers  = "Content-type: text/html; charset=utf-8 \r\n"; 
	$headers .= "From: Online Testkampange <info@kramski.com>\r\n"; 
	$headers .= 'Cc: Merlin.Schart@fdi.de' . "\r\n";
	mail($to, $subject, $message, $headers);

	echo json_encode(array('status' => 'success'));
} else {
	echo json_encode(array('status' => 'error'));
}

?>
