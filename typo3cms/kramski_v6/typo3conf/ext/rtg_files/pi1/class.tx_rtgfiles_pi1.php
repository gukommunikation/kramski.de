<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2007 Robert Gonda <robert.gonda@gmail.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/


/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *
 *
 *   57: class tx_rtgfiles_pi1 extends tslib_pibase
 *   87:     function main( $content,$conf )
 *  148:     function download( $uid )
 *  213:     function view( $uid )
 *  245:     function getFiles()
 *  286:     function getSystems()
 *  319:     function sizeReadable( $size, $unit = null, $retstring = null, $si = true )
 *  357:     function parseTemplate( $subpart )
 *
 * TOTAL FUNCTIONS: 7
 * (This index is automatically created/updated by the extension "extdeveval")
 *
 */


##require_once(PATH_tslib.'class.tslib_pibase.php');


/**
 * Plugin 'Documents' for the 'rtg_files' extension.
 *
 * @author	rRobert Gonda <robert.gonda@gmail.com>
 * @package	TYPO3
 * @subpackage	tx_rtgfiles
 * @author_company	BSP Magnetica s.r.o.
 */
class tx_rtgfiles_pi1 extends tslib_pibase {

	var $prefixId = 'tx_rtgfiles_pi1';
	var $scriptRelPath = 'pi1/class.tx_rtgfiles_pi1.php';
	var $extKey = 'rtg_files';
	var $pi_checkCHash = TRUE;

	// Classes
	var $local_cObj = false;
	var $geshi = false;

	var $conf;
	var $GPvars;
	var $filesPath = 'uploads/tx_rtgfiles/';
	var $content = '';
	var $num_rows = 0;
	var $orderBy = '';
	var $counter = 0;
	var $error = true;
	var $errorWord = '';
	var $wrapErrors = '|';
	var $valueWord = '';
	var $file;


	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The		content that is displayed on the website
	 */
	function main( $content,$conf ) {

		// Configuration
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		$GLOBALS['TSFE']->set_no_cache();
		$this->local_cObj = t3lib_div::makeInstance('tslib_cObj');
		$this->GPvars = t3lib_div::_GP( 'tx_rtgfiles_pi1' );
		
		// Init FlexForm configuration for plugin
		$this->pi_initPIflexForm(); 
		$this->pi_flexform = $this->cObj->data['pi_flexform'];
		$cmd = $this->pi_getFFvalue( $this->pi_flexform, 'cmd', 'sDEF' );
		$pages = $this->pi_getFFvalue( $this->pi_flexform, 'pages', 'sDEF' );
		$recursive = $this->pi_getFFvalue( $this->pi_flexform, 'recursive', 'sDEF' );
		$pidview = intval( $this->pi_getFFvalue( $this->pi_flexform, 'pidview', 'sDEF' ) );
		$template = $this->pi_getFFvalue( $this->pi_flexform, 'template', 'sDEF' );
		$template = !empty( $template ) ? $this->filesPath.$template : '';
		$wrapErrorMessages = $this->pi_getFFvalue( $this->pi_flexform, 'wrapErrorMessages', 'sDEF' );

		// Configuration from TS
		$this->id = $GLOBALS['TSFE']->id;
		$this->pid = !empty( $pages ) ? $pages : ( $this->conf['pidrootpage'] > 0 ? $this->conf['pidrootpage']: $this->id );
        $this->pid = $this->pi_getPidList( $this->pid, ( $recursive > 0 ? $recursive : $this->conf['recursive'] ) );
		$this->pidView = $pidview > 0 ? $pidview : ( $this->conf['pidview'] > 0 ? intval( $this->conf['pidview'] ): $this->id );
		$this->totalTemplate = $this->cObj->fileResource( empty( $template ) ? $this->conf['template'] : $template );
		$this->wrapErrors = !empty( $wrapErrorMessages ) ? $wrapErrorMessages : ( $this->conf['wrapErrorMessages'] ? $this->conf['wrapErrorMessages']: '|' );
		$this->orderBy = $this->conf['orderBy'] == '' ? 'sorting': $this->conf['orderBy'];
		$this->conf['limit'] = intval( $this->conf['limit'] );
		$this->conf['limit'] = $this->conf['limit'] > 0 ? $this->conf['limit'] : 1000;
		
		// Download file by MIME type
		$this->GPvars['cmd'] = empty( $cmd ) ? $this->GPvars['cmd'] : $cmd;
		if( $this->GPvars['cmd'] == 'DOWNLOAD' && $this->GPvars['uid'] > 0 ) {
			$this->download( intval( $this->GPvars['uid'] ) );
		}
		elseif( $this->GPvars['cmd'] == 'VIEW' && $this->GPvars['uid'] > 0 ) {
			$content = $this->view( intval( $this->GPvars['uid'] ) );
			return $this->pi_wrapInBaseClass( $content );
		}
		elseif( $this->GPvars['cmd'] == 'SYSTEMS' ) {
			$this->files = $this->systems();
			$content = $this->parseTemplateSystems( 'SYSTEMS_LIST', $this->files );
			return $this->pi_wrapInBaseClass( $content );
		}
		// Form is submitted
		elseif( $_POST['submit'] || isset( $this->GPvars['page'] ) ) {
			$this->valueSystem = intval( $this->GPvars['system'] );
			$this->GPvars['word'] = strip_tags( trim( $this->GPvars['word'] ) );
			// Form data validation
			if( strlen( $this->GPvars['word'] ) >= 2 ) {
				$this->valueWord = $this->GPvars['word'];
				$this->error = false;
			}
			elseif( $this->valueSystem > 0 ) {
				$this->valueWord = '';
				$this->error = false;
			}
			else {
				$this->errorWord = $this->pi_getLL( 'error_word' );
				$this->error = true;
			}
		}
		$this->content = $this->parseTemplate( 'FILES_LIST' );

		// Debug information
		error_reporting( E_ERROR | E_WARNING );
		// $this->debug .= '<pre>$this->GPvars:<br />'.print_r( $this->GPvars, true ).'</pre>';
		// $this->debug .= '<pre>$this->pi_flexform:<br />'.print_r( $this->pi_flexform, true ).'</pre>';
		// $this->debug .= '<pre>$pages: '.$pages.'<br />$recursive: '.$recursive.'</pre>';

		// Content rendering
		$content = $this->content; // .'<br />'.$this->debug;
		return $this->pi_wrapInBaseClass( $content );
	}

	/**
	 * Download file
	 *
	 * @param	int		$uid:
	 * @return	void
	 */
	function download( $uid ) {

		$where = 'uid = '.$uid.' '.$this->local_cObj->enableFields( 'tx_rtgfiles_files' );
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery( 'uid,file,url,clicks', 'tx_rtgfiles_files', $where );
		if( $res ) {
			if( $this->file = $GLOBALS['TYPO3_DB']->sql_fetch_assoc( $res ) ) {
				if( file_exists( $this->filesPath.$this->file['file'] ) ) {

					// Update clicks
					$values = array( 'clicks' => intval( $this->file['clicks'] ) + 1 );
					$GLOBALS['TYPO3_DB']->exec_UPDATEquery( 'tx_rtgfiles_files', 'uid = '.$uid, $values );

					// Redirect to external URL
					if( $this->file['url'] != '' ) {
						header( 'Location: '.$this->file['url'] );
						exit;
					}

					// Set MIME type by file extension
					$ext = strtolower( substr( ( $t = strrchr( $data['file'], '.' ) ) !== false ? $t : '' ,1 ) );
					switch( $ext ) {
						
						// Others
						case 'pdf': $ctype = 'application/pdf'; break;
						case 'exe': $ctype = 'application/octet-stream'; break;
						case 'zip': $ctype = 'application/zip'; break;
						case 'txt': $ctype = 'text/plain'; break;

						// Microsoft Office
						case 'dot':
						case 'doc': $ctype = 'application/msword'; break;
						case 'xla':
						case 'xlc':
						case 'xlm':
						case 'xls':
						case 'xlt':
						case 'xlw': $ctype = 'application/vnd.ms-excel'; break;
						case 'pot':	
						case 'pps':
						case 'ppt': $ctype = 'application/vnd.ms-powerpoint'; break;

						// Image, video, flash
						case 'bmp': $ctype = 'image/bmp'; break;
						case 'gif': $ctype = 'image/gif'; break;
						case 'png': $ctype = 'image/png'; break;
						case 'jpeg':
						case 'jpe':
						case 'jpg': $ctype = 'image/jpg'; break;
						case 'ico': $ctype = 'image/x-icon'; break;
						case 'svg': $ctype = 'image/svg+xml'; break;
						case 'flv': $ctype = 'video/x-flv'; break;
						case 'mpa':
						case 'mpe':
						case 'mpeg':
						case 'mpg': $ctype = 'application/octet-stream'; break;
						case 'swf': $ctype = 'application/x-shockwave-flash'; break;
						
						// Force download
						default:    $ctype = 'application/force-download';
					}

					// Header data
					header( 'Pragma: public' );
					header( 'Expires: 0' );
					header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
					// header( 'Cache-Control: private', false );
					header( 'Content-Type: '.$ctype );
					// header( 'Content-Disposition: inline; filename="'.basename( $this->file['file'] ).'"' );

					// File attachment
					header( "Content-Disposition: attachment; filename=".basename( $this->file['file'] ).";" );
					header( "Content-Transfer-Encoding: binary" );
					header( "Content-Length: ".filesize( $this->filesPath.$this->file['file'] ) );
					readfile( $this->filesPath.$this->file['file'] );
					// exit;
				}
			}
		}
		return false;
	}

	/**
	 * View source file
	 *
	 * @param	int		$uid:
	 * @return	void
	 */
	function view( $uid ) {

		$where = 'uid = '.$uid.' '.$this->local_cObj->enableFields( 'tx_rtgfiles_files' );
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery( 'uid,file,url,clicks', 'tx_rtgfiles_files', $where );
		if( $res ) {
			if( $this->file = $GLOBALS['TYPO3_DB']->sql_fetch_assoc( $res ) ) {
				if( file_exists( $this->filesPath.$this->file['file'] ) ) {

					// File information
					$file = explode( '.', $this->file['file'] );
					$extPos = count( $file ) - 1;
					$fileContent = @file_get_contents( $this->filesPath.$this->file['file'] );

					// Source highligting (by geshilib)
					if( isset( $this->conf['types.'][$file[$extPos].'.']['view'] ) ) {
						require_once( t3lib_extMgm::siteRelPath( 'geshilib' ).'res/geshi.php' );
						$this->geshi = new Geshi( $fileContent, $this->conf['types.'][$file[$extPos].'.']['view'], '' );
						$this->geshi->enable_line_numbers( GESHI_NORMAL_LINE_NUMBERS, 1 );
						return $this->geshi->parse_code();
					}
					return '<pre>'.$fileContent.'</pre>';
				}
			}
		}
		return false;
	}

	/**
	 * Structured files list by systems
	 *
	 * @return	void
	 */
	function systems() {

		$query = '
			SELECT tx_rtgfiles_files.*,
				tx_rtgfiles_systems.uid AS systemsUid, tx_rtgfiles_systems.title AS systemsTitle
			FROM tx_rtgfiles_files
				LEFT JOIN tx_rtgfiles_systems
					ON ( tx_rtgfiles_systems.uid = tx_rtgfiles_files.system '.$this->local_cObj->enableFields( 'tx_rtgfiles_systems' ).' )
 			WHERE tx_rtgfiles_files.pid IN ( '.$this->pid.' )
				'.$this->local_cObj->enableFields( 'tx_rtgfiles_files' ).'
			ORDER BY tx_rtgfiles_systems.sorting, tx_rtgfiles_systems.pid, tx_rtgfiles_files.'.$this->orderBy.'
		';

		$res = $GLOBALS['TYPO3_DB']->sql_query( $query );
		if( $res ) {
			$this->num_rows = intval( $GLOBALS['TYPO3_DB']->sql_num_rows( $res ) );
			if( $this->num_rows > 0 ) {
				$rows = array();
				while( $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc( $res ) ) {
					$rows[] = $row;
				}
				$GLOBALS['TYPO3_DB']->sql_free_result( $res );
				return $rows;
			}
		}
		return false;
	}

	/**
	 * List of files from DB
	 *
	 * @return	array		return: array of files records
	 */
	function getFiles() {

		$limit = '';
		
		if( !$this->error ) {
			if( $this->valueWord != '' ) {
				$valueWord = $GLOBALS['TYPO3_DB']->escapeStrForLike( $GLOBALS['TYPO3_DB']->quoteStr( $this->valueWord, 'tx_rtgfiles_files' ), 'tx_rtgfiles_files' );
				$whereAnd .= '
					AND ( tx_rtgfiles_files.title LIKE \'%'.$valueWord.'%\'
						OR tx_rtgfiles_files.description LIKE \'%'.$valueWord.'%\'
						OR tx_rtgfiles_files.keywords LIKE \'%'.$valueWord.'%\' )';
			}
			if( $this->valueSystem > 0 ) {
				$whereAnd .= ' AND tx_rtgfiles_files.system = '.$this->valueSystem.' ';
			}
		}

		// Count files
		if( $this->conf['navigation'] > 0 ) {
			$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery( 'COUNT(*) AS counter', 'tx_rtgfiles_files', 
				'tx_rtgfiles_files.pid IN ( '.$this->pid.' )
				'.$this->local_cObj->enableFields( 'tx_rtgfiles_files' ).'
				'.$whereAnd.' '.addslashes( trim( $this->conf['where'] ) ).''
			);
			$tmp = $GLOBALS['TYPO3_DB']->sql_fetch_assoc( $res );
			$this->counter = intval( $tmp['counter'] );
			$GLOBALS['TYPO3_DB']->sql_free_result( $res );
	
			$this->GPvars['page'] = $this->GPvars['page'] > 0 ? intval( $this->GPvars['page'] ) : 0;
			$this->pages = ceil( $this->counter / $this->conf['limit'] );
			if( $this->GPvars['page'] > 0 && $this->GPvars['page'] < $this->pages ) {
				$this->start = $this->GPvars['page'];
			} else {
				$this->GPvars['page'] = 0;
			}
			$this->page = $this->GPvars['page'];
	
			// Set limitation
			if( $this->start > 0 )
				$limit = 'LIMIT '.( $this->start * $this->conf['limit'] ).', '.$this->conf['limit'];
			else
				$limit = 'LIMIT 0, '.$this->conf['limit'];
		}
		
		// Get files
		$query = '
			SELECT tx_rtgfiles_files.*,
				tx_rtgfiles_systems.uid AS systemsUid, tx_rtgfiles_systems.title AS systemsTitle
			FROM tx_rtgfiles_files
				LEFT JOIN tx_rtgfiles_systems
					ON ( tx_rtgfiles_systems.uid = tx_rtgfiles_files.system '.$this->local_cObj->enableFields( 'tx_rtgfiles_systems' ).' )
 			WHERE tx_rtgfiles_files.pid IN ( '.$this->pid.' )
				'.$this->local_cObj->enableFields( 'tx_rtgfiles_files' ).'
				'.$whereAnd.' '.addslashes( trim( $this->conf['where'] ) ).'
			ORDER BY tx_rtgfiles_files.'.$this->orderBy.'
			'.$limit.'
		';

		$res = $GLOBALS['TYPO3_DB']->sql_query( $query );
		if( $res ) {
			$this->num_rows = intval( $GLOBALS['TYPO3_DB']->sql_num_rows( $res ) );
			if( $this->num_rows > 0 ) {
				return $res;
			}
		}
		return false;
	}

	/**
	 * Get operating systems from DB
	 *
	 * @return	array		return: array of systems
	 */
	function getSystems() {

		$query = '
			SELECT uid, title
			FROM tx_rtgfiles_systems
 			WHERE 1=1 '.$this->local_cObj->enableFields( 'tx_rtgfiles_systems' ).'
			ORDER BY sorting
		';

		$res = $GLOBALS['TYPO3_DB']->sql_query( $query );
		$rows = array();
		if( $res ) {
			while( $row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc( $res ) ) {
				$rows[] = $row;
			}
			$GLOBALS['TYPO3_DB']->sql_free_result( $res );
			return $rows;
		}
		return false;
	}


	/**
	 * Return human readable sizes
	 *
	 * @param	int		$size        Size
	 * @param	int		$unit        The maximum unit
	 * @param	int		$retstring   The return string format
	 * @param	int		$si          Whether to use SI prefixes
	 * @return	float		return       size of file
	 * @author      Aidan Lister <aidan@php.net>
	 * @version     1.1.0
	 * @link        http://aidanlister.com/repos/v/function.size_readable.php
	 */
	function sizeReadable( $size, $unit = null, $retstring = null, $si = true ) {

	    // Units
	    if ($si === true) {
	        $sizes = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
	        $mod   = 1000;
	    } else {
	        $sizes = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
	        $mod   = 1024;
	    }
	    $ii = count($sizes) - 1;

	    // Max unit
	    $unit = array_search((string) $unit, $sizes);
	    if ($unit === null || $unit === false)
	        $unit = $ii;

	    // Return string
	    if ($retstring === null)
	        $retstring = '%01.2f %s';

	    // Loop
	    $i = 0;
	    while( $unit != $i && $size >= 1024 && $i < $ii ) {
	        $size /= $mod;
	        $i++;
	    }

	    return sprintf( $retstring, $size, $sizes[$i] );
	}


	/**
	 * Parse the subpart of the template into content with the various variables
	 *
	 * @param	string		$subpart: Needed subpart of the template
	 * @return	string		return: output template html
	 */
	function parseTemplate( $subpart ) {

		$template = array();
		$marks = array();
		$submarks = array();
		$subparts = array();
		$wrappedSubpart = array();

		// Formular
		if( $subpart == 'FILES_LIST' ) {

			$template['total'] = $this->local_cObj->getSubpart( $this->totalTemplate, '###'.$subpart.'###' );
			$template['files'] = $this->local_cObj->getSubpart( $template['total'], '###FILES_LIST_ITEM###' );
			$template['pages'] = $this->cObj->getSubpart( $template['total'], '###PAGE_LISTING###' );
			$template['pages_list'] = $this->cObj->getSubpart( $template['pages'], '###PAGES_LIST###' );
			$subparts['###PAGE_LISTING###'] = '';

			// Form attributes, titles...
			$marks['###URL###'] = $this->pi_getPageLink( $this->id );
			$marks['###HIDDEN###'] = '<input type="hidden" class="hidden" name="no_cache" value="1" />';
			$marks['###PAGETREE###'] = $this->local_cObj->cObjGetSingle( $this->conf['pagetree'], $this->conf['pagetree.'] );

			// Any errors?
			$marks['###SUBMIT_TITLE###'] = $this->pi_getLL( 'submit_title' );
			$marks['###SUBMIT_BUTTON_LABEL###'] = $this->pi_getLL( 'submit_button_label' );
			$marks['###WORD_ERROR###'] = $this->errorWord != '' ? str_replace( "|", $this->errorWord, $this->wrapErrors ): '';
			$marks['###SYSTEMS_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.system' );
			$marks['###SYSTEMS_ERROR###'] = $this->errorSystem != '' ? str_replace( "|", $this->errorSystem, $this->wrapErrors ): '';

			// Form values form post
			$marks['###WORD_VALUE###'] = $this->valueWord ? htmlspecialchars( $this->valueWord ): '';

			// Systems selects
			$listSystems = $this->getSystems();
			if( count( $listSystems ) > 0 ) {
				$marks['###SYSTEMS_SELECT###'] = '<option value="0">...</option>';
				foreach( $listSystems as $itemS )
					$marks['###SYSTEMS_SELECT###'] .= '<option value="'.$itemS['uid'].'" '.( $this->valueSystem == $itemS['uid'] ? 'selected="selected"': '' ).'>'.stripslashes( $itemS['title'] ).'</option>';
			}
			else {
				$marks['###SYSTEMS_SELECT###'] = '';
			}

			// Files list
			$res = $this->getFiles();
			if( $this->num_rows ) {

				// Navigation bar
				if( $this->conf['navigation'] > 0 ) {
					$navsubparts = array();
					$navmarks = array();
					$navwrappedSubpart = array();
					$this->conf['searchParams'][$this->prefixId.'[word]'] = $this->GPvars['word'];
					$this->conf['searchParams'][$this->prefixId.'[system]'] = $this->GPvars['system'];
					if( $this->parseNavigation( $template['pages_list'], $navsubparts, $navmarks, $navwrappedSubpart, $this->conf['searchParams'], $this->counter, $this->start, $this->pages ) ) {
						$subparts['###PAGE_LISTING###'] = $this->cObj->substituteMarkerArrayCached( $template['pages'], $navmarks, $navsubparts, $navwrappedSubpart );
					}
				}
				
				// Fields titles
				$submarks['###TSTAMP_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.date' );
				$submarks['###CLICKS_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.clicks' );
				$submarks['###SIZE_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.size' );

				while( $itemF = $GLOBALS['TYPO3_DB']->sql_fetch_assoc( $res ) ) {

					$submarks['###TITLE###'] = stripslashes( $itemF['title'] );
					$submarks['###DESCRIPTION###'] = $this->local_cObj->stdWrap( $itemF['description'], $this->conf['description_stdWrap.'] );
					$submarks['###TSTAMP###'] = $this->local_cObj->stdWrap( $itemF['tstamp'], $this->conf['tstamp_stdWrap.'] );
					$submarks['###CLICKS###'] = $itemF['clicks'];
					$subsubparts = array();
					$this->getLinks( $itemF, $submarks, $subsubparts, $wrappedSubpart );
					$content_row .= $this->cObj->substituteMarkerArrayCached( $template['files'], $submarks, $subsubparts, $wrappedSubpart );
				}
			}
			else {
				$content_row = '<tr><td colspan="3">'.$this->pi_getLL( 'empty_list' ).'</td></tr>';
			}
			
			// Final content template
			$subparts['###FILES_LIST_ITEM###'] = $content_row;
			$template = $this->cObj->substituteMarkerArrayCached( $template['total'], $marks, $subparts, $subparts );
			return $template;
		}
		return ''; // Empty output, no subpart
	}


	/**
	 * Parse the subpart of the template into content with the various variables
	 *
	 * @param	string		$subpart: Needed subpart of the template
	 * @param	array		&$list: array of file objects
	 * @return	string		return: output template html
	 */
	function parseTemplateSystems( $subpart, &$list ) {

		$template = array();
		$marks = array();
		$submarks = array();
		$wrappedSubpart = array();

		// Formular
		if( $subpart == 'SYSTEMS_LIST' ) {

			$template['total'] = $this->local_cObj->getSubpart( $this->totalTemplate, '###'.$subpart.'###' );
			$template['systems'] = $this->local_cObj->getSubpart( $template['total'], '###SYSTEMS_LIST_ITEM###' );
			$template['files'] = $this->local_cObj->getSubpart( $template['systems'], '###FILES_LIST_ITEM###' );

			// Systems/files list
			if( $this->num_rows > 0 ) {

				// Fields titles
				$submarks['###TSTAMP_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.date' );
				$submarks['###CLICKS_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.clicks' );
				$submarks['###SIZE_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.size' );

				for( $i = 0; $i < $this->num_rows; $i++ ) {

					$itemF = &$list[$i];
					$submarks['###TITLE###'] = stripslashes( $itemF['title'] );
					$submarks['###DESCRIPTION###'] = $this->local_cObj->stdWrap( $itemF['description'], $this->conf['description_stdWrap.'] );
					$submarks['###TSTAMP###'] = $this->local_cObj->stdWrap( $itemF['tstamp'], $this->conf['tstamp_stdWrap.'] );
					$submarks['###CLICKS###'] = $itemF['clicks'];
					$this->getLinks( $itemF, $submarks, $subsubparts, $wrappedSubpart );
					$sysmarks['###SYSTEM###'] = $itemF['systemsTitle'] == '' ? $this->pi_getLL( 'tx_rtgfiles_systems.none' ): $itemF['systemsTitle'];

					if( $itemF['systemsUid'] == $list[$i+1]['systemsUid'] ) {
						$files_row .= $this->cObj->substituteMarkerArrayCached( $template['files'], $submarks, $subsubparts, $wrappedSubpart );
					}
					else {
						$syssubparts['###FILES_LIST_ITEM###'] = $files_row;
						$content_row .= $this->cObj->substituteMarkerArrayCached( $template['systems'], $sysmarks, $syssubparts );
						$files_row = '';
					}
				}
			}
			else {
				$content_row = '<p>'.$this->pi_getLL( 'empty_list' ).'</p>';
			}

			// Final content template
			$subparts['###SYSTEMS_LIST_ITEM###'] = $content_row;
			$template = $this->cObj->substituteMarkerArrayCached( $template['total'], $marks, $subparts, $subparts );
			return $template;
		}
		return ''; // Empty output, no subpart
	}


	/**
	 * Parse item links
	 *
	 * @param	array		$itemF: record data
	 * @param	array		$subpart: marks
	 * @param	array		$subpart: subparts
	 * @param	array		$subpart: wrapped subparts
	 * @return	void
	 */
	function getLinks( &$itemF, &$submarks, &$subsubparts, &$wrappedSubpart ) {

		// File information
		$file = explode( '.', $itemF['file'] );
		$extPos = count( $file ) - 1;

		// External URL or document file?
		if( $itemF['url'] != '' ) {
			$submarks['###FILE_NAME_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.url' );
			$submarks['###FILE_NAME###'] = $itemF['url'];
			$submarks['###TYPE_IMAGE###'] = $this->conf['types.']['html.']['image'] != '' ? '<img src="'.$this->conf['iconsUploadPath'].$this->conf['types.']['html.']['image'].'" alt="'.$this->conf['types.']['html.']['title'].'" />': '';
			// Unknown file size
			$submarks['###SIZE###'] = $this->pi_getLL( 'tx_rtgfiles_files.size_unknown' );
			$subsubparts['###SIZE_BOX###'] = '';
			$wrappedSubpart['###FILE_LINK###'] = array( '<a href="'.$itemF['url'].'" title="'.$itemF['title'].'" target="_blank">', '</a>' );
			// Link to download script
			/*$params = array(
				$this->prefixId.'[cmd]' => 'DOWNLOAD',
				$this->prefixId.'[uid]' => intval( $itemF['uid'] )
			);*/
			$params = array(
				'eID' => 'tx_rtgfiles_download',
				$this->prefixId.'[uid]' => intval( $itemF['uid'] )
			);
			$link = '<a href="'.str_replace( '&', '&amp;', $this->pi_getPageLink( $this->id, '_self', $params ) ).'" title="'.$itemF['title'].'">';
			$wrappedSubpart['###FILE_DOWNLOAD###'] = array( $link, '</a>' );
		}
		else { // File
			$submarks['###FILE_NAME_TITLE###'] = $this->pi_getLL( 'tx_rtgfiles_files.file' );
			$submarks['###FILE_NAME###'] = $itemF['file'];
			$submarks['###TYPE_IMAGE###'] = $this->conf['types.'][$file[$extPos].'.']['image'] != '' ? '<img src="'.$this->conf['iconsUploadPath'].$this->conf['types.'][$file[$extPos].'.']['image'].'" alt="'.$this->conf['types.'][$file[$extPos].'.']['title'].'" />': '';
			$submarks['###SIZE###'] = $this->sizeReadable( filesize( $this->filesPath.$itemF['file'] ), 'kB', $this->conf['size_stdWrap'] );
			$wrappedSubpart['###FILE_LINK###'] = array( '<a href="'.$this->filesPath.$itemF['file'].'" title="'.$itemF['title'].'" target="_blank">', '</a>' );
			// Link to download script
			/*$params = array(
				$this->prefixId.'[cmd]' => 'DOWNLOAD',
				$this->prefixId.'[uid]' => intval( $itemF['uid'] )
			);*/
			$params = array(
				'eID' => 'tx_rtgfiles_download',
				$this->prefixId.'[uid]' => intval( $itemF['uid'] )
			);
			$link = '<a href="'.str_replace( '&', '&amp;', $this->pi_getPageLink( $this->id, '_self', $params ) ).'" title="'.$itemF['title'].'">';
			$wrappedSubpart['###FILE_DOWNLOAD###'] = array( $link, '</a>' );
		}

		// Link - view source
		if( isset( $this->conf['types.'][$file[$extPos].'.']['view'] ) ) {
			$submarks['###VIEW###'] = $this->pi_getLL( 'view' );
			$params = array(
				$this->prefixId.'[cmd]' => 'VIEW',
				$this->prefixId.'[uid]' => intval( $itemF['uid'] )
			);
			$link = '<a href="'.str_replace( '&', '&amp;', $this->pi_getPageLink( $this->pidView, '_top', $params ) ).'" title="'.$itemF['title'].'" target="_top">';
			$wrappedSubpart['###LINK_VIEW###'] = array( $link, '</a>' );
		}
		else {
			$subsubparts['###VIEW_BOX###'] = '';
		}
	}

	/**
	 * Navigation bar
	 *
	 * @param	array		$template: template subpart for navigation pages list
	 * @param	array		&$subparts:
	 * @param	array		&$marks:
	 * @param	array		&$wrappedSubpart:
	 * @param	array		&$searchParams:
	 * @param	int		$counter: items
	 * @param	int		$start: from
	 * @param	int		$pages:
	 * @return	bool	return
	 */
	function parseNavigation( $template, &$subparts, &$marks, &$wrappedSubpart, &$searchParams, $counter, $start, $pages ) {

		// Search params
		$params = array();

		// More than one pages
		if( $counter > $this->conf['limit'] ) {

			// Prev marks, link
			if( $start > 0 ) {
				$params = $searchParams;
				// Link to first page
				$marks['###PAGE_START###'] = '&lt;&lt;';
				$params[$this->prefixId.'[page]'] = 0;
				$link = '<a href="'.str_replace( '&', '&amp;', $this->pi_getPageLink( $this->id, '_self', $params ) ).'" target="_self" title="'.$marks['###PAGE_START###'].'">';
				$wrappedSubpart['###LINK_START###'] = array( $link, '</a>' );
				// Link to previous page
				$marks['###PAGE_PREV###'] = '&lt;';
				$params[$this->prefixId.'[page]'] = $start - 1;
				$link = '<a href="'.str_replace( '&', '&amp;', $this->pi_getPageLink( $this->id, '_self', $params ) ).'" target="_self" title="'.$marks['###PAGE_PREV###'].'">';
				$wrappedSubpart['###LINK_PREV###'] = array( $link, '</a>' );
			}
			else {
				$marks['###PAGE_START###'] = '&lt;&lt;';
				$wrappedSubpart['###LINK_START###'] = '';
				$marks['###PAGE_PREV###'] = '&lt;';
				$wrappedSubpart['###LINK_PREV###'] = '';
			}

			// Next marks, link
			if( $start < ( $pages - 1 ) ) {
				$params = $searchParams;
				// Link to last page
				$marks['###PAGE_END###'] = '&gt;&gt;';
				$params[$this->prefixId.'[page]'] = $pages - 1;
				$link = '<a href="'.str_replace( '&', '&amp;', $this->pi_getPageLink( $this->id, '_self', $params ) ).'" target="_self" title="'.$marks['###PAGE_END###'].'">';
				$wrappedSubpart['###LINK_END###'] = array( $link, '</a>' );
				// Link to next page
				$marks['###PAGE_NEXT###'] = '&gt;';
				$params[$this->prefixId.'[page]'] = $start + 1;
				$link = '<a href="'.str_replace( '&', '&amp;', $this->pi_getPageLink( $this->id, '_self', $params ) ).'" target="_self" title="'.$marks['###PAGE_NEXT###'].'">';
				$wrappedSubpart['###LINK_NEXT###'] = array( $link, '</a>' );
			}
			else {
				$marks['###PAGE_END###'] = '&gt;&gt;';
				$wrappedSubpart['###LINK_END###'] = '';
				$marks['###PAGE_NEXT###'] = '&gt;';
				$wrappedSubpart['###LINK_NEXT###'] = '';
			}

			// Pages list
			$p = intval( $this->page / 10 ) * 10;
			for( $i = 0; $p < $pages && $i < 10; $p++, $i++ ) {
				$pageNumber = $p;
				$marksP['###PAGE###'] = $pageNumber + 1;
				// Link to page
				if( $pageNumber != $this->page ) {
					$params = $searchParams;
					$params[$this->prefixId.'[page]'] = $pageNumber;
					$link = '<a href="'.str_replace( '&', '&amp;', $this->pi_getPageLink( $this->id, '_self', $params ) ).'" target="_self" title="'.$marksP['###PAGE###'].'">';
					$wrappedSubpartP['###LINK_PAGE###'] = array( $link, '</a>' );
				}
				else {
					$wrappedSubpartP['###LINK_PAGE###'] = array( '<strong>', '</strong>' );
				}
				$rows .= $this->cObj->substituteMarkerArrayCached( $template, $marksP, array(), $wrappedSubpartP );
			}

			// Complete all subparts
			$subparts['###PAGES_LIST###'] = $rows;
			return true;
		}
		return false;
	}
}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/rtg_files/pi1/class.tx_rtgfiles_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/rtg_files/pi1/class.tx_rtgfiles_pi1.php']);
}

?>