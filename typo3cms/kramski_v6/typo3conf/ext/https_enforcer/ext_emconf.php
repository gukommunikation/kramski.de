<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "https_enforcer".
 *
 * Auto generated 17-10-2014 14:09
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Page HTTP/HTTPS Enforcer',
	'description' => 'Adds a page record option to enforce HTTP/HTTPS access based on server port and environment vars. Can handle shared secure domains and SSL-proxys. Compatible with the RealURL extension.',
	'category' => 'be',
	'version' => '1.0.15',
	'state' => 'stable',
	'uploadfolder' => true,
	'createDirs' => '',
	'clearcacheonload' => true,
	'author' => 'Thomas Vogt',
	'author_email' => 'tvogt@gv-bayern.de',
	'author_company' => '',
	'constraints' => 
	array (
		'depends' => 
		array (
			'typo3' => '6.0.0-6.2.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

