<?php

$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
	0 => 'LLL:EXT:dce/Resources/Private/Language/locallang_db.xml:tx_dce_domain_model_dce_long',
	1 => '--div--'
);



	///////////////////////////////////////////////////// uid: 12 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid12',
	'Überschrift grün'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid12'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid12'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>General</sheetTitle></TCEforms><type>array</type><el><settings.headline><TCEforms><label>Überschrift</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.headline></el></ROOT></sheet0></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid12']['showitem'] = 'CType;;dce_palette_12;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_12']['showitem'] = 'sys_language_uid, l18n_parent, colPos, spaceBefore, spaceAfter, section_frame, sectionIndex';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid12 {
	    icon = ../uploads/tx_dce/dce_trennlinie_01.gif
	    title = Überschrift grün
	    description = Überschrift Gesundheitsförderung
	    tt_content_defValues {
	        CType = dce_dceuid12
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid12)
');




	///////////////////////////////////////////////////// uid: 11 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid11',
	'Linkbox mit Linien und Pfeilen'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid11'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid11'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>General</sheetTitle></TCEforms><type>array</type><el><settings.linktext><TCEforms><label>Linktexte (ein Link pro Zeile)</label><config><type>text</type><rows>5</rows><cols>30</cols><eval>trim</eval></config><defaultExtras>richtext[*]:rte_transform[mode=ts_css]</defaultExtras></TCEforms></settings.linktext><settings.hidearrows><TCEforms><label>Pfeile verbergen</label><config><type>check</type><default>0</default></config></TCEforms></settings.hidearrows></el></ROOT></sheet0></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid11']['showitem'] = 'CType;;dce_palette_11;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_11']['showitem'] = 'sys_language_uid, l18n_parent, colPos, spaceBefore, spaceAfter, section_frame, sectionIndex';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid11 {
	    icon = gfx/c_wiz/bullet_list.gif
	    title = Linkbox mit Linien und Pfeilen
	    description = 
	    tt_content_defValues {
	        CType = dce_dceuid11
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid11)
');




	///////////////////////////////////////////////////// uid: 9 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid9',
	'Blauer Balken mit Überschrift und Pfeilen'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid9'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid9'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>General</sheetTitle></TCEforms><type>array</type><el><settings.headline><TCEforms><label>Überschrift</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.headline></el></ROOT></sheet0></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid9']['showitem'] = 'CType;;dce_palette_9;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_9']['showitem'] = 'sys_language_uid, l18n_parent, colPos, spaceBefore, spaceAfter, section_frame, sectionIndex';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid9 {
	    icon = ../uploads/tx_dce/dce_trennlinie.gif
	    title = Blauer Balken mit Überschrift und Pfeilen
	    description = Blauer Balken mit weißem Text
	    tt_content_defValues {
	        CType = dce_dceuid9
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid9)
');




	///////////////////////////////////////////////////// uid: 6 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid6',
	'Iconbox'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid6'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid6'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>General</sheetTitle></TCEforms><type>array</type><el><settings.icon><TCEforms><label>Icon</label><config><type>group</type><internal_type>file</internal_type><allowed>jpg,jpeg,png,gif</allowed><size>1</size><minitems>0</minitems><maxitems>1</maxitems><uploadfolder>uploads/pics</uploadfolder><show_thumbs>1</show_thumbs></config></TCEforms></settings.icon><settings.headline><TCEforms><label>Überschrift</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.headline><settings.bodytext><TCEforms><label>Text</label><config><type>text</type><rows>5</rows><cols>30</cols><eval>trim</eval></config><defaultExtras>richtext[]:rte_transform[mode=ts_css]</defaultExtras></TCEforms></settings.bodytext></el></ROOT></sheet0></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid6']['showitem'] = 'CType;;dce_palette_6;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_6']['showitem'] = 'sys_language_uid,l18n_parent,colPos,spaceBefore,spaceAfter,section_frame,sectionIndex,inherit';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid6 {
	    icon = ../uploads/tx_dce/dce_iconbox.gif
	    title = Iconbox
	    description = Iconbox
	    tt_content_defValues {
	        CType = dce_dceuid6
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid6)
');




	///////////////////////////////////////////////////// uid: 5 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid5',
	'Linktext'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid5'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid5'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>Verlinkung</sheetTitle></TCEforms><type>array</type><el><settings.linktext><TCEforms><label>Linktext</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.linktext><settings.link><TCEforms><label>Link</label><config><type>input</type><size>30</size><eval>trim</eval><wizards><_PADDING>2</_PADDING><link><type>popup</type><title>Link</title><icon>link_popup.gif</icon><script>browse_links.php?mode=wizard</script><params><!--<blindLinkOptions>page,file,folder,url,spec</blindLinkOptions>--></params><JSopenParams>height=500,width=500,status=0,menubar=0,scrollbars=1</JSopenParams></link></wizards></config></TCEforms></settings.link></el></ROOT></sheet0><sheet1><ROOT><TCEforms><sheetTitle>Farbschema</sheetTitle></TCEforms><type>array</type><el><settings.black><TCEforms><label>schwarz</label><config><type>check</type><default>0</default></config></TCEforms></settings.black><settings.white><TCEforms><label>weiß</label><config><type>check</type><default>0</default></config></TCEforms></settings.white></el></ROOT></sheet1></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid5']['showitem'] = 'CType;;dce_palette_5;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_5']['showitem'] = 'sys_language_uid,l18n_parent,colPos,spaceBefore,spaceAfter,section_frame,sectionIndex,inherit';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid5 {
	    icon = ../uploads/tx_dce/dce_linktext_01.gif
	    title = Linktext
	    description = Linktext
	    tt_content_defValues {
	        CType = dce_dceuid5
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid5)
');




	///////////////////////////////////////////////////// uid: 4 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid4',
	'Bild/Text Box'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid4'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid4'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>Bilder</sheetTitle></TCEforms><type>array</type><el><settings.image><TCEforms><label>Bilder</label><config><type>group</type><internal_type>file</internal_type><allowed>jpg,jpeg,png,gif</allowed><size>5</size><minitems>0</minitems><maxitems>10</maxitems><uploadfolder>uploads/pics</uploadfolder><show_thumbs>1</show_thumbs></config></TCEforms></settings.image><settings.slideshow><TCEforms><label>Slideshow aktivieren</label><config><type>check</type><default>0</default></config></TCEforms></settings.slideshow></el></ROOT></sheet0><sheet1><ROOT><TCEforms><sheetTitle>Inhalte</sheetTitle></TCEforms><type>array</type><el><settings.headline><TCEforms><label>Überschrift</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.headline><settings.subheadline><TCEforms><label>Subheadline</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.subheadline><settings.bodytext><TCEforms><label>Text</label><config><type>text</type><rows>5</rows><cols>30</cols><eval>trim</eval></config><defaultExtras>richtext[]:rte_transform[mode=ts_css]</defaultExtras></TCEforms></settings.bodytext></el></ROOT></sheet1><sheet2><ROOT><TCEforms><sheetTitle>Ausrichtung</sheetTitle></TCEforms><type>array</type><el><settings.imageleft><TCEforms><label>Bild links, Text rechts</label><config><type>check</type><default>0</default></config></TCEforms></settings.imageleft><settings.imageright><TCEforms><label>Bild rechts, Text links</label><config><type>check</type><default>0</default></config></TCEforms></settings.imageright></el></ROOT></sheet2><sheet3><ROOT><TCEforms><sheetTitle>Weitere Informationen</sheetTitle></TCEforms><type>array</type><el><settings.morelinktitle><TCEforms><label>Mehr Link Titel</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.morelinktitle><settings.morelinkid><TCEforms><label>Zeige Element mit folgender ID (z.B. 199)</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.morelinkid></el></ROOT></sheet3></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid4']['showitem'] = 'CType;;dce_palette_4;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_4']['showitem'] = 'sys_language_uid,l18n_parent,colPos,spaceBefore,spaceAfter,section_frame,sectionIndex,inherit';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid4 {
	    icon = gfx/c_wiz/text_image_right.gif
	    title = Bild/Text Box
	    description = Bild/Text Box
	    tt_content_defValues {
	        CType = dce_dceuid4
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid4)
');




	///////////////////////////////////////////////////// uid: 3 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid3',
	'Historieneintrag'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid3'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid3'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>General</sheetTitle></TCEforms><type>array</type><el><settings.date><TCEforms><label>Datum</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.date><settings.info><TCEforms><label>Information</label><config><type>text</type><rows>5</rows><cols>30</cols><eval>trim</eval></config><defaultExtras>richtext[]:rte_transform[mode=ts_css]</defaultExtras></TCEforms></settings.info></el></ROOT></sheet0></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid3']['showitem'] = 'CType;;dce_palette_3;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_3']['showitem'] = 'sys_language_uid,l18n_parent,colPos,spaceBefore,spaceAfter,section_frame,sectionIndex,inherit';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid3 {
	    icon = ../uploads/tx_dce/dce_history.gif
	    title = Historieneintrag
	    description = Historien Element
	    tt_content_defValues {
	        CType = dce_dceuid3
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid3)
');




	///////////////////////////////////////////////////// uid: 2 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid2',
	'Buttonbox mit Text'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid2'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid2'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>Bilder</sheetTitle></TCEforms><type>array</type><el><settings.stdimage><TCEforms><label>Standard Bild</label><config><type>group</type><internal_type>file</internal_type><allowed>jpg,jpeg,png,gif</allowed><size>1</size><minitems>0</minitems><maxitems>1</maxitems><uploadfolder>uploads/pics</uploadfolder><show_thumbs>1</show_thumbs></config></TCEforms></settings.stdimage><settings.hoverimage><TCEforms><label>Hover Bild</label><config><type>group</type><internal_type>file</internal_type><allowed>jpg,jpeg,png,gif</allowed><size>1</size><minitems>0</minitems><maxitems>1</maxitems><uploadfolder>uploads/pics</uploadfolder><show_thumbs>1</show_thumbs></config></TCEforms></settings.hoverimage></el></ROOT></sheet0><sheet1><ROOT><TCEforms><sheetTitle>Verlinkung</sheetTitle></TCEforms><type>array</type><el><settings.linktext><TCEforms><label>Linktext</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.linktext><settings.link><TCEforms><label>Link</label><config><type>input</type><size>30</size><eval>trim</eval><wizards><_PADDING>2</_PADDING><link><type>popup</type><title>Link</title><icon>link_popup.gif</icon><script>browse_links.php?mode=wizard</script><params><!--<blindLinkOptions>page,file,folder,url,spec</blindLinkOptions>--></params><JSopenParams>height=500,width=500,status=0,menubar=0,scrollbars=1</JSopenParams></link></wizards></config></TCEforms></settings.link></el></ROOT></sheet1><sheet2><ROOT><TCEforms><sheetTitle>Inhalte</sheetTitle></TCEforms><type>array</type><el><settings.headline1><TCEforms><label>Überschrift grau/klein</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.headline1><settings.headline2><TCEforms><label>Überschrift blau/groß</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.headline2><settings.bodytext><TCEforms><label>Text</label><config><type>text</type><rows>5</rows><cols>30</cols><eval>trim</eval></config><defaultExtras>richtext[]:rte_transform[mode=ts_css]</defaultExtras></TCEforms></settings.bodytext><settings.imageleft><TCEforms><label>Bild links, Text rechts</label><config><type>check</type><default>0</default></config></TCEforms></settings.imageleft><settings.bluebg><TCEforms><label>blauer Hintergrund</label><config><type>check</type><default>0</default></config></TCEforms></settings.bluebg><settings.textleft><TCEforms><label>Texte linksbündig</label><config><type>check</type><default>0</default></config></TCEforms></settings.textleft></el></ROOT></sheet2></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid2']['showitem'] = 'CType;;dce_palette_2;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_2']['showitem'] = 'sys_language_uid,l18n_parent,colPos,spaceBefore,spaceAfter,section_frame,sectionIndex,inherit';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid2 {
	    icon = ../uploads/tx_dce/dce_buttonbox.gif
	    title = Buttonbox mit Text
	    description = Buttonbox mit Text
	    tt_content_defValues {
	        CType = dce_dceuid2
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid2)
');




	///////////////////////////////////////////////////// uid: 1 ///
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'ArminVieweg.dce',
	'dceuid1',
	'Großes Bild mit Text (im Bild)'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['dce_dceuid1'] = 'pi_flexform';
$GLOBALS['TCA']['tt_content']['columns']['pi_flexform']['config']['ds'][',dce_dceuid1'] = '<T3DataStructure><meta><langDisable>1</langDisable></meta><sheets><sheet0><ROOT><TCEforms><sheetTitle>Inhalte</sheetTitle></TCEforms><type>array</type><el><settings.image><TCEforms><label>Bild</label><config><type>group</type><internal_type>file</internal_type><allowed>jpg,jpeg,png,gif</allowed><size>1</size><minitems>0</minitems><maxitems>1</maxitems><uploadfolder>uploads/pics</uploadfolder><show_thumbs>1</show_thumbs></config></TCEforms></settings.image><settings.imagemobile><TCEforms><label>Bild mobil</label><config><type>group</type><internal_type>file</internal_type><allowed>jpg,jpeg,png,gif</allowed><size>1</size><minitems>0</minitems><maxitems>1</maxitems><uploadfolder>uploads/pics</uploadfolder><show_thumbs>1</show_thumbs></config></TCEforms></settings.imagemobile><settings.headline><TCEforms><label>Überschrift</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.headline><settings.firstblue><TCEforms><label>1. Zeile der Überschrift blau</label><config><type>check</type><default>0</default></config></TCEforms></settings.firstblue><settings.headlineindividual><TCEforms><label>Überschriften individuell</label><config><type>text</type><rows>5</rows><cols>30</cols><eval>trim</eval></config><defaultExtras>richtext[]:rte_transform[mode=ts_css]</defaultExtras></TCEforms></settings.headlineindividual><settings.hidemobile><TCEforms><label>Überschriften individuell für Smartphones ausblenden</label><config><type>check</type><default>0</default></config></TCEforms></settings.hidemobile><settings.bodytext><TCEforms><label>Text (wird mobil ausgeblendet)</label><config><type>text</type><rows>5</rows><cols>30</cols><eval>trim</eval></config><defaultExtras>richtext[]:rte_transform[mode=ts_css]</defaultExtras></TCEforms></settings.bodytext></el></ROOT></sheet0><sheet1><ROOT><TCEforms><sheetTitle>Verlinkung</sheetTitle></TCEforms><type>array</type><el><settings.linktext><TCEforms><label>Linktext</label><config><type>input</type><size>30</size><eval>trim</eval></config></TCEforms></settings.linktext><settings.link><TCEforms><label>Link</label><config><type>input</type><size>30</size><eval>trim</eval><wizards><_PADDING>2</_PADDING><link><type>popup</type><title>Link</title><icon>link_popup.gif</icon><script>browse_links.php?mode=wizard</script><params><!--<blindLinkOptions>page,file,folder,url,spec</blindLinkOptions>--></params><JSopenParams>height=500,width=500,status=0,menubar=0,scrollbars=1</JSopenParams></link></wizards></config></TCEforms></settings.link></el></ROOT></sheet1><sheet2><ROOT><TCEforms><sheetTitle>Farbschema</sheetTitle></TCEforms><type>array</type><el><settings.black><TCEforms><label>Farbe schwarz</label><config><type>check</type><default>0</default></config></TCEforms></settings.black><settings.whiteblue><TCEforms><label>Farbe weiß/blau</label><config><type>check</type><default>0</default></config></TCEforms></settings.whiteblue><settings.whitegreen><TCEforms><label>Farbe weiß/grün</label><config><type>check</type><default>0</default></config></TCEforms></settings.whitegreen></el></ROOT></sheet2><sheet3><ROOT><TCEforms><sheetTitle>Ausrichtung</sheetTitle></TCEforms><type>array</type><el><settings.textlinks><TCEforms><label>Textblock links</label><config><type>check</type><default>0</default></config></TCEforms></settings.textlinks><settings.textrechts><TCEforms><label>Textblock rechts</label><config><type>check</type><default>0</default></config></TCEforms></settings.textrechts><settings.inherit><TCEforms><label>Inhalte einrücken</label><config><type>check</type><default>0</default></config></TCEforms></settings.inherit></el></ROOT></sheet3><sheet4><ROOT><TCEforms><sheetTitle>Höhe</sheetTitle></TCEforms><type>array</type><el><settings.height400><TCEforms><label>400px Höhe</label><config><type>check</type><default>0</default></config></TCEforms></settings.height400><settings.height320><TCEforms><label>320px Höhe</label><config><type>check</type><default>0</default></config></TCEforms></settings.height320><settings.height250><TCEforms><label>250px Höhe</label><config><type>check</type><default>0</default></config></TCEforms></settings.height250></el></ROOT></sheet4></sheets></T3DataStructure>';
$GLOBALS['TCA']['tt_content']['types']['dce_dceuid1']['showitem'] = 'CType;;dce_palette_1;button;1-1-1,pi_flexform; ;,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.visibility;visibility, --palette--;LLL:EXT:cms/locallang_ttc.xml:palette.access;access,--div--;LLL:EXT:cms/locallang_ttc.xml:tabs.extended,tx_gridelements_container,tx_gridelements_columns';
$GLOBALS['TCA']['tt_content']['palettes']['dce_palette_1']['showitem'] = 'sys_language_uid,l18n_parent,colPos,spaceBefore,spaceAfter,section_frame,sectionIndex,inherit';


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
	mod.wizards.newContentElement.wizardItems.common.elements.dce_dceuid1 {
	    icon = ../uploads/tx_dce/dce_headerbox.gif
	    title = Großes Bild mit Text (im Bild)
	    description = Großes Bild mit Text (im Bild)
	    tt_content_defValues {
	        CType = dce_dceuid1
	    }
	}
	    mod.wizards.newContentElement.wizardItems.common.show := addToList(dce_dceuid1)
');




// global definitions
$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = array(
	0 => 'LLL:EXT:dce/Resources/Private/Language/locallang_db.xml:tx_dce_domain_model_dce.miscellaneous',
	1 => '--div--'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('mod.wizards.newContentElement.wizardItems.dce.header = LLL:EXT:dce/Resources/Private/Language/locallang_db.xml:tx_dce_domain_model_dce_long');