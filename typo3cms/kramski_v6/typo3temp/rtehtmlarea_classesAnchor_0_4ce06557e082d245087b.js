HTMLArea.classesAnchorSetup = [ 
 { 
name : "external-link"
,type : "url"
,addIconAfterLink : false
,altText : '\u00d6ffnet\u0020diese\u0020Seite.'
,titleText : '\u00d6ffnet\u0020diese\u0020Seite.'
}
, { 
name : "external-link-new-window"
,type : "url"
,addIconAfterLink : false
,altText : '\u00d6ffnet\u0020diese\u0020Seite\u0020in\u0020neuem\u0020Fenster.'
,titleText : '\u00d6ffnet\u0020diese\u0020Seite\u0020in\u0020neuem\u0020Fenster.'
}
, { 
name : "internal-link"
,type : "page"
,addIconAfterLink : false
,altText : '\u00d6ffnet\u0020diese\u0020Seite.'
,titleText : '\u00d6ffnet\u0020diese\u0020Seite.'
}
, { 
name : "internal-link-new-window"
,type : "page"
,addIconAfterLink : false
,altText : '\u00d6ffnet\u0020diese\u0020Seite\u0020in\u0020neuem\u0020Fenster.'
,titleText : '\u00d6ffnet\u0020diese\u0020Seite\u0020in\u0020neuem\u0020Fenster.'
}
, { 
name : "download"
,type : "file"
,addIconAfterLink : false
,altText : '\u00d6ffnet\u0020die\u0020Datei.'
,titleText : '\u00d6ffnet\u0020die\u0020Datei.'
}
, { 
name : "mail"
,type : "mail"
,addIconAfterLink : false
,altText : 'Eine\u0020E-Mail\u0020an\u0020diese\u0020Adresse\u0020senden.'
,titleText : 'Eine\u0020E-Mail\u0020an\u0020diese\u0020Adresse\u0020senden.'
}
];
