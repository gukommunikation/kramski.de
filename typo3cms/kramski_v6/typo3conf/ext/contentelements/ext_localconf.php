<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

Tx_Extbase_Utility_Extension::configurePlugin (
    // unique plugin name
    $_EXTKEY,
    'ContentRenderer',
    // accessible controller-action-combinations
    array('Elements' => ''),
    // non-cachable controller-action-combinations (they must already be enabled)
    array('Elements' => '')
);

t3lib_extMgm::addTyposcript($_EXTKEY,'setup',
    '

	',
    true
);

?>