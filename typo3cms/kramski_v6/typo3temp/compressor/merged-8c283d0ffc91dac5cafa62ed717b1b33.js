
/*! HTML5 Shiv vpre3.6 | @afarkas @jdalton @jon_neal @rem | MIT/GPL2 Licensed */
;(function(window, document) {

  /** Preset options */
  var options = window.html5 || {};

  /** Used to skip problem elements */
  var reSkip = /^<|^(?:button|form|map|select|textarea|object|iframe|option|optgroup)$/i;

  /** Not all elements can be cloned in IE (this list can be shortend) **/
  var saveClones = /^<|^(?:a|b|button|code|div|fieldset|form|h1|h2|h3|h4|h5|h6|i|iframe|img|input|label|li|link|ol|option|p|param|q|script|select|span|strong|style|table|tbody|td|textarea|tfoot|th|thead|tr|ul)$/i;

  /** Detect whether the browser supports default html5 styles */
  var supportsHtml5Styles;

  /** Detect whether the browser supports unknown elements */
  var supportsUnknownElements;

  (function() {
    var a = document.createElement('a');

    a.innerHTML = '<xyz></xyz>';

    //if the hidden property is implemented we can assume, that the browser supports HTML5 Styles | this fails in Chrome 8
    supportsHtml5Styles = ('hidden' in a);
    //if we are part of Modernizr, we do an additional test to solve the Chrome 8 fail
    if(supportsHtml5Styles && typeof injectElementWithStyles == 'function'){
        injectElementWithStyles('#modernizr{}', function(node){
            node.hidden = true;
            supportsHtml5Styles = (window.getComputedStyle ?
                  getComputedStyle(node, null) :
                  node.currentStyle).display == 'none';
        });
    }

    supportsUnknownElements = a.childNodes.length == 1 || (function() {
      // assign a false positive if unable to shiv
      try {
        (document.createElement)('a');
      } catch(e) {
        return true;
      }
      var frag = document.createDocumentFragment();
      return (
        typeof frag.cloneNode == 'undefined' ||
        typeof frag.createDocumentFragment == 'undefined' ||
        typeof frag.createElement == 'undefined'
      );
    }());

  }());

  /*--------------------------------------------------------------------------*/

  /**
   * Creates a style sheet with the given CSS text and adds it to the document.
   * @private
   * @param {Document} ownerDocument The document.
   * @param {String} cssText The CSS text.
   * @returns {StyleSheet} The style element.
   */
  function addStyleSheet(ownerDocument, cssText) {
    var p = ownerDocument.createElement('p'),
        parent = ownerDocument.getElementsByTagName('head')[0] || ownerDocument.documentElement;

    p.innerHTML = 'x<style>' + cssText + '</style>';
    return parent.insertBefore(p.lastChild, parent.firstChild);
  }

  /**
   * Returns the value of `html5.elements` as an array.
   * @private
   * @returns {Array} An array of shived element node names.
   */
  function getElements() {
    var elements = html5.elements;
    return typeof elements == 'string' ? elements.split(' ') : elements;
  }

  /**
   * Shivs the `createElement` and `createDocumentFragment` methods of the document.
   * @private
   * @param {Document|DocumentFragment} ownerDocument The document.
   */
  function shivMethods(ownerDocument) {
    var cache = {},
        docCreateElement = ownerDocument.createElement,
        docCreateFragment = ownerDocument.createDocumentFragment,
        frag = docCreateFragment();

    ownerDocument.createElement = function(nodeName) {
      //abort shiv
      if(!html5.shivMethods){
          return docCreateElement(nodeName);
      }

      var node;

      if(cache[nodeName]){
          node = cache[nodeName].cloneNode();
      } else if(saveClones.test(nodeName)){
           node = (cache[nodeName] = docCreateElement(nodeName)).cloneNode();
      } else {
          node = docCreateElement(nodeName);
      }

      // Avoid adding some elements to fragments in IE < 9 because
      // * Attributes like `name` or `type` cannot be set/changed once an element
      //   is inserted into a document/fragment
      // * Link elements with `src` attributes that are inaccessible, as with
      //   a 403 response, will cause the tab/window to crash
      // * Script elements appended to fragments will execute when their `src`
      //   or `text` property is set
      return node.canHaveChildren && !reSkip.test(nodeName) ? frag.appendChild(node) : node;
    };

    ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
      'var n=f.cloneNode(),c=n.createElement;' +
      'h.shivMethods&&(' +
        // unroll the `createElement` calls
        getElements().join().replace(/\w+/g, function(nodeName) {
          docCreateElement(nodeName);
          frag.createElement(nodeName);
          return 'c("' + nodeName + '")';
        }) +
      ');return n}'
    )(html5, frag);
  }

  /*--------------------------------------------------------------------------*/

  /**
   * Shivs the given document.
   * @memberOf html5
   * @param {Document} ownerDocument The document to shiv.
   * @returns {Document} The shived document.
   */
  function shivDocument(ownerDocument) {
    var shived;
    if (ownerDocument.documentShived) {
      return ownerDocument;
    }
    if (html5.shivCSS && !supportsHtml5Styles) {
      shived = !!addStyleSheet(ownerDocument,
        // corrects block display not defined in IE6/7/8/9
        'article,aside,details,figcaption,figure,footer,header,hgroup,nav,section{display:block}' +
        // corrects audio display not defined in IE6/7/8/9
        'audio{display:none}' +
        // corrects canvas and video display not defined in IE6/7/8/9
        'canvas,video{display:inline-block;*display:inline;*zoom:1}' +
        // corrects 'hidden' attribute and audio[controls] display not present in IE7/8/9
        '[hidden]{display:none}audio[controls]{display:inline-block;*display:inline;*zoom:1}' +
        // adds styling not present in IE6/7/8/9
        'mark{background:#FF0;color:#000}'
      );
    }
    if (!supportsUnknownElements) {
      shived = !shivMethods(ownerDocument);
    }
    if (shived) {
      ownerDocument.documentShived = shived;
    }
    return ownerDocument;
  }

  /*--------------------------------------------------------------------------*/

  /**
   * The `html5` object is exposed so that more elements can be shived and
   * existing shiving can be detected on iframes.
   * @type Object
   * @example
   *
   * // options can be changed before the script is included
   * html5 = { 'elements': 'mark section', 'shivCSS': false, 'shivMethods': false };
   */
  var html5 = {

    /**
     * An array or space separated string of node names of the elements to shiv.
     * @memberOf html5
     * @type Array|String
     */
    'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video',

    /**
     * A flag to indicate that the HTML5 style sheet should be inserted.
     * @memberOf html5
     * @type Boolean
     */
    'shivCSS': !(options.shivCSS === false),

    /**
     * A flag to indicate that the document's `createElement` and `createDocumentFragment`
     * methods should be overwritten.
     * @memberOf html5
     * @type Boolean
     */
    'shivMethods': !(options.shivMethods === false),

    /**
     * A string to describe the type of `html5` object ("default" or "default print").
     * @memberOf html5
     * @type String
     */
    'type': 'default',

    // shivs the document according to the specified `html5` object options
    'shivDocument': shivDocument
  };

  /*--------------------------------------------------------------------------*/

  // expose html5
  window.html5 = html5;

  // shiv the document
  shivDocument(document);

}(this, document));
/*
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2013 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.9.0
 *
 */
 
(function($, window, document, undefined) {
    var $window = $(window);

    $.fn.lazyload = function(options) {
        var elements = this;
        var $container;
        var settings = {
            threshold       : 0,
            failure_limit   : 0,
            event           : "scroll",
            effect          : "show",
            container       : window,
            data_attribute  : "original",
            skip_invisible  : true,
            appear          : null,
            load            : null,
            placeholder     : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
        };

        function update() {
            var counter = 0;
      
            elements.each(function() {
                var $this = $(this);
                if (settings.skip_invisible && !$this.is(":visible")) {
                    return;
                }
                if ($.abovethetop(this, settings) ||
                    $.leftofbegin(this, settings)) {
                        /* Nothing. */
                } else if (!$.belowthefold(this, settings) &&
                    !$.rightoffold(this, settings)) {
                        $this.trigger("appear");
                        /* if we found an image we'll load, reset the counter */
                        counter = 0;
                } else {
                    if (++counter > settings.failure_limit) {
                        return false;
                    }
                }
            });

        }

        if(options) {
            /* Maintain BC for a couple of versions. */
            if (undefined !== options.failurelimit) {
                options.failure_limit = options.failurelimit;
                delete options.failurelimit;
            }
            if (undefined !== options.effectspeed) {
                options.effect_speed = options.effectspeed;
                delete options.effectspeed;
            }

            $.extend(settings, options);
        }

        /* Cache container as jQuery as object. */
        $container = (settings.container === undefined ||
                      settings.container === window) ? $window : $(settings.container);

        /* Fire one scroll event per scroll. Not one scroll event per image. */
        if (0 === settings.event.indexOf("scroll")) {
            $container.bind(settings.event, function() {
                return update();
            });
        }

        this.each(function() {
            var self = this;
            var $self = $(self);

            self.loaded = false;

            /* If no src attribute given use data:uri. */
            if ($self.attr("src") === undefined || $self.attr("src") === false) {
                $self.attr("src", settings.placeholder);
            }
            
            /* When appear is triggered load original image. */
            $self.one("appear", function() {
                if (!this.loaded) {
                    if (settings.appear) {
                        var elements_left = elements.length;
                        settings.appear.call(self, elements_left, settings);
                    }
                    $("<img />")
                        .bind("load", function() {
                            var original = $self.data(settings.data_attribute);
                            $self.hide();
                            if ($self.is("img")) {
                                $self.attr("src", original);
                            } else {
                                $self.css("background-image", "url('" + original + "')");
                            }
                            $self[settings.effect](settings.effect_speed);
                            
                            self.loaded = true;

                            /* Remove image from array so it is not looped next time. */
                            var temp = $.grep(elements, function(element) {
                                return !element.loaded;
                            });
                            elements = $(temp);

                            if (settings.load) {
                                var elements_left = elements.length;
                                settings.load.call(self, elements_left, settings);
                            }
                        })
                        .attr("src", $self.data(settings.data_attribute));
                }
            });

            /* When wanted event is triggered load original image */
            /* by triggering appear.                              */
            if (0 !== settings.event.indexOf("scroll")) {
                $self.bind(settings.event, function() {
                    if (!self.loaded) {
                        $self.trigger("appear");
                    }
                });
            }
        });

        /* Check if something appears when window is resized. */
        $window.bind("resize", function() {
            update();
        });
              
        /* With IOS5 force loading images when navigating with back button. */
        /* Non optimal workaround. */
        if ((/iphone|ipod|ipad.*os 5/gi).test(navigator.appVersion)) {
            $window.bind("pageshow", function(event) {
                if (event.originalEvent && event.originalEvent.persisted) {
                    elements.each(function() {
                        $(this).trigger("appear");
                    });
                }
            });
        }

        /* Force initial check if images should appear. */
        $(document).ready(function() {
            update();
        });
        
        return this;
    };

    /* Convenience methods in jQuery namespace.           */
    /* Use as  $.belowthefold(element, {threshold : 100, container : window}) */

    $.belowthefold = function(element, settings) {
        var fold;
        
        if (settings.container === undefined || settings.container === window) {
            fold = (window.innerHeight ? window.innerHeight : $window.height()) + $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top + $(settings.container).height();
        }

        return fold <= $(element).offset().top - settings.threshold;
    };
    
    $.rightoffold = function(element, settings) {
        var fold;

        if (settings.container === undefined || settings.container === window) {
            fold = $window.width() + $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left + $(settings.container).width();
        }

        return fold <= $(element).offset().left - settings.threshold;
    };
        
    $.abovethetop = function(element, settings) {
        var fold;
        
        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top;
        }

        return fold >= $(element).offset().top + settings.threshold  + $(element).height();
    };
    
    $.leftofbegin = function(element, settings) {
        var fold;
        
        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left;
        }

        return fold >= $(element).offset().left + settings.threshold + $(element).width();
    };

    $.inviewport = function(element, settings) {
         return !$.rightoffold(element, settings) && !$.leftofbegin(element, settings) &&
                !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
     };

    /* Custom selectors for your convenience.   */
    /* Use as $("img:below-the-fold").something() or */
    /* $("img").filter(":below-the-fold").something() which is faster */

    $.extend($.expr[":"], {
        "below-the-fold" : function(a) { return $.belowthefold(a, {threshold : 0}); },
        "above-the-top"  : function(a) { return !$.belowthefold(a, {threshold : 0}); },
        "right-of-screen": function(a) { return $.rightoffold(a, {threshold : 0}); },
        "left-of-screen" : function(a) { return !$.rightoffold(a, {threshold : 0}); },
        "in-viewport"    : function(a) { return $.inviewport(a, {threshold : 0}); },
        /* Maintain BC for couple of versions. */
        "above-the-fold" : function(a) { return !$.belowthefold(a, {threshold : 0}); },
        "right-of-fold"  : function(a) { return $.rightoffold(a, {threshold : 0}); },
        "left-of-fold"   : function(a) { return !$.rightoffold(a, {threshold : 0}); }
    });

})(jQuery, window, document);

// jquery on document ready
jQuery(document).ready(function() {
  delinkTel();
  scrollTop();
  hoverbox();
  headerSlideshow();
  paddingImagetextbox();
  // $('img.lazy').removeAttr("src").removeAttr("alt");
  // $("img.lazy").lazyload();
  
  getThirdlevelmenu();
  resizeThirdlevelmenu();
  imagetextboxDetail();
  mobilemenu();
  buttonboxHeight();
  
  slideCol6();
  navCol6nav3items();
  navCol6nav1item();  
  
  centerHeader();
  jQuery(".fancybox").fancybox();  
  
  // change class of first catmenu element
  $('.news-catmenu li').first().addClass('act');
  $('.news-catmenu li:not(:eq(0))').each(function() {
    if($(this).hasClass('act')) {
      $('.news-catmenu li').first().removeClass('act');
    }
  });
  
  var searchwidth = $(window).width() - 58;
  $('.search-searchfield').width(searchwidth);
  $('.buttonbox.noimageleft').each(function() {
    element = $(this);
    element.find('.standard').height('auto');
    element.find('.hover').height('auto');
    var height = element.find('.standard').height();
    var width = element.find('.standard').width();
    var ratio = width / height;
    resizeButtonboxNoimageleft(element,ratio);
  });
  
  // hide border if downloadlist is empty
  $('.tx-rtgfiles-pi1').each(function() {
    if(!($(this).find('.downloadlist-wrap').length)) {
      $(this).css('border','none');
    }
  });
  
  // set csc-default margin 0 for only header
  $('header').each(function() {
    if(($(this).parent().hasClass('csc-default')) && !$(this).next().html()) {
      $(this).parent().css('margin',0);
    }
  });
  $('.blue-container header').each(function() {
    $(this).parent().css('margin-bottom','1.5em');
  });
}); 

// jquery window on resize
jQuery(window).resize(function() {
  setImageWidth();
  paddingImagetextbox();
  resizeImagetextbox();
  imageTextboxOrder();
  iconbox();
  
  slideCol6();
  //navCol6nav3items();
 // navCol6nav1item();
  
  
  if ($(window).width() >= 624) {
  $('#mobilemenu').hide();
  } else {
    if($('#mobilemenu-button').hasClass('up')) {
    $('#mobilemenu').show();
    }
  }
  var searchwidth = $(window).width() - 78;
  $('.search-searchfield').width(searchwidth);
  hoverboxResize();
   buttonboxHeight();
   $('.buttonbox.noimageleft').each(function() {
  element = $(this);
  element.find('.standard').height('auto');
  element.find('.hover').height('auto');
  var height = element.find('.standard').height();
  var width = element.find('.standard').width();
  var ratio = width / height;
  resizeButtonboxNoimageleft(element,ratio);
   });
  resizeThirdlevelmenu();
  centerHeader();
 
  resizeNewsGrids();
});

// jquery window on load functions
jQuery(window).load(function(){  
  
  imageTextboxOrder();
  imagetextboxSlideshow();
  setImageWidth();  
  resizeImagetextbox();
  hoverboxResize();
  buttonboxHeight();
  iconbox();  
  
  $('.iconbox-inner').each(function() {
    var iconbox = $(this);
    iconbox.click(function() {
    if ($(window).width() <= 624) {
      iconbox.find('.iconbox-bodytext').toggle();
      iconbox.toggleClass('up');
    }
    });
  });
  splitheader();
  $('.showafterload').css('visibility','visible');
  centerHeader();
  resizeNewsGrids();
});

$(window).scroll(function() {
  var offset = $('.headerarea').offset();
  offset = offset.top + $('.headerarea').height();
  if (window.pageYOffset >= offset) {
    $('#thirdlevelmenu').addClass('fixed');
  } else {
    $('#thirdlevelmenu').removeClass('fixed');
  }
});

function resizeNewsGrids() {
	if($('.news-latest-container.startseite').html()) {
		var parent = $(this).parent('.col100');
		console.log(parent.html());
		parent.addClass('gridcontainer-news');
	}
}

function centerHeader() {
  if(($(window).width() <= 1600) && ($(window).width() > 784)) {
    $('.headerarea .headerbox').each(function() {  
      var left = '-' + (($(this).find('.notmobile').width() - $(window).width()) / 2) + 'px';
      $(this).find('.notmobile').css('left',left);
    });
    $('.slide-header').each(function() {  
      if($(this).find('.notmobile').width()) {
        var left = '-' + (($(this).find('.notmobile').width() - $(window).width()) / 2) + 'px';
      }
      $(this).find('.notmobile').css('left',left);
    });
  } else {
    if($(window).width() > 1600) {
      $('.headerarea .headerbox').each(function() {  
        if($(this).find('.notmobile').width()) {
          var left = (($(window).width() - $(this).find('.notmobile').width()) / 2) + 'px';
        }
        $(this).find('.notmobile').css('left',left);
      });
      $('.slide-header').each(function() {
        $(this).find('.notmobile').css('left',0);
      });
    } else {
      $('.headerarea .headerbox').find('.notmobile').css('left',0);
    }
  }
  if(($(window).width() <= 1280) && ($(window).width() > 784)) {
    $('.contentarea .headerbox').each(function() {  
      var left = '-' + (($(this).find('.notmobile').width() - $(window).width()) / 2) + 'px';
      $(this).find('.notmobile').css('left',left);
    });
  } else {
    $('.contentarea .headerbox').find('.notmobile').css('left',0);
  }
  
  $('.headerbox-container').each(function() {
    element = $(this).find('.headerbox');
    if($(window).width() <= 784) {
      // set height for mobile header
      var height = element.find('.mobile').height();
      if(height > 0) {
        element.height(height);
        element.find('.headerbox-content').height(height);
      }
      // create mobile box and move bodytext
      element.find('.headerbox-separator').hide();
      
      element.next('.headerbox-mobile').show();
      element.next('.headerbox-mobile').find('.headerbox-bodytext').show();
      element.next('.headerbox-mobile').find('.headerbox-headlineindividual').show();
      element.next('.headerbox-mobile').find('.headerbox-linktext').show();
      
      var bodytext = '<div class="headerbox-bodytext">' + element.find('.headerbox-bodytext').html() + '</div>';
      var textbox = '<div class="headerbox-mobile"><div class="headerbox-mobiletext-wrap inherit"></div></div>';
      // set textbox if any content
      element.find('.headerbox-bodytext').hide();
      if(!element.next().hasClass('headerbox-mobile')) {
        if(element.find('.headerbox-bodytext').html() || element.find('.headerbox-headlineindividual').html() || element.find('.headerbox-headline').html()) {
          element.after(textbox);
        }
      }
      // set bodytext if exists
      if(!element.next('.headerbox-mobile').find('.headerbox-bodytext').html()) {
        if(element.find('.headerbox-bodytext').html()) {
          element.next('.headerbox-mobile').find('.headerbox-mobiletext-wrap').append(bodytext);
        }
      }
      
      // move individual text
      if(element.find('.headerbox-linktext').html()) {
        var linktext = '<div class="headerbox-linktext"><br />' + element.find('.headerbox-linktext').html() + '</div>';
      }
      element.find('.headerbox-linktext').hide();
      if(!element.next('.headerbox-mobile').find('.headerbox-linktext').html()) {
        if(element.find('.headerbox-linktext').html()) {
          element.next('.headerbox-mobile').find('.headerbox-mobiletext-wrap').append(linktext);
        }
      }
      
      // move linktext
      if(element.find('.headerbox-headlineindividual').html()) {
        var individual = '<div class="headerbox-headlineindividual">' + element.find('.headerbox-headlineindividual').html() + '</div>';
      }
      element.find('.headerbox-headlineindividual').hide();
      if(!element.next('.headerbox-mobile').find('.headerbox-headlineindividual').html()) {
        if(element.find('.headerbox-headlineindividual').html()) {
          element.next('.headerbox-mobile').find('.headerbox-mobiletext-wrap').prepend(individual);
          // remove br at beginning of individual
          element.next('.headerbox-mobile').find('.headerbox-headlineindividual span:first-child br:lt(2)').remove();
        }
      }

    } else {
      // move bodytext and hide mobile box
      element.find('.headerbox-separator').show();
      element.find('.headerbox-bodytext').show();
      element.next('.headerbox-mobile').hide();
      
      // move individual text
      element.find('.headerbox-headlineindividual').show();
      element.next('.headerbox-mobile').find('.headerbox-headlineindividual').hide();
      
      // move linktext
      element.find('.headerbox-linktext').show();
      element.next('.headerbox-mobile').find('.headerbox-linktext').hide();
      
      // set height
      element.attr("height","").css('height','');
      element.closest('.slide-header').attr("height","").css('height','');
      element.find('.headerbox-content').attr("height","").css('height','');
    }
    if($(window).width() <= 640) {
    
      element.next('.headerbox-mobile').find('.headerbox-headline').show();
            
      // move headline
      if(element.find('.headerbox-headline').html()) {
        var headline = '<h1 class="headerbox-headline">' + element.find('.headerbox-headline').html() + '</h1>';
      }
      element.find('.headerbox-headline').hide();
      if(!element.next('.headerbox-mobile').find('.headerbox-headline').html()) {
        if(element.find('.headerbox-headline').html()) {
          element.next('.headerbox-mobile').find('.headerbox-mobiletext-wrap').prepend(headline);
        }
      }  
      
      // show textbox if individual headline is set 
      if(element.find('.headerbox-headlineindividual').html()) {
          element.next('.headerbox-mobile').show();
      }
    } else {
      // move headline
      element.find('.headerbox-headline').show();
      element.next('.headerbox-mobile').find('.headerbox-headline').hide();
    }
  });  
  
  // set slideshow heightvar height = element.find('.mobile').height();
  if($(window).width() <= 785) {
    var newheight = $(window).width() / 2;
    sliderheight = newheight + $('.slide-header .headerbox-container.active').find('.headerbox-mobile').height();
    if($('.slide-header .headerbox-container.active').find('.headerbox-mobile').height() > 0) {
      sliderheight = newheight + $('.slide-header .headerbox-container.active').find('.headerbox-mobile').height();
    }
    $('.slide-header .headerbox-container').closest('.slide-header').height(sliderheight);
    $('.slide-header .headerbox-container').find('.headerbox').height(newheight);
    $('.slide-header .headerbox-container').height(sliderheight);
    $('.slide-header .headerbox-container').find('.headerbox-content').height(newheight);
    //set top position of arrows
    var top = newheight / 2 + 'px';
    $('.slide-prev,.slide-next').css('top',top);
  } else {
    $('.slide-prev,.slide-next').css('top','50%');
  }
}

function splitheader() {
  $(".splittedheader").each(function() {
        var headerContent = $(this).text().split(' ');
        for (var i = 0; i < headerContent.length; i++) 
        {
            headerContent[i] = '<span class="subhead">' + headerContent[i] + '</span>';
        }
        $(this).html(headerContent.join(''));
    text = $(this).html();
    newtext = text.replace(/<br>|<BR>/g,'</span><br /><span class="subhead">');
    $(this).html(newtext);
    });
}


// make slideshow for header
function headerSlideshow() {
  $('.slide-header').each(function() {
    var slider = $(this);
    var nrOfItems = slider.find('.headerbox-container').size();
    var slidernav = '<div class="slider-nav"><ul class="menu">';
    var itemNr = 1;
    slider.find('.headerbox-container').each(function() {
      var itemClass = 'slide-' + itemNr;
      $(this).addClass(itemClass);
      slidernav += '<li class="slide-' + itemNr + '"><div class="boppel"></div></li>';
    $(this).css('z-index',itemNr);
    if(itemNr != 1) {
    $(this).hide();
    }
      itemNr++;
    });
    slidernav += '</ul></div>';
    slider.after(slidernav);
    slider.parent().find('.slider-nav li').click(function() {
    var navElement = $(this).attr('class');
    var className = navElement.match(/slide-([^ ]+)/);
    className = '.' + className[0];
    if(!$(this).hasClass('actnav')) {
      slider.parent().find('.headerbox-container').fadeOut(1000);
      slider.parent().find('.headerbox-container').removeClass('active');
      
      slider.find(className).addClass('active');
      slider.find(className).fadeIn(1000);
    }    
    slider.parent().find('.slider-nav li').removeClass('actnav');            
    $(this).addClass('actnav');
    });
    slider.parent().find('.slide-prev').click(function() {
    var slideract = slider.parent().find('.headerbox-container.active');
    var slidernav = slider.parent().find('.slider-nav li.actnav');
    var act = slideract;
    var actnav = slidernav;
    var next = slideract.prev('.headerbox-container');
    var nextnav = slidernav.prev();
    if( next.length == 0 ){
      next = slider.parent().find('.headerbox-container:last');
    }
    if( nextnav.length == 0 ){
      nextnav = slider.parent().find('.slider-nav li:last');
    }
    act.removeClass('active').fadeOut(500);
    actnav.removeClass('actnav');  
    next.fadeIn(500);  
    next.addClass('active');
    nextnav.addClass('actnav');  
    });
    slider.parent().find('.slide-next').click(function() {
    var slideract = slider.parent().find('.headerbox-container.active');
    var slidernav = slider.parent().find('.slider-nav li.actnav');
    var act = slideract;
    var actnav = slidernav;
    var next = slideract.next('.headerbox-container');
    var nextnav = slidernav.next();
    if( next.length == 0 ){
      next = slider.parent().find('.headerbox-container:first');
    }
    if( nextnav.length == 0 ){
      nextnav = slider.parent().find('.slider-nav li:first');
    }
    act.removeClass('active').fadeOut(500);
    actnav.removeClass('actnav');  
    next.fadeIn(500);  
    next.addClass('active');
    nextnav.addClass('actnav');  
    });
  // set slideshow heightvar height = element.find('.mobile').height();
  if($(window).width() <= 785) {
    var newheight = $(window).width() / 2;
    sliderheight = newheight + $('.slide-header .headerbox-container.active').find('.headerbox-mobile').height();
    if($('.slide-header .headerbox-container.active').find('.headerbox-mobile').height() > 0) {
      sliderheight = newheight + $('.slide-header .headerbox-container.active').find('.headerbox-mobile').height();
    }
    $('.slide-header .headerbox-container').closest('.slide-header').height(sliderheight);
    $('.slide-header .headerbox-container').find('.headerbox').height(newheight);
    $('.slide-header .headerbox-container').height(sliderheight);
    $('.slide-header .headerbox-container').find('.headerbox-content').height(newheight);
    //set top position of arrows
    var top = newheight / 2 + 'px';
    $('.slide-prev,.slide-next').css('top',top);
  } else {
    $('.slide-prev,.slide-next').css('top','50%');
  }
  
    rotateImages(); 
  $('.slide-navilr').css('visibility','visible');
  
  });
}
function rotateImages(){
  
  var slider = $('.slide-header .headerbox-container.active'); 
  var slidernav = $('.slider-nav li.actnav');
  var act = slider;
  var actnav = slidernav;
  var next = slider.next('.headerbox-container');
  var nextnav = slidernav.next();
  
  // set slideshow heightvar height = element.find('.mobile').height();
  if($(window).width() <= 785) {
    var newheight = $(window).width() / 2;
    sliderheight = newheight + slider.find('.headerbox-mobile').height();
    if(slider.find('.headerbox-mobile').height() > 0) {
      sliderheight = newheight + slider.find('.headerbox-mobile').height();
    }
    slider.closest('.slide-header').height(sliderheight);
    slider.find('.headerbox').height(newheight);
    slider.height(sliderheight);
    slider.find('.headerbox-content').height(newheight);
    //set top position of arrows
    var top = newheight / 2 + 'px';
    $('.slide-prev,.slide-next').css('top',top);
  } else {
    $('.slide-prev,.slide-next').css('top','50%');
  }
  
  if( next.length == 0 ){
    next = $('.slide-header .headerbox-container:first');
  }
  if( nextnav.length == 0 ){
    nextnav = $('.slider-nav li:first');
  }
  
  
  
  next.fadeIn(500);  
  next.addClass('active');
  nextnav.addClass('actnav'); 
  act.fadeOut(500).removeClass('active');
  
  actnav.removeClass('actnav');  
  setTimeout('rotateImages()',10000);
}

function resizeThirdlevelmenu() {
  if ($(window).width() <= 624) {
    $('#thirdlevelmenu li.item').hide(); 
    $('#thirdlevelmenu ul').removeClass('up');
    $('#thirdlevelmenu li.item').click(function() {  
      if ($(window).width() <= 624) {
        $('#thirdlevelmenu li.item').hide(); 
        $('#thirdlevelmenu ul').removeClass('up');  
        return false;
      }
    });
  } else {
    $('#thirdlevelmenu li.item').show(); 
    $('#thirdlevelmenu ul').addClass('up'); 
    $('#thirdlevelmenu li.item').click(function() {  
      if ($(window).width() > 624) {
        $('#thirdlevelmenu li.item').show(); 
        $('#thirdlevelmenu ul').addClass('up');  
        return false;
      }
    });
  }
}

function resizeButtonboxNoimageleft(element,ratio) {
  var newheight = element.find('.buttonbox-imagewrap').width() / ratio;
  element.find('.hover').height(newheight).css('max-height',newheight);
  element.find('.hover').width(element.find('.buttonbox-imagewrap').width()).css('max-width',element.find('.buttonbox-imagewrap').width());
  element.find('.standard').height(newheight).css('max-height',newheight);
  element.find('.standard').width(element.find('.buttonbox-imagewrap').width()).css('max-width',element.find('.buttonbox-imagewrap').width());
}

function buttonboxHeight() {
  $('.col100').each(function() {
    var maxh = 0;
    var width = $(this).find('.buttonbox.noimageleft:last').width();
    $(this).find('.buttonbox.noimageleft').each(function() {
      var height = $(this).find('.standard').height();
      $(this).find('.buttonbox-imagewrap').height(height);
      $(this).find('.buttonbox-content-inner').height('auto');
      var height = $(this).find('.buttonbox-content-inner').height();
      if(height > maxh) {
        maxh = height;
      }
      $(this).find('.buttonbox-imagewrap').width(width);
      $(this).find('.buttonbox-linkwrap').width(width);
    });  
    $(this).find('.buttonbox.noimageleft .buttonbox-content-inner').height(maxh);
  });
}

function hoverboxResize() {
  $('.buttonbox.imageleft').each(function() {
    var height = $(this).find('.standard').height();
    var width = $(this).find('.buttonbox-imagewrap').width();
    var ratio = width / height; 
    var heightnew = width / ratio; 
    $(this).find('.buttonbox-imagewrap').height(heightnew);
    //$(this).find('.buttonbox-imagewrap img').width(width);
    //$(this).find('.buttonbox-imagewrap img').height(heightnew);
  });
  if ($(window).width() >= 900) {
    $('.buttonbox.imageleft').each(function() {
      var height = $(this).find('.standard').height();
      $(this).find('.buttonbox-content').height(height);
      $(this).find('.buttonbox-imagewrap img').height('auto').css({
        'width' : 'auto',
        'max-width' : '100%',
        'left' : '0',
        'height' : 'auto'
      });
    });
  } else {
    $('.buttonbox.imageleft').each(function() {
      var height = $(this).find('.standard').height();
      var width = $(this).find('.standard').width();
      $(this).find('.buttonbox-content').height(height);
      if(height < $(this).find('.buttonbox-content').height()) {
        left = '-' + ((width - $(this).find('.buttonbox-imagewrap').width()) / 2) + 'px';
        $(this).find('.buttonbox-imagewrap img').height($(this).find('.buttonbox-content').height()).css({
          'width' : 'auto',
          'max-width' : 'none',
          'left' : left,
        });
        $(this).find('.buttonbox-imagewrap').height($(this).find('.buttonbox-content').height());
      }
    });
  }
  if ($(window).width() <= 624) {
    $('.buttonbox.imageleft').each(function() {
      $(this).find('.buttonbox-imagewrap img').height('auto').css({
        'width' : '100%',
        'max-width' : '100%',
        'left' : '0',
        'height' : 'auto'
      });
      $(this).find('.buttonbox-content').height('auto');
      $(this).find('.buttonbox-imagewrap').height($(this).find('.buttonbox-imagewrap img').height());
    });
  }
}

function imageTextboxOrder() {
  if($(window).width() <= 624) {
    $('.imagetextbox.imageright').each(function() {
      saveimage = $(this).find('.imagetextbox-images');
      $(this).find('.imagetextbox-images').remove();
      $(this).find('.imagetextbox-text').before(saveimage);
      $(this).find('.imagetextbox-images').css('margin-bottom','1.5em');
    });
  }
}

function mobilemenu() {
  $('#mobilemenu-button').click(function () {
    $('#mobilemenu').slideToggle();
    $(this).toggleClass('up');
    return false;
  });
  $('#mobilemenu li .menuarrow').click(function() {
    if($(this).parent().hasClass('NO')) {
      $(this).toggleClass('up');
    $(this).next().next('ul.submenu').toggle();
    }
    return false;
  });
  
}

function iconbox() {
  if ($(window).width() <= 624) {
    $('.iconbox-inner').each(function() {
    var iconbox = $(this);
    var height = iconbox.find('.iconbox-icon').height() + 'px';
      iconbox.find('.iconbox-headline').css('line-height',height);
    iconbox.find('.iconbox-bodytext').hide();
    iconbox.removeClass('up');
    });
  } else {
  $('.iconbox-inner').find('.iconbox-bodytext').show();
  $('.iconbox-inner').find('.iconbox-headline').css('line-height','120%');
  }
  
}

function navCol6nav1item() {
    $('.col6').each(function () {
      var slider = $(this);
      slider.find('.col6-slide-prev').click(function() {
        if ($(window).width() <= 624) {
          var slideract = slider.find('.col6-subcol.active');
          var slidernav = slider.find('.col6-nav.nav1item li.actnav');
          var act = slideract;
          var actnav = slidernav;
          var next = slideract.prev('.col6-subcol');
          var nextnav = slidernav.prev();
          if( next.length == 0 ){
            next = slider.find('.col6-subcol:last');
          }
          
          if( nextnav.length == 0 ){
            nextnav = slider.find('.col6-nav.nav1item li:last');
          }
          act.removeClass('active').hide();
          actnav.removeClass('actnav');  
          next.show();  
          next.addClass('active');
          nextnav.addClass('actnav');  
        }
      });
      slider.find('.col6-slide-next').click(function() {
        if ($(window).width() <= 624) {
          var slideract = slider.find('.col6-subcol.active');
          var slidernav = slider.find('.col6-nav.nav1item li.actnav');
          var act = slideract;
          var actnav = slidernav;
          var next = slideract.next('.col6-subcol');
          var nextnav = slidernav.next();
          if( next.length == 0 ){
            next = slider.find('.col6-subcol:first');
          }
          if( nextnav.length == 0 ){
            nextnav = slider.find('.col6-nav.nav1item li:first');
          }
          act.removeClass('active').hide();
          actnav.removeClass('actnav');  
          next.show();  
          next.addClass('active');
          nextnav.addClass('actnav');  
        }
      });
    });  
}
function navCol6nav3items() {    
    $('.col6').each(function () {
      var slider = $(this);
      slider.find('.col6-slide-next').click(function() {  
        if (($(window).width() <= 1008) && ($(window).width() > 624)) { 
          slider.find('.col6-subcol').removeClass('active');
          if(slider.find('.slide-1wrap').hasClass('actnav')) {
            slider.find('.col6-1wrap .col6-subcol').hide(); 
            slider.find('.col6-2wrap .col6-subcol').show();
            slider.find('.slide-1wrap').removeClass('actnav');
            slider.find('.slide-2wrap').addClass('actnav');
          } else {
            slider.find('.col6-1wrap .col6-subcol').show(); 
            slider.find('.col6-2wrap .col6-subcol').hide();
            slider.find('.slide-1wrap').addClass('actnav');
            slider.find('.slide-2wrap').removeClass('actnav');
          }
        }
      });
      slider.find('.col6-slide-prev').click(function() {  
        if (($(window).width() <= 1008) && ($(window).width() > 624)) { 
          slider.find('.col6-subcol').removeClass('active');
          if(slider.find('.slide-1wrap').hasClass('actnav')) {
            slider.find('.col6-1wrap .col6-subcol').hide(); 
            slider.find('.col6-2wrap .col6-subcol').show();
            slider.find('.slide-1wrap').removeClass('actnav');
            slider.find('.slide-2wrap').addClass('actnav');
          } else {
            slider.find('.col6-1wrap .col6-subcol').show(); 
            slider.find('.col6-2wrap .col6-subcol').hide();
            slider.find('.slide-1wrap').addClass('actnav');
            slider.find('.slide-2wrap').removeClass('actnav');
          }
        }
      });
    });
}

function slideCol6() {
  $('.col6').each(function () {
      var slider = $(this);
      var maxh = '';
      $('.col6-subcol').each(function () {
        var height = $(this).height();
        if(height > maxh) {
          maxh = height;
        }
      });
      $('.col6-subcol').height(maxh);
      
    });
  $('.col6').each(function() {
    var slider = $(this);
    if ($(window).width() >= 1008) {
      slider.find('.col6-subcol').css('display','table-cell');
    } 
    if ($(window).width() <= 624) {
      slider.find('.col6-subcol').hide();
      var slidernav = '<div class="col6-nav nav1item"><ul class="menu">';
      var itemNr = 1;
      slider.find('.col6-subcol').each(function() {
        var itemClass = 'slide-' + itemNr;
        $(this).addClass(itemClass);
        slidernav += '<li class="slide-' + itemNr + '"><div class="boppel"></div></li>';
        itemNr++;
      });
      slidernav += '</ul></div>';
      if(slider.find('.col6-nav.nav1item').length == 0) {
        slider.find('.col6-inner').after(slidernav);
      }
      slider.find('.col6-subcol.col6-1').addClass('active').show();
      slider.find('.col6-nav li').removeClass('actnav');
      slider.find('.col6-nav li.slide-1').addClass('actnav');
      
      slider.find('.col6-nav li').click(function() {
        var navElement = $(this).attr('class');
        className = navElement.match(/slide-([^ ]+)/);
        className = '.' + className[0];
        if(!$(this).hasClass('actnav')) {
          slider.find('.col6-subcol').hide();
          slider.find('.col6-subcol').removeClass('active');
                
          slider.find('.col6-inner').find(className).addClass('active');  
          slider.find(className).show();
        }        
        slider.find('.col6-nav li').removeClass('actnav');
        $(this).addClass('actnav');
      });
    }
    if (($(window).width() <= 1008) && ($(window).width() > 624)) { 
      slider.find('.col6-subcol').hide();
      var slidernav = '<div class="col6-nav nav3items"><ul class="menu">';
      slidernav += '<li class="slide-1wrap"><div class="boppel"></div></li>';
      slidernav += '<li class="slide-2wrap"><div class="boppel"></div></li>';
      slidernav += '</ul></div>';
      if(slider.find('.col6-nav.nav3items').length == 0) {
        slider.find('.col6-inner').after(slidernav);
      }
      slider.find('.col6-1wrap').addClass('active');
      slider.find('.col6-2wrap').removeClass('active');
      slider.find('.col6-1wrap .col6-subcol').show();
      slider.find('.col6-nav li.slide-1wrap').addClass('actnav');
      slider.find('.col6-nav li.slide-2wrap').removeClass('actnav');
      
      slider.find('.col6-nav li.slide-1wrap').click(function() {
        slider.find('.col6-1wrap').addClass('active');
        slider.find('.col6-1wrap .col6-subcol').show();
        slider.find('.col6-nav li.slide-1wrap').addClass('actnav');
        slider.find('.col6-2wrap').removeClass('active');
        slider.find('.col6-2wrap .col6-subcol').hide();
        slider.find('.col6-nav li.slide-2wrap').removeClass('actnav');
      });
      slider.find('.col6-nav li.slide-2wrap').click(function() {
        slider.find('.col6-2wrap').addClass('active');
        slider.find('.col6-2wrap .col6-subcol').show();
        slider.find('.col6-nav li.slide-2wrap').addClass('actnav');
        slider.find('.col6-1wrap').removeClass('active');
        slider.find('.col6-1wrap .col6-subcol').hide();
        slider.find('.col6-nav li.slide-1wrap').removeClass('actnav');
      });
    }
  });
}

function imagetextboxDetail() {
  $('.imagetextbox-more').each(function (){
    var elid = '#' + $(this).find('a').attr('class');
    $(elid).hide();
    $(this).click(function(){
      $(elid).slideToggle();
      $(this).toggleClass('up');
      return false; 
    });
  });
}

// make thirdlevel menu from all h1 in contentarea
function getThirdlevelmenu() {
  if($('.contentarea h2').length == '') {
    $('#thirdlevelmenu').hide();
  }
  $('.contentarea h2').each(function() {
    var elid = $(this).closest('div[id^="c"][class^="csc-default"]').attr('id');
    var menuel = '<li class="' + elid + ' item"><a href="#">' + $(this).html() + '</a></li>';
    $('#thirdlevelmenu ul').append(menuel);
  });
  $('#thirdlevelmenu li.item').click(function() {
    var elclass = $(this).attr('class');
    elclass = '#' + elclass.match(/c([^ ]+)/);
    var scrolltop = $(elclass).offset().top - 100;
    $('html,body').animate({scrollTop: scrolltop}, 'slow');
    return false;
  });
  $('#thirdlevelmenu li.select').click(function() {
    $('#thirdlevelmenu li.item').toggle(); 
    $('#thirdlevelmenu ul').toggleClass('up'); 
  });
}


// set padding for text elements in imagetextboxes
function paddingImagetextbox() {
  if ($(window).width() > 990) {
    $('.imagetextbox.imageleft').each(function() {
      var totalwidth = $(this).width();
      var padding = (totalwidth - 960) / 2;
      padding = padding + 'px';
      $(this).find('.imagetextbox-text-inner').css('padding-right',padding);
    });
    $('.imagetextbox.imageright').each(function() {
      var totalwidth = $(this).width();
      var padding = (totalwidth - 960) / 2;
      padding = padding + 'px';
      $(this).find('.imagetextbox-text-inner').css('padding-left',padding);
    });
  } else {
    if ($(window).width() > 624) {
      $('.imagetextbox.imageleft').each(function() {
        $(this).find('.imagetextbox-text-inner').css('padding-right','4%');
      });
      $('.imagetextbox.imageright').each(function() {
        $(this).find('.imagetextbox-text-inner').css('padding-left','4%');
      });
    } else {
      $('.imagetextbox.imageleft').each(function() {
        $(this).find('.imagetextbox-text-inner').css('padding-right','2%');
      });
      $('.imagetextbox.imageright').each(function() {
        $(this).find('.imagetextbox-text-inner').css('padding-left','2%');
      });
    }
  }
}

function resizeImagetextbox() {
  $('.slideimages').each(function() {
    var height = $(this).find('img').height();
    $(this).height(height);
  });
  
}

// do slideshow for imagetextboxes
function imagetextboxSlideshow() {
  $('.slideimages').each(function() {
    var slider = $(this);
    var nrOfItems = $(this).find('img').size();
    if(nrOfItems >= 2) {
    var slidernav = '<div class="slider-nav"><ul class="menu">';
    var itemNr = 1;
    slider.find('img').each(function() {
      var itemClass = 'slide-' + itemNr;
      $(this).addClass(itemClass);
      slidernav += '<li class="slide-' + itemNr + '"><div class="boppel"></div></li>';
      itemNr++;
    });
    slidernav += '</ul></div>';
    slider.append(slidernav);
    slider.find('img.slide-1').addClass('active').show();
    slider.find('li.slide-1').addClass('actnav');
    
    slider.find('li').click(function() {
      var navElement = $(this).attr('class');
      className = navElement.match(/slide-([^ ]+)/);
      className = '.' + className[0];
      if(!$(this).hasClass('actnav')) {
        slider.find('img').fadeOut(1000);
        slider.find('img').removeClass('active');
              
        slider.find('.slide-image').find(className).addClass('active');  
        slider.find(className).fadeIn(1000);
      }        
      slider.find('li').removeClass('actnav');
      $(this).addClass('actnav');
    });
    slider.find('.slide-prev').click(function() {
      var slideract = slider.find('img.active');
      var slidernav = slider.find('.slider-nav li.actnav');
      var act = slideract;
      var actnav = slidernav;
      var next = slideract.parent().prev('.slide-image');
      var nextnav = slidernav.prev();
      if( next.length == 0 ){
        next = slider.parent().find('.slide-image:last');
      }
      
      if( nextnav.length == 0 ){
        nextnav = slider.parent().find('.slider-nav li:last');
      }
      act.removeClass('active').fadeOut(500);
      actnav.removeClass('actnav');  
      next.find('img').fadeIn(500);  
      next.find('img').addClass('active');
      nextnav.addClass('actnav');  
    });
    slider.find('.slide-next').click(function() {
      var slideract = slider.find('img.active');
      var slidernav = slider.find('.slider-nav li.actnav');
      var act = slideract;
      var actnav = slidernav;
      var next = slideract.parent().next('.slide-image');
      var nextnav = slidernav.next();
      if( next.length == 0 ){
        next = slider.parent().find('.slide-image:first');
      }
      if( nextnav.length == 0 ){
        nextnav = slider.parent().find('.slider-nav li:first');
      }
      act.removeClass('active').fadeOut(500);
      actnav.removeClass('actnav');  
      next.find('img').fadeIn(500);  
      next.find('img').addClass('active');
      nextnav.addClass('actnav');  
    });
    } else {
      slider.find('img').show();
    }
  });
}

function hoverbox() {
  if($(window).width() >= 624) {
    $('.buttonbox').each(function() {
    $(this).find('.buttonbox-linkwrap').bind({
      mouseenter: function() {        
        $(this).find('.hover').stop().fadeIn('normal');
        $(this).mouseleave(function() {
          $(this).find('.hover').stop().fadeOut('normal');
        });
      }     
    }); 
    });
  }
}



// change status of phone numbers
function delinkTel() {
    jQuery('a[href*="tel:"]').each(function() {
        if (jQuery(window).width() > 1026) {
            jQuery(this).click(function() { return false; }).css('cursor','text');
            jQuery(this).css("color", "#212121");
            jQuery(this).css("font-weight", "normal");
        }
    });
}

// set image widths
function setImageWidth() {
if (jQuery(window).width() >= 624) {   
    jQuery('.csc-textpic-imagerow').each(function() {
      cols = jQuery(this).find('.csc-textpic-imagecolumn').size();
      width = 100 / cols + '%';
      jQuery(this).find('.csc-textpic-imagecolumn').width(width);
    });
  // set margins for intext left and right
  jQuery('.csc-textpic-intext-left-nowrap').each(function() {
    imgwidth = jQuery(this).find('.csc-textpic-imagewrap').outerWidth() + 20;
    jQuery(this).find('.csc-textpic-text').css('margin-left',imgwidth);
  });
  jQuery('.csc-textpic-intext-right-nowrap').each(function() {
    imgwidth = jQuery(this).find('.csc-textpic-imagewrap').width() + 20;
    jQuery(this).find('.csc-textpic-text').css('margin-right',imgwidth);
  });
  } else {
    jQuery('.csc-textpic-imagerow').find('.csc-textpic-imagecolumn').width('100%');
  
  // set margins for intext left and right
  jQuery('.csc-textpic-intext-left-nowrap').each(function() {
    jQuery(this).find('.csc-textpic-text').css('margin-left','0');
  });
  jQuery('.csc-textpic-intext-right-nowrap').each(function() {
    jQuery(this).find('.csc-textpic-text').css('margin-right','0');
  });
  }
}

// top scroller
function scrollTop() {
  jQuery('.toplink').click(function(e) {
    e.preventDefault();
    jQuery('html,body').animate({scrollTop: 0}, 'slow');
  });
}
/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */
(function(d){function e(a){var b=a||window.event,c=[].slice.call(arguments,1),f=0,e=0,g=0,a=d.event.fix(b);a.type="mousewheel";b.wheelDelta&&(f=b.wheelDelta/120);b.detail&&(f=-b.detail/3);g=f;b.axis!==void 0&&b.axis===b.HORIZONTAL_AXIS&&(g=0,e=-1*f);b.wheelDeltaY!==void 0&&(g=b.wheelDeltaY/120);b.wheelDeltaX!==void 0&&(e=-1*b.wheelDeltaX/120);c.unshift(a,f,e,g);return(d.event.dispatch||d.event.handle).apply(this,c)}var c=["DOMMouseScroll","mousewheel"];if(d.event.fixHooks)for(var h=c.length;h;)d.event.fixHooks[c[--h]]=
d.event.mouseHooks;d.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=c.length;a;)this.addEventListener(c[--a],e,false);else this.onmousewheel=e},teardown:function(){if(this.removeEventListener)for(var a=c.length;a;)this.removeEventListener(c[--a],e,false);else this.onmousewheel=null}};d.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery);
/*! fancyBox v2.1.4 fancyapps.com | fancyapps.com/fancybox/#license */
(function(C,z,f,r){var q=f(C),n=f(z),b=f.fancybox=function(){b.open.apply(this,arguments)},H=navigator.userAgent.match(/msie/),w=null,s=z.createTouch!==r,t=function(a){return a&&a.hasOwnProperty&&a instanceof f},p=function(a){return a&&"string"===f.type(a)},F=function(a){return p(a)&&0<a.indexOf("%")},l=function(a,d){var e=parseInt(a,10)||0;d&&F(a)&&(e*=b.getViewport()[d]/100);return Math.ceil(e)},x=function(a,b){return l(a,b)+"px"};f.extend(b,{version:"2.1.4",defaults:{padding:15,margin:20,width:800,
height:600,minWidth:100,minHeight:100,maxWidth:9999,maxHeight:9999,autoSize:!0,autoHeight:!1,autoWidth:!1,autoResize:!0,autoCenter:!s,fitToView:!0,aspectRatio:!1,topRatio:0.5,leftRatio:0.5,scrolling:"auto",wrapCSS:"",arrows:!0,closeBtn:!0,closeClick:!1,nextClick:!1,mouseWheel:!0,autoPlay:!1,playSpeed:3E3,preload:3,modal:!1,loop:!0,ajax:{dataType:"html",headers:{"X-fancyBox":!0}},iframe:{scrolling:"auto",preload:!0},swf:{wmode:"transparent",allowfullscreen:"true",allowscriptaccess:"always"},keys:{next:{13:"left",
34:"up",39:"left",40:"up"},prev:{8:"right",33:"down",37:"right",38:"down"},close:[27],play:[32],toggle:[70]},direction:{next:"left",prev:"right"},scrollOutside:!0,index:0,type:null,href:null,content:null,title:null,tpl:{wrap:'<div class="fancybox-wrap" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',image:'<img class="fancybox-image" src="{href}" alt="" />',iframe:'<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen'+
(H?' allowtransparency="true"':"")+"></iframe>",error:'<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',closeBtn:'<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"></a>',next:'<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',prev:'<a title="Previous" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'},openEffect:"fade",openSpeed:250,openEasing:"swing",openOpacity:!0,
openMethod:"zoomIn",closeEffect:"fade",closeSpeed:250,closeEasing:"swing",closeOpacity:!0,closeMethod:"zoomOut",nextEffect:"elastic",nextSpeed:250,nextEasing:"swing",nextMethod:"changeIn",prevEffect:"elastic",prevSpeed:250,prevEasing:"swing",prevMethod:"changeOut",helpers:{overlay:!0,title:!0},onCancel:f.noop,beforeLoad:f.noop,afterLoad:f.noop,beforeShow:f.noop,afterShow:f.noop,beforeChange:f.noop,beforeClose:f.noop,afterClose:f.noop},group:{},opts:{},previous:null,coming:null,current:null,isActive:!1,
isOpen:!1,isOpened:!1,wrap:null,skin:null,outer:null,inner:null,player:{timer:null,isActive:!1},ajaxLoad:null,imgPreload:null,transitions:{},helpers:{},open:function(a,d){if(a&&(f.isPlainObject(d)||(d={}),!1!==b.close(!0)))return f.isArray(a)||(a=t(a)?f(a).get():[a]),f.each(a,function(e,c){var k={},g,h,j,m,l;"object"===f.type(c)&&(c.nodeType&&(c=f(c)),t(c)?(k={href:c.data("fancybox-href")||c.attr("href"),title:c.data("fancybox-title")||c.attr("title"),isDom:!0,element:c},f.metadata&&f.extend(!0,k,
c.metadata())):k=c);g=d.href||k.href||(p(c)?c:null);h=d.title!==r?d.title:k.title||"";m=(j=d.content||k.content)?"html":d.type||k.type;!m&&k.isDom&&(m=c.data("fancybox-type"),m||(m=(m=c.prop("class").match(/fancybox\.(\w+)/))?m[1]:null));p(g)&&(m||(b.isImage(g)?m="image":b.isSWF(g)?m="swf":"#"===g.charAt(0)?m="inline":p(c)&&(m="html",j=c)),"ajax"===m&&(l=g.split(/\s+/,2),g=l.shift(),l=l.shift()));j||("inline"===m?g?j=f(p(g)?g.replace(/.*(?=#[^\s]+$)/,""):g):k.isDom&&(j=c):"html"===m?j=g:!m&&(!g&&
k.isDom)&&(m="inline",j=c));f.extend(k,{href:g,type:m,content:j,title:h,selector:l});a[e]=k}),b.opts=f.extend(!0,{},b.defaults,d),d.keys!==r&&(b.opts.keys=d.keys?f.extend({},b.defaults.keys,d.keys):!1),b.group=a,b._start(b.opts.index)},cancel:function(){var a=b.coming;a&&!1!==b.trigger("onCancel")&&(b.hideLoading(),b.ajaxLoad&&b.ajaxLoad.abort(),b.ajaxLoad=null,b.imgPreload&&(b.imgPreload.onload=b.imgPreload.onerror=null),a.wrap&&a.wrap.stop(!0,!0).trigger("onReset").remove(),b.coming=null,b.current||
b._afterZoomOut(a))},close:function(a){b.cancel();!1!==b.trigger("beforeClose")&&(b.unbindEvents(),b.isActive&&(!b.isOpen||!0===a?(f(".fancybox-wrap").stop(!0).trigger("onReset").remove(),b._afterZoomOut()):(b.isOpen=b.isOpened=!1,b.isClosing=!0,f(".fancybox-item, .fancybox-nav").remove(),b.wrap.stop(!0,!0).removeClass("fancybox-opened"),b.transitions[b.current.closeMethod]())))},play:function(a){var d=function(){clearTimeout(b.player.timer)},e=function(){d();b.current&&b.player.isActive&&(b.player.timer=
setTimeout(b.next,b.current.playSpeed))},c=function(){d();f("body").unbind(".player");b.player.isActive=!1;b.trigger("onPlayEnd")};if(!0===a||!b.player.isActive&&!1!==a){if(b.current&&(b.current.loop||b.current.index<b.group.length-1))b.player.isActive=!0,f("body").bind({"afterShow.player onUpdate.player":e,"onCancel.player beforeClose.player":c,"beforeLoad.player":d}),e(),b.trigger("onPlayStart")}else c()},next:function(a){var d=b.current;d&&(p(a)||(a=d.direction.next),b.jumpto(d.index+1,a,"next"))},
prev:function(a){var d=b.current;d&&(p(a)||(a=d.direction.prev),b.jumpto(d.index-1,a,"prev"))},jumpto:function(a,d,e){var c=b.current;c&&(a=l(a),b.direction=d||c.direction[a>=c.index?"next":"prev"],b.router=e||"jumpto",c.loop&&(0>a&&(a=c.group.length+a%c.group.length),a%=c.group.length),c.group[a]!==r&&(b.cancel(),b._start(a)))},reposition:function(a,d){var e=b.current,c=e?e.wrap:null,k;c&&(k=b._getPosition(d),a&&"scroll"===a.type?(delete k.position,c.stop(!0,!0).animate(k,200)):(c.css(k),e.pos=f.extend({},
e.dim,k)))},update:function(a){var d=a&&a.type,e=!d||"orientationchange"===d;e&&(clearTimeout(w),w=null);b.isOpen&&!w&&(w=setTimeout(function(){var c=b.current;c&&!b.isClosing&&(b.wrap.removeClass("fancybox-tmp"),(e||"load"===d||"resize"===d&&c.autoResize)&&b._setDimension(),"scroll"===d&&c.canShrink||b.reposition(a),b.trigger("onUpdate"),w=null)},e&&!s?0:300))},toggle:function(a){b.isOpen&&(b.current.fitToView="boolean"===f.type(a)?a:!b.current.fitToView,s&&(b.wrap.removeAttr("style").addClass("fancybox-tmp"),
b.trigger("onUpdate")),b.update())},hideLoading:function(){n.unbind(".loading");f("#fancybox-loading").remove()},showLoading:function(){var a,d;b.hideLoading();a=f('<div id="fancybox-loading"><div></div></div>').click(b.cancel).appendTo("body");n.bind("keydown.loading",function(a){if(27===(a.which||a.keyCode))a.preventDefault(),b.cancel()});b.defaults.fixed||(d=b.getViewport(),a.css({position:"absolute",top:0.5*d.h+d.y,left:0.5*d.w+d.x}))},getViewport:function(){var a=b.current&&b.current.locked||
!1,d={x:q.scrollLeft(),y:q.scrollTop()};a?(d.w=a[0].clientWidth,d.h=a[0].clientHeight):(d.w=s&&C.innerWidth?C.innerWidth:q.width(),d.h=s&&C.innerHeight?C.innerHeight:q.height());return d},unbindEvents:function(){b.wrap&&t(b.wrap)&&b.wrap.unbind(".fb");n.unbind(".fb");q.unbind(".fb")},bindEvents:function(){var a=b.current,d;a&&(q.bind("orientationchange.fb"+(s?"":" resize.fb")+(a.autoCenter&&!a.locked?" scroll.fb":""),b.update),(d=a.keys)&&n.bind("keydown.fb",function(e){var c=e.which||e.keyCode,k=
e.target||e.srcElement;if(27===c&&b.coming)return!1;!e.ctrlKey&&(!e.altKey&&!e.shiftKey&&!e.metaKey&&(!k||!k.type&&!f(k).is("[contenteditable]")))&&f.each(d,function(d,k){if(1<a.group.length&&k[c]!==r)return b[d](k[c]),e.preventDefault(),!1;if(-1<f.inArray(c,k))return b[d](),e.preventDefault(),!1})}),f.fn.mousewheel&&a.mouseWheel&&b.wrap.bind("mousewheel.fb",function(d,c,k,g){for(var h=f(d.target||null),j=!1;h.length&&!j&&!h.is(".fancybox-skin")&&!h.is(".fancybox-wrap");)j=h[0]&&!(h[0].style.overflow&&
"hidden"===h[0].style.overflow)&&(h[0].clientWidth&&h[0].scrollWidth>h[0].clientWidth||h[0].clientHeight&&h[0].scrollHeight>h[0].clientHeight),h=f(h).parent();if(0!==c&&!j&&1<b.group.length&&!a.canShrink){if(0<g||0<k)b.prev(0<g?"down":"left");else if(0>g||0>k)b.next(0>g?"up":"right");d.preventDefault()}}))},trigger:function(a,d){var e,c=d||b.coming||b.current;if(c){f.isFunction(c[a])&&(e=c[a].apply(c,Array.prototype.slice.call(arguments,1)));if(!1===e)return!1;c.helpers&&f.each(c.helpers,function(d,
e){e&&(b.helpers[d]&&f.isFunction(b.helpers[d][a]))&&(e=f.extend(!0,{},b.helpers[d].defaults,e),b.helpers[d][a](e,c))});f.event.trigger(a+".fb")}},isImage:function(a){return p(a)&&a.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp)((\?|#).*)?$)/i)},isSWF:function(a){return p(a)&&a.match(/\.(swf)((\?|#).*)?$/i)},_start:function(a){var d={},e,c;a=l(a);e=b.group[a]||null;if(!e)return!1;d=f.extend(!0,{},b.opts,e);e=d.margin;c=d.padding;"number"===f.type(e)&&(d.margin=[e,e,e,e]);"number"===f.type(c)&&
(d.padding=[c,c,c,c]);d.modal&&f.extend(!0,d,{closeBtn:!1,closeClick:!1,nextClick:!1,arrows:!1,mouseWheel:!1,keys:null,helpers:{overlay:{closeClick:!1}}});d.autoSize&&(d.autoWidth=d.autoHeight=!0);"auto"===d.width&&(d.autoWidth=!0);"auto"===d.height&&(d.autoHeight=!0);d.group=b.group;d.index=a;b.coming=d;if(!1===b.trigger("beforeLoad"))b.coming=null;else{c=d.type;e=d.href;if(!c)return b.coming=null,b.current&&b.router&&"jumpto"!==b.router?(b.current.index=a,b[b.router](b.direction)):!1;b.isActive=
!0;if("image"===c||"swf"===c)d.autoHeight=d.autoWidth=!1,d.scrolling="visible";"image"===c&&(d.aspectRatio=!0);"iframe"===c&&s&&(d.scrolling="scroll");d.wrap=f(d.tpl.wrap).addClass("fancybox-"+(s?"mobile":"desktop")+" fancybox-type-"+c+" fancybox-tmp "+d.wrapCSS).appendTo(d.parent||"body");f.extend(d,{skin:f(".fancybox-skin",d.wrap),outer:f(".fancybox-outer",d.wrap),inner:f(".fancybox-inner",d.wrap)});f.each(["Top","Right","Bottom","Left"],function(a,b){d.skin.css("padding"+b,x(d.padding[a]))});b.trigger("onReady");
if("inline"===c||"html"===c){if(!d.content||!d.content.length)return b._error("content")}else if(!e)return b._error("href");"image"===c?b._loadImage():"ajax"===c?b._loadAjax():"iframe"===c?b._loadIframe():b._afterLoad()}},_error:function(a){f.extend(b.coming,{type:"html",autoWidth:!0,autoHeight:!0,minWidth:0,minHeight:0,scrolling:"no",hasError:a,content:b.coming.tpl.error});b._afterLoad()},_loadImage:function(){var a=b.imgPreload=new Image;a.onload=function(){this.onload=this.onerror=null;b.coming.width=
this.width;b.coming.height=this.height;b._afterLoad()};a.onerror=function(){this.onload=this.onerror=null;b._error("image")};a.src=b.coming.href;!0!==a.complete&&b.showLoading()},_loadAjax:function(){var a=b.coming;b.showLoading();b.ajaxLoad=f.ajax(f.extend({},a.ajax,{url:a.href,error:function(a,e){b.coming&&"abort"!==e?b._error("ajax",a):b.hideLoading()},success:function(d,e){"success"===e&&(a.content=d,b._afterLoad())}}))},_loadIframe:function(){var a=b.coming,d=f(a.tpl.iframe.replace(/\{rnd\}/g,
(new Date).getTime())).attr("scrolling",s?"auto":a.iframe.scrolling).attr("src",a.href);f(a.wrap).bind("onReset",function(){try{f(this).find("iframe").hide().attr("src","//about:blank").end().empty()}catch(a){}});a.iframe.preload&&(b.showLoading(),d.one("load",function(){f(this).data("ready",1);s||f(this).bind("load.fb",b.update);f(this).parents(".fancybox-wrap").width("100%").removeClass("fancybox-tmp").show();b._afterLoad()}));a.content=d.appendTo(a.inner);a.iframe.preload||b._afterLoad()},_preloadImages:function(){var a=
b.group,d=b.current,e=a.length,c=d.preload?Math.min(d.preload,e-1):0,f,g;for(g=1;g<=c;g+=1)f=a[(d.index+g)%e],"image"===f.type&&f.href&&((new Image).src=f.href)},_afterLoad:function(){var a=b.coming,d=b.current,e,c,k,g,h;b.hideLoading();if(a&&!1!==b.isActive)if(!1===b.trigger("afterLoad",a,d))a.wrap.stop(!0).trigger("onReset").remove(),b.coming=null;else{d&&(b.trigger("beforeChange",d),d.wrap.stop(!0).removeClass("fancybox-opened").find(".fancybox-item, .fancybox-nav").remove());b.unbindEvents();
e=a.content;c=a.type;k=a.scrolling;f.extend(b,{wrap:a.wrap,skin:a.skin,outer:a.outer,inner:a.inner,current:a,previous:d});g=a.href;switch(c){case "inline":case "ajax":case "html":a.selector?e=f("<div>").html(e).find(a.selector):t(e)&&(e.data("fancybox-placeholder")||e.data("fancybox-placeholder",f('<div class="fancybox-placeholder"></div>').insertAfter(e).hide()),e=e.show().detach(),a.wrap.bind("onReset",function(){f(this).find(e).length&&e.hide().replaceAll(e.data("fancybox-placeholder")).data("fancybox-placeholder",
!1)}));break;case "image":e=a.tpl.image.replace("{href}",g);break;case "swf":e='<object id="fancybox-swf" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="movie" value="'+g+'"></param>',h="",f.each(a.swf,function(a,b){e+='<param name="'+a+'" value="'+b+'"></param>';h+=" "+a+'="'+b+'"'}),e+='<embed src="'+g+'" type="application/x-shockwave-flash" width="100%" height="100%"'+h+"></embed></object>"}(!t(e)||!e.parent().is(a.inner))&&a.inner.append(e);b.trigger("beforeShow");
a.inner.css("overflow","yes"===k?"scroll":"no"===k?"hidden":k);b._setDimension();b.reposition();b.isOpen=!1;b.coming=null;b.bindEvents();if(b.isOpened){if(d.prevMethod)b.transitions[d.prevMethod]()}else f(".fancybox-wrap").not(a.wrap).stop(!0).trigger("onReset").remove();b.transitions[b.isOpened?a.nextMethod:a.openMethod]();b._preloadImages()}},_setDimension:function(){var a=b.getViewport(),d=0,e=!1,c=!1,e=b.wrap,k=b.skin,g=b.inner,h=b.current,c=h.width,j=h.height,m=h.minWidth,u=h.minHeight,n=h.maxWidth,
v=h.maxHeight,s=h.scrolling,q=h.scrollOutside?h.scrollbarWidth:0,y=h.margin,p=l(y[1]+y[3]),r=l(y[0]+y[2]),z,A,t,D,B,G,C,E,w;e.add(k).add(g).width("auto").height("auto").removeClass("fancybox-tmp");y=l(k.outerWidth(!0)-k.width());z=l(k.outerHeight(!0)-k.height());A=p+y;t=r+z;D=F(c)?(a.w-A)*l(c)/100:c;B=F(j)?(a.h-t)*l(j)/100:j;if("iframe"===h.type){if(w=h.content,h.autoHeight&&1===w.data("ready"))try{w[0].contentWindow.document.location&&(g.width(D).height(9999),G=w.contents().find("body"),q&&G.css("overflow-x",
"hidden"),B=G.height())}catch(H){}}else if(h.autoWidth||h.autoHeight)g.addClass("fancybox-tmp"),h.autoWidth||g.width(D),h.autoHeight||g.height(B),h.autoWidth&&(D=g.width()),h.autoHeight&&(B=g.height()),g.removeClass("fancybox-tmp");c=l(D);j=l(B);E=D/B;m=l(F(m)?l(m,"w")-A:m);n=l(F(n)?l(n,"w")-A:n);u=l(F(u)?l(u,"h")-t:u);v=l(F(v)?l(v,"h")-t:v);G=n;C=v;h.fitToView&&(n=Math.min(a.w-A,n),v=Math.min(a.h-t,v));A=a.w-p;r=a.h-r;h.aspectRatio?(c>n&&(c=n,j=l(c/E)),j>v&&(j=v,c=l(j*E)),c<m&&(c=m,j=l(c/E)),j<u&&
(j=u,c=l(j*E))):(c=Math.max(m,Math.min(c,n)),h.autoHeight&&"iframe"!==h.type&&(g.width(c),j=g.height()),j=Math.max(u,Math.min(j,v)));if(h.fitToView)if(g.width(c).height(j),e.width(c+y),a=e.width(),p=e.height(),h.aspectRatio)for(;(a>A||p>r)&&(c>m&&j>u)&&!(19<d++);)j=Math.max(u,Math.min(v,j-10)),c=l(j*E),c<m&&(c=m,j=l(c/E)),c>n&&(c=n,j=l(c/E)),g.width(c).height(j),e.width(c+y),a=e.width(),p=e.height();else c=Math.max(m,Math.min(c,c-(a-A))),j=Math.max(u,Math.min(j,j-(p-r)));q&&("auto"===s&&j<B&&c+y+
q<A)&&(c+=q);g.width(c).height(j);e.width(c+y);a=e.width();p=e.height();e=(a>A||p>r)&&c>m&&j>u;c=h.aspectRatio?c<G&&j<C&&c<D&&j<B:(c<G||j<C)&&(c<D||j<B);f.extend(h,{dim:{width:x(a),height:x(p)},origWidth:D,origHeight:B,canShrink:e,canExpand:c,wPadding:y,hPadding:z,wrapSpace:p-k.outerHeight(!0),skinSpace:k.height()-j});!w&&(h.autoHeight&&j>u&&j<v&&!c)&&g.height("auto")},_getPosition:function(a){var d=b.current,e=b.getViewport(),c=d.margin,f=b.wrap.width()+c[1]+c[3],g=b.wrap.height()+c[0]+c[2],c={position:"absolute",
top:c[0],left:c[3]};d.autoCenter&&d.fixed&&!a&&g<=e.h&&f<=e.w?c.position="fixed":d.locked||(c.top+=e.y,c.left+=e.x);c.top=x(Math.max(c.top,c.top+(e.h-g)*d.topRatio));c.left=x(Math.max(c.left,c.left+(e.w-f)*d.leftRatio));return c},_afterZoomIn:function(){var a=b.current;a&&(b.isOpen=b.isOpened=!0,b.wrap.css("overflow","visible").addClass("fancybox-opened"),b.update(),(a.closeClick||a.nextClick&&1<b.group.length)&&b.inner.css("cursor","pointer").bind("click.fb",function(d){!f(d.target).is("a")&&!f(d.target).parent().is("a")&&
(d.preventDefault(),b[a.closeClick?"close":"next"]())}),a.closeBtn&&f(a.tpl.closeBtn).appendTo(b.skin).bind("click.fb",function(a){a.preventDefault();b.close()}),a.arrows&&1<b.group.length&&((a.loop||0<a.index)&&f(a.tpl.prev).appendTo(b.outer).bind("click.fb",b.prev),(a.loop||a.index<b.group.length-1)&&f(a.tpl.next).appendTo(b.outer).bind("click.fb",b.next)),b.trigger("afterShow"),!a.loop&&a.index===a.group.length-1?b.play(!1):b.opts.autoPlay&&!b.player.isActive&&(b.opts.autoPlay=!1,b.play()))},_afterZoomOut:function(a){a=
a||b.current;f(".fancybox-wrap").trigger("onReset").remove();f.extend(b,{group:{},opts:{},router:!1,current:null,isActive:!1,isOpened:!1,isOpen:!1,isClosing:!1,wrap:null,skin:null,outer:null,inner:null});b.trigger("afterClose",a)}});b.transitions={getOrigPosition:function(){var a=b.current,d=a.element,e=a.orig,c={},f=50,g=50,h=a.hPadding,j=a.wPadding,m=b.getViewport();!e&&(a.isDom&&d.is(":visible"))&&(e=d.find("img:first"),e.length||(e=d));t(e)?(c=e.offset(),e.is("img")&&(f=e.outerWidth(),g=e.outerHeight())):
(c.top=m.y+(m.h-g)*a.topRatio,c.left=m.x+(m.w-f)*a.leftRatio);if("fixed"===b.wrap.css("position")||a.locked)c.top-=m.y,c.left-=m.x;return c={top:x(c.top-h*a.topRatio),left:x(c.left-j*a.leftRatio),width:x(f+j),height:x(g+h)}},step:function(a,d){var e,c,f=d.prop;c=b.current;var g=c.wrapSpace,h=c.skinSpace;if("width"===f||"height"===f)e=d.end===d.start?1:(a-d.start)/(d.end-d.start),b.isClosing&&(e=1-e),c="width"===f?c.wPadding:c.hPadding,c=a-c,b.skin[f](l("width"===f?c:c-g*e)),b.inner[f](l("width"===
f?c:c-g*e-h*e))},zoomIn:function(){var a=b.current,d=a.pos,e=a.openEffect,c="elastic"===e,k=f.extend({opacity:1},d);delete k.position;c?(d=this.getOrigPosition(),a.openOpacity&&(d.opacity=0.1)):"fade"===e&&(d.opacity=0.1);b.wrap.css(d).animate(k,{duration:"none"===e?0:a.openSpeed,easing:a.openEasing,step:c?this.step:null,complete:b._afterZoomIn})},zoomOut:function(){var a=b.current,d=a.closeEffect,e="elastic"===d,c={opacity:0.1};e&&(c=this.getOrigPosition(),a.closeOpacity&&(c.opacity=0.1));b.wrap.animate(c,
{duration:"none"===d?0:a.closeSpeed,easing:a.closeEasing,step:e?this.step:null,complete:b._afterZoomOut})},changeIn:function(){var a=b.current,d=a.nextEffect,e=a.pos,c={opacity:1},f=b.direction,g;e.opacity=0.1;"elastic"===d&&(g="down"===f||"up"===f?"top":"left","down"===f||"right"===f?(e[g]=x(l(e[g])-200),c[g]="+=200px"):(e[g]=x(l(e[g])+200),c[g]="-=200px"));"none"===d?b._afterZoomIn():b.wrap.css(e).animate(c,{duration:a.nextSpeed,easing:a.nextEasing,complete:b._afterZoomIn})},changeOut:function(){var a=
b.previous,d=a.prevEffect,e={opacity:0.1},c=b.direction;"elastic"===d&&(e["down"===c||"up"===c?"top":"left"]=("up"===c||"left"===c?"-":"+")+"=200px");a.wrap.animate(e,{duration:"none"===d?0:a.prevSpeed,easing:a.prevEasing,complete:function(){f(this).trigger("onReset").remove()}})}};b.helpers.overlay={defaults:{closeClick:!0,speedOut:200,showEarly:!0,css:{},locked:!s,fixed:!0},overlay:null,fixed:!1,create:function(a){a=f.extend({},this.defaults,a);this.overlay&&this.close();this.overlay=f('<div class="fancybox-overlay"></div>').appendTo("body");
this.fixed=!1;a.fixed&&b.defaults.fixed&&(this.overlay.addClass("fancybox-overlay-fixed"),this.fixed=!0)},open:function(a){var d=this;a=f.extend({},this.defaults,a);this.overlay?this.overlay.unbind(".overlay").width("auto").height("auto"):this.create(a);this.fixed||(q.bind("resize.overlay",f.proxy(this.update,this)),this.update());a.closeClick&&this.overlay.bind("click.overlay",function(a){f(a.target).hasClass("fancybox-overlay")&&(b.isActive?b.close():d.close())});this.overlay.css(a.css).show()},
close:function(){f(".fancybox-overlay").remove();q.unbind("resize.overlay");this.overlay=null;!1!==this.margin&&(f("body").css("margin-right",this.margin),this.margin=!1);this.el&&this.el.removeClass("fancybox-lock")},update:function(){var a="100%",b;this.overlay.width(a).height("100%");H?(b=Math.max(z.documentElement.offsetWidth,z.body.offsetWidth),n.width()>b&&(a=n.width())):n.width()>q.width()&&(a=n.width());this.overlay.width(a).height(n.height())},onReady:function(a,b){f(".fancybox-overlay").stop(!0,
!0);this.overlay||(this.margin=n.height()>q.height()||"scroll"===f("body").css("overflow-y")?f("body").css("margin-right"):!1,this.el=z.all&&!z.querySelector?f("html"):f("body"),this.create(a));a.locked&&this.fixed&&(b.locked=this.overlay.append(b.wrap),b.fixed=!1);!0===a.showEarly&&this.beforeShow.apply(this,arguments)},beforeShow:function(a,b){b.locked&&(this.el.addClass("fancybox-lock"),!1!==this.margin&&f("body").css("margin-right",l(this.margin)+b.scrollbarWidth));this.open(a)},onUpdate:function(){this.fixed||
this.update()},afterClose:function(a){this.overlay&&!b.isActive&&this.overlay.fadeOut(a.speedOut,f.proxy(this.close,this))}};b.helpers.title={defaults:{type:"float",position:"bottom"},beforeShow:function(a){var d=b.current,e=d.title,c=a.type;f.isFunction(e)&&(e=e.call(d.element,d));if(p(e)&&""!==f.trim(e)){d=f('<div class="fancybox-title fancybox-title-'+c+'-wrap">'+e+"</div>");switch(c){case "inside":c=b.skin;break;case "outside":c=b.wrap;break;case "over":c=b.inner;break;default:c=b.skin,d.appendTo("body"),
H&&d.width(d.width()),d.wrapInner('<span class="child"></span>'),b.current.margin[2]+=Math.abs(l(d.css("margin-bottom")))}d["top"===a.position?"prependTo":"appendTo"](c)}}};f.fn.fancybox=function(a){var d,e=f(this),c=this.selector||"",k=function(g){var h=f(this).blur(),j=d,k,l;!g.ctrlKey&&(!g.altKey&&!g.shiftKey&&!g.metaKey)&&!h.is(".fancybox-wrap")&&(k=a.groupAttr||"data-fancybox-group",l=h.attr(k),l||(k="rel",l=h.get(0)[k]),l&&(""!==l&&"nofollow"!==l)&&(h=c.length?f(c):e,h=h.filter("["+k+'="'+l+
'"]'),j=h.index(this)),a.index=j,!1!==b.open(h,a)&&g.preventDefault())};a=a||{};d=a.index||0;!c||!1===a.live?e.unbind("click.fb-start").bind("click.fb-start",k):n.undelegate(c,"click.fb-start").delegate(c+":not('.fancybox-item, .fancybox-nav')","click.fb-start",k);this.filter("[data-fancybox-start=1]").trigger("click");return this};n.ready(function(){f.scrollbarWidth===r&&(f.scrollbarWidth=function(){var a=f('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo("body"),b=a.children(),
b=b.innerWidth()-b.height(99).innerWidth();a.remove();return b});if(f.support.fixedPosition===r){var a=f.support,d=f('<div style="position:fixed;top:20px;"></div>').appendTo("body"),e=20===d[0].offsetTop||15===d[0].offsetTop;d.remove();a.fixedPosition=e}f.extend(b.defaults,{scrollbarWidth:f.scrollbarWidth(),fixed:f.support.fixedPosition,parent:f("body")})})})(window,document,jQuery);