page.includeCSS {
  base = {$path.public}/Css/base.css
  main = {$path.public}/Css/main.css
  extensions = {$path.public}/Css/extensions.css
  fancybox = {$path.libs}/fancybox/source/jquery.fancybox.css
  cookiedata = {$path.public}/Css/css.css
}

page.includeJS {
  jquery = {$path.public}/Js/jquery-1.10.2.min.js
}
page.includeJSFooter {
  html5 = {$path.libs}/html5shiv/html5shiv.js
##  cookie = {$path.public}/Js/jquery.cookie.js
##  easing = {$path.public}/Js/jquery.easing.js
##  cycle = {$path.public}/Js/jquery.cycle.all.js
##  ui = {$path.public}/Js/jquery-ui-1.8.17.custom.min.js
##  fullsizebg = {$path.public}/Js/jquery.fullsizebackground.js
##  jpanel = {$path.public}/Js/jquery.jpanelmenu.min.js
  lazyload = {$path.public}/Js/jquery.lazyload.js
  functions = {$path.public}/Js/jquery.functions.js
  fancymousewheel = {$path.libs}/fancybox/lib/jquery.mousewheel-3.0.6.pack.js
  fancybox = {$path.libs}/fancybox/source/jquery.fancybox.pack.js
  cookiedata = {$path.public}/Js/js.js
}  

config {
  concatenateJs = 1
##  concatenateCss = 1
  compressJs = 1
##  compressCss = 1
}