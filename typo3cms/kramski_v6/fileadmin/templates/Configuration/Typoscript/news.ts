plugin.tt_news.usePiBasePagebrowser = 5
plugin.tt_news.pageBrowser {
	showRange = 0
	showResultCount = 0
	showFirstLast = 0
	hscText = 0
	inactiveLinkWrap = <span class="inactiveLinkWrap browseLink">|</span>
	activeLinkWrap = <span class="activeLinkWrap browseLink">|</span>
}

# tt_news date & time formats
plugin.tt_news {
  archiveTitleCObject {
    10.strftime = %B - %Y
  }
  getRelatedCObject {
      20.strftime = %d.%m.%Y %H:%M
  }
  displaySingle {
    date_stdWrap.strftime= %A %d. %B %Y
    time_stdWrap.strftime= %H:%M
  }
  displayLatest {
    date_stdWrap.strftime= %d. %B %Y
    time_stdWrap.strftime= %d.%m.%y %H:%M
  }
  displayList {
    date_stdWrap.strftime= %d. %B %Y
    time_stdWrap.strftime= %d.%m.%y %H:%M
  }
}

# Erlaube frontend Editing

plugin.tt_news {
      general_stdWrap.editPanel = 1
      general_stdWrap.editPanel {
      allow = edit,new,move,toolbar,hide,delete
      line=5
      label = %s
      onlyCurrentPid = 0
      previewBorder=4
      edit.displayRecord = 1
      }
   }

# Entfernen des <dd>|</dd> Wrap um die Jahreszahl im Archiv
plugin.tt_news.wrap3.wrap = 

# allow image caption in news latest 
plugin.tt_news.displayLatest.caption_stdWrap.if.directReturn = 1
plugin.tt_news.displayList.caption_stdWrap.if.directReturn = 0


# allow for more teaser text than std. and set default author
plugin.tt_news.displayLatest.subheader_stdWrap.crop = 300 | ... | 1

# template file in use 
plugin.tt_news.templateFile = {$path.private}/Templates/tt_news.html 

plugin.tt_news {
   _LOCAL_LANG.default {
    noNewsToListMsg =
    latestHeader = 
    more = mehr
    pi_list_browseresults_page = <span></span>
    pi_list_browseresults_first = <<
    pi_list_browseresults_prev = <
    pi_list_browseresults_next = >
    pi_list_browseresults_last = >>
 }
}
plugin.tt_news {
   _LOCAL_LANG.de {
    noNewsToListMsg =
    latestHeader = 
    more = mehr
    pi_list_browseresults_page = <span></span>
    pi_list_browseresults_first = <<
    pi_list_browseresults_prev = <
    pi_list_browseresults_next = >
    pi_list_browseresults_last = >>
 }
}
plugin.tt_news {
   _LOCAL_LANG.en {
    noNewsToListMsg =
    latestHeader = 
    more = more
    pi_list_browseresults_page = <span></span>
    pi_list_browseresults_first = <<
    pi_list_browseresults_prev = <
    pi_list_browseresults_next = >
    pi_list_browseresults_last = >>
 }
}
# Configure tt_news to display the xml template 
plugin.tt_news {
  displayXML {
    # rss091_tmplFile = EXT:tt_news/res/rss_0_91.tmpl
    # rdf_tmplFile = EXT:tt_news/res/rdf.tmpl
    # atom03_tmplFile = EXT:tt_news/res/atom_0_3.tmpl
    # atom1_tmplFile = EXT:tt_news/res/atom_1_0.tmpl
    rss2_tmplFile = {$path.private}/Templates/rss.tmpl

    # possibile values: rss091 / rss2 / rdf / atom03 / atom1  
    xmlFormat = rss2

    xmlTitle = {$sitename}: Aktuelle Nachrichten
    xmlLink = {$siteurl}
    xmlDesc = Aktuelle Nachrichten {$sitename}
    xmlLang = de
    xmlIcon = {$path.public}/Images/RSSLogo.png
    title_stdWrap.htmlSpecialChars = 1
    title_stdWrap.htmlSpecialChars.preserveEntities = 1
    subheader_stdWrap.stripHtml = 1
    subheader_stdWrap.htmlSpecialChars = 1
    subheader_stdWrap.htmlSpecialChars.preserveEntities = 1
    subheader_stdWrap.crop = 250 | ... | 1
    subheader_stdWrap.ifEmpty.field = bodytext
    xmlLastBuildDate = 1
  }
}

xmlnews = PAGE
xmlnews {
  typeNum = 100

  10 >
  10 < plugin.tt_news
  10.pid_list >
  10.pid_list = 8
  10.singlePid = 10
  10.defaultCode = XML

  config {
    disableAllHeaderCode = 1
    additionalHeaders = Content-type:text/xml
    no_cache = 1
    xhtml_cleaning = 0
  }
}
page.headerData.1200 = TEXT
page.headerData.1200 {
  wrap = <link rel="alternate" type="application/rss+xml" title="RSS-Feed" href="http://{$siteurl}/rss.xml">
}
plugin.tt_news {
  displayLatest.image.file {
    maxW = 500
    maxH = 500
    width = 500c
    height = 500c
  }
  displayList.image.file {
    maxW = 640
    maxH = 500
    width = 400c
    height = 
  }
  displaySingle.image.file {
    maxW = 640
    maxH = 640
    width = 640
  }
}
plugin.tt_news.displayLatest.subheader_stdWrap {
  append >
}
plugin.tt_news.displayList.subheader_stdWrap {
  append >
}
# ein Standardbild anzeigen falls kein Bild da ist
plugin.tt_news.displayLatest.image.noImage_stdWrap {
    cObject = IMAGE
    cObject {
        file = {$path.public}/Images/newsbild.jpg
        wrap =
        file.maxW = 500
    file.maxH = 500
    file.width = 500c
    file.height = 500c
    }
}
# ein Standardbild anzeigen falls kein Bild da ist
plugin.tt_news.displayList.image.noImage_stdWrap {
    cObject = IMAGE
    cObject {
        file = {$path.public}/Images/newsbild.jpg
        wrap =
        file.maxW = 640
    file.maxH = 500
    file.width = 400c
    file.height =
    }
}
plugin.tt_news.displayLatest.caption_stdWrap.if.directReturn = 0
config.absRefPrefix = http://{$siteurl}/

plugin.tt_news {
  altLayoutsOptionSplit = 0 |*| 1 |*| 2
}

plugin.tt_news.displaySingle {
  image.wrap = <div class="news-img-container"> |
  caption_stdWrap.wrap = <p class="news-single-imgcaption"> | </p></div>
  caption_stdWrap.dataWrap >
}

## hide caption in latest view
plugin.tt_news.displayLatest {
  caption_stdWrap.if.directReturn = 0
}

plugin.tt_news.excludeAlreadyDisplayedNews = 0 


plugin.tt_news {
  _LOCAL_LANG.de {
    catmenuHeader = Nachrichten
  }
}    
plugin.tt_news.displayCatMenu {
 catmenuItem_NO_stdWrap.wrap = <li>|</li>
 catmenuItem_ACT_stdWrap.wrap = <li class="act">|</li>
}
plugin.tt_news.displayCatMenu{
 catmenuIconMode = -1
 includePrototypeJS = 0
 mode = nestedWraps
 catmenuIconFile {
 width = 0
 height = 0
 }
}
plugin.tt_news.displayCatMenu.catOrderBy = title
plugin.tt_news.displayCatMenu.catmenuNoRootIcon = 1
plugin.tt_news.displayCatMenu.expandable = 0
plugin.tt_news.displayCatMenu.catmenu_stdWrap.wrap = <div class="news-catmenu"><ul>|</ul></div>
plugin.tt_news.displayCatMenu.catmenuLevel1_stdWrap.wrap = |
plugin.tt_news.displayCatMenu.catmenuLevel2_stdWrap.wrap = |
plugin.tt_news.excludeAlreadyDisplayedNews =0

plugin.tt_news.displayCatMenu.catmenuHeader_stdWrap.wrap = <span class="news-catmenu-header">|</span>

plugin.tt_news.genericmarkers {
	printbutton = TEXT
	printbutton {
		value = Rezept drucken
		wrap = <div class="col2 col100 bordertopbottom"><div class="col20 fleft"></div><div class="col80 fright">|</div></div>
		stdWrap.wrap = |
		stdWrap.typolinkno_cache = 1
		stdWrap.typolink.target = print
		stdWrap.typolink.ATagParams = target = _blank
		stdWrap.typolink.parameter.cObject = COA
		stdWrap.typolink.parameter.cObject {
	 
			5 = TEXT
			5.data = page:uid
			5.wrap = index.php?id=|
			5.required = 1
	 
			10 = TEXT
			10.value = &no_cache=1
	 
			20 = TEXT
			20.data = GP:L
			20.wrap = &L=|
			20.required = 1
	 
			30 = TEXT
			30.data = GP:tx_ttnews | backPid
			30.wrap = &tx_ttnews[backPid]=|
			30.required = 1
	 
			40 = TEXT
			40.data = GP:tx_ttnews | tt_news
			40.wrap = &tx_ttnews[tt_news]=|
			40.required = 1
	 
			50 = TEXT
			50.data = GP:cHash
			50.wrap = &cHash=|
			50.required = 1
	 
			60 = TEXT
			60.value = &type=98
		}
	}
}

subparts.content < styles.content.get
subparts.content.select.where = colPos = 1
   
print_page = PAGE
print_page {
  typeNum = 98
  bodyTag =<body onload="javascript:window.print()">
  30 = TEMPLATE
  30 {
    template = FILE 
    template.file = {$path.private}/Templates/print.html
    marks { 
      PAGE_TITLE = TEXT 
      PAGE_TITLE.field = title 
      PAGE_SUBTITLE = TEXT 
      PAGE_SUBTITLE.field = subtitle 
      PAGE_AUTHOR = TEXT 
      PAGE_AUTHOR.field = author 
      PAGE_AUTHOR.required=1 
      PAGE_AUTHOR.typolink.parameter.field = author_email 
      PAGE_UID = TEXT 
      PAGE_UID.field = uid 
      CONTENT < subparts.content
    } 
  workOnSubpart = DOCUMENT_BODY
  
  }
  includeCSS {
	base = {$path.public}/Css/base.css
	main = {$path.public}/Css/main.css
	extensions = {$path.public}/Css/extensions.css
  }  
}