<?php
/*******************************************************************
 *  Copyright notice
 *
 *  (c) 2012 Thomas Löffler <loeffler@spooner-web.de>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************/

/**
 * Link ViewHelper
 */
class Tx_Contentelements_ViewHelpers_LinkViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractTagBasedViewHelper {


	/**
	 *
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('headerLink', 'string', 'Link data from CE', FALSE, FALSE);
	}

	/**
	 * Renders a Link
	 *
	 * @param string $content Content to wrap
	 * @return string Wrapped content
	 */
	public function render($content = NULL) {
		if ($content === NULL) {
			$content = $this->renderChildren();
		}

		if ($this->hasArgument('headerLink')) {
			$local_cObj = t3lib_div::makeInstance('tslib_cObj');
			$content = $local_cObj->typolink($content, array('parameter' => $this->arguments['headerLink']));
		}

		return $content;
	}

}
?>