#############################
## render content elements ##
#############################
mod.wizards.newContentElement.renderMode = tabs
mod.wizards.newContentElement.wizardItems.common.show = text,image,dce_dceuid1,dce_dceuid2,dce_dceuid3,dce_dceuid4,dce_dceuid5,dce_dceuid6,dce_dceuid9,dce_dceuid11,table,dce_dceuid12
mod.wizards.newContentElement.wizardItems.special.show = shortcut,html,menu,div
mod.wizards.newContentElement.wizardItems.special {
	elements.shortcut {
		icon = gfx/c_wiz/regular_text.gif
		title = Datensatz einfügen
		description = Zeigt Datensätze von beliebiger Seite an
		tt_content_defValues {
			CType = shortcut	
		}		
	}
}