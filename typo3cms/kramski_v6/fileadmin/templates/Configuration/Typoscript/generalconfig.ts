config {
  ## Set DOCtype
  doctype = html5
  
  ## Clean XHTML Code
  xhtml_cleaning = all
  
  ## Removes comments around content elements
  disablePrefixComment = 1
 
  ## Language configuration ## 
  lang = de
  language = de
  locale_all = de_DE.utf-8
  htmlTag_langKey = de
  
  prefixLocalAnchors = all

  ## Activate Admin-Panel
  admPanel = 0

  ## Should be deactivated after going online!
  debug = 1
    
  ## Spam protection, encode email-address, exchanging @ to '(at)':
  spamProtectEmailAddresses = 1 
  spamProtectEmailAddresses_atSubst = &thinsp;(at)&thinsp;

  ## Save klicks on external links in table sys_stat 
  jumpurl = 1

  ## Enable indexedsearch also for extern files (pdf, doc, etc.)
  index_enable = 1         
  index_externals = 1     

  ## Save frontend user access
  tx_loginusertrack_enable = 1
  
  ## realurl config  
  baseURL = http://{$siteurl}/
  tx_realurl_enable = 1
  simulateStaticDocuments = 0
}

## Allow HTML tags in headers, table and caption
lib.stdheader.10.setCurrent.htmlSpecialChars = 0
tt_content.table.20.innerStdWrap.htmlSpecialChars = 0
tt_content.image.20.caption.1.1.htmlSpecialChars = 0

## Activate Scriptmerger
plugin.tx_scriptmerger.javascript.parseBody = 1


## die autovervollstaendigung der html objekte verhindern
tt_content.html.prefixComment >
tt_content.stdWrap.innerWrap.override = |
tt_content.stdWrap.innerWrap.override.if {
  equals = html
  value.field = CType
}

## add ke_stats to headerdata
page.headerData.555 < plugin.tx_kestats_pi1

## sitemap styles
tt_content.menu.20.2.1.wrap = <ul class="level1">|</ul>
tt_content.menu.20.2.2.wrap = <ul class="level2">|</ul>
tt_content.menu.20.2.3.wrap = <ul class="level3">|</ul>
tt_content.menu.20.2.4.wrap = <ul class="level4">|</ul>
tt_content.menu.20.2.5.wrap = <ul class="level5">|</ul>

tt_content.stdWrap.wrap {
   cObject = TEXT
   cObject.value = |
   override {
     cObject = TEXT
     cObject.value = <div class="inherit"> | </div>
     if.isTrue.field = inherit
   
   }
}


## make separator after h1
lib.stdheader.10.1.dataWrap = <h1{register:headerClass}>|</h1><div class="header-separator"></div>
lib.stdheader.10.2.dataWrap = <h2{register:headerClass}>|</h2><div class="header-separator"></div>