## Fileadmin Paths
path.public = fileadmin/templates/Resources/Public
path.private = fileadmin/templates/Resources/Private
path.configuration = fileadmin/templates/Configuration
path.libs = fileadmin/templates/Resources/Libs

## Languages
deutsch = 0
english = 1

## WICHTIG: URL der Website anpassen, z.B. fuer config.baseURL 
siteurl =  www.kramski.de

## Name des Internetauftrittes
sitename = KRAMSKI GmbH

## Kurze Beschreibung der Seite
sitedesc = KRAMSKI GmbH entwickelt und produziert technologisch sehr anspruchsvolle Stanz- und Spritzgießteile sowie Baugruppen und Werkzeuge für komplizierte Produkte. 

## ID der Startseite in der obersten Ebene
rootId = 1

PAGE_TARGET = 
styles.content.links.allowTags = a,abbr, acronym, address, blockquote, b, br, caption, center, cite,code,del, div, em, h1, h2, h3, h4, h5, h6, hr, i, img, li, link, ol, p,pre, q, span, strike, strong, sub, sup, table, thead, tbody, tfoot, td, th, tr, tt, u, ul, quote

## Bildunterschriften unter dem jeweiligen Bild 
styles.content.imgtext.imageTextSplit = 1

##für css validierung
styles.content.imgtext.borderSelector = DIV.csc-textpic-border DIV.csc-textpic-imagewrap .csc-textpic-image IMG
styles.content.imgtext.borderSpace= 0
styles.content.links.wrap = |
styles.content.textStyle.size = 1
styles.content.textStyle.altWrap = |
styles.content.imgtext.maxW = 2000
styles.content.imgtext.maxWInText = 0
styles.content.table.altWrap = |
styles.content.uploads.descriptionWrap = |
styles.content.loginform.pid = 0
styles.content.imgtext.linkWrap.width = 800
styles.content.imgtext.colSpace = 5
styles.content.imgtext.maxWInText = 2000


## Seo Konstanten
plugin.tq_seo.metaTags.description = KRAMSKI GmbH entwickelt und produziert technologisch sehr anspruchsvolle Stanz- und Spritzgießteile sowie Baugruppen und Werkzeuge für komplizierte Produkte. 
plugin.tq_seo.metaTags.keywords = KRAMSKI,putter,high precision, golf, accessories, pforzheim, stanzteile, verbundteile, werkzeuge, ersatzteile, schneidbuchsen, metall, kunststoff  
plugin.tq_seo.metaTags.author = KRAMSKI GmbH
plugin.tq_seo.metaTags.publisher = GKMB GmbH
plugin.tq_seo.metaTags.geoPositionLatitude = 48.902326
plugin.tq_seo.metaTags.geoPositionLongitude = 8.653963
plugin.tq_seo.metaTags.geoRegion = DE-BW
plugin.tq_seo.metaTags.geoPlacename = Pforzheim
plugin.tq_seo.metaTags.googleVerification = jU6jOVh2fIXz0AX5txSHgepSPuvmm-UNFALI3qHdeS0
plugin.tq_seo.sitemap.changeFrequency = 3
plugin.tq_seo.metaTags.robotsIndex = 1
plugin.tq_seo.metaTags.enableDC = 0
plugin.tq_seo.metaTags.useLastUpdate = 0
plugin.tq_seo.metaTags.linkGeneration = 0
plugin.tq_seo.metaTags.enableDC = 0
plugin.tq_seo.metaTags.useLastUpdate = 0
plugin.tq_seo.metaTags.revisit = 0
plugin.tq_seo.metaTags.useDetectLanguage = 0

## rgnewsce
plugin.tt_news.rgnewsce.displaySingle.renderWithCssStyledContent = 0
