# Setting up the language variable "L" to be passed along with links
config.linkVars = L(0-1)
config.uniqueLinkVars = 1

#values for default language
config.sys_language_uid = 0
config.language = de
config.locale_all = de_DE.utf-8
plugin.tx_indexedsearch._DEFAULT_PI_VARS.lang = 0
config.htmlTag_langKey = de

# English language, sys_language.uid = 1
[globalVar = GP:L = {$english}]
config.sys_language_uid = 1
config.language = en
config.locale_all = en_EN.utf-8
config.htmlTag_langKey = en
plugin.tx_indexedsearch._DEFAULT_PI_VARS.lang = 1
[global]

page.config.sys_language_error_pid = 55

plugin.tt_news.sys_language_mode = strict
plugin.tt_news.showNewsWithoutDefaultTranslation = 1