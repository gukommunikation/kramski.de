/*
* Created by Danny Schubert
*/
var prodID = 0;

function sendMail() {
	console.log("sendMail");
   
	sendEmail("danny@dannyschubert.de", "Test", "Hello World");
	console.log("send");
} 

function toogleConfigurator(){
  var e = document.getElementById("showConfigurator");
      if(e.style.display == 'block'){
         e.style.display = 'none';
         document.getElementById("configBackground").style.background = "#0099CC";
         document.getElementById("arrow").className = "arrowDown"
      } else{
         e.style.display = 'block';
         document.getElementById("configBackground").style.background = "#F5F5F3";
         document.getElementById("arrow").className = "arrowUp"
      }
}

function addItemToBasket(){

  var quantity = document.forms["product"]["quantity"].value;
  var d1 = document.forms["product"]["d1"].value;
  var d3 = document.forms["product"]["d3"].value;
  var stamp = document.forms["product"]["stamp"].value;
  var thickness = document.forms["product"]["thickness"].value;
  var strength = document.forms["product"]["strength"].value;

  var addProduct = document.getElementById("products").innerHTML;

  var ni = document.getElementById('products');
  var numi = document.forms["product"]["quantity"].value;
  var newdiv = document.createElement('div');


  var productPriceResult = getProductPrice(quantity, d1);
  prodID++;
  newdiv.setAttribute('id', prodID);
  newdiv.innerHTML =
  '<table>' +
  '<tr>' +
  '<td>' + 'Stückzahl:' + '</td>' +
  '<td id="pieces" class="alignRight">' + quantity + '</td>' +
  '</tr>' +
  '<tr>' +
  '<td>' + '&empty; d1:' + '</td>' +
  '<td id="d1" class="alignRight">' + d1 + ' mm</td>' +
  '</tr>' +
  '<tr>' +
  '<td>' + '&empty; d3:' + '</td>' +
  '<td id="d3" class="alignRight">' + d3 + ' mm</td>' +
  '</tr>' +
  '<tr>' +
  '<td>' + '&empty; Lochstempel:' + '</td>' +
  '<td id="stamp" class="alignRight">' + stamp + ' mm</td>' +
  '</tr>' +
  '<tr>' +
  '<td>' + 'Materialdicke:' + '</td>' +
  '<td id="thick" class="alignRight">' + thickness + ' mm</td>' +
  '</tr>' +
  '<tr>' +
  '<td>' + 'Materialfestigkeit:' + '</td>' +
  '<td id="strength" class="alignRight">' + strength + ' mm</td>' +
  '</tr>' +
  '<tr>' +
  '<td>' + 'Summe:' + '</td>' +
  '<td id="productprice' + prodID + '" class="alignRight">' + productPriceResult +'&euro;'+ '</td>' +
  '</tr>' +
  '<tr>' +
  '<td>' + '<a href="#configurator" onClick="changeBasketItem('+ prodID +')">Werte &auml;ndern &rarr; </a>' + '</td>' +
  '</tr>' +
  '<tr>' +
  '<td>' + '<a href="#orderOverview" onClick="removeItemFromBasket('+ prodID +')">Artikel l&ouml;schen &rarr; </a>' + '</td>' +
  '</tr>' +
  '</table>';

  getBasketPrice(productPriceResult);
  switchContactForm();
  clearForm();
  document.getElementById("orderOverview").style.display = "block";
  ni.appendChild(newdiv);
  //alert("Artikel in den Warenkorb gelegt.");orderOverview
}

function removeItemFromBasket(divID) {

  var d = document.getElementById('products');
  
  var olddiv = document.getElementById(divID);

  subtractBasketPrice(document.getElementById("productprice" + divID).innerHTML);
  d.removeChild(olddiv);

}

function changeBasketItem(divID){
  var d = document.getElementById('products');

  var changeDIV = document.getElementById(divID);

  document.forms["product"]["quantity"].value = parseInt(document.getElementById('pieces').innerHTML);
  document.forms["product"]["d1"].value = parseFloat(document.getElementById('d1').innerHTML);
  document.forms["product"]["d3"].value = parseFloat(document.getElementById('d3').innerHTML);
  document.forms["product"]["stamp"].value = parseFloat(document.getElementById('stamp').innerHTML);
  document.forms["product"]["thickness"].value = parseFloat(document.getElementById('thick').innerHTML);
  document.forms["product"]["strength"].value = parseFloat(document.getElementById('strength').innerHTML);


  subtractBasketPrice(document.getElementById("productprice" + divID).innerHTML);

  d.removeChild(changeDIV);
}

function setBasketPrice(price){
  document.getElementById("totalPrice").innerHTML = price;
}

function getBasketPrice(price){
  var currentPrice = document.getElementById("totalPrice").innerHTML;
  currentPrice = parseFloat(currentPrice);
  price = parseFloat(price);
  var newTotalPrice = currentPrice + price;
  document.getElementById("totalPrice").innerHTML = newTotalPrice;
}

function subtractBasketPrice(price){
  var currentPrice = document.getElementById("totalPrice").innerHTML;
  currentPrice = parseFloat(currentPrice);
  price = parseFloat(price);

  var newTotalPrice = currentPrice - price;
  
  if (newTotalPrice >= 0){
    document.getElementById("totalPrice").innerHTML = newTotalPrice;
  } else {
    document.getElementById("totalPrice").innerHTML = 0;
  }
}

function clearForm(){
  document.getElementById("product").reset();
}

function switchContactForm(){
  document.getElementById("agbCheck").style.display = "block";
  document.getElementById("orderText").style.display = "block";
  document.getElementById("contactTitle").innerHTML = "IHRE DATEN";
  document.getElementById("agbInput").required = true;
  document.getElementById("contactSubmit").value = "KOSTENPFLICHTIG BESTELLEN";
  document.getElementById("contactSubmit").setAttribute( "onClick", "ga('send', 'event', 'Bestellung', 'Abschicken');" );
}

function getProductPrice(quantity,gradation){
  var result = 0.0;
  var price = 0.0;

  // Piece-Count: 1; Gradation from 1.00 to 1.20
  if((gradation >= 1.00) && (gradation <= 1.20) && (quantity == 1)){
    price = 192.00;
  }else
  // Piece-Count: 2; Gradation from 1.00 to 1.20
  if((gradation >= 1.00) && (gradation <= 1.20) && (quantity == 2)){
    price = 175.00;
  }else
  // Piece-Count: 3-9; Gradation from 1.00 to 1.20
  if((gradation >= 1.00) && (gradation <= 1.20) && (quantity >= 3) && (quantity <= 9)){
    price = 153.00;
  }else
  // Piece-Count: 10-15; Gradation from 1.00 to 1.20
  if((gradation >= 1.00) && (gradation <= 1.20) && (quantity >= 10) && (quantity <= 15)){
    price = 145.00;
  }else
  // Piece-Count: 16-100; Gradation from 1.00 to 1.20
  if((gradation >= 1.00) && (gradation <= 1.20) && (quantity >= 16) && (quantity <= 100)){
    price = 138.00;
  } else

  // Piece-Count: 1; Gradation from 1.21 to 1.50
  if((gradation >= 1.21) && (gradation <= 1.50) && (quantity == 1)){
    price = 183.00;
  }else
  // Piece-Count: 2; Gradation from 1.21 to 1.50
  if((gradation >= 1.21) && (gradation <= 1.50) && (quantity == 2)){
    price = 169.00;
  }else
  // Piece-Count: 3-9; Gradation from 1.21 to 1.50
  if((gradation >= 1.21) && (gradation <= 1.50) && (quantity >= 3) && (quantity <= 9)){
    price = 146.00;
  }else
  // Piece-Count: 10-15; Gradation from 1.21 to 1.50
  if((gradation >= 1.21) && (gradation <= 1.50) && (quantity >= 10) && (quantity <= 15)){
    price = 139.00;
  }else
  // Piece-Count: 16-100; Gradation from 1.21 to 1.50
  if((gradation >= 1.21) && (gradation <= 1.50) && (quantity >= 16) && (quantity <= 100)){
    price = 132.00;
  } else

  // Piece-Count: 1; Gradation from 1.51 to 2.00
  if((gradation >= 1.51) && (gradation <= 2.00) && (quantity == 1)){
    price = 178.00;
  }else
  // Piece-Count: 2; Gradation from 1.51 to 2.00
  if((gradation >= 1.51) && (gradation <= 2.00) && (quantity == 2)){
    price = 162.00;
  }else
  // Piece-Count: 3-9; Gradation from 1.51 to 2.00
  if((gradation >= 1.51) && (gradation <= 2.00) && (quantity >= 3) && (quantity <= 9)){
    price = 142.00;
  }else
  // Piece-Count: 10-15; Gradation from 1.51 to 2.00
  if((gradation >= 1.51) && (gradation <= 2.00) && (quantity >= 10) && (quantity <= 15)){
    price = 135.00;
  }else
  // Piece-Count: 16-100; Gradation from 1.51 to 2.00
  if((gradation >= 1.51) && (gradation <= 2.00) && (quantity >= 16) && (quantity <= 100)){
    price = 128.00;
  } else

  // Piece-Count: 1; Gradation from 2.01 to 3.00
  if((gradation >= 2.01) && (gradation <= 3.00) && (quantity == 1)){
    price = 170.00;
  }else
  // Piece-Count: 2; Gradation from 2.01 to 3.00
  if((gradation >= 2.01) && (gradation <= 3.00) && (quantity == 2)){
    price = 156.00;
  }else
  // Piece-Count: 3-9; Gradation from 2.01 to 3.00
  if((gradation >= 2.01) && (gradation <= 3.00) && (quantity >= 3) && (quantity <= 9)){
    price = 137.00;
  }else
  // Piece-Count: 10-15; Gradation from 2.01 to 3.00
  if((gradation >= 2.01) && (gradation <= 3.00) && (quantity >= 10) && (quantity <= 15)){
    price = 129.00;
  }else
  // Piece-Count: 16-100; Gradation from 2.01 to 3.00
  if((gradation >= 2.01) && (gradation <= 3.00) && (quantity >= 16) && (quantity <= 100)){
    price = 123.00;
  } else

  // Piece-Count: 1; Gradation from 3.01 to 4.00
  if((gradation >= 3.01) && (gradation <= 4.00) && (quantity == 1)){
    price = 161.00;
  }else
  // Piece-Count: 2; Gradation from 3.01 to 4.00
  if((gradation >= 3.01) && (gradation <= 4.00) && (quantity == 2)){
    price = 148.00;
  }else
  // Piece-Count: 3-9; Gradation from 3.01 to 4.00
  if((gradation >= 3.01) && (gradation <= 4.00) && (quantity >= 3) && (quantity <= 9)){
    price = 129.00;
  }else
  // Piece-Count: 10-15; Gradation from 3.01 to 4.00
  if((gradation >= 3.01) && (gradation <= 4.00) && (quantity >= 10) && (quantity <= 15)){
    price = 123.00;
  }else
  // Piece-Count: 16-100; Gradation from 3.01 to 4.00
  if((gradation >= 3.01) && (gradation <= 4.00) && (quantity >= 16) && (quantity <= 100)){
    price = 116.00;
  } else

  // Piece-Count: 1; Gradation from 4.01 to 5.00
  if((gradation >= 4.01) && (gradation <= 5.00) && (quantity == 1)){
    price = 170.00;
  }else
  // Piece-Count: 2; Gradation from 4.01 to 5.00
  if((gradation >= 4.01) && (gradation <= 5.00) && (quantity == 2)){
    price = 156.00;
  }else
  // Piece-Count: 3-9; Gradation from 4.01 to 5.00
  if((gradation >= 4.01) && (gradation <= 5.00) && (quantity >= 3) && (quantity <= 9)){
    price = 137.00;
  }else
  // Piece-Count: 10-15; Gradation from 4.01 to 5.00
  if((gradation >= 4.01) && (gradation <= 5.00) && (quantity >= 10) && (quantity <= 15)){
    price = 129.00;
  }else
  // Piece-Count: 16-100; Gradation from 4.01 to 5.00
  if((gradation >= 4.01) && (gradation <= 5.00) && (quantity >= 16) && (quantity <= 100)){
    price = 123.00;
  } else

  // Piece-Count: 1; Gradation from 5.01 to 6.00
  if((gradation >= 5.01) && (gradation <= 6.00) && (quantity == 1)){
    price = 178.00;
  }else
  // Piece-Count: 2; Gradation from 5.01 to 6.00
  if((gradation >= 5.01) && (gradation <= 6.00) && (quantity == 2)){
    price = 162.00;
  }else
  // Piece-Count: 3-9; Gradation from 5.01 to 6.00
  if((gradation >= 5.01) && (gradation <= 6.00) && (quantity >= 3) && (quantity <= 9)){
    price = 142.00;
  }else
  // Piece-Count: 10-15; Gradation from 5.01 to 6.00
  if((gradation >= 5.01) && (gradation <= 6.00) && (quantity >= 10) && (quantity <= 15)){
    price = 135.00;
  }else
  // Piece-Count: 16-100; Gradation from 5.01 to 6.00
  if((gradation >= 5.01) && (gradation <= 6.00) && (quantity >= 16) && (quantity <= 100)){
    price = 128.00;
  } else

  // Piece-Count: 1; Gradation from 6.01 to 8.00
  if((gradation >= 6.01) && (gradation <= 8.00) && (quantity == 1)){
    price = 184.00;
  }else
  // Piece-Count: 2; Gradation from 6.01 to 8.00
  if((gradation >= 6.01) && (gradation <= 8.00) && (quantity == 2)){
    price = 170.00;
  }else
  // Piece-Count: 3-9; Gradation from 6.01 to 8.00
  if((gradation >= 6.01) && (gradation <= 8.00) && (quantity >= 3) && (quantity <= 9)){
    price = 147.00;
  }else
  // Piece-Count: 10-15; Gradation from 6.01 to 8.00
  if((gradation >= 6.01) && (gradation <= 8.00) && (quantity >= 10) && (quantity <= 15)){
    price = 140.00;
  }else
  // Piece-Count: 16-100; Gradation from 6.01 to 8.00
  if((gradation >= 6.01) && (gradation <= 8.00) && (quantity >= 16) && (quantity <= 100)){
    price = 133.00;
  } else

  // Piece-Count: 1; Gradation from 8.01 to 10.00
  if((gradation >= 8.01) && (gradation <= 10.00) && (quantity == 1)){
    price = 200.00;
  }else
  // Piece-Count: 2; Gradation from 8.01 to 10.00
  if((gradation >= 8.01) && (gradation <= 10.00) && (quantity == 2)){
    price = 184.00;
  }else
  // Piece-Count: 3-9; Gradation from 8.01 to 10.00
  if((gradation >= 8.01) && (gradation <= 10.00) && (quantity >= 3) && (quantity <= 9)){
    price = 160.00;
  }else
  // Piece-Count: 10-15; Gradation from 8.01 to 10.00
  if((gradation >= 8.01) && (gradation <= 10.00) && (quantity >= 10) && (quantity <= 15)){
    price = 153.00;
  }else
  // Piece-Count: 16-100; Gradation from 8.01 to 10.00
  if((gradation >= 8.01) && (gradation <= 10.00) && (quantity >= 16) && (quantity <= 100)){
    price = 145.00;
  } else

  // Piece-Count: 1; Gradation from 10.01 to 12.00
  if((gradation >= 10.01) && (gradation <= 12.00) && (quantity == 1)){
    price = 208.00;
  }else
  // Piece-Count: 2; Gradation from 10.01 to 12.00
  if((gradation >= 10.01) && (gradation <= 12.00) && (quantity == 2)){
    price = 192.00;
  }else
  // Piece-Count: 3-9; Gradation from 10.01 to 12.00
  if((gradation >= 10.01) && (gradation <= 12.00) && (quantity >= 3) && (quantity <= 9)){
    price = 166.00;
  }else
  // Piece-Count: 10-15; Gradation from 10.01 to 12.00
  if((gradation >= 10.01) && (gradation <= 12.00) && (quantity >= 10) && (quantity <= 15)){
    price = 158.00;
  }else
  // Piece-Count: 16-100; Gradation from 10.01 to 12.00
  if((gradation >= 10.01) && (gradation <= 12.00) && (quantity >= 16) && (quantity <= 100)){
    price = 150.00;
  } else

  // Piece-Count: 1; Gradation from 12.01 to 15.00
  if((gradation >= 12.01) && (gradation <= 15.00) && (quantity == 1)){
    price = 215.00;
  }else
  // Piece-Count: 2; Gradation from 12.01 to 15.00
  if((gradation >= 12.01) && (gradation <= 15.00) && (quantity == 2)){
    price = 197.00;
  }else
  // Piece-Count: 3-9; Gradation from 12.01 to 15.00
  if((gradation >= 12.01) && (gradation <= 15.00) && (quantity >= 3) && (quantity <= 9)){
    price = 172.00;
  }else
  // Piece-Count: 10-15; Gradation from 12.01 to 15.00
  if((gradation >= 12.01) && (gradation <= 15.00) && (quantity >= 10) && (quantity <= 15)){
    price = 164.00;
  }else
  // Piece-Count: 16-100; Gradation from 12.01 to 15.00
  if((gradation >= 12.01) && (gradation <= 15.00) && (quantity >= 16) && (quantity <= 100)){
    price = 155.00;
  }

  // calcualate final price of product
  result = quantity * price;
  return result;
}

$('document').ready(function() {
	$("#mailSender").submit(function (e) {
    e.preventDefault();
	var form_data = $(this).serialize(); 
    var count = 0;
	var countID = 1;
	
	if($('#1').length != 0){
		$.each( $('#orderOverview'), function(i, left) {
			
			// extract total price from basekt
			form_data += "&totalPrice=" + $(this).find("#totalPrice").text() + " €";
			form_data += "&products=";
			// extracting products from basket
			$('div > div',left).each(function() {
				if(count != 0){
				
				form_data += 	"<br\><b>Produkt:</b><br\><br\>" + 
								"<b>Stückzahl: </b>" + $(this).find("#pieces").text() + "<br\>" + 
								"<b>D1: </b>" + $(this).find("#d1").text() + "<br\>" + 
								"<b>D3: </b>" + $(this).find("#d3").text() + "<br\>" + 
								"<b>Lochstempel: </b>" + $(this).find("#stamp").text() + "<br\>" + 
								"<b>Materialdicke: </b>" + $(this).find("#thick").text() + "<br\>" + 
								"<b>Materialfestigkeit: </b>" + $(this).find("#strength").text() + "<br\>" + 
								"<b>Produktpreis: </b>" + $(this).find("#productprice" + countID).text() + "<br\>";  
								
				countID += 1;
				} else{
					count = 1;
				}
		   });
		})
	} 
	
	$.ajax({
      type: "POST", 
      url: "php/mail.php", 
      dataType: "json", // Add datatype
      data: form_data,
    }).done(function (data) {
        if($('#1').length != 0){
			alert("Danke für Ihre Bestellung. Wir werden uns in Kürze bei Ihnen melden.");
		} else{
			alert("Danke für Ihre Nachricht. Wir werden uns in Kürze bei Ihnen melden.");
		}
    }).fail(function (data) {
		alert("Ein unvorhergesehener Fehler ist aufgetreten. Bitte versuchen Sie es zu einem späteren Zeitpunkt nochmal.");
    });
  }); 
});