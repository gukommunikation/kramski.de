// jquery on document ready
jQuery(document).ready(function() {
  delinkTel();
  scrollTop();
  hoverbox();
  headerSlideshow();
  paddingImagetextbox();
  // $('img.lazy').removeAttr("src").removeAttr("alt");
  // $("img.lazy").lazyload();

  getThirdlevelmenu();
  resizeThirdlevelmenu();
  imagetextboxDetail();
  mobilemenu();
  buttonboxHeight();

  slideCol6();
  navCol6nav3items();
  navCol6nav1item();

  centerHeader();
  jQuery(".fancybox").fancybox();

  // change class of first catmenu element
  $('.news-catmenu li').first().addClass('act');
  $('.news-catmenu li:not(:eq(0))').each(function() {
    if($(this).hasClass('act')) {
      $('.news-catmenu li').first().removeClass('act');
    }
  });

  var searchwidth = $(window).width() - 58;
  $('.search-searchfield').width(searchwidth);
  $('.buttonbox.noimageleft').each(function() {
    element = $(this);
    element.find('.standard').height('auto');
    element.find('.hover').height('auto');
    var height = element.find('.standard').height();
    var width = element.find('.standard').width();
    var ratio = width / height;
    resizeButtonboxNoimageleft(element,ratio);
  });

  // hide border if downloadlist is empty
  $('.tx-rtgfiles-pi1').each(function() {
    if(!($(this).find('.downloadlist-wrap').length)) {
      $(this).css('border','none');
    }
  });

  // set csc-default margin 0 for only header
  $('header').each(function() {
    if(($(this).parent().hasClass('csc-default')) && !$(this).next().html()) {
      $(this).parent().css('margin',0);
    }
  });
  $('.blue-container header').each(function() {
    $(this).parent().css('margin-bottom','1.5em');
  });
});

// jquery window on resize
jQuery(window).resize(function() {
  setImageWidth();
  paddingImagetextbox();
  resizeImagetextbox();
  imageTextboxOrder();
  iconbox();

  slideCol6();
  //navCol6nav3items();
 // navCol6nav1item();


  if ($(window).width() >= 624) {
  $('#mobilemenu').hide();
  } else {
    if($('#mobilemenu-button').hasClass('up')) {
    $('#mobilemenu').show();
    }
  }
  var searchwidth = $(window).width() - 78;
  $('.search-searchfield').width(searchwidth);
  hoverboxResize();
   buttonboxHeight();
   $('.buttonbox.noimageleft').each(function() {
  element = $(this);
  element.find('.standard').height('auto');
  element.find('.hover').height('auto');
  var height = element.find('.standard').height();
  var width = element.find('.standard').width();
  var ratio = width / height;
  resizeButtonboxNoimageleft(element,ratio);
   });
  resizeThirdlevelmenu();
  centerHeader();

  resizeNewsGrids();
});

// jquery window on load functions
jQuery(window).load(function(){

  imageTextboxOrder();
  imagetextboxSlideshow();
  setImageWidth();
  resizeImagetextbox();
  hoverboxResize();
  buttonboxHeight();
  iconbox();

  $('.iconbox-inner').each(function() {
    var iconbox = $(this);
    iconbox.click(function() {
    if ($(window).width() <= 624) {
      iconbox.find('.iconbox-bodytext').toggle();
      iconbox.toggleClass('up');
    }
    });
  });
  splitheader();
  $('.showafterload').css('visibility','visible');
  centerHeader();
  resizeNewsGrids();
});

$(window).scroll(function() {
  var offset = $('.headerarea').offset();
  offset = offset.top + $('.headerarea').height()-155;
  if (window.pageYOffset >= offset) {
    $('#thirdlevelmenu').addClass('fixed');
  } else {
    $('#thirdlevelmenu').removeClass('fixed');
  }
});

function resizeNewsGrids() {
	var winwidth = Math.max( $(window).width(), window.innerWidth);
	if(winwidth > 900) {
		$('.gridcontainer-news .col100').each(function() {
			var container = $(this);
			container.find('.news-latest-container').outerHeight('inherit');
			container.find('.buttonbox').outerHeight('inherit');
			if((container.find('> .col30').outerHeight() - 21) > container.find('.news-latest-container-inner').outerHeight()) {
				container.find('.news-latest-container').outerHeight(container.find('> .col30').outerHeight()-21);
			} else {
				var boxheight = (container.find('.news-latest-container').outerHeight() - 21) / 2;
				container.find('.buttonbox').outerHeight(boxheight);
			}
		});
	}
}

function centerHeader() {
  if(($(window).width() <= 1600) && ($(window).width() > 784)) {
    $('.headerarea .headerbox').each(function() {
      var left = '-' + (($(this).find('.notmobile').width() - $(window).width()) / 2) + 'px';
      $(this).find('.notmobile').css('left',left);
    });
    $('.slide-header').each(function() {
      if($(this).find('.notmobile').width()) {
        var left = '-' + (($(this).find('.notmobile').width() - $(window).width()) / 2) + 'px';
      }
      $(this).find('.notmobile').css('left',left);
    });
  } else {
    if($(window).width() > 1600) {
      $('.headerarea .headerbox').each(function() {
        if($(this).find('.notmobile').width()) {
          var left = (($(window).width() - $(this).find('.notmobile').width()) / 2) + 'px';
        }
        $(this).find('.notmobile').css('left',left);
      });
      $('.slide-header').each(function() {
        $(this).find('.notmobile').css('left',0);
      });
    } else {
      $('.headerarea .headerbox').find('.notmobile').css('left',0);
    }
  }
  if(($(window).width() <= 1280) && ($(window).width() > 784)) {
    $('.contentarea .headerbox').each(function() {
      var left = '-' + (($(this).find('.notmobile').width() - $(window).width()) / 2) + 'px';
      $(this).find('.notmobile').css('left',left);
    });
  } else {
    $('.contentarea .headerbox').find('.notmobile').css('left',0);
  }

  $('.headerbox-container').each(function() {
    element = $(this).find('.headerbox');
    if($(window).width() <= 784) {
      // set height for mobile header
      var height = element.find('.mobile').height();
      if(height > 0) {
        element.height(height);
        element.find('.headerbox-content').height(height);
      }
      // create mobile box and move bodytext
      element.find('.headerbox-separator').hide();

      element.next('.headerbox-mobile').show();
      element.next('.headerbox-mobile').find('.headerbox-bodytext').show();
      element.next('.headerbox-mobile').find('.headerbox-headlineindividual').show();
      element.next('.headerbox-mobile').find('.headerbox-linktext').show();

      var bodytext = '<div class="headerbox-bodytext">' + element.find('.headerbox-bodytext').html() + '</div>';
      var textbox = '<div class="headerbox-mobile"><div class="headerbox-mobiletext-wrap inherit"></div></div>';
      // set textbox if any content
      element.find('.headerbox-bodytext').hide();
      if(!element.next().hasClass('headerbox-mobile')) {
        if(element.find('.headerbox-bodytext').html() || element.find('.headerbox-headlineindividual').html() || element.find('.headerbox-headline').html()) {
          element.after(textbox);
        }
      }
      // set bodytext if exists
      if(!element.next('.headerbox-mobile').find('.headerbox-bodytext').html()) {
        if(element.find('.headerbox-bodytext').html()) {
          element.next('.headerbox-mobile').find('.headerbox-mobiletext-wrap').append(bodytext);
        }
      }

      // move individual text
      if(element.find('.headerbox-linktext').html()) {
        var linktext = '<div class="headerbox-linktext"><br />' + element.find('.headerbox-linktext').html() + '</div>';
      }
      element.find('.headerbox-linktext').hide();
      if(!element.next('.headerbox-mobile').find('.headerbox-linktext').html()) {
        if(element.find('.headerbox-linktext').html()) {
          element.next('.headerbox-mobile').find('.headerbox-mobiletext-wrap').append(linktext);
        }
      }

      // move linktext
      if(element.find('.headerbox-headlineindividual').html()) {
        var individual = '<div class="headerbox-headlineindividual">' + element.find('.headerbox-headlineindividual').html() + '</div>';
      }
      element.find('.headerbox-headlineindividual').hide();
      if(!element.next('.headerbox-mobile').find('.headerbox-headlineindividual').html()) {
        if(element.find('.headerbox-headlineindividual').html()) {
          element.next('.headerbox-mobile').find('.headerbox-mobiletext-wrap').prepend(individual);
          // remove br at beginning of individual
          element.next('.headerbox-mobile').find('.headerbox-headlineindividual span:first-child br:lt(2)').remove();
        }
      }

    } else {
      // move bodytext and hide mobile box
      element.find('.headerbox-separator').show();
      element.find('.headerbox-bodytext').show();
      element.next('.headerbox-mobile').hide();

      // move individual text
      element.find('.headerbox-headlineindividual').show();
      element.next('.headerbox-mobile').find('.headerbox-headlineindividual').hide();

      // move linktext
      element.find('.headerbox-linktext').show();
      element.next('.headerbox-mobile').find('.headerbox-linktext').hide();

      // set height
      element.attr("height","").css('height','');
      element.closest('.slide-header').attr("height","").css('height','');
      element.find('.headerbox-content').attr("height","").css('height','');
    }
    if($(window).width() <= 640) {

      element.next('.headerbox-mobile').find('.headerbox-headline').show();

      // move headline
      if(element.find('.headerbox-headline').html()) {
        var headline = '<h1 class="headerbox-headline">' + element.find('.headerbox-headline').html() + '</h1>';
      }
      element.find('.headerbox-headline').hide();
      if(!element.next('.headerbox-mobile').find('.headerbox-headline').html()) {
        if(element.find('.headerbox-headline').html()) {
          element.next('.headerbox-mobile').find('.headerbox-mobiletext-wrap').prepend(headline);
        }
      }

      // show textbox if individual headline is set
      if(element.find('.headerbox-headlineindividual').html()) {
          element.next('.headerbox-mobile').show();
      }
    } else {
      // move headline
      element.find('.headerbox-headline').show();
      element.next('.headerbox-mobile').find('.headerbox-headline').hide();
    }
  });

  // set slideshow heightvar height = element.find('.mobile').height();
  if($(window).width() <= 785) {
    var newheight = $(window).width() / 2;
    sliderheight = newheight + $('.slide-header .headerbox-container.active').find('.headerbox-mobile').height();
    if($('.slide-header .headerbox-container.active').find('.headerbox-mobile').height() > 0) {
      sliderheight = newheight + $('.slide-header .headerbox-container.active').find('.headerbox-mobile').height();
    }
    $('.slide-header .headerbox-container').closest('.slide-header').height(sliderheight);
    $('.slide-header .headerbox-container').find('.headerbox').height(newheight);
    $('.slide-header .headerbox-container').height(sliderheight);
    $('.slide-header .headerbox-container').find('.headerbox-content').height(newheight);
    //set top position of arrows
    var top = newheight / 2 + 'px';
    $('.slide-prev,.slide-next').css('top',top);
  } else {
    $('.slide-prev,.slide-next').css('top','50%');
  }
}

function splitheader() {
  $(".splittedheader").each(function() {
        var headerContent = $(this).text().split(' ');
        for (var i = 0; i < headerContent.length; i++)
        {
            headerContent[i] = '<span class="subhead">' + headerContent[i] + '</span>';
        }
        $(this).html(headerContent.join(''));
    text = $(this).html();
    newtext = text.replace(/<br>|<BR>/g,'</span><br /><span class="subhead">');
    $(this).html(newtext);
    });
}


// make slideshow for header
function headerSlideshow() {
  $('.slide-header').each(function() {
    var slider = $(this);
    var nrOfItems = slider.find('.headerbox-container').size();
    var slidernav = '<div class="slider-nav"><ul class="menu">';
    var itemNr = 1;
    slider.find('.headerbox-container').each(function() {
      var itemClass = 'slide-' + itemNr;
      $(this).addClass(itemClass);
      slidernav += '<li class="slide-' + itemNr + '"><div class="boppel"></div></li>';
    $(this).css('z-index',itemNr);
    if(itemNr != 1) {
    $(this).hide();
    }
      itemNr++;
    });
    slidernav += '</ul></div>';
    slider.after(slidernav);
    slider.parent().find('.slider-nav li').click(function() {
    var navElement = $(this).attr('class');
    var className = navElement.match(/slide-([^ ]+)/);
    className = '.' + className[0];
    if(!$(this).hasClass('actnav')) {
      slider.parent().find('.headerbox-container').fadeOut(1000);
      slider.parent().find('.headerbox-container').removeClass('active');

      slider.find(className).addClass('active');
      slider.find(className).fadeIn(1000);
    }
    slider.parent().find('.slider-nav li').removeClass('actnav');
    $(this).addClass('actnav');
    });
    slider.parent().find('.slide-prev').click(function() {
    var slideract = slider.parent().find('.headerbox-container.active');
    var slidernav = slider.parent().find('.slider-nav li.actnav');
    var act = slideract;
    var actnav = slidernav;
    var next = slideract.prev('.headerbox-container');
    var nextnav = slidernav.prev();
    if( next.length == 0 ){
      next = slider.parent().find('.headerbox-container:last');
    }
    if( nextnav.length == 0 ){
      nextnav = slider.parent().find('.slider-nav li:last');
    }
    act.removeClass('active').fadeOut(500);
    actnav.removeClass('actnav');
    next.fadeIn(500);
    next.addClass('active');
    nextnav.addClass('actnav');
    });
    slider.parent().find('.slide-next').click(function() {
    var slideract = slider.parent().find('.headerbox-container.active');
    var slidernav = slider.parent().find('.slider-nav li.actnav');
    var act = slideract;
    var actnav = slidernav;
    var next = slideract.next('.headerbox-container');
    var nextnav = slidernav.next();
    if( next.length == 0 ){
      next = slider.parent().find('.headerbox-container:first');
    }
    if( nextnav.length == 0 ){
      nextnav = slider.parent().find('.slider-nav li:first');
    }
    act.removeClass('active').fadeOut(500);
    actnav.removeClass('actnav');
    next.fadeIn(500);
    next.addClass('active');
    nextnav.addClass('actnav');
    });
  // set slideshow heightvar height = element.find('.mobile').height();
  if($(window).width() <= 785) {
    var newheight = $(window).width() / 2;
    sliderheight = newheight + $('.slide-header .headerbox-container.active').find('.headerbox-mobile').height();
    if($('.slide-header .headerbox-container.active').find('.headerbox-mobile').height() > 0) {
      sliderheight = newheight + $('.slide-header .headerbox-container.active').find('.headerbox-mobile').height();
    }
    $('.slide-header .headerbox-container').closest('.slide-header').height(sliderheight);
    $('.slide-header .headerbox-container').find('.headerbox').height(newheight);
    $('.slide-header .headerbox-container').height(sliderheight);
    $('.slide-header .headerbox-container').find('.headerbox-content').height(newheight);
    //set top position of arrows
    var top = newheight / 2 + 'px';
    $('.slide-prev,.slide-next').css('top',top);
  } else {
    $('.slide-prev,.slide-next').css('top','50%');
  }

    rotateImages();
  $('.slide-navilr').css('visibility','visible');

  });
}
function rotateImages(){

  var slider = $('.slide-header .headerbox-container.active');
  var slidernav = $('.slider-nav li.actnav');
  var act = slider;
  var actnav = slidernav;
  var next = slider.next('.headerbox-container');
  var nextnav = slidernav.next();

  // set slideshow heightvar height = element.find('.mobile').height();
  if($(window).width() <= 785) {
    var newheight = $(window).width() / 2;
    sliderheight = newheight + slider.find('.headerbox-mobile').height();
    if(slider.find('.headerbox-mobile').height() > 0) {
      sliderheight = newheight + slider.find('.headerbox-mobile').height();
    }
    slider.closest('.slide-header').height(sliderheight);
    slider.find('.headerbox').height(newheight);
    slider.height(sliderheight);
    slider.find('.headerbox-content').height(newheight);
    //set top position of arrows
    var top = newheight / 2 + 'px';
    $('.slide-prev,.slide-next').css('top',top);
  } else {
    $('.slide-prev,.slide-next').css('top','50%');
  }

  if( next.length == 0 ){
    next = $('.slide-header .headerbox-container:first');
  }
  if( nextnav.length == 0 ){
    nextnav = $('.slider-nav li:first');
  }



  next.fadeIn(500);
  next.addClass('active');
  nextnav.addClass('actnav');
  act.fadeOut(500).removeClass('active');

  actnav.removeClass('actnav');
  setTimeout('rotateImages()',10000);
}

function resizeThirdlevelmenu() {
  if ($(window).width() <= 624) {
    $('#thirdlevelmenu li.item').hide();
    $('#thirdlevelmenu ul').removeClass('up');
    $('#thirdlevelmenu li.item').click(function() {
      if ($(window).width() <= 624) {
        $('#thirdlevelmenu li.item').hide();
        $('#thirdlevelmenu ul').removeClass('up');
        return false;
      }
    });
  } else {
    $('#thirdlevelmenu li.item').show();
    $('#thirdlevelmenu ul').addClass('up');
    $('#thirdlevelmenu li.item').click(function() {
      if ($(window).width() > 624) {
        $('#thirdlevelmenu li.item').show();
        $('#thirdlevelmenu ul').addClass('up');
        return false;
      }
    });
  }
}

function resizeButtonboxNoimageleft(element,ratio) {
  var newheight = element.find('.buttonbox-imagewrap').width() / ratio;
  element.find('.hover').height(newheight).css('max-height',newheight);
  element.find('.hover').width(element.find('.buttonbox-imagewrap').width()).css('max-width',element.find('.buttonbox-imagewrap').width());
  element.find('.standard').height(newheight).css('max-height',newheight);
  element.find('.standard').width(element.find('.buttonbox-imagewrap').width()).css('max-width',element.find('.buttonbox-imagewrap').width());
}

function buttonboxHeight() {
  $('.col2.col100').each(function() {
    var width = $(this).find('.buttonbox.noimageleft:last').width();
	$(this).find('.col50').width(width);
  });
  $('.col100').each(function() {
    var maxh = 0;
    var width = $(this).find('.buttonbox.noimageleft:last').width();
    $(this).find('.buttonbox.noimageleft').each(function() {
      var height = $(this).find('.standard').height();
      $(this).find('.buttonbox-imagewrap').height(height);
      $(this).find('.buttonbox-content-inner').height('auto');
      var height = $(this).find('.buttonbox-content-inner').height();
      if(height > maxh) {
        maxh = height;
      }
      $(this).find('.buttonbox-imagewrap').width(width);
      $(this).find('.buttonbox-linkwrap').width(width);
    });
    $(this).find('.buttonbox.noimageleft .buttonbox-content-inner').height(maxh);
  });
}

function hoverboxResize() {
  $('.buttonbox.imageleft').each(function() {
    var height = $(this).find('.standard').height();
    var width = $(this).find('.buttonbox-imagewrap').width();
    var ratio = width / height;
    var heightnew = width / ratio;
    $(this).find('.buttonbox-imagewrap').height(heightnew);
    //$(this).find('.buttonbox-imagewrap img').width(width);
    //$(this).find('.buttonbox-imagewrap img').height(heightnew);
  });
  if ($(window).width() >= 900) {
    $('.buttonbox.imageleft').each(function() {
      var height = $(this).find('.standard').height();
      $(this).find('.buttonbox-content').height(height);
      $(this).find('.buttonbox-imagewrap img').height('auto').css({
        'width' : 'auto',
        'max-width' : '100%',
        'left' : '0',
        'height' : 'auto'
      });
    });
  } else {
    $('.buttonbox.imageleft').each(function() {
      var height = $(this).find('.standard').height();
      var width = $(this).find('.standard').width();
      $(this).find('.buttonbox-content').height(height);
      if(height < $(this).find('.buttonbox-content').height()) {
        left = '-' + ((width - $(this).find('.buttonbox-imagewrap').width()) / 2) + 'px';
        $(this).find('.buttonbox-imagewrap img').height($(this).find('.buttonbox-content').height()).css({
          'width' : 'auto',
          'max-width' : 'none',
          'left' : left,
        });
        $(this).find('.buttonbox-imagewrap').height($(this).find('.buttonbox-content').height());
      }
    });
  }
  if ($(window).width() <= 624) {
    $('.buttonbox.imageleft').each(function() {
      $(this).find('.buttonbox-imagewrap img').height('auto').css({
        'width' : '100%',
        'max-width' : '100%',
        'left' : '0',
        'height' : 'auto'
      });
      $(this).find('.buttonbox-content').height('auto');
      $(this).find('.buttonbox-imagewrap').height($(this).find('.buttonbox-imagewrap img').height());
    });
  }
}

function imageTextboxOrder() {
  if($(window).width() <= 624) {
    $('.imagetextbox.imageright').each(function() {
      saveimage = $(this).find('.imagetextbox-images');
      $(this).find('.imagetextbox-images').remove();
      $(this).find('.imagetextbox-text').before(saveimage);
      $(this).find('.imagetextbox-images').css('margin-bottom','1.5em');
    });
  }
}

function mobilemenu() {
  $('#mobilemenu-button').click(function () {
    $('#mobilemenu').slideToggle();
    $(this).toggleClass('up');
    return false;
  });
  $('#mobilemenu li .menuarrow').click(function() {
    //if($(this).parent().hasClass('NO') || $(this).parent().hasClass('ACT') || $(this).parent().hasClass('CUR')) {
      $(this).toggleClass('up');
    $(this).next().next('ul.submenu').toggle();
    //}
    return false;
  });

}

function iconbox() {
  if ($(window).width() <= 624) {
    $('.iconbox-inner').each(function() {
    var iconbox = $(this);
    var height = iconbox.find('.iconbox-icon').height() + 'px';
      iconbox.find('.iconbox-headline').css('line-height',height);
    iconbox.find('.iconbox-bodytext').hide();
    iconbox.removeClass('up');
    });
  } else {
  $('.iconbox-inner').find('.iconbox-bodytext').show();
  $('.iconbox-inner').find('.iconbox-headline').css('line-height','120%');
  }

}

function navCol6nav1item() {
    $('.col6').each(function () {
      var slider = $(this);
      slider.find('.col6-slide-prev').click(function() {
        if ($(window).width() <= 624) {
          var slideract = slider.find('.col6-subcol.active');
          var slidernav = slider.find('.col6-nav.nav1item li.actnav');
          var act = slideract;
          var actnav = slidernav;
          var next = slideract.prev('.col6-subcol');
          var nextnav = slidernav.prev();
          if( next.length == 0 ){
            next = slider.find('.col6-subcol:last');
          }

          if( nextnav.length == 0 ){
            nextnav = slider.find('.col6-nav.nav1item li:last');
          }
          act.removeClass('active').hide();
          actnav.removeClass('actnav');
          next.show();
          next.addClass('active');
          nextnav.addClass('actnav');
        }
      });
      slider.find('.col6-slide-next').click(function() {
        if ($(window).width() <= 624) {
          var slideract = slider.find('.col6-subcol.active');
          var slidernav = slider.find('.col6-nav.nav1item li.actnav');
          var act = slideract;
          var actnav = slidernav;
          var next = slideract.next('.col6-subcol');
          var nextnav = slidernav.next();
          if( next.length == 0 ){
            next = slider.find('.col6-subcol:first');
          }
          if( nextnav.length == 0 ){
            nextnav = slider.find('.col6-nav.nav1item li:first');
          }
          act.removeClass('active').hide();
          actnav.removeClass('actnav');
          next.show();
          next.addClass('active');
          nextnav.addClass('actnav');
        }
      });
    });
}
function navCol6nav3items() {
    $('.col6').each(function () {
      var slider = $(this);
      slider.find('.col6-slide-next').click(function() {
        if (($(window).width() <= 1008) && ($(window).width() > 624)) {
          slider.find('.col6-subcol').removeClass('active');
          if(slider.find('.slide-1wrap').hasClass('actnav')) {
            slider.find('.col6-1wrap .col6-subcol').hide();
            slider.find('.col6-2wrap .col6-subcol').show();
            slider.find('.slide-1wrap').removeClass('actnav');
            slider.find('.slide-2wrap').addClass('actnav');
          } else {
            slider.find('.col6-1wrap .col6-subcol').show();
            slider.find('.col6-2wrap .col6-subcol').hide();
            slider.find('.slide-1wrap').addClass('actnav');
            slider.find('.slide-2wrap').removeClass('actnav');
          }
        }
      });
      slider.find('.col6-slide-prev').click(function() {
        if (($(window).width() <= 1008) && ($(window).width() > 624)) {
          slider.find('.col6-subcol').removeClass('active');
          if(slider.find('.slide-1wrap').hasClass('actnav')) {
            slider.find('.col6-1wrap .col6-subcol').hide();
            slider.find('.col6-2wrap .col6-subcol').show();
            slider.find('.slide-1wrap').removeClass('actnav');
            slider.find('.slide-2wrap').addClass('actnav');
          } else {
            slider.find('.col6-1wrap .col6-subcol').show();
            slider.find('.col6-2wrap .col6-subcol').hide();
            slider.find('.slide-1wrap').addClass('actnav');
            slider.find('.slide-2wrap').removeClass('actnav');
          }
        }
      });
    });
}

function slideCol6() {
  $('.col6').each(function () {
      var slider = $(this);
      var maxh = '';
      $('.col6-subcol').each(function () {
        var height = $(this).height();
        if(height > maxh) {
          maxh = height;
        }
      });
      $('.col6-subcol').height(maxh);

    });
  $('.col6').each(function() {
    var slider = $(this);
    if ($(window).width() >= 1008) {
      slider.find('.col6-subcol').css('display','table-cell');
    }
    if ($(window).width() <= 624) {
      slider.find('.col6-subcol').hide();
      var slidernav = '<div class="col6-nav nav1item"><ul class="menu">';
      var itemNr = 1;
      slider.find('.col6-subcol').each(function() {
        var itemClass = 'slide-' + itemNr;
        $(this).addClass(itemClass);
        slidernav += '<li class="slide-' + itemNr + '"><div class="boppel"></div></li>';
        itemNr++;
      });
      slidernav += '</ul></div>';
      if(slider.find('.col6-nav.nav1item').length == 0) {
        slider.find('.col6-inner').after(slidernav);
      }
      slider.find('.col6-subcol.col6-1').addClass('active').show();
      slider.find('.col6-nav li').removeClass('actnav');
      slider.find('.col6-nav li.slide-1').addClass('actnav');

      slider.find('.col6-nav li').click(function() {
        var navElement = $(this).attr('class');
        className = navElement.match(/slide-([^ ]+)/);
        className = '.' + className[0];
        if(!$(this).hasClass('actnav')) {
          slider.find('.col6-subcol').hide();
          slider.find('.col6-subcol').removeClass('active');

          slider.find('.col6-inner').find(className).addClass('active');
          slider.find(className).show();
        }
        slider.find('.col6-nav li').removeClass('actnav');
        $(this).addClass('actnav');
      });
    }
    if (($(window).width() <= 1008) && ($(window).width() > 624)) {
      slider.find('.col6-subcol').hide();
      var slidernav = '<div class="col6-nav nav3items"><ul class="menu">';
      slidernav += '<li class="slide-1wrap"><div class="boppel"></div></li>';
      slidernav += '<li class="slide-2wrap"><div class="boppel"></div></li>';
      slidernav += '</ul></div>';
      if(slider.find('.col6-nav.nav3items').length == 0) {
        slider.find('.col6-inner').after(slidernav);
      }
      slider.find('.col6-1wrap').addClass('active');
      slider.find('.col6-2wrap').removeClass('active');
      slider.find('.col6-1wrap .col6-subcol').show();
      slider.find('.col6-nav li.slide-1wrap').addClass('actnav');
      slider.find('.col6-nav li.slide-2wrap').removeClass('actnav');

      slider.find('.col6-nav li.slide-1wrap').click(function() {
        slider.find('.col6-1wrap').addClass('active');
        slider.find('.col6-1wrap .col6-subcol').show();
        slider.find('.col6-nav li.slide-1wrap').addClass('actnav');
        slider.find('.col6-2wrap').removeClass('active');
        slider.find('.col6-2wrap .col6-subcol').hide();
        slider.find('.col6-nav li.slide-2wrap').removeClass('actnav');
      });
      slider.find('.col6-nav li.slide-2wrap').click(function() {
        slider.find('.col6-2wrap').addClass('active');
        slider.find('.col6-2wrap .col6-subcol').show();
        slider.find('.col6-nav li.slide-2wrap').addClass('actnav');
        slider.find('.col6-1wrap').removeClass('active');
        slider.find('.col6-1wrap .col6-subcol').hide();
        slider.find('.col6-nav li.slide-1wrap').removeClass('actnav');
      });
    }
  });
}

function imagetextboxDetail() {
  $('.imagetextbox-more').each(function (){
    var elid = '#' + $(this).find('a').attr('class');
    $(elid).hide();
    $(this).click(function(){
      $(elid).slideToggle();
      $(this).toggleClass('up');
      return false;
    });
  });
}

// make thirdlevel menu from all h1 in contentarea
function getThirdlevelmenu() {
  if($('.contentarea h2').length == '') {
    $('#thirdlevelmenu').hide();
  }
  $('.contentarea h2').each(function() {
    var elid = $(this).closest('div[id^="c"][class^="csc-default"]').attr('id');
    var menuel = '<li class="' + elid + ' item"><a href="#">' + $(this).html() + '</a></li>';
    $('#thirdlevelmenu ul').append(menuel);
  });
  $('#thirdlevelmenu li.item').click(function() {
    var elclass = $(this).attr('class');
    elclass = '#' + elclass.match(/c([^ ]+)/);
    var scrolltop = $(elclass).offset().top - 220;
    $('html,body').animate({scrollTop: scrolltop}, 'slow');
    return false;
  });
  $('#thirdlevelmenu li.select').click(function() {
    $('#thirdlevelmenu li.item').toggle();
    $('#thirdlevelmenu ul').toggleClass('up');
  });
}


// set padding for text elements in imagetextboxes
function paddingImagetextbox() {
  if ($(window).width() > 990) {
    $('.imagetextbox.imageleft').each(function() {
      var totalwidth = $(this).width();
      var padding = (totalwidth - 960) / 2;
      padding = padding + 'px';
      $(this).find('.imagetextbox-text-inner').css('padding-right',padding);
    });
    $('.imagetextbox.imageright').each(function() {
      var totalwidth = $(this).width();
      var padding = (totalwidth - 960) / 2;
      padding = padding + 'px';
      $(this).find('.imagetextbox-text-inner').css('padding-left',padding);
    });
  } else {
    if ($(window).width() > 624) {
      $('.imagetextbox.imageleft').each(function() {
        $(this).find('.imagetextbox-text-inner').css('padding-right','4%');
      });
      $('.imagetextbox.imageright').each(function() {
        $(this).find('.imagetextbox-text-inner').css('padding-left','4%');
      });
    } else {
      $('.imagetextbox.imageleft').each(function() {
        $(this).find('.imagetextbox-text-inner').css('padding-right','2%');
      });
      $('.imagetextbox.imageright').each(function() {
        $(this).find('.imagetextbox-text-inner').css('padding-left','2%');
      });
    }
  }
}

function resizeImagetextbox() {
  $('.slideimages').each(function() {
    var height = $(this).find('img').height();
    $(this).height(height);
  });

}

// do slideshow for imagetextboxes
function imagetextboxSlideshow() {
  $('.slideimages').each(function() {
    var slider = $(this);
    var nrOfItems = $(this).find('img').size();
    if(nrOfItems >= 2) {
    var slidernav = '<div class="slider-nav"><ul class="menu">';
    var itemNr = 1;
    slider.find('img').each(function() {
      var itemClass = 'slide-' + itemNr;
      $(this).addClass(itemClass);
      slidernav += '<li class="slide-' + itemNr + '"><div class="boppel"></div></li>';
      itemNr++;
    });
    slidernav += '</ul></div>';
    slider.append(slidernav);
    slider.find('img.slide-1').addClass('active').show();
    slider.find('li.slide-1').addClass('actnav');

    slider.find('li').click(function() {
      var navElement = $(this).attr('class');
      className = navElement.match(/slide-([^ ]+)/);
      className = '.' + className[0];
      if(!$(this).hasClass('actnav')) {
        slider.find('img').fadeOut(1000);
        slider.find('img').removeClass('active');

        slider.find('.slide-image').find(className).addClass('active');
        slider.find(className).fadeIn(1000);
      }
      slider.find('li').removeClass('actnav');
      $(this).addClass('actnav');
    });
    slider.find('.slide-prev').click(function() {
      var slideract = slider.find('img.active');
      var slidernav = slider.find('.slider-nav li.actnav');
      var act = slideract;
      var actnav = slidernav;
      var next = slideract.parent().prev('.slide-image');
      var nextnav = slidernav.prev();
      if( next.length == 0 ){
        next = slider.parent().find('.slide-image:last');
      }

      if( nextnav.length == 0 ){
        nextnav = slider.parent().find('.slider-nav li:last');
      }
      act.removeClass('active').fadeOut(500);
      actnav.removeClass('actnav');
      next.find('img').fadeIn(500);
      next.find('img').addClass('active');
      nextnav.addClass('actnav');
    });
    slider.find('.slide-next').click(function() {
      var slideract = slider.find('img.active');
      var slidernav = slider.find('.slider-nav li.actnav');
      var act = slideract;
      var actnav = slidernav;
      var next = slideract.parent().next('.slide-image');
      var nextnav = slidernav.next();
      if( next.length == 0 ){
        next = slider.parent().find('.slide-image:first');
      }
      if( nextnav.length == 0 ){
        nextnav = slider.parent().find('.slider-nav li:first');
      }
      act.removeClass('active').fadeOut(500);
      actnav.removeClass('actnav');
      next.find('img').fadeIn(500);
      next.find('img').addClass('active');
      nextnav.addClass('actnav');
    });
    } else {
      slider.find('img').show();
    }
  });
}

function hoverbox() {
  if($(window).width() >= 624) {
    $('.buttonbox').each(function() {
    $(this).find('.buttonbox-linkwrap').bind({
      mouseenter: function() {
        $(this).find('.hover').stop().fadeIn('normal');
        $(this).mouseleave(function() {
          $(this).find('.hover').stop().fadeOut('normal');
        });
      }
    });
    });
  }
}



// change status of phone numbers
function delinkTel() {
    jQuery('a[href*="tel:"]').each(function() {
        if (jQuery(window).width() > 1026) {
            jQuery(this).click(function() { return false; }).css('cursor','text');
            jQuery(this).css("color", "#212121");
            jQuery(this).css("font-weight", "normal");
        }
    });
}

// set image widths
function setImageWidth() {
if (jQuery(window).width() >= 624) {
    jQuery('.csc-textpic-imagerow').each(function() {
      cols = jQuery(this).find('.csc-textpic-imagecolumn').size();
      width = 100 / cols + '%';
      jQuery(this).find('.csc-textpic-imagecolumn').width(width);
    });
  // set margins for intext left and right
  jQuery('.csc-textpic-intext-left-nowrap').each(function() {
    imgwidth = jQuery(this).find('.csc-textpic-imagewrap').outerWidth() + 20;
    jQuery(this).find('.csc-textpic-text').css('margin-left',imgwidth);
  });
  jQuery('.csc-textpic-intext-right-nowrap').each(function() {
    imgwidth = jQuery(this).find('.csc-textpic-imagewrap').width() + 20;
    jQuery(this).find('.csc-textpic-text').css('margin-right',imgwidth);
  });
  } else {
    jQuery('.csc-textpic-imagerow').find('.csc-textpic-imagecolumn').width('100%');

  // set margins for intext left and right
  jQuery('.csc-textpic-intext-left-nowrap').each(function() {
    jQuery(this).find('.csc-textpic-text').css('margin-left','0');
  });
  jQuery('.csc-textpic-intext-right-nowrap').each(function() {
    jQuery(this).find('.csc-textpic-text').css('margin-right','0');
  });
  }
}

// top scroller
function scrollTop() {
  jQuery('.toplink').click(function(e) {
    e.preventDefault();
    jQuery('html,body').animate({scrollTop: 0}, 'slow');
  });
}