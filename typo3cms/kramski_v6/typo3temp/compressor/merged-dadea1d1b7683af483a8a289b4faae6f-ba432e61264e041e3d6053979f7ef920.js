
/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */


Element.addMethods({
	pngHack: function(element) {
		element = $(element);
		var transparentGifPath = 'clear.gif';

			// If there is valid element, it is an image and the image file ends with png:
		if (Object.isElement(element) && element.tagName === 'IMG' && element.src.endsWith('.png')) {
			var alphaImgSrc = element.src;
			var sizingMethod = 'scale';
			element.src = transparentGifPath;
		}

		if (alphaImgSrc) {
			element.style.filter = 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="#{alphaImgSrc}",sizingMethod="#{sizingMethod}")'.interpolate(
			{
				alphaImgSrc: alphaImgSrc,
				sizingMethod: sizingMethod
			});
		}

		return element;
	}
});

var IECompatibility = Class.create({

	/**
	 * initializes the compatibility class
	 */
	initialize: function() {
		Event.observe(document, 'dom:loaded', function() {
			$$('input[type="checkbox"]').invoke('addClassName', 'checkbox');
		}.bind(this));

		Event.observe(window, 'load', function() {
			if (Prototype.Browser.IE) {
				var version = parseFloat(navigator.appVersion.split(';')[1].strip().split(' ')[1]);
				if (version === 6) {
					$$('img').each(function(img) {
						img.pngHack();
					});
					$$('#typo3-menu li ul li').each(function(li) {
						li.setStyle({height: '21px'});
					});
				}
			}
		});
	}
});

if (Prototype.Browser.IE) {
	var TYPO3IECompatibilty = new IECompatibility();
}

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Javascript functions regarding the clickmenu
 * relies on the javascript library "prototype"
 */

/**
 * new clickmenu code to make an AJAX call and render the
 * AJAX result in a layer next to the mouse cursor
 */
var Clickmenu = {
	clickURL: 'alt_clickmenu.php',
	ajax: true, // \TYPO3\CMS\Backend\Template\DocumentTemplate::isCMLayers
	mousePos: { X: null, Y: null },
	delayClickMenuHide: false,

	/**
	 * main function, called from most clickmenu links
	 * @param	table		table from where info should be fetched
	 * @param	uid		the UID of the item
	 * @param	listFr		list Frame?
	 * @param	enDisItems	Items to disable / enable
	 * @param	backPath	TYPO3 backPath
	 * @param	addParams	additional params
	 * @return	nothing
	 */
	show: function(table, uid, listFr, enDisItems, backPath, addParams) {
		var params = 'table=' + encodeURIComponent(table) +
			'&uid=' + uid +
			'&listFr=' + listFr +
			'&enDisItems=' + enDisItems +
			'&backPath=' + backPath +
			'&addParams=' + addParams;
		this.callURL(params);
	},


	/**
	 * switch function that either makes an AJAX call
	 * or loads the request in the top frame
	 *
	 * @param	params	parameters added to the URL
	 * @return	nothing
	 */
	callURL: function(params) {
		if (this.ajax && Ajax.getTransport()) { // run with AJAX
			params += '&ajax=1';
			var call = new Ajax.Request(this.clickURL, {
				method: 'get',
				parameters: params,
				onComplete: function(xhr) {
					var response = xhr.responseXML;

					if (!response.getElementsByTagName('data')[0]) {
						var res = params.match(/&reloadListFrame=(0|1|2)(&|$)/);
						var reloadListFrame = parseInt(res[1], 0);
						if (reloadListFrame) {
							var doc = reloadListFrame != 2 ? top.content.list_frame : top.content;
							doc.location.reload(true);
						}
						return;
					}
					var menu  = response.getElementsByTagName('data')[0].getElementsByTagName('clickmenu')[0];
					var data  = menu.getElementsByTagName('htmltable')[0].firstChild.data;
					var level = menu.getElementsByTagName('cmlevel')[0].firstChild.data;
					this.populateData(data, level);

				}.bind(this)
			});
		}
	},


	/**
	 * fills the clickmenu with content and displays it correctly
	 * depending on the mouse position
	 * @param	data	the data that will be put in the menu
	 * @param	level	the depth of the clickmenu
	 */
	populateData: function(data, level) {
		if (!$('contentMenu0')) {
			this.addClickmenuItem();
		}
		level = parseInt(level, 10) || 0;
		var obj = $('contentMenu' + level);

		if (obj && (level === 0 || Element.visible('contentMenu' + (level-1)))) {
			obj.innerHTML = data;
			var x = this.mousePos.X;
			var y = this.mousePos.Y;
			var dimsWindow = document.viewport.getDimensions();
			dimsWindow.width = dimsWindow.width-20; // saving margin for scrollbars

			var dims = Element.getDimensions(obj); // dimensions for the clickmenu
			var offset = document.viewport.getScrollOffsets();
			var relative = { X: this.mousePos.X - offset.left, Y: this.mousePos.Y - offset.top };

			// adjusting the Y position of the layer to fit it into the window frame
			// if there is enough space above then put it upwards,
			// otherwise adjust it to the bottom of the window
			if (dimsWindow.height - dims.height < relative.Y) {
				if (relative.Y > dims.height) {
					y -= (dims.height - 10);
				} else {
					y += (dimsWindow.height - dims.height - relative.Y);
				}
			}
			// adjusting the X position like Y above, but align it to the left side of the viewport if it does not fit completely
			if (dimsWindow.width - dims.width < relative.X) {
				if (relative.X > dims.width) {
					x -= (dims.width - 10);
				} else if ((dimsWindow.width - dims.width - relative.X) < offset.left) {
					x = offset.left;
				} else {
					x += (dimsWindow.width - dims.width - relative.X);
				}
			}

			obj.style.left = x + 'px';
			obj.style.top  = y + 'px';
			Element.show(obj);
		}
		if (/MSIE5/.test(navigator.userAgent)) {
			this._toggleSelectorBoxes('hidden');
		}
	},


	/**
	 * event handler function that saves the actual position of the mouse
	 * in the Clickmenu object
	 * @param	event	the event object
	 */
	calcMousePosEvent: function(event) {
		if (!event) {
			event = window.event;
		}
		this.mousePos.X = Event.pointerX(event);
		this.mousePos.Y = Event.pointerY(event);
		this.mouseOutFromMenu('contentMenu0');
		this.mouseOutFromMenu('contentMenu1');
	},


	/**
	 * hides a visible menu if the mouse has moved outside
	 * of the object
	 * @param	obj	the object to hide
	 * @result	nothing
	 */
	mouseOutFromMenu: function(obj) {
		obj = $(obj);
		if (obj && Element.visible(obj) && !Position.within(obj, this.mousePos.X, this.mousePos.Y)) {
			this.hide(obj);
			if (/MSIE5/.test(navigator.userAgent) && obj.id === 'contentMenu0') {
				this._toggleSelectorBoxes('visible');
			}
		} else if (obj && Element.visible(obj)) {
			this.delayClickMenuHide = true;
		}
	},

	/**
	 * hides a clickmenu
	 *
	 * @param	obj	the clickmenu object to hide
	 * @result	nothing
	 */
	hide: function(obj) {
		this.delayClickMenuHide = false;
		window.setTimeout(function() {
			if (!Clickmenu.delayClickMenuHide) {
				Element.hide(obj);
			}
		}, 500);
	},

	/**
	 * hides all clickmenus
	 */
	hideAll: function() {
		this.hide('contentMenu0');
		this.hide('contentMenu1');
	},


	/**
	 * hides / displays all selector boxes in a page, fixes an IE 5 selector problem
	 * originally by Michiel van Leening
	 *
	 * @param	action 	hide (= "hidden") or show (= "visible")
	 * @result	nothing
	 */
	_toggleSelectorBoxes: function(action) {
		for (var i = 0; i < document.forms.length; i++) {
			for (var j = 0; j < document.forms[i].elements.length; j++) {
				if (document.forms[i].elements[j].type == 'select-one') {
					document.forms[i].elements[j].style.visibility = action;
				}
			}
		}
	},


	/**
	 * manipulates the DOM to add the divs needed for clickmenu at the bottom of the <body>-tag
	 *
	 * @return	nothing
	 */
	addClickmenuItem: function() {
		var code = '<div id="contentMenu0" style="display: block;"></div><div id="contentMenu1" style="display: block;"></div>';
		var insert = new Insertion.Bottom(document.getElementsByTagName('body')[0], code);
	}
}

// register mouse movement inside the document
Event.observe(document, 'mousemove', Clickmenu.calcMousePosEvent.bindAsEventListener(Clickmenu), true);


// @deprecated: Deprecated functions since 4.2, here for compatibility, remove in 4.4+
// ## BEGIN ##

// Still used in Core: typo3/alt_clickmenu.php::linkItem()
function showClickmenu_raw(url) {
	var parts = url.split('?');
	if (parts.length === 2) {
		Clickmenu.clickURL = parts[0];
		Clickmenu.callURL(parts[1]);
	} else {
		Clickmenu.callURL(url);
	}
}
function showClickmenu_noajax(url) {
	Clickmenu.ajax = false; showClickmenu_raw(url);
}
function setLayerObj(tableData, cmLevel) {
	Clickmenu.populateData(tableData, cmLevel);
}
function hideEmpty() {
	Clickmenu.hideAll();
	return false;
}
function hideSpecific(level) {
	if (level === 0 || level === 1) {
		Clickmenu.hide('contentMenu'+level);
	}
}
function showHideSelectorBoxes(action) {
	toggleSelectorBoxes(action);
}
// ## END ##

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * javascript functions regarding the "dyntabmenu" used throughout the TYPO3 backend
 */

var DTM_array = DTM_array || [];

	// if tabs are used in a popup window the array might not exists
if (!top.DTM_currentTabs) {
	top.DTM_currentTabs = [];
}

function DTM_activate(idBase,index,doToogle) {
		// Check, whether the given index really exists
	if (!document.getElementById(idBase+'-'+index+'-MENU')) {
		// force the index to be the first one in case of saved settings are not valid anymore
		index = 1;
	}

		// Hiding all:
	if (DTM_array[idBase]) {
		for(var cnt = 0; cnt < DTM_array[idBase].length; cnt++) {
			if (DTM_array[idBase][cnt] !== idBase + '-' + index) {
				document.getElementById(DTM_array[idBase][cnt]+'-DIV').style.display = 'none';
				// Only Overriding when Tab not disabled
				if (document.getElementById(DTM_array[idBase][cnt]+'-MENU').attributes.getNamedItem('class').nodeValue !== 'disabled') {
					document.getElementById(DTM_array[idBase][cnt]+'-MENU').attributes.getNamedItem('class').nodeValue = 'tab';
				}
			}
		}
	}

		// Showing one:
	if (document.getElementById(idBase+'-'+index+'-DIV')) {
		if (doToogle && document.getElementById(idBase+'-'+index+'-DIV').style.display === 'block') {
			document.getElementById(idBase+'-'+index+'-DIV').style.display = 'none';
			document.getElementById(idBase+'-'+index+'-MENU').attributes.getNamedItem('class').nodeValue = 'tab';
			top.DTM_currentTabs[idBase] = -1;
		} else {
			document.getElementById(idBase+'-'+index+'-DIV').style.display = 'block';
			document.getElementById(idBase+'-'+index+'-MENU').attributes.getNamedItem('class').nodeValue = 'tabact';
			top.DTM_currentTabs[idBase] = index;
		}
	}
	document.getElementById(idBase+'-'+index+'-MENU').attributes.getNamedItem('class').nodeValue = 'tabact';
}
function DTM_toggle(idBase,index,isInit) {
		// Showing one:
	if (document.getElementById(idBase+'-'+index+'-DIV')) {
		if (document.getElementById(idBase+'-'+index+'-DIV').style.display === 'block') {
			document.getElementById(idBase+'-'+index+'-DIV').style.display = 'none';
			if (isInit) {
				document.getElementById(idBase+'-'+index+'-MENU').attributes.getNamedItem('class').nodeValue = 'tab';
			}
			top.DTM_currentTabs[idBase+'-'+index] = 0;
		} else {
			document.getElementById(idBase+'-'+index+'-DIV').style.display = 'block';
			if (isInit) {
				document.getElementById(idBase+'-'+index+'-MENU').attributes.getNamedItem('class').nodeValue = 'tabact';
			}
			top.DTM_currentTabs[idBase+'-'+index] = 1;
		}
	}
}

// CodeMirror version 2.21
var CodeMirror=function(){function $(a){return/\w/.test(a)||a.toUpperCase()!=a.toLowerCase()}function Z(a,b){if(a.indexOf)return a.indexOf(b);for(var c=0,d=a.length;c<d;++c)if(a[c]==b)return c;return-1}function Y(a,b){if(!b)return 0;if(!a)return b.length;for(var c=a.length,d=b.length;c>=0&&d>=0;--c,--d)if(a.charAt(c)!=b.charAt(d))break;return d+1}function X(a){W.textContent=a;return W.innerHTML}function V(a){return{line:a.line,ch:a.ch}}function U(a,b){return a.line<b.line||a.line==b.line&&a.ch<b.ch}function T(a,b){return a.line==b.line&&a.ch==b.ch}function S(a){if(b){a.selectionStart=0;a.selectionEnd=a.value.length}else a.select()}function R(a){return a.textContent||a.innerText||a.nodeValue||""}function Q(a,b){var c=a.ownerDocument.body;var d=0,e=0,f=false;for(var g=a;g;g=g.offsetParent){var h=g.offsetLeft,i=g.offsetTop;if(g==c){d+=Math.abs(h);e+=Math.abs(i)}else{d+=h,e+=i}if(b&&P(g).position=="fixed")f=true}var j=b&&!f?null:c;for(var g=a.parentNode;g!=j;g=g.parentNode)if(g.scrollLeft!=null){d-=g.scrollLeft;e-=g.scrollTop}return{left:d,top:e}}function P(a){if(a.currentStyle)return a.currentStyle;return window.getComputedStyle(a,null)}function O(a,b,c){if(b==null){b=a.search(/[^\s\u00a0]/);if(b==-1)b=a.length}for(var d=0,e=0;d<b;++d){if(a.charAt(d)=="\t")e+=c-e%c;else++e}return e}function I(){this.id=null}function H(a,b,c,d){if(typeof a.addEventListener=="function"){a.addEventListener(b,c,false);if(d)return function(){a.removeEventListener(b,c,false)}}else{var e=function(a){c(a||window.event)};a.attachEvent("on"+b,e);if(d)return function(){a.detachEvent("on"+b,e)}}}function G(a,b){var c=a.override&&a.override.hasOwnProperty(b);return c?a.override[b]:a[b]}function F(a){if(a.which)return a.which;else if(a.button&1)return 1;else if(a.button&2)return 3;else if(a.button&4)return 2}function E(a){return a.target||a.srcElement}function D(a){B(a);C(a)}function C(a){if(a.stopPropagation)a.stopPropagation();else a.cancelBubble=true}function B(a){if(a.preventDefault)a.preventDefault();else a.returnValue=false}function A(a){if(!a.stop)a.stop=z;return a}function z(){D(this)}function y(){this.time=0;this.done=[];this.undone=[]}function x(a,b){var c=0;outer:do{for(var d=0,e=a.children.length;d<e;++d){var f=a.children[d],g=f.chunkSize();if(b<g){a=f;continue outer}b-=g;c+=f.height}return c}while(!a.lines);for(var d=0;d<b;++d)c+=a.lines[d].height;return c}function w(a,b){var c=0;outer:do{for(var d=0,e=a.children.length;d<e;++d){var f=a.children[d],g=f.height;if(b<g){a=f;continue outer}b-=g;c+=f.chunkSize()}return c}while(!a.lines);for(var d=0,e=a.lines.length;d<e;++d){var h=a.lines[d],i=h.height;if(b<i)break;b-=i}return c+d}function v(a){if(a.parent==null)return null;var b=a.parent,c=Z(b.lines,a);for(var d=b.parent;d;b=d,d=d.parent){for(var e=0,f=d.children.length;;++e){if(d.children[e]==b)break;c+=d.children[e].chunkSize()}}return c}function u(a,b){while(!a.lines){for(var c=0;;++c){var d=a.children[c],e=d.chunkSize();if(b<e){a=d;break}b-=e}}return a.lines[b]}function t(a){this.children=a;var b=0,c=0;for(var d=0,e=a.length;d<e;++d){var f=a[d];b+=f.chunkSize();c+=f.height;f.parent=this}this.size=b;this.height=c;this.parent=null}function s(a){this.lines=a;this.parent=null;for(var b=0,c=a.length,d=0;b<c;++b){a[b].parent=this;d+=a[b].height}this.height=d}function r(a,b,c,d){for(var e=0,f=0,g=0;f<b;e+=2){var h=c[e],i=f+h.length;if(g==0){if(i>a)d.push(h.slice(a-f,Math.min(h.length,b-f)),c[e+1]);if(i>=a)g=1}else if(g==1){if(i>b)d.push(h.slice(0,b-f),c[e+1]);else d.push(h,c[e+1])}f=i}}function q(a,b){this.styles=b||[a,null];this.text=a;this.height=1;this.marked=this.gutterMarker=this.className=this.handlers=null;this.stateAfter=this.parent=this.hidden=null}function p(a){this.from=a;this.to=a;this.line=null}function o(a,b,c,d){this.from=a;this.to=b;this.style=c;this.set=d}function n(a,b){this.pos=this.start=0;this.string=a;this.tabSize=b||8}function m(a,b,c){return a.startState?a.startState(b,c):true}function l(a,b){if(b===true)return b;if(a.copyState)return a.copyState(b);var c={};for(var d in b){var e=b[d];if(e instanceof Array)e=e.concat([]);c[d]=e}return c}function k(a){var b=bb[G(a,"keyCode")];return b=="Ctrl"||b=="Alt"||b=="Shift"||b=="Mod"}function j(a,b,c){function d(a,b,c){var e=b[a];if(e!=null)return e;if(c==null)c=b.fallthrough;if(c==null)return b.catchall;if(typeof c=="string")return d(a,i[c]);for(var f=0,g=c.length;f<g;++f){e=d(a,i[c[f]]);if(e!=null)return e}return null}return b?d(a,b,c):d(a,i[c])}function a(d,e){function dO(a){return function(){if(!(dN++))dL();try{var b=a.apply(this,arguments)}finally{if(!--dN)dM()}return b}}function dM(){var a=false,b;if(bE)a=!cz();if(bC.length)b=cC(bC,true);else{if(bE)cG();if(bG)cF()}if(a)cz();if(bE){cy();dD()}if(br&&!bF&&(bA===true||bA!==false&&bE))cw(bB);if(bE&&f.matchBrackets)setTimeout(dO(function(){if(bM){bM();bM=null}if(T(bs.from,bs.to))dF(false)}),20);var c=bD,d=bH;if(bE&&f.onCursorActivity)f.onCursorActivity(bT);if(c&&f.onChange&&bT)f.onChange(bT,c);for(var e=0;e<d.length;++e)d[e](bT);if(b&&f.onUpdate)f.onUpdate(bT)}function dL(){bA=bB=bD=null;bC=[];bE=false;bH=[]}function dK(a){if(!bq.length)return;bm.set(a,dO(dJ))}function dJ(){var a=+(new Date)+f.workTime;var b=bq.length;while(bq.length){if(!bU(bJ).stateAfter)var c=bJ;else var c=bq.pop();if(c>=bp.size)continue;var d=dG(c),e=d&&bU(d-1).stateAfter;if(e)e=l(bo,e);else e=m(bo);var g=0,h=bo.compareStates,i=false,j=d,k=false;bp.iter(j,bp.size,function(b){var d=b.stateAfter;if(+(new Date)>a){bq.push(j);dK(f.workDelay);if(i)bC.push({from:c,to:j+1});return k=true}var m=b.highlight(bo,e,f.tabSize);if(m)i=true;b.stateAfter=l(bo,e);if(h){if(d&&h(d,e))return true}else{if(m!==false||!d)g=0;else if(++g>3&&(!bo.indent||bo.indent(d,"")==bo.indent(e,"")))return true}++j});if(k)return;if(i)bC.push({from:c,to:j+1})}if(b&&f.onHighlightComplete)f.onHighlightComplete(bT)}function dI(a,b){var c=dH(a);bp.iter(a,b,function(a){a.highlight(bo,c,f.tabSize);a.stateAfter=l(bo,c)})}function dH(a){var b=dG(a),c=b&&bU(b-1).stateAfter;if(!c)c=m(bo);else c=l(bo,c);bp.iter(b,a,function(a){a.highlight(bo,c,f.tabSize);a.stateAfter=l(bo,c)});if(b<a)bC.push({from:b,to:a});if(a<bp.size&&!bU(a).stateAfter)bq.push(a);return c}function dG(a){var b,c;for(var d=a,e=a-40;d>e;--d){if(d==0)return 0;var g=bU(d-1);if(g.stateAfter)return d;var h=g.indentation(f.tabSize);if(c==null||b>h){c=d-1;b=h}}return c}function dF(a){function p(a,b,c){if(!a.text)return;var d=a.styles,e=g?0:a.text.length-1,f;for(var i=g?0:d.length-2,j=g?d.length:-2;i!=j;i+=2*h){var k=d[i];if(d[i+1]!=null&&d[i+1]!=m){e+=h*k.length;continue}for(var l=g?0:k.length-1,p=g?k.length:-1;l!=p;l+=h,e+=h){if(e>=b&&e<c&&o.test(f=k.charAt(l))){var q=dE[f];if(q.charAt(1)==">"==g)n.push(f);else if(n.pop()!=q.charAt(0))return{pos:e,match:false};else if(!n.length)return{pos:e,match:true}}}}}var b=bs.inverted?bs.from:bs.to,c=bU(b.line),d=b.ch-1;var e=d>=0&&dE[c.text.charAt(d)]||dE[c.text.charAt(++d)];if(!e)return;var f=e.charAt(0),g=e.charAt(1)==">",h=g?1:-1,i=c.styles;for(var j=d+1,k=0,l=i.length;k<l;k+=2)if((j-=i[k].length)<=0){var m=i[k+1];break}var n=[c.text.charAt(d)],o=/[(){}[\]]/;for(var k=b.line,l=g?Math.min(k+100,bp.size):Math.max(-1,k-100);k!=l;k+=h){var c=bU(k),q=k==b.line;var r=p(c,q&&g?d+1:0,q&&!g?d:c.text.length);if(r)break}if(!r)r={pos:null,match:false};var m=r.match?"CodeMirror-matchingbracket":"CodeMirror-nonmatchingbracket";var s=dc({line:b.line,ch:d},{line:b.line,ch:d+1},m),t=r.pos!=null&&dc({line:k,ch:r.pos},{line:k,ch:r.pos+1},m);var u=dO(function(){s.clear();t&&t.clear()});if(a)setTimeout(u,800);else bM=u}function dD(){clearInterval(bn);var a=true;bh.style.visibility="";bn=setInterval(function(){bh.style.visibility=(a=!a)?"":"hidden"},650)}function dC(a){function e(){var a=_(O.value).join("\n");if(a!=d)dO(cn)(a,"end");N.style.position="relative";O.style.cssText=c;bF=false;cw(true);cs()}var b=dB(a);if(!b||window.opera)return;if(T(bs.from,bs.to)||U(b,bs.from)||!U(b,bs.to))dO(cL)(b.line,b.ch);var c=O.style.cssText;N.style.position="absolute";O.style.cssText="position: fixed; width: 30px; height: 30px; top: "+(a.clientY-5)+"px; left: "+(a.clientX-5)+"px; z-index: 1000; background: white; "+"border-width: 0; outline: none; overflow: hidden; opacity: .05; filter: alpha(opacity=5);";bF=true;var d=O.value=cq();cx();O.select();if(K){D(a);var f=H(window,"mouseup",function(){f();setTimeout(e,20)},true)}else{setTimeout(e,50)}}function dB(a,b){var c=Q(P,true),d,e;try{d=a.clientX;e=a.clientY}catch(a){return null}if(!b&&(d-c.left>P.clientWidth||e-c.top>P.clientHeight))return null;var f=Q(bf,true);return dq(d-f.left,e-f.top)}function dA(){return bf.offsetLeft}function dz(){return bf.offsetTop}function dy(){if(P.clientWidth==dx)return dw;dx=P.clientWidth;return dw=dk("x")}function dv(){if(du==null){du="<pre>";for(var a=0;a<49;++a)du+="x<br/>";du+="x</pre>"}var b=bj.clientHeight;if(b==dt)return ds;dt=b;bg.innerHTML=du;ds=bg.firstChild.offsetHeight/50||1;bg.innerHTML="";return ds}function dr(a){var b=dp(a,true),c=Q(bf);return{x:c.left+b.x,y:c.top+b.y,yBot:c.top+b.yBot}}function dq(a,b){function l(a){var b=dn(h,a);if(j){var d=Math.round(b.top/c);return Math.max(0,b.left+(d-k)*P.clientWidth)}return b.left}if(b<0)b=0;var c=dv(),d=dy(),e=bI+Math.floor(b/c);var g=w(bp,e);if(g>=bp.size)return{line:bp.size-1,ch:bU(bp.size-1).text.length};var h=bU(g),i=h.text;var j=f.lineWrapping,k=j?e-x(bp,g):0;if(a<=0&&k==0)return{line:g,ch:0};var m=0,n=0,o=i.length,p;var q=Math.min(o,Math.ceil((a+k*P.clientWidth*.9)/d));for(;;){var r=l(q);if(r<=a&&q<o)q=Math.min(o,Math.ceil(q*1.2));else{p=r;o=q;break}}if(a>p)return{line:g,ch:o};q=Math.floor(o*.8);r=l(q);if(r<a){m=q;n=r}for(;;){if(o-m<=1)return{line:g,ch:p-a>a-n?m:o};var s=Math.ceil((m+o)/2),t=l(s);if(t>a){o=s;p=t}else{m=s;n=t}}}function dp(a,b){var c,d=dv(),e=d*(x(bp,a.line)-(b?bI:0));if(a.ch==0)c=0;else{var g=dn(bU(a.line),a.ch);c=g.left;if(f.lineWrapping)e+=Math.max(0,g.top)}return{x:c,y:e,yBot:e+d}}function dn(a,b){if(b==0)return{top:0,left:0};var c="";if(f.lineWrapping){var d=a.text.indexOf(" ",b+2);c=X(a.text.slice(b+1,d<0?a.text.length:d+(L?5:0)))}bg.innerHTML="<pre>"+a.getHTML(bP,b)+'<span id="CodeMirror-temp-'+dm+'">'+X(a.text.charAt(b)||" ")+"</span>"+c+"</pre>";var e=document.getElementById("CodeMirror-temp-"+dm);var g=e.offsetTop,h=e.offsetLeft;if(L&&g==0&&h==0){var i=document.createElement("span");i.innerHTML="x";e.parentNode.insertBefore(i,e.nextSibling);g=i.offsetTop}return{top:g,left:h}}function dl(a,b){function e(a){bg.innerHTML="<pre><span>"+c.getHTML(bP,a)+"</span></pre>";return bg.firstChild.firstChild.offsetWidth}if(b<=0)return 0;var c=bU(a),d=c.text;var f=0,g=0,h=d.length,i;var j=Math.min(h,Math.ceil(b/dy()));for(;;){var k=e(j);if(k<=b&&j<h)j=Math.min(h,Math.ceil(j*1.2));else{i=k;h=j;break}}if(b>i)return h;j=Math.floor(h*.8);k=e(j);if(k<b){f=j;g=k}for(;;){if(h-f<=1)return i-b>b-g?f:h;var l=Math.ceil((f+h)/2),m=e(l);if(m>b){h=l;i=m}else{f=l;g=m}}}function dk(a){bg.innerHTML="<pre><span>x</span></pre>";bg.firstChild.firstChild.firstChild.nodeValue=a;return bg.firstChild.firstChild.offsetWidth||10}function dj(a){if(typeof a=="number"){if(!bS(a))return null;var b=a;a=bU(a);if(!a)return null}else{var b=v(a);if(b==null)return null}var c=a.gutterMarker;return{line:b,handle:a,text:a.text,markerText:c&&c.text,markerClass:c&&c.style,lineClass:a.className}}function di(a,b){return dg(a,function(a,c){if(a.hidden!=b){a.hidden=b;bV(a,b?0:1);var d=bs.from.line,e=bs.to.line;if(b&&(d==c||e==c)){var f=d==c?cK({line:d,ch:0},d,0):bs.from;var g=e==c?cK({line:e,ch:0},e,0):bs.to;cJ(f,g)}return bG=true}})}function dh(a,b){return dg(a,function(a){if(a.className!=b){a.className=b;return true}})}function dg(a,b){var c=a,d=a;if(typeof a=="number")d=bU(cM(a));else c=v(a);if(c==null)return null;if(b(d,c))bC.push({from:c,to:c+1});else return null;return d}function df(a){if(typeof a=="number")a=bU(cM(a));a.gutterMarker=null;bG=true}function de(a,b,c){if(typeof a=="number")a=bU(cM(a));a.gutterMarker={text:b,style:c};bG=true;return a}function dd(a){a=cN(a);var b=new p(a.ch);bU(a.line).addMark(b);return b}function dc(a,b,c){function e(a,b,c,e){bU(a).addMark(new o(b,c,e,d.set))}a=cN(a);b=cN(b);var d=new db;if(a.line==b.line)e(a.line,a.ch,b.ch,c);else{e(a.line,a.ch,null,c);for(var f=a.line+1,g=b.line;f<g;++f)e(f,null,null,c);e(b.line,null,b.ch,c)}bC.push({from:a.line,to:b.line+1});return d}function db(){this.set=[]}function da(){P.className=P.className.replace(/\s*cm-s-\w+/g,"")+f.theme.replace(/(^|\s)\s*/g," cm-s-")}function c_(){bP=c$();cC(true)}function c$(){for(var a='<span class="cm-tab">',b=0;b<f.tabSize;++b)a+=" ";return a+"</span>"}function cZ(a,b){if(f.lineWrapping){C.className+=" CodeMirror-wrap";var c=P.clientWidth/dy()-3;bp.iter(0,bp.size,function(a){if(a.hidden)return;var b=Math.ceil(a.text.length/c)||1;if(b!=1)bV(a,b)});bf.style.width=S.style.width=""}else{C.className=C.className.replace(" CodeMirror-wrap","");bO=null;bN="";bp.iter(0,bp.size,function(a){if(a.height!=1&&!a.hidden)bV(a,1);if(a.text.length>bN.length)bN=a.text})}bC.push({from:0,to:bp.size})}function cY(){var a=f.gutter||f.lineNumbers;bd.style.display=a?"":"none";if(a)bG=true;else bj.parentNode.style.marginLeft=0}function cX(){bo=a.getMode(f,f.mode);bp.iter(0,bp.size,function(a){a.stateAfter=null});bq=[0];dK()}function cW(a,b){if(!b)b="add";if(b=="smart"){if(!bo.indent)b="prev";else var c=dH(a)}var d=bU(a),e=d.indentation(f.tabSize),g=d.text.match(/^\s*/)[0],h;if(b=="prev"){if(a)h=bU(a-1).indentation(f.tabSize);else h=0}else if(b=="smart")h=bo.indent(c,d.text.slice(g.length),d.text);else if(b=="add")h=e+f.indentUnit;else if(b=="subtract")h=e-f.indentUnit;h=Math.max(0,h);var i=h-e;if(!i){if(bs.from.line!=a&&bs.to.line!=a)return;var j=g}else{var j="",k=0;if(f.indentWithTabs)for(var l=Math.floor(h/f.tabSize);l;--l){k+=f.tabSize;j+="\t"}while(k<h){++k;j+=" "}}cm(j,{line:a,ch:0},{line:a,ch:g.length})}function cV(a){if(T(bs.from,bs.to))return cW(bs.from.line,a);var b=bs.to.line-(bs.to.ch?0:1);for(var c=bs.from.line;c<=b;++c)cW(c,a)}function cU(a){cI({line:a,ch:0},{line:a,ch:bU(a).text.length})}function cT(a){var b=bU(a.line).text;var c=a.ch,d=a.ch;while(c>0&&$(b.charAt(c-1)))--c;while(d<b.length&&$(b.charAt(d)))++d;cI({line:a.line,ch:c},{line:a.line,ch:d})}function cS(a,b){var c=0,d=dp(bs.inverted?bs.from:bs.to,true);if(cR!=null)d.x=cR;if(b=="page")c=Math.min(P.clientHeight,window.innerHeight||document.documentElement.clientHeight);else if(b=="line")c=dv();var e=dq(d.x,d.y+c*a+2);cL(e.line,e.ch,true);cR=d.x}function cQ(a,b){if(!T(bs.from,bs.to))cm("",bs.from,bs.to);else if(a<0)cm("",cO(a,b),bs.to);else cm("",bs.from,cO(a,b));bB=true}function cP(a,b){var c=a<0?bs.from:bs.to;if(bt||T(bs.from,bs.to))c=cO(a,b);cL(c.line,c.ch,true)}function cO(a,b){function h(b){if(e==(a<0?0:f.text.length)){if(!b&&g())e=a<0?f.text.length:0;else return false}else e+=a;return true}function g(){for(var b=d+a,c=a<0?-1:bp.size;b!=c;b+=a){var e=bU(b);if(!e.hidden){d=b;f=e;return true}}}var c=bs.inverted?bs.from:bs.to,d=c.line,e=c.ch;var f=bU(d);if(b=="char")h();else if(b=="column")h(true);else if(b=="word"){var i=false;for(;;){if(a<0)if(!h())break;if($(f.text.charAt(e)))i=true;else if(i){if(a<0){a=1;h()}break}if(a>0)if(!h())break}}return{line:d,ch:e}}function cN(a){if(a.line<0)return{line:0,ch:0};if(a.line>=bp.size)return{line:bp.size-1,ch:bU(bp.size-1).text.length};var b=a.ch,c=bU(a.line).text.length;if(b==null||b>c)return{line:a.line,ch:c};else if(b<0)return{line:a.line,ch:0};else return a}function cM(a){return Math.max(0,Math.min(a,bp.size-1))}function cL(a,b,c){var d=cN({line:a,ch:b||0});(c?cI:cJ)(d,d)}function cK(a,b,c){function d(b){var d=a.line+b,e=b==1?bp.size:-1;while(d!=e){var f=bU(d);if(!f.hidden){var g=a.ch;if(g>c||g>f.text.length)g=f.text.length;return{line:d,ch:g}}d+=b}}var e=bU(a.line);if(!e.hidden)return a;if(a.line>=b)return d(1)||d(-1);else return d(-1)||d(1)}function cJ(a,b,c,d){cR=null;if(c==null){c=bs.from.line;d=bs.to.line}if(T(bs.from,a)&&T(bs.to,b))return;if(U(b,a)){var e=b;b=a;a=e}if(a.line!=c)a=cK(a,c,bs.from.ch);if(b.line!=d)b=cK(b,d,bs.to.ch);if(T(a,b))bs.inverted=false;else if(T(a,bs.to))bs.inverted=false;else if(T(b,bs.from))bs.inverted=true;bs.from=a;bs.to=b;bE=true}function cI(a,b){var c=bt&&cN(bt);if(c){if(U(c,a))a=c;else if(U(b,c))b=c}cJ(a,b);bB=true}function cH(a){if(a)bt=bt||(bs.inverted?bs.to:bs.from);else bt=null}function cG(){var a=T(bs.from,bs.to);var b=dp(bs.from,true);var c=a?b:dp(bs.to,true);var d=bs.inverted?b:c,e=dv();var g=Q(C),h=Q(bj);N.style.top=Math.max(0,Math.min(P.offsetHeight,d.y+h.top-g.top))+"px";N.style.left=Math.max(0,Math.min(P.offsetWidth,d.x+h.left-g.left))+"px";if(a){bh.style.top=d.y+"px";bh.style.left=(f.lineWrapping?Math.min(d.x,bf.offsetWidth):d.x)+"px";bh.style.display="";bi.style.display="none"}else{var i=b.y==c.y,j="";function k(a,b,c,d){j+='<div class="CodeMirror-selected" style="position: absolute; left: '+a+"px; top: "+b+"px; right: "+c+"px; height: "+d+'px"></div>'}if(bs.from.ch&&b.y>=0){var l=i?bf.clientWidth-c.x:0;k(b.x,b.y,l,e)}var m=Math.max(0,b.y+(bs.from.ch?e:0));var n=Math.min(c.y,bf.clientHeight)-m;if(n>.2*e)k(0,m,0,n);if((!i||!bs.from.ch)&&c.y<bf.clientHeight-.5*e)k(0,c.y,bf.clientWidth-c.x,e);bi.innerHTML=j;bh.style.display="none";bi.style.display=""}}function cF(){if(!f.gutter&&!f.lineNumbers)return;var a=bc.offsetHeight,b=P.clientHeight;bd.style.height=(a-b<2?b:a)+"px";var c=[],d=bJ;bp.iter(bJ,Math.max(bK,bJ+1),function(a){if(a.hidden){c.push("<pre></pre>")}else{var b=a.gutterMarker;var e=f.lineNumbers?d+f.firstLineNumber:null;if(b&&b.text)e=b.text.replace("%N%",e!=null?e:"");else if(e==null)e=" ";c.push(b&&b.style?'<pre class="'+b.style+'">':"<pre>",e);for(var g=1;g<a.height;++g)c.push("<br/>&#160;");c.push("</pre>")}++d});bd.style.display="none";be.innerHTML=c.join("");var e=String(bp.size).length,g=be.firstChild,h=R(g),i="";while(h.length+i.length<e)i+=" ";if(i)g.insertBefore(z.createTextNode(i),g.firstChild);bd.style.display="";bf.style.marginLeft=bd.offsetWidth+"px";bG=false}function cE(a,b,c){if(!c.length)bj.innerHTML="";else{function d(a){var b=a.nextSibling;a.parentNode.removeChild(a);return b}var e=0,f=bj.firstChild,g;for(var h=0;h<c.length;++h){var i=c[h];while(i.domStart>e){f=d(f);e++}for(var j=0,k=i.to-i.from;j<k;++j){f=f.nextSibling;e++}}while(f)f=d(f)}var l=c.shift(),f=bj.firstChild,j=a;var m=z.createElement("div"),n;bp.iter(a,b,function(a){if(l&&l.to==j)l=c.shift();if(!l||l.from>j){if(a.hidden)var b=m.innerHTML="<pre></pre>";else{var b="<pre>"+a.getHTML(bP)+"</pre>";if(a.className)b='<div style="position: relative"><pre class="'+a.className+'" style="position: absolute; left: 0; right: 0; top: 0; bottom: 0; z-index: -2">&#160;</pre>'+b+"</div>"}m.innerHTML=b;bj.insertBefore(m.firstChild,f)}else{f=f.nextSibling}++j})}function cD(a,b){for(var c=0,d=b.length||0;c<d;++c){var e=b[c],f=[],g=e.diff||0;for(var h=0,i=a.length;h<i;++h){var j=a[h];if(e.to<=j.from&&e.diff)f.push({from:j.from+g,to:j.to+g,domStart:j.domStart});else if(e.to<=j.from||e.from>=j.to)f.push(j);else{if(e.from>j.from)f.push({from:j.from,to:e.from,domStart:j.domStart});if(e.to<j.to)f.push({from:e.to+g,to:j.to+g,domStart:j.domStart+(e.to-j.from)})}}a=f}return a}function cC(a,b){if(!P.clientWidth){bJ=bK=bI=0;return}var c=cB();if(a!==true&&a.length==0&&c.from>bJ&&c.to<bK)return;var d=Math.max(c.from-100,0),e=Math.min(bp.size,c.to+100);if(bJ<d&&d-bJ<20)d=bJ;if(bK>e&&bK-e<20)e=Math.min(bp.size,bK);var g=a===true?[]:cD([{from:bJ,to:bK,domStart:0}],a);var h=0;for(var i=0;i<g.length;++i){var j=g[i];if(j.from<d){j.domStart+=d-j.from;j.from=d}if(j.to>e)j.to=e;if(j.from>=j.to)g.splice(i--,1);else h+=j.to-j.from}if(h==e-d)return;g.sort(function(a,b){return a.domStart-b.domStart});var k=dv(),l=bd.style.display;bj.style.display="none";cE(d,e,g);bj.style.display=bd.style.display="";var m=d!=bJ||e!=bK||bL!=P.clientHeight+k;if(m)bL=P.clientHeight+k;bJ=d;bK=e;bI=x(bp,d);bc.style.top=bI*k+"px";if(P.clientHeight)S.style.height=bp.height*k+2*dz()+"px";if(bj.childNodes.length!=bK-bJ)throw new Error("BAD PATCH! "+JSON.stringify(g)+" size="+(bK-bJ)+" nodes="+bj.childNodes.length);if(f.lineWrapping){bO=P.clientWidth;var n=bj.firstChild,o=false;bp.iter(bJ,bK,function(a){if(!a.hidden){var b=Math.round(n.offsetHeight/k)||1;if(a.height!=b){bV(a,b);bG=o=true}}n=n.nextSibling});if(o)S.style.height=bp.height*k+2*dz()+"px"}else{if(bO==null)bO=dk(bN);if(bO>P.clientWidth){bf.style.width=bO+"px";S.style.width="";S.style.width=P.scrollWidth+"px"}else{bf.style.width=S.style.width=""}}bd.style.display=l;if(m||bG)cF();cG();if(!b&&f.onUpdate)f.onUpdate(bT);return true}function cB(){var a=dv(),b=P.scrollTop-dz();var c=Math.max(0,Math.floor(b/a));var d=Math.ceil((b+P.clientHeight)/a);return{from:w(bp,c),to:w(bp,d)}}function cA(a,b,c,d){var e=dA(),g=dz(),h=dv();b+=g;d+=g;a+=e;c+=e;var i=P.clientHeight,j=P.scrollTop,k=false,l=true;if(b<j){P.scrollTop=Math.max(0,b-2*h);k=true}else if(d>j+i){P.scrollTop=d+h-i;k=true}var m=P.clientWidth,n=P.scrollLeft;var o=f.fixedGutter?bd.clientWidth:0;if(a<n+o){if(a<50)a=0;P.scrollLeft=Math.max(0,a-10-o);k=true}else if(c>m+n-3){P.scrollLeft=c+10-m;k=true;if(c>S.clientWidth)l=false}if(k&&f.onScroll)f.onScroll(bT);return l}function cz(){var a=dp(bs.inverted?bs.from:bs.to);var b=f.lineWrapping?Math.min(a.x,bf.offsetWidth):a.x;return cA(b,a.y,b,a.yBot)}function cy(){if(!bh.getBoundingClientRect)return;var a=bh.getBoundingClientRect();if(L&&a.top==a.bottom)return;var b=window.innerHeight||Math.max(document.body.offsetHeight,document.documentElement.offsetHeight);if(a.top<0||a.bottom>b)bh.scrollIntoView()}function cx(){if(f.readOnly!="nocursor")O.focus()}function cw(a){if(!T(bs.from,bs.to)){cu="";O.value=cq();O.select()}else if(a)cu=O.value=""}function cv(){if(bF||!br||ba(O)||f.readOnly)return false;var a=O.value;if(a==cu)return false;bt=null;var b=0,c=Math.min(cu.length,a.length);while(b<c&&cu[b]==a[b])++b;if(b<cu.length)bs.from={line:bs.from.line,ch:bs.from.ch-(cu.length-b)};else if(by&&T(bs.from,bs.to))bs.to={line:bs.to.line,ch:Math.min(bU(bs.to.line).text.length,bs.to.ch+(a.length-b))};cn(a.slice(b),"end");cu=a;return true}function ct(){function b(){dL();var c=cv();if(!c&&!a){a=true;bl.set(60,b)}else{cr=false;cs()}dM()}var a=false;cr=true;bl.set(20,b)}function cs(){if(cr)return;bl.set(f.pollInterval,function(){dL();cv();if(br)cs();dM()})}function cq(){return cp(bs.from,bs.to)}function cp(a,b){var c=a.line,d=b.line;if(c==d)return bU(c).text.slice(a.ch,b.ch);var e=[bU(c).text.slice(a.ch)];bp.iter(c+1,d,function(a){e.push(a.text)});e.push(bU(d).text.slice(0,b.ch));return e.join("\n")}function co(a,b,c,d){var e=a.length==1?a[0].length+b.ch:a[a.length-1].length;var f=d({line:b.line+a.length-1,ch:e});ch(b,c,a,f.from,f.to)}function cn(a,b){co(_(a),bs.from,bs.to,function(a){if(b=="end")return{from:a,to:a};else if(b=="start")return{from:bs.from,to:bs.from};else return{from:bs.from,to:a}})}function cm(a,b,c){function d(d){if(U(d,b))return d;if(!U(c,d))return e;var f=d.line+a.length-(c.line-b.line)-1;var g=d.ch;if(d.line==c.line)g+=a[a.length-1].length-(c.ch-(c.line==b.line?b.ch:0));return{line:f,ch:g}}b=cN(b);if(!c)c=b;else c=cN(c);a=_(a);var e;co(a,b,c,function(a){e=a;return{from:d(bs.from),to:d(bs.to)}});return e}function cl(a,b,c,d,e){function y(a){return a<=Math.min(b.line,b.line+s)?a:a+s}if(bz)return;var g=false,h=bN.length;if(!f.lineWrapping)bp.iter(a.line,b.line,function(a){if(a.text.length==h){g=true;return true}});if(a.line!=b.line||c.length>1)bG=true;var i=b.line-a.line,j=bU(a.line),k=bU(b.line);if(a.ch==0&&b.ch==0&&c[c.length-1]==""){var l=[],m=null;if(a.line){m=bU(a.line-1);m.fixMarkEnds(k)}else k.fixMarkStarts();for(var n=0,o=c.length-1;n<o;++n)l.push(q.inheritMarks(c[n],m));if(i)bp.remove(a.line,i,bH);if(l.length)bp.insert(a.line,l)}else if(j==k){if(c.length==1)j.replace(a.ch,b.ch,c[0]);else{k=j.split(b.ch,c[c.length-1]);j.replace(a.ch,null,c[0]);j.fixMarkEnds(k);var l=[];for(var n=1,o=c.length-1;n<o;++n)l.push(q.inheritMarks(c[n],j));l.push(k);bp.insert(a.line+1,l)}}else if(c.length==1){j.replace(a.ch,null,c[0]);k.replace(null,b.ch,"");j.append(k);bp.remove(a.line+1,i,bH)}else{var l=[];j.replace(a.ch,null,c[0]);k.replace(null,b.ch,c[c.length-1]);j.fixMarkEnds(k);for(var n=1,o=c.length-1;n<o;++n)l.push(q.inheritMarks(c[n],j));if(i>1)bp.remove(a.line+1,i-1,bH);bp.insert(a.line+1,l)}if(f.lineWrapping){var p=P.clientWidth/dy()-3;bp.iter(a.line,a.line+c.length,function(a){if(a.hidden)return;var b=Math.ceil(a.text.length/p)||1;if(b!=a.height)bV(a,b)})}else{bp.iter(a.line,n+c.length,function(a){var b=a.text;if(b.length>h){bN=b;h=b.length;bO=null;g=false}});if(g){h=0;bN="";bO=null;bp.iter(0,bp.size,function(a){var b=a.text;if(b.length>h){h=b.length;bN=b}})}}var r=[],s=c.length-i-1;for(var n=0,t=bq.length;n<t;++n){var u=bq[n];if(u<a.line)r.push(u);else if(u>b.line)r.push(u+s)}var v=a.line+Math.min(c.length,500);dI(a.line,v);r.push(v);bq=r;dK(100);bC.push({from:a.line,to:b.line+1,diff:s});var w={from:a,to:b,text:c};if(bD){for(var x=bD;x.next;x=x.next){}x.next=w}else bD=w;cJ(d,e,y(bs.from.line),y(bs.to.line));if(P.clientHeight)S.style.height=bp.height*dv()+2*dz()+"px"}function ck(){ci(bQ.undone,bQ.done,1)}function cj(){ci(bQ.done,bQ.undone,-1)}function ci(a,b,c){var d=a.pop(),e=d?d.length:0,f=[];for(var g=c>0?0:e-1,h=c>0?e:-1;g!=h;g+=c){var i=d[g];var j=[],k=i.start+i.added;bp.iter(i.start,k,function(a){j.push(a.text)});f.push({start:i.start,added:i.old.length,old:j});var l=cN({line:i.start+i.old.length-1,ch:Y(j[j.length-1],i.old[i.old.length-1])});cl({line:i.start,ch:0},{line:k-1,ch:bU(k-1).text.length},i.old,l,l)}bA=true;b.push(f)}function ch(a,b,c,d,e){if(bz)return;if(bQ){var g=[];bp.iter(a.line,b.line+1,function(a){g.push(a.text)});bQ.addChange(a.line,c.length,g);while(bQ.done.length>f.undoDepth)bQ.done.shift()}cl(a,b,c,d,e)}function cg(){if(br){if(f.onBlur)f.onBlur(bT);br=false;if(bM)dO(function(){if(bM){bM();bM=null}})();C.className=C.className.replace(" CodeMirror-focused","")}clearInterval(bn);setTimeout(function(){if(!br)bt=null},150)}function cf(){if(f.readOnly=="nocursor")return;if(!br){if(f.onFocus)f.onFocus(bT);br=true;if(C.className.search(/\bCodeMirror-focused\b/)==-1)C.className+=" CodeMirror-focused";if(!bF)cw(true)}cs();dD()}function ce(a){if(f.onKeyEvent&&f.onKeyEvent(bT,A(a)))return;if(G(a,"keyCode")==16)bt=null}function cd(a){var b=G(a,"keyCode"),c=G(a,"charCode");if(window.opera&&b==cb){cb=null;B(a);return}if(f.onKeyEvent&&f.onKeyEvent(bT,A(a)))return;if(window.opera&&!a.which&&ca(a))return;if(f.electricChars&&bo.electricChars&&f.smartIndent&&!f.readOnly){var d=String.fromCharCode(c==null?b:c);if(bo.electricChars.indexOf(d)>-1)setTimeout(dO(function(){cW(bs.to.line,"smart")}),75)}ct()}function cc(a){if(!br)cf();if(L&&a.keyCode==27){a.returnValue=false}if(f.onKeyEvent&&f.onKeyEvent(bT,A(a)))return;var b=G(a,"keyCode");cH(b==16||G(a,"shiftKey"));var d=ca(a);if(window.opera){cb=d?b:null;if(!d&&b==88&&G(a,c?"metaKey":"ctrlKey"))cn("")}}function ca(a){function g(){return c.call?c.call(null,bT):c}var b=bb[G(a,"keyCode")],c=i[f.keyMap].auto,d,e;if(b==null||a.altGraphKey){if(c)f.keyMap=g();return null}if(G(a,"altKey"))b="Alt-"+b;if(G(a,"ctrlKey"))b="Ctrl-"+b;if(G(a,"metaKey"))b="Cmd-"+b;if(G(a,"shiftKey")&&(d=j("Shift-"+b,f.extraKeys,f.keyMap))){e=true}else{d=j(b,f.extraKeys,f.keyMap)}if(typeof d=="string"){if(h.propertyIsEnumerable(d))d=h[d];else d=null}if(c&&(d||!k(a)))f.keyMap=g();if(!d)return false;var l=bt;try{if(f.readOnly)bz=true;if(e)bt=null;d(bT)}finally{bt=l;bz=false}B(a);return true}function b_(a){var b=cq();X(b);a.dataTransfer.setDragImage(W,0,0);a.dataTransfer.setData("Text",b)}function b$(a){a.preventDefault();var b=dB(a,true),c=a.dataTransfer.files;if(!b||f.readOnly)return;if(c&&c.length&&window.FileReader&&window.File){function d(a,c){var d=new FileReader;d.onload=function(){g[c]=d.result;if(++h==e){b=cN(b);dO(function(){var a=cm(g.join(""),b,b);cI(b,a)})()}};d.readAsText(a)}var e=c.length,g=Array(e),h=0;for(var i=0;i<e;++i)d(c[i],i)}else{try{var g=a.dataTransfer.getData("Text");if(g){var j=bs.from,k=bs.to;cI(b,b);if(bx)cm("",j,k);cn(g);cx()}}catch(a){}}}function bZ(a){for(var b=E(a);b!=C;b=b.parentNode)if(b.parentNode==be)return B(a);var c=dB(a);if(!c)return;bv={time:+(new Date),pos:c};B(a);cT(c)}function bY(a){function j(a){var b=dB(a,true);if(b&&!T(b,g)){if(!br)cf();g=b;cI(d,b);bA=false;var c=cB();if(b.line>=c.to||b.line<c.from)h=setTimeout(dO(function(){j(a)}),150)}}cH(G(a,"shiftKey"));for(var b=E(a);b!=C;b=b.parentNode)if(b.parentNode==S&&b!=bc)return;for(var b=E(a);b!=C;b=b.parentNode)if(b.parentNode==be){if(f.onGutterClick)f.onGutterClick(bT,Z(be.childNodes,b)+bJ,a);return B(a)}var d=dB(a);switch(F(a)){case 3:if(K&&!c)dC(a);return;case 2:if(d)cL(d.line,d.ch,true);return}if(!d){if(E(a)==P)B(a);return}if(!br)cf();var e=+(new Date);if(bv&&bv.time>e-400&&T(bv.pos,d)){B(a);setTimeout(cx,20);return cU(d.line)}else if(bu&&bu.time>e-400&&T(bu.pos,d)){bv={time:e,pos:d};B(a);return cT(d)}else{bu={time:e,pos:d}}var g=d,h;if(J&&!f.readOnly&&!T(bs.from,bs.to)&&!U(d,bs.from)&&!U(bs.to,d)){if(M)bf.draggable=true;var i=H(z,"mouseup",dO(function(b){if(M)bf.draggable=false;bx=false;i();if(Math.abs(a.clientX-b.clientX)+Math.abs(a.clientY-b.clientY)<10){B(b);cL(d.line,d.ch,true);cx()}}),true);bx=true;return}B(a);cL(d.line,d.ch,true);var k=H(z,"mousemove",dO(function(a){clearTimeout(h);B(a);j(a)}),true);var i=H(z,"mouseup",dO(function(a){clearTimeout(h);var b=dB(a);if(b)cI(d,b);B(a);cx();bA=true;k();i()}),true)}function bX(a){var b=[];bp.iter(0,bp.size,function(a){b.push(a.text)});return b.join("\n")}function bW(a){var b={line:0,ch:0};ch(b,{line:bp.size-1,ch:bU(bp.size-1).text.length},_(a),b,b);bA=true}function bV(a,b){bG=true;var c=b-a.height;for(var d=a;d;d=d.parent)d.height+=c}function bU(a){return u(bp,a)}function bS(a){return a>=0&&a<bp.size}var f={},n=a.defaults;for(var r in n)if(n.hasOwnProperty(r))f[r]=(e&&e.hasOwnProperty(r)?e:n)[r];var z=f["document"];var C=z.createElement("div");C.className="CodeMirror"+(f.lineWrapping?" CodeMirror-wrap":"");C.innerHTML='<div style="overflow: hidden; position: relative; width: 3px; height: 0px;">'+'<textarea style="position: absolute; padding: 0; width: 1px; height: 1em" wrap="off" '+'autocorrect="off" autocapitalize="off"></textarea></div>'+'<div class="CodeMirror-scroll" tabindex="-1">'+'<div style="position: relative">'+'<div style="position: relative">'+'<div class="CodeMirror-gutter"><div class="CodeMirror-gutter-text"></div></div>'+'<div class="CodeMirror-lines"><div style="position: relative; z-index: 0">'+'<div style="position: absolute; width: 100%; height: 0; overflow: hidden; visibility: hidden; outline: 5px auto none"></div>'+'<pre class="CodeMirror-cursor">&#160;</pre>'+'<div style="position: relative; z-index: -1"></div><div></div>'+"</div></div></div></div></div>";if(d.appendChild)d.appendChild(C);else d(C);var N=C.firstChild,O=N.firstChild,P=C.lastChild,S=P.firstChild,bc=S.firstChild,bd=bc.firstChild,be=bd.firstChild,bf=bd.nextSibling.firstChild,bg=bf.firstChild,bh=bg.nextSibling,bi=bh.nextSibling,bj=bi.nextSibling;da();if(b)O.style.width="0px";if(!M)bf.draggable=true;bf.style.outline="none";if(f.tabindex!=null)O.tabIndex=f.tabindex;if(!f.gutter&&!f.lineNumbers)bd.style.display="none";try{dk("x")}catch(bk){if(bk.message.match(/runtime/i))bk=new Error("A CodeMirror inside a P-style element does not work in Internet Explorer. (innerHTML bug)");throw bk}var bl=new I,bm=new I,bn;var bo,bp=new t([new s([new q("")])]),bq,br;cX();var bs={from:{line:0,ch:0},to:{line:0,ch:0},inverted:false};var bt,bu,bv,bw=0,bx,by=false,bz=false;var bA,bB,bC,bD,bE,bF,bG,bH;var bI=0,bJ=0,bK=0,bL=0;var bM;var bN="",bO,bP=c$();dO(function(){bW(f.value||"");bA=false})();var bQ=new y;H(P,"mousedown",dO(bY));H(P,"dblclick",dO(bZ));H(bf,"dragstart",b_);H(bf,"selectstart",B);if(!K)H(P,"contextmenu",dC);H(P,"scroll",function(){bw=P.scrollTop;cC([]);if(f.fixedGutter)bd.style.left=P.scrollLeft+"px";if(f.onScroll)f.onScroll(bT)});H(window,"resize",function(){cC(true)});H(O,"keyup",dO(ce));H(O,"input",ct);H(O,"keydown",dO(cc));H(O,"keypress",dO(cd));H(O,"focus",cf);H(O,"blur",cg);H(P,"dragenter",D);H(P,"dragover",D);H(P,"drop",dO(b$));H(P,"paste",function(){cx();ct()});H(O,"paste",ct);H(O,"cut",dO(function(){if(!f.readOnly)cn("")}));var bR;try{bR=z.activeElement==O}catch(bk){}if(bR)setTimeout(cf,20);else cg();var bT=C.CodeMirror={getValue:bX,setValue:dO(bW),getSelection:cq,replaceSelection:dO(cn),focus:function(){cx();cf();ct()},setOption:function(a,b){var c=f[a];f[a]=b;if(a=="mode"||a=="indentUnit")cX();else if(a=="readOnly"&&b=="nocursor"){cg();O.blur()}else if(a=="readOnly"&&!b){cw(true)}else if(a=="theme")da();else if(a=="lineWrapping"&&c!=b)dO(cZ)();else if(a=="tabSize")dO(c_)();if(a=="lineNumbers"||a=="gutter"||a=="firstLineNumber"||a=="theme")cC(true)},getOption:function(a){return f[a]},undo:dO(cj),redo:dO(ck),indentLine:dO(function(a,b){if(typeof b!="string"){if(b==null)b=f.smartIndent?"smart":"prev";else b=b?"add":"subtract"}if(bS(a))cW(a,b)}),indentSelection:dO(cV),historySize:function(){return{undo:bQ.done.length,redo:bQ.undone.length}},clearHistory:function(){bQ=new y},matchBrackets:dO(function(){dF(true)}),getTokenAt:dO(function(a){a=cN(a);return bU(a.line).getTokenAt(bo,dH(a.line),a.ch)}),getStateAfter:function(a){a=cM(a==null?bp.size-1:a);return dH(a+1)},cursorCoords:function(a){if(a==null)a=bs.inverted;return dr(a?bs.from:bs.to)},charCoords:function(a){return dr(cN(a))},coordsChar:function(a){var b=Q(bf);return dq(a.x-b.left,a.y-b.top)},markText:dO(dc),setBookmark:dd,setMarker:dO(de),clearMarker:dO(df),setLineClass:dO(dh),hideLine:dO(function(a){return di(a,true)}),showLine:dO(function(a){return di(a,false)}),onDeleteLine:function(a,b){if(typeof a=="number"){if(!bS(a))return null;a=bU(a)}(a.handlers||(a.handlers=[])).push(b);return a},lineInfo:dj,addWidget:function(a,b,c,d,e){a=dp(cN(a));var f=a.yBot,g=a.x;b.style.position="absolute";S.appendChild(b);if(d=="over")f=a.y;else if(d=="near"){var h=Math.max(P.offsetHeight,bp.height*dv()),i=Math.max(S.clientWidth,bf.clientWidth)-dA();if(a.yBot+b.offsetHeight>h&&a.y>b.offsetHeight)f=a.y-b.offsetHeight;if(g+b.offsetWidth>i)g=i-b.offsetWidth}b.style.top=f+dz()+"px";b.style.left=b.style.right="";if(e=="right"){g=S.clientWidth-b.offsetWidth;b.style.right="0px"}else{if(e=="left")g=0;else if(e=="middle")g=(S.clientWidth-b.offsetWidth)/2;b.style.left=g+dA()+"px"}if(c)cA(g,f,g+b.offsetWidth,f+b.offsetHeight)},lineCount:function(){return bp.size},clipPos:cN,getCursor:function(a){if(a==null)a=bs.inverted;return V(a?bs.from:bs.to)},somethingSelected:function(){return!T(bs.from,bs.to)},setCursor:dO(function(a,b,c){if(b==null&&typeof a.line=="number")cL(a.line,a.ch,c);else cL(a,b,c)}),setSelection:dO(function(a,b,c){(c?cI:cJ)(cN(a),cN(b||a))}),getLine:function(a){if(bS(a))return bU(a).text},getLineHandle:function(a){if(bS(a))return bU(a)},setLine:dO(function(a,b){if(bS(a))cm(b,{line:a,ch:0},{line:a,ch:bU(a).text.length})}),removeLine:dO(function(a){if(bS(a))cm("",{line:a,ch:0},cN({line:a+1,ch:0}))}),replaceRange:dO(cm),getRange:function(a,b){return cp(cN(a),cN(b))},execCommand:function(a){return h[a](bT)},moveH:dO(cP),deleteH:dO(cQ),moveV:dO(cS),toggleOverwrite:function(){by=!by},posFromIndex:function(a){var b=0,c;bp.iter(0,bp.size,function(d){var e=d.text.length+1;if(e>a){c=a;return true}a-=e;++b});return cN({line:b,ch:c})},indexFromPos:function(a){if(a.line<0||a.ch<0)return 0;var b=a.ch;bp.iter(0,a.line,function(a){b+=a.text.length+1});return b},scrollTo:function(a,b){if(a!=null)P.scrollTop=a;if(b!=null)P.scrollLeft=b;cC([])},operation:function(a){return dO(a)()},refresh:function(){cC(true);if(P.scrollHeight>bw)P.scrollTop=bw},getInputField:function(){return O},getWrapperElement:function(){return C},getScrollerElement:function(){return P},getGutterElement:function(){return bd}};var cb=null;var cr=false;var cu="";var cR=null;db.prototype.clear=dO(function(){var a=Infinity,b=-Infinity;for(var c=0,d=this.set.length;c<d;++c){var e=this.set[c],f=e.marked;if(!f||!e.parent)continue;var g=v(e);a=Math.min(a,g);b=Math.max(b,g);for(var h=0;h<f.length;++h)if(f[h].set==this.set)f.splice(h--,1)}if(a!=Infinity)bC.push({from:a,to:b+1})});db.prototype.find=function(){var a,b;for(var c=0,d=this.set.length;c<d;++c){var e=this.set[c],f=e.marked;for(var g=0;g<f.length;++g){var h=f[g];if(h.set==this.set){if(h.from!=null||h.to!=null){var i=v(e);if(i!=null){if(h.from!=null)a={line:i,ch:h.from};if(h.to!=null)b={line:i,ch:h.to}}}}}}return{from:a,to:b}};var dm=Math.floor(Math.random()*16777215).toString(16);var ds,dt,du;var dw,dx=0;var dE={"(":")>",")":"(<","[":"]>","]":"[<","{":"}>","}":"{<"};var dN=0;for(var dP in g)if(g.propertyIsEnumerable(dP)&&!bT.propertyIsEnumerable(dP))bT[dP]=g[dP];return bT}a.defaults={value:"",mode:null,theme:"default",indentUnit:2,indentWithTabs:false,smartIndent:true,tabSize:4,keyMap:"default",extraKeys:null,electricChars:true,onKeyEvent:null,lineWrapping:false,lineNumbers:false,gutter:false,fixedGutter:false,firstLineNumber:1,readOnly:false,onChange:null,onCursorActivity:null,onGutterClick:null,onHighlightComplete:null,onUpdate:null,onFocus:null,onBlur:null,onScroll:null,matchBrackets:false,workTime:100,workDelay:200,pollInterval:100,undoDepth:40,tabindex:null,document:window.document};var b=/AppleWebKit/.test(navigator.userAgent)&&/Mobile\/\w+/.test(navigator.userAgent);var c=b||/Mac/.test(navigator.platform);var d=/Win/.test(navigator.platform);var e={},f={};a.defineMode=function(b,c){if(!a.defaults.mode&&b!="null")a.defaults.mode=b;e[b]=c};a.defineMIME=function(a,b){f[a]=b};a.getMode=function(b,c){if(typeof c=="string"&&f.hasOwnProperty(c))c=f[c];if(typeof c=="string")var d=c,g={};else if(c!=null)var d=c.name,g=c;var h=e[d];if(!h){if(window.console)console.warn("No mode "+d+" found, falling back to plain text.");return a.getMode(b,"text/plain")}return h(b,g||{})};a.listModes=function(){var a=[];for(var b in e)if(e.propertyIsEnumerable(b))a.push(b);return a};a.listMIMEs=function(){var a=[];for(var b in f)if(f.propertyIsEnumerable(b))a.push({mime:b,mode:f[b]});return a};var g=a.extensions={};a.defineExtension=function(a,b){g[a]=b};var h=a.commands={selectAll:function(a){a.setSelection({line:0,ch:0},{line:a.lineCount()-1})},killLine:function(a){var b=a.getCursor(true),c=a.getCursor(false),d=!T(b,c);if(!d&&a.getLine(b.line).length==b.ch)a.replaceRange("",b,{line:b.line+1,ch:0});else a.replaceRange("",b,d?c:{line:b.line})},deleteLine:function(a){var b=a.getCursor().line;a.replaceRange("",{line:b,ch:0},{line:b})},undo:function(a){a.undo()},redo:function(a){a.redo()},goDocStart:function(a){a.setCursor(0,0,true)},goDocEnd:function(a){a.setSelection({line:a.lineCount()-1},null,true)},goLineStart:function(a){a.setCursor(a.getCursor().line,0,true)},goLineStartSmart:function(a){var b=a.getCursor();var c=a.getLine(b.line),d=Math.max(0,c.search(/\S/));a.setCursor(b.line,b.ch<=d&&b.ch?0:d,true)},goLineEnd:function(a){a.setSelection({line:a.getCursor().line},null,true)},goLineUp:function(a){a.moveV(-1,"line")},goLineDown:function(a){a.moveV(1,"line")},goPageUp:function(a){a.moveV(-1,"page")},goPageDown:function(a){a.moveV(1,"page")},goCharLeft:function(a){a.moveH(-1,"char")},goCharRight:function(a){a.moveH(1,"char")},goColumnLeft:function(a){a.moveH(-1,"column")},goColumnRight:function(a){a.moveH(1,"column")},goWordLeft:function(a){a.moveH(-1,"word")},goWordRight:function(a){a.moveH(1,"word")},delCharLeft:function(a){a.deleteH(-1,"char")},delCharRight:function(a){a.deleteH(1,"char")},delWordLeft:function(a){a.deleteH(-1,"word")},delWordRight:function(a){a.deleteH(1,"word")},indentAuto:function(a){a.indentSelection("smart")},indentMore:function(a){a.indentSelection("add")},indentLess:function(a){a.indentSelection("subtract")},insertTab:function(a){a.replaceSelection("\t","end")},transposeChars:function(a){var b=a.getCursor(),c=a.getLine(b.line);if(b.ch>0&&b.ch<c.length-1)a.replaceRange(c.charAt(b.ch)+c.charAt(b.ch-1),{line:b.line,ch:b.ch-1},{line:b.line,ch:b.ch+1})},newlineAndIndent:function(a){a.replaceSelection("\n","end");a.indentLine(a.getCursor().line)},toggleOverwrite:function(a){a.toggleOverwrite()}};var i=a.keyMap={};i.basic={Left:"goCharLeft",Right:"goCharRight",Up:"goLineUp",Down:"goLineDown",End:"goLineEnd",Home:"goLineStartSmart",PageUp:"goPageUp",PageDown:"goPageDown",Delete:"delCharRight",Backspace:"delCharLeft",Tab:"indentMore","Shift-Tab":"indentLess",Enter:"newlineAndIndent",Insert:"toggleOverwrite"};i.pcDefault={"Ctrl-A":"selectAll","Ctrl-D":"deleteLine","Ctrl-Z":"undo","Shift-Ctrl-Z":"redo","Ctrl-Y":"redo","Ctrl-Home":"goDocStart","Alt-Up":"goDocStart","Ctrl-End":"goDocEnd","Ctrl-Down":"goDocEnd","Ctrl-Left":"goWordLeft","Ctrl-Right":"goWordRight","Alt-Left":"goLineStart","Alt-Right":"goLineEnd","Ctrl-Backspace":"delWordLeft","Ctrl-Delete":"delWordRight","Ctrl-S":"save","Ctrl-F":"find","Ctrl-G":"findNext","Shift-Ctrl-G":"findPrev","Shift-Ctrl-F":"replace","Shift-Ctrl-R":"replaceAll",fallthrough:"basic"};i.macDefault={"Cmd-A":"selectAll","Cmd-D":"deleteLine","Cmd-Z":"undo","Shift-Cmd-Z":"redo","Cmd-Y":"redo","Cmd-Up":"goDocStart","Cmd-End":"goDocEnd","Cmd-Down":"goDocEnd","Alt-Left":"goWordLeft","Alt-Right":"goWordRight","Cmd-Left":"goLineStart","Cmd-Right":"goLineEnd","Alt-Backspace":"delWordLeft","Ctrl-Alt-Backspace":"delWordRight","Alt-Delete":"delWordRight","Cmd-S":"save","Cmd-F":"find","Cmd-G":"findNext","Shift-Cmd-G":"findPrev","Cmd-Alt-F":"replace","Shift-Cmd-Alt-F":"replaceAll",fallthrough:["basic","emacsy"]};i["default"]=c?i.macDefault:i.pcDefault;i.emacsy={"Ctrl-F":"goCharRight","Ctrl-B":"goCharLeft","Ctrl-P":"goLineUp","Ctrl-N":"goLineDown","Alt-F":"goWordRight","Alt-B":"goWordLeft","Ctrl-A":"goLineStart","Ctrl-E":"goLineEnd","Ctrl-V":"goPageUp","Shift-Ctrl-V":"goPageDown","Ctrl-D":"delCharRight","Ctrl-H":"delCharLeft","Alt-D":"delWordRight","Alt-Backspace":"delWordLeft","Ctrl-K":"killLine","Ctrl-T":"transposeChars"};a.fromTextArea=function(b,c){function d(){b.value=h.getValue()}if(!c)c={};c.value=b.value;if(!c.tabindex&&b.tabindex)c.tabindex=b.tabindex;if(b.form){var e=H(b.form,"submit",d,true);if(typeof b.form.submit=="function"){var f=b.form.submit;function g(){d();b.form.submit=f;b.form.submit();b.form.submit=g}b.form.submit=g}}b.style.display="none";var h=a(function(a){b.parentNode.insertBefore(a,b.nextSibling)},c);h.save=d;h.getTextArea=function(){return b};h.toTextArea=function(){d();b.parentNode.removeChild(h.getWrapperElement());b.style.display="";if(b.form){e();if(typeof b.form.submit=="function")b.form.submit=f}};return h};a.copyState=l;a.startState=m;n.prototype={eol:function(){return this.pos>=this.string.length},sol:function(){return this.pos==0},peek:function(){return this.string.charAt(this.pos)},next:function(){if(this.pos<this.string.length)return this.string.charAt(this.pos++)},eat:function(a){var b=this.string.charAt(this.pos);if(typeof a=="string")var c=b==a;else var c=b&&(a.test?a.test(b):a(b));if(c){++this.pos;return b}},eatWhile:function(a){var b=this.pos;while(this.eat(a)){}return this.pos>b},eatSpace:function(){var a=this.pos;while(/[\s\u00a0]/.test(this.string.charAt(this.pos)))++this.pos;return this.pos>a},skipToEnd:function(){this.pos=this.string.length},skipTo:function(a){var b=this.string.indexOf(a,this.pos);if(b>-1){this.pos=b;return true}},backUp:function(a){this.pos-=a},column:function(){return O(this.string,this.start,this.tabSize)},indentation:function(){return O(this.string,null,this.tabSize)},match:function(a,b,c){if(typeof a=="string"){function d(a){return c?a.toLowerCase():a}if(d(this.string).indexOf(d(a),this.pos)==this.pos){if(b!==false)this.pos+=a.length;return true}}else{var e=this.string.slice(this.pos).match(a);if(e&&b!==false)this.pos+=e[0].length;return e}},current:function(){return this.string.slice(this.start,this.pos)}};a.StringStream=n;o.prototype={attach:function(a){this.set.push(a)},detach:function(a){var b=Z(this.set,a);if(b>-1)this.set.splice(b,1)},split:function(a,b){if(this.to<=a&&this.to!=null)return null;var c=this.from<a||this.from==null?null:this.from-a+b;var d=this.to==null?null:this.to-a+b;return new o(c,d,this.style,this.set)},dup:function(){return new o(null,null,this.style,this.set)},clipTo:function(a,b,c,d,e){if(this.from!=null&&this.from>=b)this.from=Math.max(d,this.from)+e;if(this.to!=null&&this.to>b)this.to=d<this.to?this.to+e:b;if(a&&d>this.from&&(d<this.to||this.to==null))this.from=null;if(c&&(b<this.to||this.to==null)&&(b>this.from||this.from==null))this.to=null},isDead:function(){return this.from!=null&&this.to!=null&&this.from>=this.to},sameSet:function(a){return this.set==a.set}};p.prototype={attach:function(a){this.line=a},detach:function(a){if(this.line==a)this.line=null},split:function(a,b){if(a<this.from){this.from=this.to=this.from-a+b;return this}},isDead:function(){return this.from>this.to},clipTo:function(a,b,c,d,e){if((a||b<this.from)&&(c||d>this.to)){this.from=0;this.to=-1}else if(this.from>b){this.from=this.to=Math.max(d,this.from)+e}},sameSet:function(a){return false},find:function(){if(!this.line||!this.line.parent)return null;return{line:v(this.line),ch:this.from}},clear:function(){if(this.line){var a=Z(this.line.marked,this);if(a!=-1)this.line.marked.splice(a,1);this.line=null}}};q.inheritMarks=function(a,b){var c=new q(a),d=b&&b.marked;if(d){for(var e=0;e<d.length;++e){if(d[e].to==null&&d[e].style){var f=c.marked||(c.marked=[]),g=d[e];var h=g.dup();f.push(h);h.attach(c)}}}return c};q.prototype={replace:function(a,b,c){var d=[],e=this.marked,f=b==null?this.text.length:b;r(0,a,this.styles,d);if(c)d.push(c,null);r(f,this.text.length,this.styles,d);this.styles=d;this.text=this.text.slice(0,a)+c+this.text.slice(f);this.stateAfter=null;if(e){var g=c.length-(f-a);for(var h=0;h<e.length;++h){var i=e[h];i.clipTo(a==null,a||0,b==null,f,g);if(i.isDead()){i.detach(this);e.splice(h--,1)}}}},split:function(a,b){var c=[b,null],d=this.marked;r(a,this.text.length,this.styles,c);var e=new q(b+this.text.slice(a),c);if(d){for(var f=0;f<d.length;++f){var g=d[f];var h=g.split(a,b.length);if(h){if(!e.marked)e.marked=[];e.marked.push(h);h.attach(e)}}}return e},append:function(a){var b=this.text.length,c=a.marked,d=this.marked;this.text+=a.text;r(0,a.text.length,a.styles,this.styles);if(d){for(var e=0;e<d.length;++e)if(d[e].to==null)d[e].to=b}if(c&&c.length){if(!d)this.marked=d=[];outer:for(var e=0;e<c.length;++e){var f=c[e];if(!f.from){for(var g=0;g<d.length;++g){var h=d[g];if(h.to==b&&h.sameSet(f)){h.to=f.to==null?null:f.to+b;if(h.isDead()){h.detach(this);c.splice(e--,1)}continue outer}}}d.push(f);f.attach(this);f.from+=b;if(f.to!=null)f.to+=b}}},fixMarkEnds:function(a){var b=this.marked,c=a.marked;if(!b)return;for(var d=0;d<b.length;++d){var e=b[d],f=e.to==null;if(f&&c){for(var g=0;g<c.length;++g)if(c[g].sameSet(e)){f=false;break}}if(f)e.to=this.text.length}},fixMarkStarts:function(){var a=this.marked;if(!a)return;for(var b=0;b<a.length;++b)if(a[b].from==null)a[b].from=0},addMark:function(a){a.attach(this);if(this.marked==null)this.marked=[];this.marked.push(a);this.marked.sort(function(a,b){return(a.from||0)-(b.from||0)})},highlight:function(a,b,c){var d=new n(this.text,c),e=this.styles,f=0;var g=false,h=e[0],i;if(this.text==""&&a.blankLine)a.blankLine(b);while(!d.eol()){var j=a.token(d,b);var k=this.text.slice(d.start,d.pos);d.start=d.pos;if(f&&e[f-1]==j)e[f-2]+=k;else if(k){if(!g&&(e[f+1]!=j||f&&e[f-2]!=i))g=true;e[f++]=k;e[f++]=j;i=h;h=e[f]}if(d.pos>5e3){e[f++]=this.text.slice(d.pos);e[f++]=null;break}}if(e.length!=f){e.length=f;g=true}if(f&&e[f-2]!=i)g=true;return g||(e.length<5&&this.text.length<10?null:false)},getTokenAt:function(a,b,c){var d=this.text,e=new n(d);while(e.pos<c&&!e.eol()){e.start=e.pos;var f=a.token(e,b)}return{start:e.start,end:e.pos,string:e.current(),className:f||null,state:b}},indentation:function(a){return O(this.text,null,a)},getHTML:function(a,b){function j(a){if(!a)return null;return"cm-"+a.replace(/ +/g," cm-")}function e(b,e){if(!b)return;if(d&&L&&b.charAt(0)==" ")b=" "+b.slice(1);d=false;if(e)c.push('<span class="',e,'">',X(b).replace(/\t/g,a),"</span>");else c.push(X(b).replace(/\t/g,a))}var c=[],d=true;var f=this.styles,g=this.text,h=this.marked;var i=g.length;if(b!=null)i=Math.min(b,i);if(!g&&b==null)e(" ");else if(!h||!h.length)for(var k=0,l=0;l<i;k+=2){var m=f[k],n=f[k+1],o=m.length;if(l+o>i)m=m.slice(0,i-l);l+=o;e(m,j(n))}else{var p=0,k=0,q="",n,r=0;var s=h[0].from||0,t=[],u=0;function v(){var a;while(u<h.length&&((a=h[u]).from==p||a.from==null)){if(a.style!=null)t.push(a);++u}s=u<h.length?h[u].from:Infinity;for(var b=0;b<t.length;++b){var c=t[b].to||Infinity;if(c==p)t.splice(b--,1);else s=Math.min(c,s)}}var w=0;while(p<i){if(s==p)v();var x=Math.min(i,s);while(true){if(q){var y=p+q.length;var z=n;for(var A=0;A<t.length;++A)z=(z?z+" ":"")+t[A].style;e(y>x?q.slice(0,x-p):q,z);if(y>=x){q=q.slice(x-p);p=x;break}p=y}q=f[k++];n=j(f[k++])}}}return c.join("")},cleanUp:function(){this.parent=null;if(this.marked)for(var a=0,b=this.marked.length;a<b;++a)this.marked[a].detach(this)}};s.prototype={chunkSize:function(){return this.lines.length},remove:function(a,b,c){for(var d=a,e=a+b;d<e;++d){var f=this.lines[d];this.height-=f.height;f.cleanUp();if(f.handlers)for(var g=0;g<f.handlers.length;++g)c.push(f.handlers[g])}this.lines.splice(a,b)},collapse:function(a){a.splice.apply(a,[a.length,0].concat(this.lines))},insertHeight:function(a,b,c){this.height+=c;this.lines.splice.apply(this.lines,[a,0].concat(b));for(var d=0,e=b.length;d<e;++d)b[d].parent=this},iterN:function(a,b,c){for(var d=a+b;a<d;++a)if(c(this.lines[a]))return true}};t.prototype={chunkSize:function(){return this.size},remove:function(a,b,c){this.size-=b;for(var d=0;d<this.children.length;++d){var e=this.children[d],f=e.chunkSize();if(a<f){var g=Math.min(b,f-a),h=e.height;e.remove(a,g,c);this.height-=h-e.height;if(f==g){this.children.splice(d--,1);e.parent=null}if((b-=g)==0)break;a=0}else a-=f}if(this.size-b<25){var i=[];this.collapse(i);this.children=[new s(i)];this.children[0].parent=this}},collapse:function(a){for(var b=0,c=this.children.length;b<c;++b)this.children[b].collapse(a)},insert:function(a,b){var c=0;for(var d=0,e=b.length;d<e;++d)c+=b[d].height;this.insertHeight(a,b,c)},insertHeight:function(a,b,c){this.size+=b.length;this.height+=c;for(var d=0,e=this.children.length;d<e;++d){var f=this.children[d],g=f.chunkSize();if(a<=g){f.insertHeight(a,b,c);if(f.lines&&f.lines.length>50){while(f.lines.length>50){var h=f.lines.splice(f.lines.length-25,25);var i=new s(h);f.height-=i.height;this.children.splice(d+1,0,i);i.parent=this}this.maybeSpill()}break}a-=g}},maybeSpill:function(){if(this.children.length<=10)return;var a=this;do{var b=a.children.splice(a.children.length-5,5);var c=new t(b);if(!a.parent){var d=new t(a.children);d.parent=a;a.children=[d,c];a=d}else{a.size-=c.size;a.height-=c.height;var e=Z(a.parent.children,a);a.parent.children.splice(e+1,0,c)}c.parent=a.parent}while(a.children.length>10);a.parent.maybeSpill()},iter:function(a,b,c){this.iterN(a,b-a,c)},iterN:function(a,b,c){for(var d=0,e=this.children.length;d<e;++d){var f=this.children[d],g=f.chunkSize();if(a<g){var h=Math.min(b,g-a);if(f.iterN(a,h,c))return true;if((b-=h)==0)break;a=0}else a-=g}}};y.prototype={addChange:function(a,b,c){this.undone.length=0;var d=+(new Date),e=this.done[this.done.length-1],f=e&&e[e.length-1];var g=d-this.time;if(g>400||!f){this.done.push([{start:a,added:b,old:c}])}else if(f.start>a+b||f.start+f.added<a-f.added+f.old.length){e.push({start:a,added:b,old:c})}else{var h=0;if(a<f.start){for(var i=f.start-a-1;i>=0;--i)f.old.unshift(c[i]);f.added+=f.start-a;f.start=a}else if(f.start<a){h=a-f.start;b+=h}for(var i=f.added-h,j=c.length;i<j;++i)f.old.push(c[i]);if(f.added<b)f.added=b}this.time=d}};a.e_stop=D;a.e_preventDefault=B;a.e_stopPropagation=C;a.connect=H;I.prototype={set:function(a,b){clearTimeout(this.id);this.id=setTimeout(b,a)}};var J=function(){if(/MSIE [1-8]\b/.test(navigator.userAgent))return false;var a=document.createElement("div");return"draggable"in a}();var K=/gecko\/\d{7}/i.test(navigator.userAgent);var L=/MSIE \d/.test(navigator.userAgent);var M=/WebKit\//.test(navigator.userAgent);var N="\n";(function(){var a=document.createElement("textarea");a.value="foo\nbar";if(a.value.indexOf("\r")>-1)N="\r\n"})();if(document.documentElement.getBoundingClientRect!=null)Q=function(a,b){try{var c=a.getBoundingClientRect();c={top:c.top,left:c.left}}catch(d){c={top:0,left:0}}if(!b){if(window.pageYOffset==null){var e=document.documentElement||document.body.parentNode;if(e.scrollTop==null)e=document.body;c.top+=e.scrollTop;c.left+=e.scrollLeft}else{c.top+=window.pageYOffset;c.left+=window.pageXOffset}}return c};var W=document.createElement("pre");if(X("a")=="\na")X=function(a){W.textContent=a;return W.innerHTML.slice(1)};else if(X("\t")!="\t")X=function(a){W.innerHTML="";W.appendChild(document.createTextNode(a));return W.innerHTML};a.htmlEscape=X;var _="\n\nb".split(/\n/).length!=3?function(a){var b=0,c,d=[];while((c=a.indexOf("\n",b))>-1){d.push(a.slice(b,a.charAt(c-1)=="\r"?c-1:c));b=c+1}d.push(a.slice(b));return d}:function(a){return a.split(/\r?\n/)};a.splitLines=_;var ba=window.getSelection?function(a){try{return a.selectionStart!=a.selectionEnd}catch(b){return false}}:function(a){try{var b=a.ownerDocument.selection.createRange()}catch(c){}if(!b||b.parentElement()!=a)return false;return b.compareEndPoints("StartToEnd",b)!=0};a.defineMode("null",function(){return{token:function(a){a.skipToEnd()}}});a.defineMIME("text/plain","null");var bb={3:"Enter",8:"Backspace",9:"Tab",13:"Enter",16:"Shift",17:"Ctrl",18:"Alt",19:"Pause",20:"CapsLock",27:"Esc",32:"Space",33:"PageUp",34:"PageDown",35:"End",36:"Home",37:"Left",38:"Up",39:"Right",40:"Down",44:"PrintScrn",45:"Insert",46:"Delete",59:";",91:"Mod",92:"Mod",93:"Mod",186:";",187:"=",188:",",189:"-",190:".",191:"/",192:"`",219:"[",220:"\\",221:"]",222:"'",63276:"PageUp",63277:"PageDown",63275:"End",63273:"Home",63234:"Left",63232:"Up",63235:"Right",63233:"Down",63302:"Insert",63272:"Delete"};a.keyNames=bb;(function(){for(var a=0;a<10;a++)bb[a+48]=String(a);for(var a=65;a<=90;a++)bb[a]=String.fromCharCode(a);for(var a=1;a<=12;a++)bb[a+111]=bb[a+63235]="F"+a})();return a}();

// Modes
CodeMirror.defineMode("xml",function(a,b){function x(a){if(a=="string")return p(x);else return o()}function w(a){if(a=="word"&&d.allowUnquoted){n="string";return p()}if(a=="string")return p(x);return o()}function v(a){if(a=="word"){n="attribute";return p(v)}if(a=="equals")return p(w,v);if(a=="string"){n="error";return p(v)}return o()}function u(a){return function(b){if(a)n="error";if(b=="endTag"){r();return p()}n="error";return p(arguments.callee)}}function t(a){return function(b){if(b=="selfcloseTag"||b=="endTag"&&d.autoSelfClosers.hasOwnProperty(m.tagName.toLowerCase()))return p();if(b=="endTag"){q(m.tagName,a);return p()}return p()}}function s(a){if(a=="openTag"){m.tagName=f;return p(v,t(m.startOfLine))}else if(a=="closeTag"){var b=false;if(m.context){b=m.context.tagName!=f}else{b=true}if(b)n="error";return p(u(b))}return p()}function r(){if(m.context)m.context=m.context.prev}function q(a,b){var c=d.doNotIndent.hasOwnProperty(a)||m.context&&m.context.noIndent;m.context={prev:m.context,tagName:a,indent:m.indented,startOfLine:b,noIndent:c}}function p(){o.apply(null,arguments);return true}function o(){for(var a=arguments.length-1;a>=0;a--)m.cc.push(arguments[a])}function l(a){return function(b,c){var d;while((d=b.next())!=null){if(d=="<"){c.tokenize=l(a+1);return c.tokenize(b,c)}else if(d==">"){if(a==1){c.tokenize=h;break}else{c.tokenize=l(a-1);return c.tokenize(b,c)}}}return"meta"}}function k(a,b){return function(c,d){while(!c.eol()){if(c.match(b)){d.tokenize=h;break}c.next()}return a}}function j(a){return function(b,c){while(!b.eol()){if(b.next()==a){c.tokenize=i;break}}return"string"}}function i(a,b){var c=a.next();if(c==">"||c=="/"&&a.eat(">")){b.tokenize=h;g=c==">"?"endTag":"selfcloseTag";return"tag"}else if(c=="="){g="equals";return null}else if(/[\'\"]/.test(c)){b.tokenize=j(c);return b.tokenize(a,b)}else{a.eatWhile(/[^\s\u00a0=<>\"\'\/?]/);return"word"}}function h(a,b){function c(c){b.tokenize=c;return c(a,b)}var d=a.next();if(d=="<"){if(a.eat("!")){if(a.eat("[")){if(a.match("CDATA["))return c(k("atom","]]>"));else return null}else if(a.match("--"))return c(k("comment","-->"));else if(a.match("DOCTYPE",true,true)){a.eatWhile(/[\w\._\-]/);return c(l(1))}else return null}else if(a.eat("?")){a.eatWhile(/[\w\._\-]/);b.tokenize=k("meta","?>");return"meta"}else{g=a.eat("/")?"closeTag":"openTag";a.eatSpace();f="";var e;while(e=a.eat(/[^\s\u00a0=<>\"\'\/?]/))f+=e;b.tokenize=i;return"tag"}}else if(d=="&"){var h;if(a.eat("#")){if(a.eat("x")){h=a.eatWhile(/[a-fA-F\d]/)&&a.eat(";")}else{h=a.eatWhile(/[\d]/)&&a.eat(";")}}else{h=a.eatWhile(/[\w]/)&&a.eat(";")}return h?"atom":"error"}else{a.eatWhile(/[^&<]/);return null}}var c=a.indentUnit;var d=b.htmlMode?{autoSelfClosers:{br:true,img:true,hr:true,link:true,input:true,meta:true,col:true,frame:true,base:true,area:true},doNotIndent:{pre:true},allowUnquoted:true}:{autoSelfClosers:{},doNotIndent:{},allowUnquoted:false};var e=b.alignCDATA;var f,g;var m,n;return{startState:function(){return{tokenize:h,cc:[],indented:0,startOfLine:true,tagName:null,context:null}},token:function(a,b){if(a.sol()){b.startOfLine=true;b.indented=a.indentation()}if(a.eatSpace())return null;n=g=f=null;var c=b.tokenize(a,b);b.type=g;if((c||g)&&c!="comment"){m=b;while(true){var d=b.cc.pop()||s;if(d(g||c))break}}b.startOfLine=false;return n||c},indent:function(a,b,d){var f=a.context;if(a.tokenize!=i&&a.tokenize!=h||f&&f.noIndent)return d?d.match(/^(\s*)/)[0].length:0;if(e&&/<!\[CDATA\[/.test(b))return 0;if(f&&/^<\//.test(b))f=f.prev;while(f&&!f.startOfLine)f=f.prev;if(f)return f.indent+c;else return 0},compareStates:function(a,b){if(a.indented!=b.indented||a.tokenize!=b.tokenize)return false;for(var c=a.context,d=b.context;;c=c.prev,d=d.prev){if(!c||!d)return c==d;if(c.tagName!=d.tagName)return false}},electricChars:"/"}});CodeMirror.defineMIME("application/xml","xml");CodeMirror.defineMIME("text/html",{name:"xml",htmlMode:true})
CodeMirror.defineMode("javascript",function(a,b){function S(a,b){if(a=="variable"){v(b);return u()}}function R(a,b){if(a=="variable"){v(b);return u(R)}if(a=="(")return u(z(")"),x,J(S,")"),A,C,y)}function Q(a){if(a!=")")u(D)}function P(a,b){if(a==";")return u(Q);if(b=="in")return u(D);return u(D,B(";"),Q)}function O(a,b){if(b=="in")return u(D);return u(F,P)}function N(a){if(a=="var")return u(L,P);if(a==";")return t(P);if(a=="variable")return u(O);return t(P)}function M(a,b){if(b=="=")return u(D,M);if(a==",")return u(L)}function L(a,b){if(a=="variable"){v(b);return u(M)}return u()}function K(a){if(a=="}")return u();return t(C,K)}function J(a,b){function c(d){if(d==",")return u(a,c);if(d==b)return u();return u(B(b))}return function d(d){if(d==b)return u();else return t(a,c)}}function I(a){if(a=="variable")s.marked="property";if(o.hasOwnProperty(a))return u(B(":"),D)}function H(a){if(a=="variable"){s.marked="property";return u()}}function G(a){if(a==":")return u(A,C);return t(F,B(";"),A)}function F(a,b){if(a=="operator"&&/\+\+|--/.test(b))return u(F);if(a=="operator")return u(D);if(a==";")return;if(a=="(")return u(z(")"),J(D,")"),A,F);if(a==".")return u(H,F);if(a=="[")return u(z("]"),D,B("]"),A,F)}function E(a){if(a.match(/[;\}\)\],]/))return t();return t(D)}function D(a){if(o.hasOwnProperty(a))return u(F);if(a=="function")return u(R);if(a=="keyword c")return u(E);if(a=="(")return u(z(")"),E,B(")"),A,F);if(a=="operator")return u(D);if(a=="[")return u(z("]"),J(D,"]"),A,F);if(a=="{")return u(z("}"),J(I,"}"),A,F);return u()}function C(a){if(a=="var")return u(z("vardef"),L,B(";"),A);if(a=="keyword a")return u(z("form"),D,C,A);if(a=="keyword b")return u(z("form"),C,A);if(a=="{")return u(z("}"),K,A);if(a==";")return u();if(a=="function")return u(R);if(a=="for")return u(z("form"),B("("),z(")"),N,B(")"),A,C,A);if(a=="variable")return u(z("stat"),G);if(a=="switch")return u(z("form"),D,z("}","switch"),B("{"),K,A,A);if(a=="case")return u(D,B(":"));if(a=="default")return u(B(":"));if(a=="catch")return u(z("form"),x,B("("),S,B(")"),C,A,y);return t(z("stat"),D,B(";"),A)}function B(a){return function b(b){if(b==a)return u();else if(a==";")return t();else return u(arguments.callee)}}function A(){var a=s.state;if(a.lexical.prev){if(a.lexical.type==")")a.indented=a.lexical.indented;a.lexical=a.lexical.prev}}function z(a,b){var c=function(){var c=s.state;c.lexical=new p(c.indented,s.stream.column(),a,null,c.lexical,b)};c.lex=true;return c}function y(){s.state.localVars=s.state.context.vars;s.state.context=s.state.context.prev}function x(){if(!s.state.context)s.state.localVars=w;s.state.context={prev:s.state.context,vars:s.state.localVars}}function v(a){var b=s.state;if(b.context){s.marked="def";for(var c=b.localVars;c;c=c.next)if(c.name==a)return;b.localVars={name:a,next:b.localVars}}}function u(){t.apply(null,arguments);return true}function t(){for(var a=arguments.length-1;a>=0;a--)s.cc.push(arguments[a])}function r(a,b,c,e,f){var g=a.cc;s.state=a;s.stream=f;s.marked=null,s.cc=g;if(!a.lexical.hasOwnProperty("align"))a.lexical.align=true;while(true){var h=g.length?g.pop():d?D:C;if(h(c,e)){while(g.length&&g[g.length-1].lex)g.pop()();if(s.marked)return s.marked;if(c=="variable"&&q(a,e))return"variable-2";return b}}}function q(a,b){for(var c=a.localVars;c;c=c.next)if(c.name==b)return true}function p(a,b,c,d,e,f){this.indented=a;this.column=b;this.type=c;this.prev=e;this.info=f;if(d!=null)this.align=d}function n(a,b){var c=false,d;while(d=a.next()){if(d=="/"&&c){b.tokenize=l;break}c=d=="*"}return k("comment","comment")}function m(a){return function(b,c){if(!h(b,a))c.tokenize=l;return k("string","string")}}function l(a,b){var c=a.next();if(c=='"'||c=="'")return g(a,b,m(c));else if(/[\[\]{}\(\),;\:\.]/.test(c))return k(c);else if(c=="0"&&a.eat(/x/i)){a.eatWhile(/[\da-f]/i);return k("number","number")}else if(/\d/.test(c)){a.match(/^\d*(?:\.\d*)?(?:[eE][+\-]?\d+)?/);return k("number","number")}else if(c=="/"){if(a.eat("*")){return g(a,b,n)}else if(a.eat("/")){a.skipToEnd();return k("comment","comment")}else if(b.reAllowed){h(a,"/");a.eatWhile(/[gimy]/);return k("regexp","string-2")}else{a.eatWhile(f);return k("operator",null,a.current())}}else if(c=="#"){a.skipToEnd();return k("error","error")}else if(f.test(c)){a.eatWhile(f);return k("operator",null,a.current())}else{a.eatWhile(/[\w\$_]/);var d=a.current(),i=e.propertyIsEnumerable(d)&&e[d];return i&&b.kwAllowed?k(i.type,i.style,d):k("variable","variable",d)}}function k(a,b,c){i=a;j=c;return b}function h(a,b){var c=false,d;while((d=a.next())!=null){if(d==b&&!c)return false;c=!c&&d=="\\"}return c}function g(a,b,c){b.tokenize=c;return c(a,b)}var c=a.indentUnit;var d=b.json;var e=function(){function a(a){return{type:a,style:"keyword"}}var b=a("keyword a"),c=a("keyword b"),d=a("keyword c");var e=a("operator"),f={type:"atom",style:"atom"};return{"if":b,"while":b,"with":b,"else":c,"do":c,"try":c,"finally":c,"return":d,"break":d,"continue":d,"new":d,"delete":d,"throw":d,"var":a("var"),"const":a("var"),let:a("var"),"function":a("function"),"catch":a("catch"),"for":a("for"),"switch":a("switch"),"case":a("case"),"default":a("default"),"in":e,"typeof":e,"instanceof":e,"true":f,"false":f,"null":f,"undefined":f,NaN:f,Infinity:f}}();var f=/[+\-*&%=<>!?|]/;var i,j;var o={atom:true,number:true,variable:true,string:true,regexp:true};var s={state:null,column:null,marked:null,cc:null};var w={name:"this",next:{name:"arguments"}};A.lex=true;return{startState:function(a){return{tokenize:l,reAllowed:true,kwAllowed:true,cc:[],lexical:new p((a||0)-c,0,"block",false),localVars:null,context:null,indented:0}},token:function(a,b){if(a.sol()){if(!b.lexical.hasOwnProperty("align"))b.lexical.align=false;b.indented=a.indentation()}if(a.eatSpace())return null;var c=b.tokenize(a,b);if(i=="comment")return c;b.reAllowed=i=="operator"||i=="keyword c"||i.match(/^[\[{}\(,;:]$/);b.kwAllowed=i!=".";return r(b,c,i,j,a)},indent:function(a,b){if(a.tokenize!=l)return 0;var d=b&&b.charAt(0),e=a.lexical,f=e.type,g=d==f;if(f=="vardef")return e.indented+4;else if(f=="form"&&d=="{")return e.indented;else if(f=="stat"||f=="form")return e.indented+c;else if(e.info=="switch"&&!g)return e.indented+(/^(?:case|default)\b/.test(b)?c:2*c);else if(e.align)return e.column+(g?0:1);else return e.indented+(g?0:c)},electricChars:":{}"}});CodeMirror.defineMIME("text/javascript","javascript");CodeMirror.defineMIME("application/json",{name:"javascript",json:true})
CodeMirror.defineMode("css",function(a){function h(a){return function(b,c){var f=false,g;while((g=b.next())!=null){if(g==a&&!f)break;f=!f&&g=="\\"}if(!f)c.tokenize=e;return d("string","string")}}function g(a,b){var c=0,f;while((f=a.next())!=null){if(c>=2&&f==">"){b.tokenize=e;break}c=f=="-"?c+1:0}return d("comment","comment")}function f(a,b){var c=false,f;while((f=a.next())!=null){if(c&&f=="/"){b.tokenize=e;break}c=f=="*"}return d("comment","comment")}function e(a,b){var c=a.next();if(c=="@"){a.eatWhile(/[\w\\\-]/);return d("meta",a.current())}else if(c=="/"&&a.eat("*")){b.tokenize=f;return f(a,b)}else if(c=="<"&&a.eat("!")){b.tokenize=g;return g(a,b)}else if(c=="=")d(null,"compare");else if((c=="~"||c=="|")&&a.eat("="))return d(null,"compare");else if(c=='"'||c=="'"){b.tokenize=h(c);return b.tokenize(a,b)}else if(c=="#"){a.eatWhile(/[\w\\\-]/);return d("atom","hash")}else if(c=="!"){a.match(/^\s*\w*/);return d("keyword","important")}else if(/\d/.test(c)){a.eatWhile(/[\w.%]/);return d("number","unit")}else if(/[,.+>*\/]/.test(c)){return d(null,"select-op")}else if(/[;{}:\[\]]/.test(c)){return d(null,c)}else{a.eatWhile(/[\w\\\-]/);return d("variable","variable")}}function d(a,b){c=b;return a}var b=a.indentUnit,c;return{startState:function(a){return{tokenize:e,baseIndent:a||0,stack:[]}},token:function(a,b){if(a.eatSpace())return null;var d=b.tokenize(a,b);var e=b.stack[b.stack.length-1];if(c=="hash"&&e=="rule")d="atom";else if(d=="variable"){if(e=="rule")d="number";else if(!e||e=="@media{")d="tag"}if(e=="rule"&&/^[\{\};]$/.test(c))b.stack.pop();if(c=="{"){if(e=="@media")b.stack[b.stack.length-1]="@media{";else b.stack.push("{")}else if(c=="}")b.stack.pop();else if(c=="@media")b.stack.push("@media");else if(e=="{"&&c!="comment")b.stack.push("rule");return d},indent:function(a,c){var d=a.stack.length;if(/^\}/.test(c))d-=a.stack[a.stack.length-1]=="rule"?2:1;return a.baseIndent+d*b},electricChars:"}"}});CodeMirror.defineMIME("text/css","css")
CodeMirror.defineMode("htmlmixed",function(a,b){function i(a,b){if(a.match(/^<\/\s*style\s*>/i,false)){b.token=f;b.localState=null;b.mode="html";return f(a,b)}return g(a,/<\/\s*style\s*>/,e.token(a,b.localState))}function h(a,b){if(a.match(/^<\/\s*script\s*>/i,false)){b.token=f;b.curState=null;b.mode="html";return f(a,b)}return g(a,/<\/\s*script\s*>/,d.token(a,b.localState))}function g(a,b,c){var d=a.current();var e=d.search(b);if(e>-1)a.backUp(d.length-e);return c}function f(a,b){var f=c.token(a,b.htmlState);if(f=="tag"&&a.current()==">"&&b.htmlState.context){if(/^script$/i.test(b.htmlState.context.tagName)){b.token=h;b.localState=d.startState(c.indent(b.htmlState,""));b.mode="javascript"}else if(/^style$/i.test(b.htmlState.context.tagName)){b.token=i;b.localState=e.startState(c.indent(b.htmlState,""));b.mode="css"}}return f}var c=CodeMirror.getMode(a,{name:"xml",htmlMode:true});var d=CodeMirror.getMode(a,"javascript");var e=CodeMirror.getMode(a,"css");return{startState:function(){var a=c.startState();return{token:f,localState:null,mode:"html",htmlState:a}},copyState:function(a){if(a.localState)var b=CodeMirror.copyState(a.token==i?e:d,a.localState);return{token:a.token,localState:b,mode:a.mode,htmlState:CodeMirror.copyState(c,a.htmlState)}},token:function(a,b){return b.token(a,b)},indent:function(a,b){if(a.token==f||/^\s*<\//.test(b))return c.indent(a.htmlState,b);else if(a.token==h)return d.indent(a.localState,b);else return e.indent(a.localState,b)},compareStates:function(a,b){return c.compareStates(a.htmlState,b.htmlState)},electricChars:"/{}:"}});CodeMirror.defineMIME("text/html","htmlmixed")
var codemirrorEditors = codemirrorEditors || [];

/**
 * Initializes the CodeMirror editor for given textarea.
 *
 * @param textarea
 * @param {string} mode
 * @return void
 */
function initCodeMirrorEditor(textarea, mode) {
	if (mode === undefined) {
		mode = 'xml';
	}

	require([
		'../typo3conf/ext/dce/Resources/Public/Libraries/codemirror/lib/codemirror',
		'../typo3conf/ext/dce/Resources/Public/Libraries/codemirror/modes/xml',
		'../typo3conf/ext/dce/Resources/Public/Libraries/codemirror/modes/htmlmixed',
	], function(CodeMirror) {
		var editor = CodeMirror.fromTextArea(textarea, {
			mode: mode,
			indentUnit: 4,
			tabSize: 4,
			lineNumbers: true,
			indentWithTabs: true,
			styleActiveLine: true
		});

		codemirrorEditors.push(editor);
		window.setTimeout(function(){
			editor.refresh();
		},10);

			// Provides available variables and add them to codemirror on select
		var variables = $(textarea.id.replace(/.*?_(.*)/gi, 'variables_$1'));
		if (variables) {
			$(variables).onchange = function() {
				if (this.value) {
					if (this.value.match(/^v:/)) {
						editor.replaceSelection('{' + this.value.replace(/.*?:(.*)/gi, '$1') + '}');
					} else if (this.value.match(/^f:/)) {
						editor.replaceSelection(this.value.replace(/.*?:([\s\S]*)/gi, '$1'));
					}
					editor.focus();
					this.value = '';
				}
			}
		}
			// Provides some "ready to start" templates and add them to codemirror on select
		var templates = $(textarea.id.replace(/.*?_(.*)/gi, 'templates_$1'));
		if (templates) {
			$(templates).onchange = function() {
				if (this.value) {
					editor.setValue(this.value);
					editor.focus();
					this.value = '';
				}
			}
		}
			// Prevents a visibility bug in codemirror if it is initialized on a hidden tab page
		$$('.typo3-dyntabmenu a').each(function(){
			this.onclick = function(){
				for (var i = 0; i < codemirrorEditors.length; i++) {
					codemirrorEditors[i].refresh();
				}
			};
		});
	});
}

/**
 * Removes type selectbox of dce section field
 *
 * @param textarea
 * @return void
 */
function disableSectionFieldType(textarea) {
	var parentTable = TYPO3.jQuery(textarea).closest('table').closest('table');
	var selectBox = TYPO3.jQuery(parentTable).find('select[name^="data[tx_dce_domain_model_dcefield]"][name$="[type]"]');
	if (selectBox && TYPO3.jQuery(selectBox).closest('.t3-form-field-record-inline', 1)) {
		var parentTableRow = $(selectBox).closest('tr').hide();
		var labelTableRow = $(parentTableRow).prev('tr').hide();
	}
}
/**
*
*  MD5 (Message-Digest Algorithm)
*  http://www.webtoolkit.info/
*
**/

function MD5(string) {

	function RotateLeft(lValue, iShiftBits) {
		return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
	}

	function AddUnsigned(lX,lY) {
		var lX4,lY4,lX8,lY8,lResult;
		lX8 = (lX & 0x80000000);
		lY8 = (lY & 0x80000000);
		lX4 = (lX & 0x40000000);
		lY4 = (lY & 0x40000000);
		lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
		if (lX4 & lY4) {
			return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
		}
		if (lX4 | lY4) {
			if (lResult & 0x40000000) {
				return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
			} else {
				return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
			}
		} else {
			return (lResult ^ lX8 ^ lY8);
		}
 	}

 	function F(x,y,z) { return (x & y) | ((~x) & z); }
 	function G(x,y,z) { return (x & z) | (y & (~z)); }
 	function H(x,y,z) { return (x ^ y ^ z); }
	function I(x,y,z) { return (y ^ (x | (~z))); }

	function FF(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};

	function GG(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};

	function HH(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};

	function II(a,b,c,d,x,s,ac) {
		a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
		return AddUnsigned(RotateLeft(a, s), b);
	};

	function ConvertToWordArray(string) {
		var lWordCount;
		var lMessageLength = string.length;
		var lNumberOfWords_temp1=lMessageLength + 8;
		var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
		var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
		var lWordArray=Array(lNumberOfWords-1);
		var lBytePosition = 0;
		var lByteCount = 0;
		while ( lByteCount < lMessageLength ) {
			lWordCount = (lByteCount-(lByteCount % 4))/4;
			lBytePosition = (lByteCount % 4)*8;
			lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
			lByteCount++;
		}
		lWordCount = (lByteCount-(lByteCount % 4))/4;
		lBytePosition = (lByteCount % 4)*8;
		lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
		lWordArray[lNumberOfWords-2] = lMessageLength<<3;
		lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
		return lWordArray;
	};

	function WordToHex(lValue) {
		var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
		for (lCount = 0;lCount<=3;lCount++) {
			lByte = (lValue>>>(lCount*8)) & 255;
			WordToHexValue_temp = "0" + lByte.toString(16);
			WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
		}
		return WordToHexValue;
	};

	function Utf8Encode(string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	};

	var x=Array();
	var k,AA,BB,CC,DD,a,b,c,d;
	var S11=7, S12=12, S13=17, S14=22;
	var S21=5, S22=9 , S23=14, S24=20;
	var S31=4, S32=11, S33=16, S34=23;
	var S41=6, S42=10, S43=15, S44=21;

	string = Utf8Encode(string);

	x = ConvertToWordArray(string);

	a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

	for (k=0;k<x.length;k+=16) {
		AA=a; BB=b; CC=c; DD=d;
		a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
		d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
		c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
		b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
		a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
		d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
		c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
		b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
		a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
		d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
		c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
		b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
		a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
		d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
		c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
		b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
		a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
		d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
		c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
		b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
		a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
		d=GG(d,a,b,c,x[k+10],S22,0x2441453);
		c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
		b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
		a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
		d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
		c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
		b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
		a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
		d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
		c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
		b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
		a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
		d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
		c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
		b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
		a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
		d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
		c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
		b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
		a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
		d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
		c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
		b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
		a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
		d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
		c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
		b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
		a=II(a,b,c,d,x[k+0], S41,0xF4292244);
		d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
		c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
		b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
		a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
		d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
		c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
		b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
		a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
		d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
		c=II(c,d,a,b,x[k+6], S43,0xA3014314);
		b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
		a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
		d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
		c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
		b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
		a=AddUnsigned(a,AA);
		b=AddUnsigned(b,BB);
		c=AddUnsigned(c,CC);
		d=AddUnsigned(d,DD);
	}

	var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

	return temp.toLowerCase();
}
/**
 *
 * Ext.ux.EventZone Extension Class for Ext 3.x Library
 *
 * @author  Nigel White
 *
 * @license Ext.ux.EventZone is licensed under the terms of
 * the Open Source LGPL 3.0 license.  Commercial use is permitted to the extent
 * that the code/component(s) do NOT become part of another Open Source or Commercially
 * licensed development library or toolkit without explicit permission.
 *
 * License details: http://www.gnu.org/licenses/lgpl.html
 *
 * @class Ext.ux.EventZone
 * <p>This class implements a "virtual element" at a relative size and position
 * <i>within</i> an existing element. It provides mouse events from a zone of an element of
 * defined dimensions.</p>
 * <p>The zone is defined using <code>top</code>, <code>right</code>, <code>bottom</code>,
 * <code>left</code>, <code>width</code> and <code>height</code> options which specify
 * the bounds of the zone in a similar manner to the CSS style properties of those names.</p>
 * @cfg {String|HtmlElement} el The element in which to create the zone.
 * @cfg {Array} points An Array of points within the element defining the event zone.
 * @cfg {Number} top The top of the zone. If negative means an offset from the bottom.
 * @cfg {Number} right The right of the zone. If negative means an offset from the right.
 * @cfg {Number} left The left of the zone. If negative means an offset from the right.
 * @cfg {Number} bottom The bottom of the zone. If negative means an offset from the bottom.
 * @cfg {Number} width The width of the zone.
 * @cfg {Number} height The height of the zone.
 * @constructor
 * Create a new EventZone
 * @param {Object} config The config object.
 */
Ext.ux.EventZone = Ext.extend(Ext.util.Observable, {

    constructor: function(config) {
        this.initialConfig = config;
        this.addEvents(
            /**
             * @event mouseenter
             * This event fires when the mouse enters the zone.
             * @param {EventObject} e the underlying mouse event.
             * @param {EventZone} this
             */
            'mouseenter',
            /**
             * @event mousedown
             * This event fires when the mouse button is depressed within the zone.
             * @param {EventObject} e the underlying mouse event.
             * @param {EventZone} this
             */
            'mousedown',
            /**
             * @event mousemove
             * This event fires when the mouse moves within the zone.
             * @param {EventObject} e the underlying mouse event.
             * @param {EventZone} this
             */
            'mousemove',
            /**
             * @event mouseup
             * This event fires when the mouse button is released within the zone.
             * @param {EventObject} e the underlying mouse event.
             * @param {EventZone} this
             */
            'mouseup',
            /**
             * @event mouseenter
             * This event fires when the mouse is clicked within the zone.
             * @param {EventObject} e the underlying mouse event.
             * @param {EventZone} this
             */
            'click',
            /**
             * @event mouseleave
             * This event fires when the mouse leaves the zone.
             * @param {EventObject} e the underlying mouse event.
             * @param {EventZone} this
             */
            'mouseleave'
        );
        Ext.apply(this, config);
        this.el = Ext.get(this.el);

//      If a polygon within the element is specified...
        if (this.points) {
            this.polygon = new Ext.lib.Polygon(this.points);
            this.points = this.polygon.points;
        }

        Ext.ux.EventZone.superclass.constructor.call(this);
        this.el.on({
            mouseenter: this.handleMouseEvent,
            mousedown: this.handleMouseEvent,
            mousemove: this.handleMouseEvent,
            mouseup: this.handleMouseEvent,
            click: this.handleMouseEvent,
            mouseleave: this.handleMouseEvent,
            scope: this
        });
    },

    handleMouseEvent: function(e) {
        var r = this.polygon ? this.getPolygon() : this.getRegion();
        var inBounds = r.contains(e.getPoint());

        switch (e.type) {
            // mouseenter fires this
            case 'mouseover':
               if (inBounds) {
                   this.mouseIn = true;
                   this.fireEvent('mouseenter', e, this);
               }
               break;
            // mouseleave fires this
            case 'mouseout':
               this.mouseIn = false;
               this.fireEvent('mouseleave', e, this);
               break;
           case 'mousemove':
               if (inBounds) {
                   if (this.mouseIn) {
                       this.fireEvent('mousemove', e, this);
                   } else {
                       this.mouseIn = true;
                       this.fireEvent('mouseenter', e, this);
                   }
               } else {
                   if (this.mouseIn) {
                       this.mouseIn = false;
                       this.fireEvent('mouseleave', e, this);
                   }
               }
               break;
           default:
               if (inBounds) {
                   this.fireEvent(e.type, e, this);
               }
        }
    },

    getPolygon: function() {
        var xy = this.el.getXY();
        return this.polygon.translate(xy[0], xy[1]);
    },

    getRegion: function() {
        var r = this.el.getRegion();

//      Adjust left boundary of region
        if (Ext.isNumber(this.left)) {
            if (this.left < 0) {
                r.left = r.right + this.left;
            } else {
                r.left += this.left;
            }
        }

//      Adjust right boundary of region
        if (Ext.isNumber(this.width)) {
            r.right = r.left + this.width;
        } else if (Ext.isNumber(this.right)) {
            r.right = (this.right < 0) ? r.right + this.right : r.left + this.right;
        }

//      Adjust top boundary of region
        if (Ext.isNumber(this.top)) {
            if (this.top < 0) {
                r.top = r.bottom + this.top;
            } else {
                r.top += this.top;
            }
        }

//      Adjust bottom boundary of region
        if (Ext.isNumber(this.height)) {
            r.bottom = r.top + this.height;
        } else if (Ext.isNumber(this.bottom)) {
            r.bottom = (this.bottom < 0) ? r.bottom + this.bottom : r.top + this.bottom;
        }

        return r;
    }
});

/**
 * @class Ext.lib.Polygon
 * <p>This class encapsulates an absolute area of the document bounded by a list of points.</p>
 * @constructor
 * Create a new Polygon
 * @param {Object} points An Array of <code>[n,n]</code> point specification Arrays, or
 * an Array of Ext.lib.Points, or an HtmlElement, or an Ext.lib.Region.
 */
Ext.lib.Polygon = Ext.extend(Ext.lib.Region, {
    constructor: function(points) {
        var i, l, el;
        if (l = points.length) {
            if (points[0].x) {
                for (i = 0; i < l; i++) {
                    points[i] = [ points[i].x, points[i].y ];
                }
            }
            this.points = points;
        } else {
            if (el = Ext.get(points)) {
                points = Ext.lib.Region.getRegion(el.dom);
            }
            if (points instanceof Ext.lib.Region) {
                this.points = [
                    [points.left, points.top],
                    [points.right, points.top],
                    [points.right, points.bottom],
                    [points.left, points.bottom]
                ];
            }
        }
    },

    /**
     * Returns a new Polygon translated by the specified <code>X</code> and <code>Y</code> increments.
     * @param xDelta {Number} The <code>X</code> translation increment.
     * @param xDelta {Number} The <code>Y</code> translation increment.
     * @return {Polygon} The resulting Polygon.
     */
    translate: function(xDelta, yDelta) {
        var r = [], p = this.points, l = p.length, i;
        for (i = 0; i < l; i++) {
            r[i] = [ p[i][0] + xDelta, p[i][1] + yDelta ];
        }
        return new Ext.lib.Polygon(r);
    },

    /**
     * Returns the area of this Polygon.
     */
    getArea: function() {
        var p = this.points, l = p.length, area = 0, i, j = 0;
        for (i = 0; i < l; i++) {
            j++;
            if (j == l) {
                j = 0;
            }
            area += (p[i][0] + p[j][0]) * (p[i][1] - p[j][1]);
        }
        return area * 0.5;
    },

    /**
     * Returns <code>true</code> if this Polygon contains the specified point. Thanks
     * to http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html for the algorithm.
     * @param pt {Point|Number} Either an Ext.lib.Point object, or the <code>X</code> coordinate to test.
     * @param py {Number} <b>Optional.</b> If the first parameter was an <code>X</code> coordinate, this is the <code>Y</code> coordinate.
     */
    contains: function(pt, py) {
        var f = (arguments.length == 1),
            testX = f ? pt.x : pt,
            testY = f ? pt.y : py,
            p = this.points,
            nvert = p.length,
            j = nvert - 1,
            i, j, c = false;
        for (i = 0; i < nvert; j = i++) {
            if ( ((p[i][1] > testY) != (p[j][1] > testY)) &&
             (testX < (p[j][0]-p[i][0]) * (testY-p[i][1]) / (p[j][1]-p[i][1]) + p[i][0])) {
                c = !c;
            }
        }
        return c;
    }
});

/**
 * @class Ext.Resizable
 * @extends Ext.util.Observable
 * This is an override of Ext.Resizable to make usage of the Ex.ux.EventZone
 * <p>Applies virtual drag handles to an element to make it resizable.</p>
 * <p>Here is the list of valid resize handles:</p>
 * <pre>
Value   Description
------  -------------------
 'n'     north
 's'     south
 'e'     east
 'w'     west
 'nw'    northwest
 'sw'    southwest
 'se'    southeast
 'ne'    northeast
 'all'   all
</pre>
 * <p>Here's an example showing the creation of a typical Resizable:</p>
 * <pre><code>
var resizer = new Ext.Resizable('element-id', {
    handles: 'all',
    minWidth: 200,
    minHeight: 100,
    maxWidth: 500,
    maxHeight: 400,
    pinned: true
});
resizer.on('resize', myHandler);
</code></pre>
 * <p>To hide a particular handle, set its display to none in CSS, or through script:<br>
 * resizer.east.setDisplayed(false);</p>
 * @constructor
 * Create a new resizable component
 * @param {Mixed} el The id or element to resize
 * @param {Object} config configuration options
  */
Ext.Resizable = function(el, config){
    this.el = Ext.get(el);

    /**
     * The proxy Element that is resized in place of the real Element during the resize operation.
     * This may be queried using {@link Ext.Element#getBox} to provide the new area to resize to.
     * Read only.
     * @type Ext.Element.
     * @property proxy
     */
    this.proxy = this.el.createProxy({tag: 'div', cls: 'x-resizable-proxy', id: this.el.id + '-rzproxy'}, Ext.getBody());
    this.proxy.unselectable();
    this.proxy.enableDisplayMode('block');

    Ext.apply(this, config);

    if(this.pinned){
        this.disableTrackOver = true;
        this.el.addClass('x-resizable-pinned');
    }
    // if the element isn't positioned, make it relative
    var position = this.el.getStyle('position');
    if(position != 'absolute' && position != 'fixed'){
        this.el.setStyle('position', 'relative');
    }
    if(!this.handles){ // no handles passed, must be legacy style
        this.handles = 's,e,se';
        if(this.multiDirectional){
            this.handles += ',n,w';
        }
    }
    if(this.handles == 'all'){
        this.handles = 'n s e w ne nw se sw';
    }
    var hs = this.handles.split(/\s*?[,;]\s*?| /);
    var ps = Ext.Resizable.positions;
    for(var i = 0, len = hs.length; i < len; i++){
        if(hs[i] && ps[hs[i]]){
            var pos = ps[hs[i]];
            this[pos] = new Ext.Resizable.Handle(this, pos);
        }
    }
    // legacy
    this.corner = this.southeast;

    if(this.handles.indexOf('n') != -1 || this.handles.indexOf('w') != -1){
        this.updateBox = true;
    }
    this.activeHandle = null;

    if(this.adjustments == 'auto'){
        var hw = this.west, he = this.east, hn = this.north, hs = this.south;
        this.adjustments = [
            (he.el ? -he.el.getWidth() : 0) + (hw.el ? -hw.el.getWidth() : 0),
            (hn.el ? -hn.el.getHeight() : 0) + (hs.el ? -hs.el.getHeight() : 0) -1
        ];
    }

    if(this.draggable){
        this.dd = this.dynamic ?
            this.el.initDD(null) : this.el.initDDProxy(null, {dragElId: this.proxy.id});
        this.dd.setHandleElId(this.el.id);
    }

    this.addEvents(
        /**
         * @event beforeresize
         * Fired before resize is allowed. Set {@link #enabled} to false to cancel resize.
         * @param {Ext.Resizable} this
         * @param {Ext.EventObject} e The mousedown event
         */
        'beforeresize',
        /**
         * @event resize
         * Fired after a resize.
         * @param {Ext.Resizable} this
         * @param {Number} width The new width
         * @param {Number} height The new height
         * @param {Ext.EventObject} e The mouseup event
         */
        'resize'
    );

    if(this.width !== null && this.height !== null){
        this.resizeTo(this.width, this.height);
    }
    if(Ext.isIE){
        this.el.dom.style.zoom = 1;
    }
    Ext.Resizable.superclass.constructor.call(this);
};

Ext.extend(Ext.Resizable, Ext.util.Observable, {

    /**
     * @cfg {Array/String} adjustments String 'auto' or an array [width, height] with values to be <b>added</b> to the
     * resize operation's new size (defaults to <tt>[0, 0]</tt>)
     */
    adjustments : [0, 0],
    /**
     * @cfg {Boolean} animate True to animate the resize (not compatible with dynamic sizing, defaults to false)
     */
    animate : false,
    /**
     * @cfg {Mixed} constrainTo Constrain the resize to a particular element
     */
    /**
     * @cfg {Boolean} draggable Convenience to initialize drag drop (defaults to false)
     */
    draggable: false,
    /**
     * @cfg {Number} duration Animation duration if animate = true (defaults to 0.35)
     */
    duration : 0.35,
    /**
     * @cfg {Boolean} dynamic True to resize the element while dragging instead of using a proxy (defaults to false)
     */
    dynamic : false,
    /**
     * @cfg {String} easing Animation easing if animate = true (defaults to <tt>'easingOutStrong'</tt>)
     */
    easing : 'easeOutStrong',
    /**
     * @cfg {Boolean} enabled False to disable resizing (defaults to true)
     */
    enabled : true,
    /**
     * @property enabled Writable. False if resizing is disabled.
     * @type Boolean
     */
    /**
     * @cfg {String} handles String consisting of the resize handles to display (defaults to undefined).
     * Specify either <tt>'all'</tt> or any of <tt>'n s e w ne nw se sw'</tt>.
     */
    handles : false,
    /**
     * @cfg {Boolean} multiDirectional <b>Deprecated</b>.  Deprecated style of adding multi-direction resize handles.
     */
    multiDirectional : false,
    /**
     * @cfg {Number} height The height of the element in pixels (defaults to null)
     */
    height : null,
    /**
     * @cfg {Number} width The width of the element in pixels (defaults to null)
     */
    width : null,
    /**
     * @cfg {Number} heightIncrement The increment to snap the height resize in pixels
     * (only applies if <code>{@link #dynamic}==true</code>). Defaults to <tt>0</tt>.
     */
    heightIncrement : 0,
    /**
     * @cfg {Number} widthIncrement The increment to snap the width resize in pixels
     * (only applies if <code>{@link #dynamic}==true</code>). Defaults to <tt>0</tt>.
     */
    widthIncrement : 0,
    /**
     * @cfg {Number} minHeight The minimum height for the element (defaults to 5)
     */
    minHeight : 5,
    /**
     * @cfg {Number} minWidth The minimum width for the element (defaults to 5)
     */
    minWidth : 5,
    /**
     * @cfg {Number} maxHeight The maximum height for the element (defaults to 10000)
     */
    maxHeight : 10000,
    /**
     * @cfg {Number} maxWidth The maximum width for the element (defaults to 10000)
     */
    maxWidth : 10000,
    /**
     * @cfg {Number} minX The minimum x for the element (defaults to 0)
     */
    minX: 0,
    /**
     * @cfg {Number} minY The minimum x for the element (defaults to 0)
     */
    minY: 0,
    /**
     * @cfg {Boolean} pinned True to ensure that the resize handles are always visible, false to display them only when the
     * user mouses over the resizable borders. This is only applied at config time. (defaults to false)
     */
    pinned : false,
    /**
     * @cfg {Boolean} preserveRatio True to preserve the original ratio between height
     * and width during resize (defaults to false)
     */
    preserveRatio : false,
    /**
     * @cfg {Ext.lib.Region} resizeRegion Constrain the resize to a particular region
     */


    /**
     * Perform a manual resize and fires the 'resize' event.
     * @param {Number} width
     * @param {Number} height
     */
    resizeTo : function(width, height){
        this.el.setSize(width, height);
        this.fireEvent('resize', this, width, height, null);
    },

    // private
    startSizing : function(e, handle){
        this.fireEvent('beforeresize', this, e);
        if(this.enabled){ // 2nd enabled check in case disabled before beforeresize handler
            e.stopEvent();

            Ext.getDoc().on({
                scope: this,
                mousemove: this.onMouseMove,
                mouseup: {
                    fn: this.onMouseUp,
                    single: true,
                    scope: this
                }
            });
            Ext.getBody().addClass('ux-resizable-handle-' + handle.position);

            this.resizing = true;
            this.startBox = this.el.getBox();
            this.startPoint = e.getXY();
            this.offsets = [(this.startBox.x + this.startBox.width) - this.startPoint[0],
                            (this.startBox.y + this.startBox.height) - this.startPoint[1]];

            if(this.constrainTo) {
                var ct = Ext.get(this.constrainTo);
                this.resizeRegion = ct.getRegion().adjust(
                    ct.getFrameWidth('t'),
                    ct.getFrameWidth('l'),
                    -ct.getFrameWidth('b'),
                    -ct.getFrameWidth('r')
                );
            }

            this.proxy.setStyle('visibility', 'hidden'); // workaround display none
            this.proxy.show();
            this.proxy.setBox(this.startBox);
            if(!this.dynamic){
                this.proxy.setStyle('visibility', 'visible');
            }
        }
    },

    // private
    onMouseDown : function(handle, e){
        if(this.enabled && !this.activeHandle){
            e.stopEvent();
            this.activeHandle = handle;
            this.startSizing(e, handle);
        }
    },

    // private
    onMouseUp : function(e){
        Ext.getBody().removeClass('ux-resizable-handle-' + this.activeHandle.position)
            .un('mousemove', this.onMouseMove, this);
        var size = this.resizeElement();
        this.resizing = false;
        this.handleOut(this.activeHandle);
        this.proxy.hide();
        this.fireEvent('resize', this, size.width, size.height, e);
        this.activeHandle = null;
    },

    // private
    snap : function(value, inc, min){
        if(!inc || !value){
            return value;
        }
        var newValue = value;
        var m = value % inc;
        if(m > 0){
            if(m > (inc/2)){
                newValue = value + (inc-m);
            }else{
                newValue = value - m;
            }
        }
        return Math.max(min, newValue);
    },

    /**
     * <p>Performs resizing of the associated Element. This method is called internally by this
     * class, and should not be called by user code.</p>
     * <p>If a Resizable is being used to resize an Element which encapsulates a more complex UI
     * component such as a Panel, this method may be overridden by specifying an implementation
     * as a config option to provide appropriate behaviour at the end of the resize operation on
     * mouseup, for example resizing the Panel, and relaying the Panel's content.</p>
     * <p>The new area to be resized to is available by examining the state of the {@link #proxy}
     * Element. Example:
<pre><code>
new Ext.Panel({
    title: 'Resize me',
    x: 100,
    y: 100,
    renderTo: Ext.getBody(),
    floating: true,
    frame: true,
    width: 400,
    height: 200,
    listeners: {
        render: function(p) {
            new Ext.Resizable(p.getEl(), {
                handles: 'all',
                pinned: true,
                transparent: true,
                resizeElement: function() {
                    var box = this.proxy.getBox();
                    p.updateBox(box);
                    if (p.layout) {
                        p.doLayout();
                    }
                    return box;
                }
           });
       }
    }
}).show();
</code></pre>
     */
    resizeElement : function(){
        var box = this.proxy.getBox();
        if(this.updateBox){
            this.el.setBox(box, false, this.animate, this.duration, null, this.easing);
        }else{
            this.el.setSize(box.width, box.height, this.animate, this.duration, null, this.easing);
        }
        if(!this.dynamic){
            this.proxy.hide();
        }
        return box;
    },

    // private
    constrain : function(v, diff, m, mx){
        if(v - diff < m){
            diff = v - m;
        }else if(v - diff > mx){
            diff = v - mx;
        }
        return diff;
    },

    // private
    onMouseMove : function(e){
        if(this.enabled && this.activeHandle){
            try{// try catch so if something goes wrong the user doesn't get hung

            if(this.resizeRegion && !this.resizeRegion.contains(e.getPoint())) {
                return;
            }

            //var curXY = this.startPoint;
            var curSize = this.curSize || this.startBox,
                x = this.startBox.x, y = this.startBox.y,
                ox = x,
                oy = y,
                w = curSize.width,
                h = curSize.height,
                ow = w,
                oh = h,
                mw = this.minWidth,
                mh = this.minHeight,
                mxw = this.maxWidth,
                mxh = this.maxHeight,
                wi = this.widthIncrement,
                hi = this.heightIncrement,
                eventXY = e.getXY(),
                diffX = -(this.startPoint[0] - Math.max(this.minX, eventXY[0])),
                diffY = -(this.startPoint[1] - Math.max(this.minY, eventXY[1])),
                pos = this.activeHandle.position,
                tw,
                th;

            switch(pos){
                case 'east':
                    w += diffX;
                    w = Math.min(Math.max(mw, w), mxw);
                    break;
                case 'south':
                    h += diffY;
                    h = Math.min(Math.max(mh, h), mxh);
                    break;
                case 'southeast':
                    w += diffX;
                    h += diffY;
                    w = Math.min(Math.max(mw, w), mxw);
                    h = Math.min(Math.max(mh, h), mxh);
                    break;
                case 'north':
                    diffY = this.constrain(h, diffY, mh, mxh);
                    y += diffY;
                    h -= diffY;
                    break;
                case 'west':
                    diffX = this.constrain(w, diffX, mw, mxw);
                    x += diffX;
                    w -= diffX;
                    break;
                case 'northeast':
                    w += diffX;
                    w = Math.min(Math.max(mw, w), mxw);
                    diffY = this.constrain(h, diffY, mh, mxh);
                    y += diffY;
                    h -= diffY;
                    break;
                case 'northwest':
                    diffX = this.constrain(w, diffX, mw, mxw);
                    diffY = this.constrain(h, diffY, mh, mxh);
                    y += diffY;
                    h -= diffY;
                    x += diffX;
                    w -= diffX;
                    break;
               case 'southwest':
                    diffX = this.constrain(w, diffX, mw, mxw);
                    h += diffY;
                    h = Math.min(Math.max(mh, h), mxh);
                    x += diffX;
                    w -= diffX;
                    break;
            }

            var sw = this.snap(w, wi, mw);
            var sh = this.snap(h, hi, mh);
            if(sw != w || sh != h){
                switch(pos){
                    case 'northeast':
                        y -= sh - h;
                    break;
                    case 'north':
                        y -= sh - h;
                        break;
                    case 'southwest':
                        x -= sw - w;
                    break;
                    case 'west':
                        x -= sw - w;
                        break;
                    case 'northwest':
                        x -= sw - w;
                        y -= sh - h;
                    break;
                }
                w = sw;
                h = sh;
            }

            if(this.preserveRatio){
                switch(pos){
                    case 'southeast':
                    case 'east':
                        h = oh * (w/ow);
                        h = Math.min(Math.max(mh, h), mxh);
                        w = ow * (h/oh);
                       break;
                    case 'south':
                        w = ow * (h/oh);
                        w = Math.min(Math.max(mw, w), mxw);
                        h = oh * (w/ow);
                        break;
                    case 'northeast':
                        w = ow * (h/oh);
                        w = Math.min(Math.max(mw, w), mxw);
                        h = oh * (w/ow);
                    break;
                    case 'north':
                        tw = w;
                        w = ow * (h/oh);
                        w = Math.min(Math.max(mw, w), mxw);
                        h = oh * (w/ow);
                        x += (tw - w) / 2;
                        break;
                    case 'southwest':
                        h = oh * (w/ow);
                        h = Math.min(Math.max(mh, h), mxh);
                        tw = w;
                        w = ow * (h/oh);
                        x += tw - w;
                        break;
                    case 'west':
                        th = h;
                        h = oh * (w/ow);
                        h = Math.min(Math.max(mh, h), mxh);
                        y += (th - h) / 2;
                        tw = w;
                        w = ow * (h/oh);
                        x += tw - w;
                       break;
                    case 'northwest':
                        tw = w;
                        th = h;
                        h = oh * (w/ow);
                        h = Math.min(Math.max(mh, h), mxh);
                        w = ow * (h/oh);
                        y += th - h;
                        x += tw - w;
                        break;

                }
            }
            this.proxy.setBounds(x, y, w, h);
            if(this.dynamic){
                this.resizeElement();
            }
            }catch(ex){}
        }
    },

    // private
    handleOver : function(handle){
        if(this.enabled){
            Ext.getBody().addClass('ux-resizable-handle-' + handle.position);
        }
    },

    // private
    handleOut : function(handle){
        if(!this.resizing){
            Ext.getBody().removeClass('ux-resizable-handle-' + handle.position);
        }
    },

    /**
     * Returns the element this component is bound to.
     * @return {Ext.Element}
     */
    getEl : function(){
        return this.el;
    },

    /**
     * Destroys this resizable. If the element was wrapped and
     * removeEl is not true then the element remains.
     * @param {Boolean} removeEl (optional) true to remove the element from the DOM
     */
    destroy : function(removeEl){
        Ext.destroy(this.dd, this.proxy);
        this.proxy = null;

        var ps = Ext.Resizable.positions;
        for(var k in ps){
            if(typeof ps[k] != 'function' && this[ps[k]]){
                this[ps[k]].destroy();
            }
        }
        if(removeEl){
            this.el.update('');
            Ext.destroy(this.el);
            this.el = null;
        }
        this.purgeListeners();
    },

    syncHandleHeight : function(){
        var h = this.el.getHeight(true);
        if(this.west.el){
            this.west.el.setHeight(h);
        }
        if(this.east.el){
            this.east.el.setHeight(h);
        }
    }
});

// private
// hash to map config positions to true positions
Ext.Resizable.positions = {
    n: 'north', s: 'south', e: 'east', w: 'west', se: 'southeast', sw: 'southwest', nw: 'northwest', ne: 'northeast'
};
Ext.Resizable.cfg = {
    north: {left: 7, right: -7, height: 7},
    south: {left: 7, right: -7, top: -7},
    east: {top: 7, bottom: -7, left: -7},
    west: {top: 7, bottom: -7, width: 7},
    southeast: {top: -7, left: -7},
    southwest: {top: -7, width: 7},
    northwest: {height: 7, width: 7},
    northeast: {left: -7, height: 7}
};

// private
Ext.Resizable.Handle = function(rz, pos){
    this.position = pos;
    this.rz = rz;
    var cfg = Ext.Resizable.cfg[pos] || Ext.Resizable.cfg[Ext.Resizable.positions[pos]];
    this.ez = new Ext.ux.EventZone(Ext.apply({
        position: pos,
        el: rz.el
    }, cfg));
    this.ez.on({
        mousedown: this.onMouseDown,
        mouseenter: this.onMouseOver,
        mouseleave: this.onMouseOut,
        scope: this
    });
};

// private
Ext.Resizable.Handle.prototype = {
    cursor: 'move',

    // private
    afterResize : function(rz){
        // do nothing
    },
    // private
    onMouseDown : function(e){
        this.rz.onMouseDown(this, e);
    },
    // private
    onMouseOver : function(e){
        this.rz.handleOver(this, e);
    },
    // private
    onMouseOut : function(e){
        this.rz.handleOut(this, e);
    },
    // private
    destroy : function(){
        Ext.destroy(this.el);
        this.el = null;
    }
};

/**
*
* Ext.ux.elasticTextArea Extension Class for Ext 3.x Library
*
* @author  Steffen Kamper
*
* @license Ext.ux.elasticTextArea is licensed under the terms of
* the Open Source LGPL 3.0 license.  Commercial use is permitted to the extent
* that the code/component(s) do NOT become part of another Open Source or Commercially
* licensed development library or toolkit without explicit permission.
*
* License details: http://www.gnu.org/licenses/lgpl.html
*
*/
Ext.ux.elasticTextArea = function(){

    var defaultConfig = function(){
        return {
            minHeight : 0
            ,maxHeight : 0
            ,growBy: 12
        }
    }

    var processOptions = function(config){
        var o = defaultConfig();
        var options = {};
        Ext.apply(options, config, o);

        return options;
    }

    return {
        div : null
        ,applyTo: function(elementId, options){

            var el = Ext.get(elementId);
            var width = el.getWidth();
            var height = el.getHeight();

            var styles = el.getStyles('padding-top', 'padding-bottom', 'padding-left', 'padding-right', 'line-height', 'font-size', 'font-family', 'font-weight');

            if(! this.div){
                var options = processOptions(options);

                this.div = Ext.DomHelper.append(Ext.getBody() || document.body, {
                    'id':elementId + '-preview-div'
                    ,'tag' : 'div'
                    ,'background': 'red'
                    ,'style' : 'position: absolute; top: -100000px; left: -100000px;'
                }, true)
                Ext.DomHelper.applyStyles(this.div, styles);

                el.on('keyup', function() {
                        this.applyTo(elementId, options);
                }, this);
            }
            this.div.setWidth(parseInt(el.getStyle('width')));
            //replace \n with <br>&nbsp; so that the enter key can trigger and height increase
            //but first remove all previous entries, so that the height measurement can be as accurate as possible
            this.div.update(
                    el.dom.value.replace(/<br \/>&nbsp;/, '<br />')
                                .replace(/<|>/g, ' ')
                                .replace(/&/g,"&amp;")
                                .replace(/\n/g, '<br />&nbsp;')
                    );

			var growBy = parseInt(el.getStyle('line-height'));
			growBy = growBy ? growBy + 1 : 1;
			if (growBy === 1) {
				growBy = options.growBy;
			}
			var textHeight = this.div.getHeight();
			textHeight = textHeight ? textHeight + growBy : growBy;

            if ( (textHeight > options.maxHeight ) && (options.maxHeight > 0) ){
                textHeight = options.maxHeight;
                el.setStyle('overflow', 'auto');
            }
            if ( (textHeight < options.minHeight ) && (options.minHeight > 0) ) {
                textHeight = options.minHeight;
                el.setStyle('overflow', 'auto');
            }

            el.setHeight(textHeight , true);
        }
    }
}

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Evaluation of TYPO3 form field content
 */

function evalFunc() {
	this.input = evalFunc_input;
	this.output = evalFunc_output;
	this.parseInt = evalFunc_parseInt;
	this.getNumChars = evalFunc_getNumChars;
	this.parseDouble = evalFunc_parseDouble;
	this.noSpace = evalFunc_noSpace;
	this.getSecs = evalFunc_getSecs;
	this.getYear = evalFunc_getYear;
	this.getTimeSecs = evalFunc_getTimeSecs;
	this.getTime = evalFunc_getTime;
	this.getDate = evalFunc_getDate;
	this.getTimestamp = evalFunc_getTimestamp;
	this.caseSwitch = evalFunc_caseSwitch;
	this.evalObjValue = evalFunc_evalObjValue;
	this.outputObjValue = evalFunc_outputObjValue;
	this.split = evalFunc_splitStr;
	this.pol = evalFunc_pol;
	this.convertClientTimestampToUTC = evalFunc_convertClientTimestampToUTC;

	this.ltrim = evalFunc_ltrim;
	this.btrim = evalFunc_btrim;
	var today = new Date();
 	this.lastYear = this.getYear(today);
 	this.lastDate = this.getDate(today);
 	this.lastTime = 0;
	this.refDate = today;
	this.isInString = '';
	this.USmode = 0;
}
function evalFunc_pol(foreign, value) {
	return eval (((foreign=="-")?'-':'')+value);
}
function evalFunc_evalObjValue(FObj,value) {
	var evallist = FObj.evallist;
	this.isInString = (FObj.is_in) ? ''+FObj.is_in : '';
	var index=1;
	var theEvalType = (FObj.evallist) ? this.split(evallist, ",", index) : false;
	var newValue=value;
	while (theEvalType) {
		if (typeof TBE_EDITOR == 'object' && TBE_EDITOR.customEvalFunctions[theEvalType] && typeof TBE_EDITOR.customEvalFunctions[theEvalType] == 'function') {
			newValue = TBE_EDITOR.customEvalFunctions[theEvalType](newValue);
		} else {
			newValue = evalFunc.input(theEvalType, newValue);
		}
		index++;
		theEvalType = this.split(evallist, ",", index);
	}
	return newValue;
}
function evalFunc_outputObjValue(FObj,value) {
	var evallist = FObj.evallist;
	var index=1;
	var theEvalType = this.split(evallist, ",", index);
	var newValue=value;
	while (theEvalType) {
		if (theEvalType != 'required') {
			newValue = evalFunc.output(theEvalType, value, FObj);
		}
		index++;
		theEvalType = this.split(evallist, ",", index);
	}
	return newValue;
}
function evalFunc_caseSwitch(type,inVal) {
	var theVal = ''+inVal;
	var newString = '';
	switch (type) {
		case "alpha":
		case "num":
		case "alphanum":
		case "alphanum_x":
			for (var a=0;a<theVal.length;a++) {
				var theChar = theVal.substr(a,1);
				var special = (theChar == '_' || theChar == '-');
				var alpha = (theChar >= 'a' && theChar <= 'z') || (theChar >= 'A' && theChar <= 'Z');
				var num = (theChar>='0' && theChar<='9');
				switch(type) {
					case "alphanum":	special=0;		break;
					case "alpha":	num=0; special=0;		break;
					case "num":	alpha=0; special=0;		break;
				}
				if (alpha || num || theChar==' ' || special) {
					newString+=theChar;
				}
			}
		break;
		case "is_in":
			if (this.isInString) {
				for (var a=0;a<theVal.length;a++) {
					var theChar = theVal.substr(a,1);
					if (this.isInString.indexOf(theChar)!=-1) {
						newString+=theChar;
					}
				}
			} else {newString = theVal;}
		break;
		case "nospace":
			newString = this.noSpace(theVal);
		break;
		case "upper":
			newString = theVal.toUpperCase();
		break;
		case "lower":
			newString = theVal.toLowerCase();
		break;
		default:
			return inVal;
	}
	return newString;
}
function evalFunc_parseInt(value) {
	var theVal = ''+value;
	if (!value) {
		return 0;
	}
	for (var a = 0; a < theVal.length; a++) {
		if (theVal.substr(a,1)!='0') {
			return parseInt(theVal.substr(a,theVal.length)) || 0;
		}
	}
	return 0;
}
function evalFunc_getNumChars(value) {
	var theVal = ''+value;
	if (!value) {
		return 0;
	}
	var outVal="";
	for (var a = 0; a < theVal.length; a++) {
		if (theVal.substr(a,1)==parseInt(theVal.substr(a,1))) {
			outVal+=theVal.substr(a,1);
		}
	}
	return outVal;
}
function evalFunc_parseDouble(value) {
	var theVal = "" + value;
	theVal = theVal.replace(/[^0-9,\.-]/g, "");
	var negative = theVal.substring(0, 1) === '-';
	theVal = theVal.replace(/-/g, "");
	theVal = theVal.replace(/,/g, ".");
	if (theVal.indexOf(".") == -1) {
		theVal += ".0";
	}
	var parts = theVal.split(".");
	var dec = parts.pop();
	theVal = Number(parts.join("") + "." + dec);
	if (negative) {
	    theVal *= -1;
	}
	theVal = theVal.toFixed(2);

	return theVal;
}
function evalFunc_noSpace(value) {
	var theVal = ''+value;
	var newString="";
	for (var a=0;a<theVal.length;a++) {
		var theChar = theVal.substr(a,1);
		if (theChar!=' ') {
			newString+=theChar;
		}
	}
	return newString;
}
function evalFunc_ltrim(value) {
	var theVal = ''+value;
	if (!value) {
		return '';
	}
	for (var a = 0; a < theVal.length; a++) {
		if (theVal.substr(a,1)!=' ') {
			return theVal.substr(a,theVal.length);
		}
	}
	return '';
}
function evalFunc_btrim(value) {
	var theVal = ''+value;
	if (!value) {
		return '';
	}
	for (var a = theVal.length; a > 0; a--) {
		if (theVal.substr(a-1,1)!=' ') {
			return theVal.substr(0,a);
		}
	}
	return '';
}
function evalFunc_splitSingle(value) {
	var theVal = ''+value;
	this.values = new Array();
	this.pointer = 3;
	this.values[1]=theVal.substr(0,2);
	this.values[2]=theVal.substr(2,2);
	this.values[3]=theVal.substr(4,10);
}
function evalFunc_split(value) {
	this.values = new Array();
	this.valPol = new Array();
	this.pointer = 0;
	var numberMode = 0;
	var theVal = "";
	value+=" ";
	for (var a=0;a<value.length;a++) {
		var theChar = value.substr(a,1);
		if (theChar<"0" || theChar>"9") {
			if (numberMode) {
				this.pointer++;
				this.values[this.pointer]=theVal;
				theVal = "";
				numberMode=0;
			}
			if (theChar=="+" || theChar=="-") {
				this.valPol[this.pointer+1] = theChar;
			}
		} else {
			theVal+=theChar;
			numberMode=1;
		}
	}
}
function evalFunc_input(type,inVal) {
	if (type=="md5") {
		return MD5(inVal);
	}
	if (type=="trim") {
		return this.ltrim(this.btrim(inVal));
	}
	if (type=="int") {
		return this.parseInt(inVal);
	}
	if (type=="double2") {
		return this.parseDouble(inVal);
	}

	var today = new Date();
	var add=0;
	var value = this.ltrim(inVal);
	var values = new evalFunc_split(value);
	var theCmd = value.substr(0,1);
	value = this.caseSwitch(type,value);
	if (value=="") {
		return "";
		return 0;	// Why would I ever return a zero??? (20/12/01)
	}
	switch (type) {
		case "datetime":
			switch (theCmd) {
				case "d":
				case "t":
				case "n":
					this.lastTime = this.convertClientTimestampToUTC(this.getTimestamp(today), 0);
					if (values.valPol[1]) {
						add = this.pol(values.valPol[1],this.parseInt(values.values[1]));
					}
				break;
				case "+":
				case "-":
					if (this.lastTime == 0) {
						this.lastTime = this.convertClientTimestampToUTC(this.getTimestamp(today), 0);
					}
					if (values.valPol[1]) {
						add = this.pol(values.valPol[1],this.parseInt(values.values[1]));
					}
				break;
				default:
					var index = value.indexOf(' ');
					if (index!=-1) {
						var dateVal = this.input("date",value.substr(index,value.length));
							// set refDate so that evalFunc_input on time will work with correct DST information
						this.refDate = new Date(dateVal*1000);
						this.lastTime = dateVal + this.input("time",value.substr(0,index));
					} else	{
							// only date, no time
						this.lastTime = this.input("date", value);
					}
			}
			this.lastTime+=add*24*60*60;
			return this.lastTime;
		break;
		case "year":
			switch (theCmd) {
				case "d":
				case "t":
				case "n":
					this.lastYear = this.getYear(today);
					if (values.valPol[1]) {
						add = this.pol(values.valPol[1],this.parseInt(values.values[1]));
					}
				break;
				case "+":
				case "-":
					if (values.valPol[1]) {
						add = this.pol(values.valPol[1],this.parseInt(values.values[1]));
					}
				break;
				default:
					if (values.valPol[2]) {
						add = this.pol(values.valPol[2],this.parseInt(values.values[2]));
					}
					var year = (values.values[1])?this.parseInt(values.values[1]):this.getYear(today);
					if ((year >= 0 && year < 38) || (year >= 70 && year<100) || (year >= 1902 && year < 2038)) {
						if (year<100) {
							year = (year<38) ? year+=2000 : year+=1900;
						}
					} else {
						year = this.getYear(today);
					}
					this.lastYear = year;
			}
			this.lastYear+=add;
			return this.lastYear;
		break;
		case "date":
			switch (theCmd) {
				case "d":
				case "t":
				case "n":
					this.lastDate = this.getTimestamp(today);
					if (values.valPol[1]) {
						add = this.pol(values.valPol[1],this.parseInt(values.values[1]));
					}
				break;
				case "+":
				case "-":
					if (values.valPol[1]) {
						add = this.pol(values.valPol[1],this.parseInt(values.values[1]));
					}
				break;
				default:
					var index = 4;
					if (values.valPol[index]) {
						add = this.pol(values.valPol[index],this.parseInt(values.values[index]));
					}
					if (values.values[1] && values.values[1].length>2) {
						if (values.valPol[2]) {
							add = this.pol(values.valPol[2],this.parseInt(values.values[2]));
						}
						var temp = values.values[1];
						values = new evalFunc_splitSingle(temp);
					}

					var year = (values.values[3])?this.parseInt(values.values[3]):this.getYear(today);
					if ((year >= 0 && year < 38) || (year >= 70 && year < 100) || (year >= 1902 && year<2038)) {
						if (year<100) {
							year = (year<38) ? year+=2000 : year+=1900;
						}
					} else {
						year = this.getYear(today);
					}
					var month = (values.values[this.USmode?1:2])?this.parseInt(values.values[this.USmode?1:2]):today.getUTCMonth()+1;
					var day = (values.values[this.USmode?2:1])?this.parseInt(values.values[this.USmode?2:1]):today.getUTCDate();

					var theTime = new Date(parseInt(year), parseInt(month)-1, parseInt(day));

						// Substract timezone offset from client
					this.lastDate = this.convertClientTimestampToUTC(this.getTimestamp(theTime), 0);
			}
			this.lastDate+=add*24*60*60;
			return this.lastDate;
		break;
		case "time":
		case "timesec":
			switch (theCmd) {
				case "d":
				case "t":
				case "n":
					this.lastTime = this.getTimeSecs(today);
					if (values.valPol[1]) {
						add = this.pol(values.valPol[1],this.parseInt(values.values[1]));
					}
				break;
				case "+":
				case "-":
					if (this.lastTime == 0) {
						this.lastTime = this.getTimeSecs(today);
					}
					if (values.valPol[1]) {
						add = this.pol(values.valPol[1],this.parseInt(values.values[1]));
					}
				break;
				default:
					var index = (type=="timesec")?4:3;
					if (values.valPol[index]) {
						add = this.pol(values.valPol[index],this.parseInt(values.values[index]));
					}
					if (values.values[1] && values.values[1].length>2) {
						if (values.valPol[2]) {
							add = this.pol(values.valPol[2],this.parseInt(values.values[2]));
						}
						var temp = values.values[1];
						values = new evalFunc_splitSingle(temp);
					}
					var sec = (values.values[3])?this.parseInt(values.values[3]):today.getUTCSeconds();
					if (sec > 59)	{sec=59;}
					var min = (values.values[2])?this.parseInt(values.values[2]):today.getUTCMinutes();
					if (min > 59)	{min=59;}
					var hour = (values.values[1])?this.parseInt(values.values[1]):today.getUTCHours();
					if (hour >= 24)	{hour=0;}

					var theTime = new Date(this.getYear(this.refDate), this.refDate.getUTCMonth(), this.refDate.getUTCDate(), hour, min, ((type=="timesec")?sec:0));

						// Substract timezone offset from client
					this.lastTime = this.convertClientTimestampToUTC(this.getTimestamp(theTime), 1);
			}
			this.lastTime+=add*60;
			if (this.lastTime<0) {this.lastTime+=24*60*60;}
			return this.lastTime;
		break;
		default:
			return value;
	}
}
function evalFunc_output(type,value,FObj) {
	var theString = "";
	switch (type) {
		case "date":
			if (!parseInt(value))	{return '';}
			var theTime = new Date(parseInt(value) * 1000);
			if (this.USmode) {
				theString = (theTime.getUTCMonth()+1)+'-'+theTime.getUTCDate()+'-'+this.getYear(theTime);
			} else {
				theString = theTime.getUTCDate()+'-'+(theTime.getUTCMonth()+1)+'-'+this.getYear(theTime);
			}
		break;
		case "datetime":
			if (!parseInt(value))	{return '';}
			theString = this.output("time",value)+' '+this.output("date",value);
		break;
		case "time":
		case "timesec":
			if (!parseInt(value))	{return '';}
			var theTime = new Date(parseInt(value) * 1000);
			var h = theTime.getUTCHours();
			var m = theTime.getUTCMinutes();
			var s = theTime.getUTCSeconds();
			theString = h+':'+((m<10)?'0':'')+m + ((type=="timesec")?':'+((s<10)?'0':'')+s:'');
		break;
		case "password":
			theString = (value)	? TS.passwordDummy : "";
		break;
		case "int":
			theString = (FObj.checkbox && value==FObj.checkboxValue)?'':value;
		break;
		default:
			theString = value;
	}
	return theString;
}
function evalFunc_getSecs(timeObj) {
	return timeObj.getUTCSeconds();
}
// Seconds since midnight:
function evalFunc_getTime(timeObj) {
	return timeObj.getUTCHours() * 60 * 60 + timeObj.getUTCMinutes() * 60 + this.getSecs(timeObj);
}
function evalFunc_getYear(timeObj) {
	return timeObj.getUTCFullYear();
}
// Seconds since midnight with client timezone offset:
function evalFunc_getTimeSecs(timeObj) {
	return timeObj.getHours()*60*60+timeObj.getMinutes()*60+timeObj.getSeconds();
}
function evalFunc_getDate(timeObj) {
	var theTime = new Date(this.getYear(timeObj), timeObj.getUTCMonth(), timeObj.getUTCDate());
	return this.getTimestamp(theTime);
}
function evalFunc_dummy (evallist,is_in,checkbox,checkboxValue) {
	this.evallist = evallist;
	this.is_in = is_in;
	this.checkboxValue = checkboxValue;
	this.checkbox = checkbox;
}
function evalFunc_splitStr(theStr1, delim, index) {
	var theStr = ''+theStr1;
	var lengthOfDelim = delim.length;
	sPos = -lengthOfDelim;
	if (index<1) {index=1;}
	for (a=1; a<index; a++) {
		sPos = theStr.indexOf(delim, sPos+lengthOfDelim);
		if (sPos==-1)	{return null;}
	}
	ePos = theStr.indexOf(delim, sPos+lengthOfDelim);
	if(ePos == -1)	{ePos = theStr.length;}
	return (theStr.substring(sPos+lengthOfDelim,ePos));
}
function evalFunc_getTimestamp(timeObj) {
	return Date.parse(timeObj)/1000;
}

// Substract timezone offset from client to a timestamp to get UTC-timestamp to be send to server
function evalFunc_convertClientTimestampToUTC(timestamp, timeonly) {
	var timeObj = new Date(timestamp*1000);
	timeObj.setTime((timestamp - timeObj.getTimezoneOffset()*60)*1000);
	if (timeonly) {
			// only seconds since midnight
		return this.getTime(timeObj);
	} else	{
			// seconds since the "unix-epoch"
		return this.getTimestamp(timeObj);
	}
}

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
/**
 * Contains JavaScript for TYPO3 Core Form generator - AKA "TCEforms"
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 * @coauthor	Oliver Hader <oh@inpublica.de>
 */


var TBE_EDITOR = {
	/* Example:
		elements: {
			'data-parentPid-table-uid': {
				'field': {
					'range':		[0, 100],
					'rangeImg':		'',
					'required':		true,
					'requiredImg':	''
				}
			}
		},
	*/

	elements: {},
	nested: {'field':{}, 'level':{}},
	ignoreElements: [],
	recentUpdatedElements: {},
	actionChecks: { submit:	[] },

	formname: '',
	formnameUENC: '',
	loadTime: 0,
	isChanged: 0,
	auth_timeout_field: 0,

	backPath: '',
	prependFormFieldNames: 'data',
	prependFormFieldNamesUENC: 'data',
	prependFormFieldNamesCnt: 0,

	isPalettedoc: null,
	doSaveFieldName: 0,

	labels: {},
	images: {
		req: new Image(),
		cm: new Image(),
		sel: new Image(),
		clear: new Image()
	},

	clearBeforeSettingFormValueFromBrowseWin: [],

	// Handling of data structures:
	addElements: function(elements) {
		TBE_EDITOR.recentUpdatedElements = elements;
		TBE_EDITOR.elements = $H(TBE_EDITOR.elements).merge(elements).toObject();
	},
	addNested: function(elements) {
		// Merge data structures:
		if (elements) {
			$H(elements).each(function(element) {
				var levelMax, i, currentLevel, subLevel;
				var nested = element.value;
				if (nested.level && nested.level.length) {
						// If the first level is of type 'inline', it could be created by a AJAX request to IRRE.
						// So, try to get the upper levels this dynamic level is nested in:
					if (typeof inline!='undefined' && nested.level[0][0]=='inline') {
						nested.level = inline.findContinuedNestedLevel(nested.level, nested.level[0][1]);
					}
					levelMax = nested.level.length-1;
					for (i=0; i<=levelMax; i++) {
						currentLevel = TBE_EDITOR.getNestedLevelIdent(nested.level[i]);
						if (typeof TBE_EDITOR.nested.level[currentLevel] == 'undefined') {
							TBE_EDITOR.nested.level[currentLevel] = { 'clean': true, 'item': {}, 'sub': {} };
						}
							// Add next sub level to the current level:
						if (i<levelMax) {
							subLevel = TBE_EDITOR.getNestedLevelIdent(nested.level[i+1]);
							TBE_EDITOR.nested.level[currentLevel].sub[subLevel] = true;
							// Add the current item to the last level in nesting:
						} else {
							TBE_EDITOR.nested.level[currentLevel].item[element.key] = nested.parts;
						}
					}
				}
			});
				// Merge the nested fields:
			TBE_EDITOR.nested.field = $H(TBE_EDITOR.nested.field).merge(elements).toObject();
		}
	},
	removeElement: function(record) {
		if (TBE_EDITOR.elements && TBE_EDITOR.elements[record]) {
				// Inform envolved levels the this record is removed and the missing requirements are resolved:
			$H(TBE_EDITOR.elements[record]).each(
				function(pair) {
					TBE_EDITOR.notifyNested(record+'['+pair.key+']', true);
				}
			);
			delete(TBE_EDITOR.elements[record]);
		}
	},
	removeElementArray: function(removeStack) {
		if (removeStack && removeStack.length) {
			TBE_EDITOR.ignoreElements = removeStack;
			for (var i=removeStack.length; i>=0; i--) {
				TBE_EDITOR.removeElement(removeStack[i]);
			}
			TBE_EDITOR.ignoreElements = [];
		}
	},
	getElement: function(record, field, type) {
		var result = null;
		var element;

		if (TBE_EDITOR.elements && TBE_EDITOR.elements[record] && TBE_EDITOR.elements[record][field]) {
			element = TBE_EDITOR.elements[record][field];
			if (type) {
				if (element[type]) result = element;
			} else {
				result = element;
			}
		}

		return result;
	},
	checkElements: function(type, recentUpdated, record, field) {
		var result = 1;
		var elementName, elementData, elementRecord, elementField;
		var source = (recentUpdated ? TBE_EDITOR.recentUpdatedElements : TBE_EDITOR.elements);

		if (TBE_EDITOR.ignoreElements.length && TBE_EDITOR.ignoreElements.indexOf(record)!=-1) {
			return result;
		}

		if (type) {
			if (record && field) {
				elementName = record+'['+field+']';
				elementData = TBE_EDITOR.getElement(record, field, type);
				if (elementData) {
					if (!TBE_EDITOR.checkElementByType(type, elementName, elementData, recentUpdated)) {
						result = 0;
					}
				}

			} else {
				var elementFieldList, elRecIndex, elRecCnt, elFldIndex, elFldCnt;
				var elementRecordList = $H(source).keys();
				for (elRecIndex=0, elRecCnt=elementRecordList.length; elRecIndex<elRecCnt; elRecIndex++) {
					elementRecord = elementRecordList[elRecIndex];
					elementFieldList = $H(source[elementRecord]).keys();
					for (elFldIndex=0, elFldCnt=elementFieldList.length; elFldIndex<elFldCnt; elFldIndex++) {
						elementField = elementFieldList[elFldIndex];
						elementData = TBE_EDITOR.getElement(elementRecord, elementField, type);
						if (elementData) {
							elementName = elementRecord+'['+elementField+']';
							if (!TBE_EDITOR.checkElementByType(type, elementName, elementData, recentUpdated)) {
								result = 0;
							}
						}
					}
				}
			}
		}

		return result;
	},
	checkElementByType: function(type, elementName, elementData, autoNotify) {
		var form, result = 1;

		if (type) {
			if (type == 'required') {
				form = document[TBE_EDITOR.formname][elementName];
				if (form) {
						// Check if we are within a deleted inline element
					var testNode = $(form.parentNode);
					while(testNode) {
						if (testNode.hasClassName && testNode.hasClassName('inlineIsDeletedRecord')) {
							return result;
						}
						testNode = $(testNode.parentNode);
					}

					var value = form.value;
					if (!value || elementData.additional && elementData.additional.isPositiveNumber && (isNaN(value) || Number(value) <= 0)) {
						result = 0;
						if (autoNotify) {
							TBE_EDITOR.setImage('req_'+elementData.requiredImg, TBE_EDITOR.images.req);
							TBE_EDITOR.notifyNested(elementName, false);
						}
					}
				}
			} else if (type == 'range' && elementData.range) {
				var numberOfElements = 0;
				form = document[TBE_EDITOR.formname][elementName+'_list'];
				if (!form) {
						// special treatment for IRRE fields:
					var tempObj = document[TBE_EDITOR.formname][elementName];
					if (tempObj && (Element.hasClassName(tempObj, 'inlineRecord') || Element.hasClassName(tempObj, 'treeRecord'))) {
						form = tempObj.value ? tempObj.value.split(',') : [];
						numberOfElements = form.length;
					}

				} else {
						// special treatment for file uploads
					var tempObj = document[TBE_EDITOR.formname][elementName.replace(/^data/, 'data_files') + '[]'];
					numberOfElements = form.length;

					if (tempObj && tempObj.type == 'file' && tempObj.value) {
						numberOfElements++; // Add new uploaded file to the number of elements
					}
				}

				if (!TBE_EDITOR.checkRange(numberOfElements, elementData.range[0], elementData.range[1])) {
					result = 0;
					if (autoNotify) {
						TBE_EDITOR.setImage('req_'+elementData.rangeImg, TBE_EDITOR.images.req);
						TBE_EDITOR.notifyNested(elementName, false);
					}
				}
			}
		}

		return result;
	},
	// Notify tabs and inline levels with nested requiredFields/requiredElements:
	notifyNested: function(elementName, resolved) {
		if (TBE_EDITOR.nested.field[elementName]) {
			var i, nested, element, fieldLevels, fieldLevelIdent, nestedLevelType, nestedLevelName;
			fieldLevels = TBE_EDITOR.nested.field[elementName].level;
			TBE_EDITOR.nestedCache = {};

			for (i=fieldLevels.length-1; i>=0; i--) {
				nestedLevelType = fieldLevels[i][0];
				nestedLevelName = fieldLevels[i][1];
				fieldLevelIdent = TBE_EDITOR.getNestedLevelIdent(fieldLevels[i]);
					// Construct the CSS id strings of the image/icon tags showing the notification:
				if (nestedLevelType == 'tab') {
					element = nestedLevelName+'-REQ';
				} else if (nestedLevelType == 'inline') {
					element = nestedLevelName+'_req';
				} else {
					continue;
				}
					// Set the icons:
				if (resolved) {
					if (TBE_EDITOR.checkNested(fieldLevelIdent)) {
						TBE_EDITOR.setImage(element, TBE_EDITOR.images.clear);
					} else {
						break;
					}
				} else {
					if (TBE_EDITOR.nested.level && TBE_EDITOR.nested.level[fieldLevelIdent]) {
						TBE_EDITOR.nested.level[fieldLevelIdent].clean = false;
					}
					TBE_EDITOR.setImage(element, TBE_EDITOR.images.req);
				}
			}
		}
	},
	// Check all the input fields on a given level of nesting - if only on is unfilled, the whole level is marked as required:
	checkNested: function(nestedLevelIdent) {
		var nestedLevel, isClean;
		if (nestedLevelIdent && TBE_EDITOR.nested.level && TBE_EDITOR.nested.level[nestedLevelIdent]) {
			nestedLevel = TBE_EDITOR.nested.level[nestedLevelIdent];
			if (!nestedLevel.clean) {
				if (typeof nestedLevel.item == 'object') {
					$H(nestedLevel.item).each(
						function(pair) {
							if (isClean || typeof isClean == 'undefined') {
								isClean = (
									TBE_EDITOR.checkElements('required', false, pair.value[0], pair.value[1]) &&
									TBE_EDITOR.checkElements('range', false, pair.value[0], pair.value[1])
								);
							}
						}
					);
					if (typeof isClean != 'undefined' && !isClean) {
						return false;
					}
				}
				if (typeof nestedLevel.sub == 'object') {
					$H(nestedLevel.sub).each(
						function(pair) {
							if (isClean || typeof isClean == 'undefined') {
								isClean = TBE_EDITOR.checkNested(pair.key);
							}
						}
					);
					if (typeof isClean != 'undefined' && !isClean) {
						return false;
					}
				}
					// Store the result, that this level (the fields on this and the sub levels) are clean:
				nestedLevel.clean = true;
			}
		}
		return true;
	},
	getNestedLevelIdent: function(level) {
		return level.join('::');
	},
	addActionChecks: function(type, checks) {
		TBE_EDITOR.actionChecks[type].push(checks);
	},

	// Regular TCEforms JSbottom scripts:
	loginRefreshed: function() {
		var date = new Date();
		TBE_EDITOR.loadTime = Math.floor(date.getTime()/1000);
		if (top.busy && top.busy.loginRefreshed) { top.busy.loginRefreshed(); }
	},
	checkLoginTimeout: function() {
		var date = new Date();
		var theTime = Math.floor(date.getTime()/1000);
		if (theTime > TBE_EDITOR.loadTime+TBE_EDITOR.auth_timeout_field-10) {
			return true;
		}
	},
	fieldChanged_fName: function(fName,el) {
		var idx=2+TBE_EDITOR.prependFormFieldNamesCnt;
		var table = TBE_EDITOR.split(fName, "[", idx);
		var uid = TBE_EDITOR.split(fName, "[", idx+1);
		var field = TBE_EDITOR.split(fName, "[", idx+2);

		table = table.substr(0,table.length-1);
		uid = uid.substr(0,uid.length-1);
		field = field.substr(0,field.length-1);
		TBE_EDITOR.fieldChanged(table,uid,field,el);
	},
	fieldChanged: function(table,uid,field,el) {
		var theField = TBE_EDITOR.prependFormFieldNames+'['+table+']['+uid+']['+field+']';
		var theRecord = TBE_EDITOR.prependFormFieldNames+'['+table+']['+uid+']';
		TBE_EDITOR.isChanged = 1;

			// Set change image:
		var imgObjName = "cm_"+table+"_"+uid+"_"+field;
		TBE_EDITOR.setImage(imgObjName,TBE_EDITOR.images.cm);

			// Set change image
		if (document[TBE_EDITOR.formname][theField] && document[TBE_EDITOR.formname][theField].type=="select-one" && document[TBE_EDITOR.formname][theField+"_selIconVal"]) {
			var imgObjName = "selIcon_"+table+"_"+uid+"_"+field+"_";
			TBE_EDITOR.setImage(imgObjName+document[TBE_EDITOR.formname][theField+"_selIconVal"].value,TBE_EDITOR.images.clear);
			document[TBE_EDITOR.formname][theField+"_selIconVal"].value = document[TBE_EDITOR.formname][theField].selectedIndex;
			TBE_EDITOR.setImage(imgObjName+document[TBE_EDITOR.formname][theField+"_selIconVal"].value,TBE_EDITOR.images.sel);
		}

			// Set required flag:
		var imgReqObjName = "req_"+table+"_"+uid+"_"+field;
		if (TBE_EDITOR.getElement(theRecord,field,'required') && document[TBE_EDITOR.formname][theField]) {
			if (TBE_EDITOR.checkElements('required', false, theRecord, field)) {
				TBE_EDITOR.setImage(imgReqObjName,TBE_EDITOR.images.clear);
				TBE_EDITOR.notifyNested(theField, true);
			} else {
				TBE_EDITOR.setImage(imgReqObjName,TBE_EDITOR.images.req);
				TBE_EDITOR.notifyNested(theField, false);
			}
		}
		if (TBE_EDITOR.getElement(theRecord,field,'range') && document[TBE_EDITOR.formname][theField]) {
			if (TBE_EDITOR.checkElements('range', false, theRecord, field)) {
				TBE_EDITOR.setImage(imgReqObjName,TBE_EDITOR.images.clear);
				TBE_EDITOR.notifyNested(theField, true);
			} else {
				TBE_EDITOR.setImage(imgReqObjName,TBE_EDITOR.images.req);
				TBE_EDITOR.notifyNested(theField, false);
			}
		}

		if (TBE_EDITOR.isPalettedoc) { TBE_EDITOR.setOriginalFormFieldValue(theField) };
	},
	setOriginalFormFieldValue: function(theField) {
		if (TBE_EDITOR.isPalettedoc && (TBE_EDITOR.isPalettedoc).document[TBE_EDITOR.formname] && (TBE_EDITOR.isPalettedoc).document[TBE_EDITOR.formname][theField]) {
			(TBE_EDITOR.isPalettedoc).document[TBE_EDITOR.formname][theField].value = document[TBE_EDITOR.formname][theField].value;
		}
	},
	isFormChanged: function(noAlert) {
		if (TBE_EDITOR.isChanged && !noAlert && confirm(TBE_EDITOR.labels.fieldsChanged)) {
			return 0;
		}
		return TBE_EDITOR.isChanged;
	},
	checkAndDoSubmit: function(sendAlert) {
		if (TBE_EDITOR.checkSubmit(sendAlert)) { TBE_EDITOR.submitForm(); }
	},
	/**
	 * Checks if the form can be submitted according to any possible restrains like required values, item numbers etc.
	 * Returns true if the form can be submitted, otherwise false (and might issue an alert message, if "sendAlert" is 1)
	 * If "sendAlert" is false, no error message will be shown upon false return value (if "1" then it will).
	 * If "sendAlert" is "-1" then the function will ALWAYS return true regardless of constraints (except if login has expired) - this is used in the case where a form field change requests a form update and where it is accepted that constraints are not observed (form layout might change so other fields are shown...)
	 */
	checkSubmit: function(sendAlert) {
		var funcIndex, funcMax, funcRes;
		var OK=1;

		// $this->additionalJS_submit:
		if (TBE_EDITOR.actionChecks && TBE_EDITOR.actionChecks.submit) {
			for (funcIndex=0, funcMax=TBE_EDITOR.actionChecks.submit.length; funcIndex<funcMax; funcIndex++) {
				try {
					eval(TBE_EDITOR.actionChecks.submit[funcIndex]);
				} catch(error) {}
			}
		}

		if(!OK) {
			if (!confirm(unescape("SYSTEM ERROR: One or more Rich Text Editors on the page could not be contacted. This IS an error, although it should not be regular.\nYou can save the form now by pressing OK, but you will loose the Rich Text Editor content if you do.\n\nPlease report the error to your administrator if it persists."))) {
				return false;
			} else {
				OK = 1;
			}
		}
		// $reqLinesCheck
		if (!TBE_EDITOR.checkElements('required', false)) { OK = 0; }
		// $reqRangeCheck
		if (!TBE_EDITOR.checkElements('range', false)) { OK = 0; }

		if (OK || sendAlert==-1) {
			return true;
		} else {
			if(sendAlert) alert(TBE_EDITOR.labels.fieldsMissing);
			return false;
		}
	},
	checkRange: function(numberOfElements, lower, upper) {
			// for backwards compatibility, check if we're dealing with an element as first parameter
		if(typeof numberOfElements == 'object') {
			numberOfElements = numberOfElements.length;
		}

		if (numberOfElements >= lower && numberOfElements <= upper) {
			return true;
		} else {
			return false;
		}
	},
	initRequired: function() {
		// $reqLinesCheck
		TBE_EDITOR.checkElements('required', true);

		// $reqRangeCheck
		TBE_EDITOR.checkElements('range', true);
	},
	setImage: function(name,image) {
		var object;
		if (document[name]) {
			object = document[name];
		} else if (document.getElementById(name)) {
			object = document.getElementById(name);
		}
		if (object) {
			if (typeof image == 'object') {
				document[name].src = image.src;
			} else {
				document[name].src = eval(image+'.src');
			}
		}
	},
	submitForm: function() {
		if (TBE_EDITOR.doSaveFieldName) {
			document[TBE_EDITOR.formname][TBE_EDITOR.doSaveFieldName].value=1;
		}
		// Set a short timeout to allow other JS processes to complete, in particular those from
		// EXT:backend/Resources/Public/JavaScript/FormEngine.js (reference: http://forge.typo3.org/issues/58755).
		// TODO: This should be solved in a better way when this script is refactored.
		window.setTimeout('document[TBE_EDITOR.formname].submit()', 10);
	},
	split: function(theStr1, delim, index) {
		var theStr = ""+theStr1;
		var lengthOfDelim = delim.length;
		sPos = -lengthOfDelim;
		if (index<1) {index=1;}
		for (var a=1; a<index; a++) {
			sPos = theStr.indexOf(delim, sPos+lengthOfDelim);
			if (sPos==-1) { return null; }
		}
		ePos = theStr.indexOf(delim, sPos+lengthOfDelim);
		if(ePos == -1) { ePos = theStr.length; }
		return (theStr.substring(sPos+lengthOfDelim,ePos));
	},
	curSelected: function(theField) {
		var fObjSel = document[TBE_EDITOR.formname][theField];
		var retVal="";
		if (fObjSel) {
			if (fObjSel.type=='select-multiple' || fObjSel.type=='select-one') {
				var l=fObjSel.length;
				for (a=0;a<l;a++) {
					if (fObjSel.options[a].selected==1) {
						retVal+=fObjSel.options[a].value+",";
					}
				}
			}
		}
		return retVal;
	},
	rawurlencode: function(str,maxlen) {
		var output = str;
		if (maxlen)	output = output.substr(0,200);
		output = encodeURIComponent(output);
		return output;
	},
	str_replace: function(match,replace,string) {
		var input = ''+string;
		var matchStr = ''+match;
		if (!matchStr) { return string; }
		var output = '';
		var pointer=0;
		var pos = input.indexOf(matchStr);
		while (pos!=-1) {
			output+=''+input.substr(pointer, pos-pointer)+replace;
			pointer=pos+matchStr.length;
			pos = input.indexOf(match,pos+1);
		}
		output+=''+input.substr(pointer);
		return output;
	},
	toggle_display_states: function(id, state_1, state_2) {
		var node = document.getElementById(id);
		if (node) {
			switch (node.style.display) {
				case state_1:
					node.style.display = state_2;
					break;
				case state_2:
					node.style.display = state_1;
					break;
			}
		}
		return false;
	},

	/**
	 * Determines backend path to be used for e.g. ajax.php
	 * @return string
	 */
	getBackendPath: function() {
		var backendPath = '';
		if (TYPO3) {
			if (TYPO3.configuration && TYPO3.configuration.PATH_typo3) {
				backendPath = TYPO3.configuration.PATH_typo3;
			} else if (TYPO3.settings && TYPO3.settings.PATH_typo3) {
				backendPath = TYPO3.settings.PATH_typo3;
			}
		}
		return backendPath;
	}
};

function typoSetup	() {
	this.passwordDummy = '********';
	this.decimalSign = '.';
}
var TS = new typoSetup();
var evalFunc = new evalFunc();

// backwards compatibility for extensions
var TBE_EDITOR_loginRefreshed = TBE_EDITOR.loginRefreshed;
var TBE_EDITOR_checkLoginTimeout = TBE_EDITOR.checkLoginTimeout;
var TBE_EDITOR_setHiddenContent = TBE_EDITOR.setHiddenContent;
var TBE_EDITOR_isChanged = TBE_EDITOR.isChanged;
var TBE_EDITOR_fieldChanged_fName = TBE_EDITOR.fieldChanged_fName;
var TBE_EDITOR_fieldChanged = TBE_EDITOR.fieldChanged;
var TBE_EDITOR_setOriginalFormFieldValue = TBE_EDITOR.setOriginalFormFieldValue;
var TBE_EDITOR_isFormChanged = TBE_EDITOR.isFormChanged;
var TBE_EDITOR_checkAndDoSubmit = TBE_EDITOR.checkAndDoSubmit;
var TBE_EDITOR_checkSubmit = TBE_EDITOR.checkSubmit;
var TBE_EDITOR_checkRange = TBE_EDITOR.checkRange;
var TBE_EDITOR_initRequired = TBE_EDITOR.initRequired;
var TBE_EDITOR_setImage = TBE_EDITOR.setImage;
var TBE_EDITOR_submitForm = TBE_EDITOR.submitForm;
var TBE_EDITOR_split = TBE_EDITOR.split;
var TBE_EDITOR_curSelected = TBE_EDITOR.curSelected;
var TBE_EDITOR_rawurlencode = TBE_EDITOR.rawurlencode;
var TBE_EDITOR_str_replace = TBE_EDITOR.str_replace;


var typo3form = {
	fieldSetNull: function(fieldName, isNull) {
		if (document[TBE_EDITOR.formname][fieldName]) {
			var formFieldItemWrapper = Element.up(document[TBE_EDITOR.formname][fieldName], '.t3-form-field-item');

			if (isNull) {
				formFieldItemWrapper.addClassName('disabled');
			} else {
				formFieldItemWrapper.removeClassName('disabled');
			}
		}
	},
	fieldTogglePlaceholder: function(fieldName, showPlaceholder) {
		if (!document[TBE_EDITOR.formname][fieldName]) {
			return;
		}

		var formFieldItemWrapper = Element.up(document[TBE_EDITOR.formname][fieldName], '.t3-form-field-item');
		var placeholder = formFieldItemWrapper.select('.t3-form-placeholder-placeholder')[0];
		var formField = formFieldItemWrapper.select('.t3-form-placeholder-formfield')[0];
		if (showPlaceholder) {
			placeholder.show();
			formField.hide();
		} else {
			placeholder.hide();
			formField.show();
		}
	},
	fieldSet: function(theField, evallist, is_in, checkbox, checkboxValue) {
		var i;

		if (document[TBE_EDITOR.formname][theField]) {
			var theFObj = new evalFunc_dummy (evallist,is_in, checkbox, checkboxValue);
			var theValue = document[TBE_EDITOR.formname][theField].value;
			if (checkbox && theValue==checkboxValue) {
				document[TBE_EDITOR.formname][theField+"_hr"].value="";
				if (document[TBE_EDITOR.formname][theField+"_cb"])	document[TBE_EDITOR.formname][theField+"_cb"].checked = "";
			} else {
				document[TBE_EDITOR.formname][theField+"_hr"].value = evalFunc.outputObjValue(theFObj, theValue);
				if (document[TBE_EDITOR.formname][theField+"_cb"])	document[TBE_EDITOR.formname][theField+"_cb"].checked = "on";
			}
		}
	},
	fieldGet: function(theField, evallist, is_in, checkbox, checkboxValue, checkbox_off, checkSetValue) {
		if (document[TBE_EDITOR.formname][theField]) {
			var theFObj = new evalFunc_dummy (evallist,is_in, checkbox, checkboxValue);
			if (checkbox_off) {
				if (document[TBE_EDITOR.formname][theField+"_cb"].checked) {
					var split = evallist.split(',');
					for (var i = 0; split.length > i; i++) {
						var el = split[i].replace(/ /g, '');
						if (el == 'datetime' || el == 'date') {
							var now = new Date();
							checkSetValue = Date.parse(now)/1000 - now.getTimezoneOffset()*60;
							break;
						} else if (el == 'time' || el == 'timesec') {
							checkSetValue = evalFunc_getTimeSecs(new Date());
							break;
						}
					}
					document[TBE_EDITOR.formname][theField].value=checkSetValue;
				} else {
					document[TBE_EDITOR.formname][theField].value=checkboxValue;
				}
			}else{
				document[TBE_EDITOR.formname][theField].value = evalFunc.evalObjValue(theFObj, document[TBE_EDITOR.formname][theField+"_hr"].value);
			}
			typo3form.fieldSet(theField, evallist, is_in, checkbox, checkboxValue);
		}
	}
};

// backwards compatibility for extensions
var typo3FormFieldSet = typo3form.fieldSet;
var typo3FormFieldGet = typo3form.fieldGet;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

Ext.onReady(function() {
	// Only use placeholder JavaScript fallback in Internet Explorer
	if (!Ext.isIE) {
		return;
	}

	// TODO rewrite in ExtJS
	$$('[placeholder]').each(function(el) {
		if (el.getAttribute('placeholder') != "") {
			el.observe('TYPO3:focus', function() {
				var input = Ext.get(this);
				if (this.getValue() == this.getAttribute('placeholder')) {
					this.setValue('');
					input.removeClass('placeholder');
				}
			});
			el.observe('focus', function() { el.fire('TYPO3:focus'); });
			el.observe('TYPO3:blur', function() {
				var input = Ext.get(this);
				if (input.getValue() == '' || this.getValue() == this.getAttribute('placeholder')) {
					this.setValue(this.getAttribute('placeholder'));
					input.addClass('placeholder');
				}
			});
			el.observe('blur', function() { el.fire('TYPO3:blur'); });
			el.fire('TYPO3:blur');
		}
	});
});
Ext.ns('Ext.ux', 'Ext.ux.menu', 'Ext.ux.form');

Ext.ux.DateTimePicker = Ext.extend(Ext.DatePicker, {

	timeFormat: 'H:i',

	initComponent: function() {
		var t = this.timeFormat.split(':');
		this.hourFormat = t[0];
		this.minuteFormat = t[1];

		Ext.ux.DateTimePicker.superclass.initComponent.call(this);
	},

	/**
	 * Replaces any existing {@link #minDate} with the new value and refreshes the DatePicker.
	 * @param {Date} value The minimum date that can be selected
	 */
	setMinTime: function(dt) {
		this.minTime = dt;
		this.update(this.value, true);
	},

	/**
	 * Replaces any existing {@link #maxDate} with the new value and refreshes the DatePicker.
	 * @param {Date} value The maximum date that can be selected
	 */
	setMaxTime: function(dt) {
		this.maxTime = dt;
		this.update(this.value, true);
	},

	/**
	 * Returns the value of the date/time field
	 */
	getValue: function() {
		return this.addTimeToValue(this.value);
	},

	/**
	 * Sets the value of the date/time field
	 * @param {Date} value The date to set
	 */
	setValue: function(value) {
		var old = this.value;
		this.value = value.clearTime(true);
		if (this.el) {
			this.update(this.value);
		}
		this.hourField.setValue(value.format(this.hourFormat));
		this.minuteField.setValue(value.format(this.minuteFormat));
	},

	/**
	 * Sets the value of the time field
	 * @param {Date} value The date to set
	 */
	setTime: function(value) {
		this.hourField.setValue(value.format(this.hourFormat));
		this.minuteField.setValue(value.format(this.minuteFormat));
	},

	/**
	 * Updates the date value with the time entered
	 * @param {Date} value The date to which time should be added
	 */
	addTimeToValue: function(date) {
		return date.clearTime().add(Date.HOUR, this.hourField.getValue()).add(Date.MINUTE, this.minuteField.getValue());
	},

	onRender: function(container, position) {
		var m = [
			'<table cellspacing="0">',
			'<tr><td class="x-date-left"><a href="#" title="',
			this.prevText ,
			'">&#160;</a></td><td class="x-date-middle" align="center"></td><td class="x-date-right"><a href="#" title="',
			this.nextText ,
			'">&#160;</a></td></tr>',
			'<tr><td colspan="3"><table class="x-date-inner" cellspacing="0"><thead><tr>'
		];
		var dn = this.dayNames;
		for (var i = 0; i < 7; i++) {
			var d = this.startDay + i;
			if (d > 6) {
				d = d - 7;
			}
			m.push('<th><span>', dn[d].substr(0, 1), '</span></th>');
		}
		m[m.length] = "</tr></thead><tbody><tr>";
		for (var i = 0; i < 42; i++) {
			if (i % 7 == 0 && i != 0) {
				m[m.length] = "</tr><tr>";
			}
			m[m.length] = '<td><a href="#" hidefocus="on" class="x-date-date" tabIndex="1"><em><span></span></em></a></td>';
		}
		m.push('</tr></tbody></table></td></tr>',
			this.showToday ? '<tr><td colspan="3" class="x-date-bottom" align="center"></td></tr>' : '',
			'</table><div class="x-date-mp"></div>'
		);

		var el = document.createElement("div");
		el.className = "x-date-picker";
		el.innerHTML = m.join("");

		container.dom.insertBefore(el, position);

		this.el = Ext.get(el);
		this.eventEl = Ext.get(el.firstChild);

		new Ext.util.ClickRepeater(this.el.child("td.x-date-left a"), {
			handler: this.showPrevMonth,
			scope: this,
			preventDefault:true,
			stopDefault:true
		});

		new Ext.util.ClickRepeater(this.el.child("td.x-date-right a"), {
			handler: this.showNextMonth,
			scope: this,
			preventDefault:true,
			stopDefault:true
		});

		this.mon(this.eventEl, "mousewheel", this.handleMouseWheel, this);

		this.monthPicker = this.el.down('div.x-date-mp');
		this.monthPicker.enableDisplayMode('block');

		var kn = new Ext.KeyNav(this.eventEl, {
			"left": function(e) {
				e.ctrlKey ?
					this.showPrevMonth() :
					this.update(this.activeDate.add("d", -1));
			},

			"right": function(e) {
				e.ctrlKey ?
					this.showNextMonth() :
					this.update(this.activeDate.add("d", 1));
			},

			"up": function(e) {
				e.ctrlKey ?
					this.showNextYear() :
					this.update(this.activeDate.add("d", -7));
			},

			"down": function(e) {
				e.ctrlKey ?
					this.showPrevYear() :
					this.update(this.activeDate.add("d", 7));
			},

			"pageUp": function(e) {
				this.showNextMonth();
			},

			"pageDown": function(e) {
				this.showPrevMonth();
			},

			"enter": function(e) {
				e.stopPropagation();
				this.fireEvent("select", this, this.value);
				return true;
			},

			scope : this
		});

		this.mon(this.eventEl, "click", this.handleDateClick, this, {delegate: "a.x-date-date"});

		this.el.select("table.x-date-inner").unselectable();
		this.cells = this.el.select("table.x-date-inner tbody td");
		this.textNodes = this.el.query("table.x-date-inner tbody span");

		this.mbtn = new Ext.Button({
			text: "&#160;",
			tooltip: this.monthYearText,
			renderTo: this.el.child("td.x-date-middle", true)
		});

		this.mon(this.mbtn, 'click', this.showMonthPicker, this);
		this.mbtn.el.child('em').addClass("x-btn-arrow");

		if (this.showToday) {
			this.todayKeyListener = this.eventEl.addKeyListener(Ext.EventObject.SPACE, this.selectToday, this);
			var today = (new Date()).dateFormat(this.format);
			this.todayBtn = new Ext.Button({
				text: String.format(this.todayText, today),
				tooltip: String.format(this.todayTip, today),
				handler: this.selectToday,
				scope: this
			});
		}

		this.formPanel = new Ext.form.FormPanel({
			layout: 'table',
			layoutConfig: {
				// The total column count must be specified here
				columns: 3
			},
			renderTo: this.el.child("td.x-date-bottom", true),
			baseCls: 'x-plain',
			hideBorders: true,
			labelAlign: 'left',
			labelWidth: 10,
			forceLayout: true,
			items: [
				{
					columnWidth: .4,
					layout: 'form',
					baseCls: 'x-plain',
					items: [
						{
							xtype: 'textfield',
							id: this.getId() + '_hour',
							maxLength: 2,
							fieldLabel: '',
							labelWidth: 30,
							width: 30,
							minValue: 0,
							maxValue: 24,
							allowBlank: false,
							labelSeparator: '',
							tabIndex: 1,
							maskRe: /[0-9]/
						}
					]
				},
				{
					columnWidth: .3,
					layout: 'form',
					baseCls: 'x-plain',
					items: [
						{
							xtype: 'textfield',
							id:	this.getId() + '_minute',
							maxLength: 2,
							fieldLabel: ':',
							labelWidth: 10,
							width: 30,
							minValue: 0,
							maxValue: 59,
							allowBlank: false,
							labelSeparator: '',
							tabIndex: 2,
							maskRe: /[0-9]/
						}
					]
				},
				{
					columnWidth: .3,
					layout: 'form',
					baseCls: 'x-plain',
					items: [this.todayBtn]
				}
			]
		});

		this.hourField = Ext.getCmp(this.getId() + '_hour');
		this.minuteField = Ext.getCmp(this.getId() + '_minute');

		this.hourField.on('blur', function(field) {
			var old = field.value;
			var h = parseInt(field.getValue());
			if (h > 23) {
				field.setValue(old);
			}
		});

		this.minuteField.on('blur', function(field) {
			var old = field.value;
			var h = parseInt(field.getValue());
			if (h > 59) {
				field.setValue(old);
			}
		});

		if (Ext.isIE) {
			this.el.repaint();
		}
		this.update(this.value);
	},

	// private
	handleDateClick : function(e, t) {
		e.stopEvent();
		if (t.dateValue && !Ext.fly(t.parentNode).hasClass("x-date-disabled")) {
			this.setValue(this.addTimeToValue(new Date(t.dateValue)));
			this.fireEvent("select", this, this.value);
		}
	},

	selectToday : function() {
		if (this.todayBtn && !this.todayBtn.disabled) {
			this.setValue(new Date());
			this.fireEvent("select", this, this.value);
		}
	},

	update : function(date, forceRefresh) {
		Ext.ux.DateTimePicker.superclass.update.call(this, date, forceRefresh);

		if (this.showToday) {
			this.setTime(new Date());
		}
	}
});
Ext.reg('datetimepicker', Ext.ux.DateTimePicker);


Ext.ux.menu.DateTimeMenu = Ext.extend(Ext.menu.Menu, {
	enableScrolling : false,
	hideOnClick : true,
	cls: 'x-date-menu x-datetime-menu',
	initComponent : function() {

		Ext.apply(this, {
			plain: true,
			showSeparator: false,
			items: this.picker = new Ext.ux.DateTimePicker(Ext.apply({
				internalRender: this.strict || !Ext.isIE,
				ctCls: 'x-menu-datetime-item x-menu-date-item'
			}, this.initialConfig))
		});
		this.picker.purgeListeners();

		Ext.ux.menu.DateTimeMenu.superclass.initComponent.call(this);
		this.relayEvents(this.picker, ['select']);
		this.on('select', this.menuHide, this);
		if (this.handler) {
			this.on('select', this.handler, this.scope || this)
		}
	},
	menuHide: function() {
		if (this.hideOnClick) {
			this.hide(true);
		}
	}
});
Ext.reg('datetimemenu', Ext.ux.menu.DateTimeMenu);
/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

Ext.ns('TYPO3');

	// class to manipulate TCEFORMS
TYPO3.TCEFORMS = {

	init: function() {
		Ext.QuickTips.init();

		this.update();
	},

	update: function() {
		this.convertDateFieldsToDatePicker();
		this.convertTextareasResizable();
	},

	convertDateFieldsToDatePicker: function() {
		var dateFields = Ext.select("input[id^=tceforms-date]"), minDate, maxDate, lowerMatch, upperMatch;
		dateFields.each(function(element) {
			var index = element.dom.id.match(/tceforms-datefield-/) ? 0 : 1;
			var format = TYPO3.settings.datePickerUSmode ? TYPO3.settings.dateFormatUS : TYPO3.settings.dateFormat;
			var datepicker = element.next('span'), menu;

			// check for daterange
			var lowerMatch = element.dom.className.match(/lower-(\d+)\b/);
			minDate = Ext.isArray(lowerMatch) ? new Date(lowerMatch[1] * 1000) : null;
			var upperMatch = element.dom.className.match(/upper-(\d+)\b/);
			maxDate = Ext.isArray(upperMatch) ? new Date(upperMatch[1] * 1000) : null;

			if (index === 0) {
				menu = new Ext.menu.DateMenu({
					id: 'p' + element.dom.id,
					format: format[index],
					value: Date.parseDate(element.dom.value, format[index]),
					minDate: minDate,
					maxDate: maxDate,
					handler: function(picker, date){
						var relElement = Ext.getDom(picker.ownerCt.id.substring(1));
						relElement.value = date.format(format[index]);
						if (Ext.isFunction(relElement.onchange)) {
							relElement.onchange.call(relElement);
						}
					},
					listeners: {
						beforeshow: function(obj) {
							var relElement = Ext.getDom(obj.picker.ownerCt.id.substring(1));
							if (relElement.value) {
								obj.picker.setValue(Date.parseDate(relElement.value, format[index]));
							}
						}
					}
				});
			} else {
				menu = new Ext.ux.menu.DateTimeMenu({
					id: 'p' + element.dom.id,
					format: format[index],
					value: Date.parseDate(element.dom.value, format[index]),
					minDate: minDate,
					maxDate: maxDate,
					listeners: {
						beforeshow: function(obj) {
							var relElement = Ext.getDom(obj.picker.ownerCt.id.substring(1));
							if (relElement.value) {
								obj.picker.setValue(Date.parseDate(relElement.value, format[index]));
							}
						},
						select: function(picker) {
							var relElement = Ext.getDom(picker.ownerCt.id.substring(1));
							relElement.value = picker.getValue().format(format[index]);
							if (Ext.isFunction(relElement.onchange)) {
								relElement.onchange.call(relElement);
							}
						}
					}
				});
			}

			datepicker.removeAllListeners();
			datepicker.on('click', function(){
				menu.show(datepicker);
			});
		});
	},

	convertTextareasResizable: function() {
		var textAreas = Ext.select("textarea[id^=tceforms-textarea-]");
		textAreas.each(function(element) {
			if (TYPO3.settings.textareaFlexible) {
				var elasticTextarea = new Ext.ux.elasticTextArea().applyTo(element.dom.id, {
					minHeight: 50,
					maxHeight: TYPO3.settings.textareaMaxHeight
				});
			}
			if (TYPO3.settings.textareaResize) {
				element.addClass('resizable');
				var dwrapped = new Ext.Resizable(element.dom.id, {
					minWidth:  300,
					minHeight: 50,
					dynamic:   true
				});
			}
		});
	}

}
Ext.onReady(TYPO3.TCEFORMS.init, TYPO3.TCEFORMS);

	// Fix for slider TCA control in IE9
Ext.override(Ext.dd.DragTracker, {
	onMouseMove:function (e, target) {
		var isIE9 = Ext.isIE && (/msie 9/.test(navigator.userAgent.toLowerCase())) && document.documentMode != 6;
		if (this.active && Ext.isIE && !isIE9 && !e.browserEvent.button) {
			e.preventDefault();
			this.onMouseUp(e);
			return;
		}
		e.preventDefault();
		var xy = e.getXY(), s = this.startXY;
		this.lastXY = xy;
		if (!this.active) {
			if (Math.abs(s[0] - xy[0]) > this.tolerance || Math.abs(s[1] - xy[1]) > this.tolerance) {
				this.triggerStart(e);
			} else {
				return;
			}
		}
		this.fireEvent('mousemove', this, e);
		this.onDrag(e);
		this.fireEvent('drag', this, e);
	}
});
/*<![CDATA[*/

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 *  Inline-Relational-Record Editing
 */

var inline = {
	classVisible: 't3-form-field-container-inline-visible',
	classCollapsed: 't3-form-field-container-inline-collapsed',
	structureSeparator: '-',
	flexFormSeparator: '---',
	flexFormSubstitute: ':',
	prependFormFieldNames: 'data',
	noTitleString: '[No title]',
	lockedAjaxMethod: {},
	sourcesLoaded: {},
	data: {},
	isLoading: false,

	addToDataArray: function(object) {
		$H(object).each(function(pair) {
			inline.data[pair.key] = $H(inline.data[pair.key]).merge(pair.value).toObject();
		});
	},
	setPrependFormFieldNames: function(value) {
		this.prependFormFieldNames = value;
	},
	setNoTitleString: function(value) {
		this.noTitleString = value;
	},
	toggleEvent: function(event) {
		var triggerElement = TYPO3.jQuery(event.target);
		if (triggerElement.parents('.t3-form-field-header-inline-ctrl').length == 1) {
			return;
		}

		var recordHeader = TYPO3.jQuery(this);
		inline.expandCollapseRecord(
			recordHeader.attr('id').replace('_header', ''),
			recordHeader.attr('data-expandSingle'),
			recordHeader.attr('data-returnURL')
		);
	},
	expandCollapseRecord: function(objectId, expandSingle, returnURL) {
		var currentUid = this.parseObjectId('none', objectId, 1);
		var objectPrefix = this.parseObjectId('full', objectId, 0, 1);
		var escapedObjectId = this.escapeObjectId(objectId);

		var currentObject = TYPO3.jQuery('#' + escapedObjectId + '_div');
			// if content is not loaded yet, get it now from server
		if((TYPO3.jQuery('#' + escapedObjectId + '_fields') && $("irre-loading-indicator" + objectId)) || inline.isLoading) {
			return false;
		} else if ($(objectId + '_fields') && $(objectId + '_fields').innerHTML.substr(0,16) == '<!--notloaded-->') {
			inline.isLoading = true;
				// add loading-indicator
			if (TYPO3.jQuery('#' + escapedObjectId + '_loadingbar').length === 0) {
				var loadingBar = '<div id="' + escapedObjectId + '_loadingbar" class="t3-form-header-inline-loadingbar loadingbar"><div class="expand"></div></div>';
				TYPO3.jQuery('#' + escapedObjectId + '_header').after(loadingBar);
			}
			return this.getRecordDetails(objectId, returnURL);
		}

		var isCollapsed = currentObject.hasClass(this.classCollapsed);
		var collapse = new Array();
		var expand = new Array();

			// if only a single record should be visibly for that set of records
			// and the record clicked itself is no visible, collapse all others
		if (expandSingle && currentObject.hasClass(this.classCollapsed)) {
			collapse = this.collapseAllRecords(objectId, objectPrefix, currentUid);
		}

		inline.toggleElement(objectId);

		if (this.isNewRecord(objectId)) {
			this.updateExpandedCollapsedStateLocally(objectId, isCollapsed);
		} else if (isCollapsed) {
			expand.push(currentUid);
		} else if (!isCollapsed) {
			collapse.push(currentUid);
		}

		this.setExpandedCollapsedState(objectId, expand.join(','), collapse.join(','));

		return false;
	},

	toggleElement: function(objectId) {
		var escapedObjectId = this.escapeObjectId(objectId);
		var jQueryObject = TYPO3.jQuery('#' + escapedObjectId + '_div');

		if (jQueryObject.hasClass(this.classCollapsed)) {
			jQueryObject.removeClass(this.classCollapsed).addClass(this.classVisible);
			jQueryObject.find('#' + escapedObjectId + '_header .t3-icon-irre-collapsed').removeClass('t3-icon-irre-collapsed').addClass('t3-icon-irre-expanded');
		} else {
			jQueryObject.removeClass(this.classVisible).addClass(this.classCollapsed);
			jQueryObject.find('#' + escapedObjectId + '_header .t3-icon-irre-expanded').addClass('t3-icon-irre-collapsed').removeClass('t3-icon-irre-expanded');
		}
	},
	collapseAllRecords: function(objectId, objectPrefix, callingUid) {
			// get the form field, where all records are stored
		var objectName = this.prependFormFieldNames+this.parseObjectId('parts', objectId, 3, 2, true);
		var formObj = document.getElementsByName(objectName);
		var collapse = [];

		if (formObj.length) {
				// the uid of the calling object (last part in objectId)
			var recObjectId = '', escapedRecordObjectId;

			var records = formObj[0].value.split(',');
			for (var i=0; i<records.length; i++) {
				recObjectId = objectPrefix + this.structureSeparator + records[i];
				escapedRecordObjectId = this.escapeObjectId(recObjectId);

				var recordEntry = TYPO3.jQuery('#' + escapedRecordObjectId);
				if (records[i] != callingUid && recordEntry.hasClass(this.classVisible)) {
					TYPO3.jQuery('#' + escapedRecordObjectId + '_div').removeClass(this.classVisible).addClass(this.classCollapsed);
					if (this.isNewRecord(recObjectId)) {
						this.updateExpandedCollapsedStateLocally(recObjectId, 0);
					} else {
						collapse.push(records[i]);
					}
				}
			}
		}

		return collapse;
	},

	updateExpandedCollapsedStateLocally: function(objectId, value) {
		var ucName = 'uc[inlineView]'+this.parseObjectId('parts', objectId, 3, 2, true);
		var ucFormObj = document.getElementsByName(ucName);
		if (ucFormObj.length) {
			ucFormObj[0].value = value;
		}
	},

	getRecordDetails: function(objectId, returnURL) {
		var context = this.getContext(this.parseObjectId('full', objectId, 0, 1));
		inline.makeAjaxCall('getRecordDetails', [inline.getNumberOfRTE(), objectId, returnURL], true, context);
		return false;
	},

	createNewRecord: function(objectId, recordUid) {
		if (this.isBelowMax(objectId)) {
			var context = this.getContext(objectId);
			if (recordUid) {
				objectId += this.structureSeparator + recordUid;
			}
			this.makeAjaxCall('createNewRecord', [this.getNumberOfRTE(), objectId], true, context);
		} else {
			alert('There are no more relations possible at this moment!');
		}
		return false;
	},

	synchronizeLocalizeRecords: function(objectId, type) {
		var context = this.getContext(objectId);
		var parameters = [this.getNumberOfRTE(), objectId, type];
		this.makeAjaxCall('synchronizeLocalizeRecords', parameters, true, context);
	},

	setExpandedCollapsedState: function(objectId, expand, collapse) {
		var context = this.getContext(objectId);
		this.makeAjaxCall('setExpandedCollapsedState', [objectId, expand, collapse], false, context);
	},

	makeAjaxCall: function(method, params, lock, context) {
		var max, url='', urlParams='', options={};
		if (method && params && params.length && this.lockAjaxMethod(method, lock)) {
			url = TBE_EDITOR.getBackendPath() + TYPO3.settings.ajaxUrls['t3lib_TCEforms_inline::' + method];
			urlParams = '';
			for (var i=0, max=params.length; i<max; i++) {
				urlParams += '&ajax[' + i + ']=' + encodeURIComponent(params[i]);
			}
			if (context) {
				urlParams += '&ajax[context]=' + encodeURIComponent(Object.toJSON(context));
			}
			options = {
				method:		'post',
				parameters:	urlParams,
				onSuccess:	function(xhr) { inline.isLoading = false; inline.processAjaxResponse(method, xhr); },
				onFailure:	function(xhr) { inline.isLoading = false; inline.showAjaxFailure(method, xhr); }
			};

			new Ajax.Request(url, options);
		}
	},

	lockAjaxMethod: function(method, lock) {
		if (!lock || !inline.lockedAjaxMethod[method]) {
			inline.lockedAjaxMethod[method] = true;
			return true;
		} else {
			return false;
		}
	},

	unlockAjaxMethod: function(method) {
		inline.lockedAjaxMethod[method] = false;
	},

	processAjaxResponse: function(method, xhr, json) {
		var addTag=null, restart=false, processedCount=0, element=null, errorCatch=[], sourcesWaiting=[];
		if (!json && xhr) {
			json = xhr.responseJSON;
		}
			// If there are elements the should be added to the <HEAD> tag (e.g. for RTEhtmlarea):
		if (json.headData) {
			var head = inline.getDomHeadTag();
			var headTags = inline.getDomHeadChildren(head);
			$A(json.headData).each(function(addTag) {
				if (!restart) {
					if (addTag && (addTag.innerHTML || !inline.searchInDomTags(headTags, addTag))) {
						if (addTag.name=='SCRIPT' && addTag.innerHTML && processedCount) {
							restart = true;
							return false;
						} else {
							if (addTag.name=='SCRIPT' && addTag.innerHTML) {
								try {
									eval(addTag.innerHTML);
								} catch(e) {
									errorCatch.push(e);
								}
							} else {
								element = inline.createNewDomElement(addTag);
									// Set onload handler for external JS scripts:
								if (addTag.name=='SCRIPT' && element.src) {
									element.onload = inline.sourceLoadedHandler(element);
									sourcesWaiting.push(element.src);
								}
								head.appendChild(element);
								processedCount++;
							}
							json.headData.shift();
						}
					}
				}
			});
		}
		if (restart || processedCount) {
			window.setTimeout(function() { inline.reprocessAjaxResponse(method, json, sourcesWaiting); }, 40);
		} else {
			if (method) {
				inline.unlockAjaxMethod(method);
			}
			if (json.scriptCall && json.scriptCall.length) {
				$A(json.scriptCall).each(function(value) { eval(value); });
			}
			TYPO3.TCEFORMS.convertDateFieldsToDatePicker();
		}
	},

		// Check if dynamically added scripts are loaded and restart inline.processAjaxResponse():
	reprocessAjaxResponse: function (method, json, sourcesWaiting) {
		var sourcesLoaded = true;
		if (sourcesWaiting && sourcesWaiting.length) {
			$A(sourcesWaiting).each(function(source) {
				if (!inline.sourcesLoaded[source]) {
					sourcesLoaded = false;
					return false;
				}
			});
		}
		if (sourcesLoaded) {
			$A(sourcesWaiting).each(function(source) {
				delete(inline.sourcesLoaded[source]);
			});
			window.setTimeout(function() { inline.processAjaxResponse(method, null, json); }, 80);
		} else {
			window.setTimeout(function() { inline.reprocessAjaxResponse(method, json, sourcesWaiting); }, 40);
		}
	},

	sourceLoadedHandler: function(element) {
		if (element && element.src) {
			inline.sourcesLoaded[element.src] = true;
		}
	},

	showAjaxFailure: function(method, xhr) {
		inline.unlockAjaxMethod(method);
		alert('Error: '+xhr.status+"\n"+xhr.statusText);
	},

		// foreign_selector: used by selector box (type='select')
	importNewRecord: function(objectId) {
		var selector = $(objectId+'_selector');
		if (selector.selectedIndex != -1) {
			var context = this.getContext(objectId);
			var selectedValue = selector.options[selector.selectedIndex].value;
			if (!this.data.unique || !this.data.unique[objectId]) {
				selector.options[selector.selectedIndex].selected = false;
			}
			this.makeAjaxCall('createNewRecord', [this.getNumberOfRTE(), objectId, selectedValue], true, context);
		}
		return false;
	},

		// foreign_selector: used by element browser (type='group/db')
	importElement: function(objectId, table, uid, type) {
		var context = this.getContext(objectId);
		inline.makeAjaxCall('createNewRecord', [inline.getNumberOfRTE(), objectId, uid], true, context);
	},

	importElementMultiple: function(objectId, table, uidArray, type) {
		uidArray.each(function(uid) {
			inline.delayedImportElement(objectId, table, uid, type);
		});
	},
	delayedImportElement: function(objectId, table, uid, type) {
		if (inline.lockedAjaxMethod['createNewRecord'] == true) {
			window.setTimeout("inline.delayedImportElement('" + objectId + "','" + table + "'," +  uid + ", null );", 300);
		} else {
			inline.importElement(objectId, table, uid, type);
		}
	},
		// Check uniqueness for element browser:
	checkUniqueElement: function(objectId, table, uid, type) {
		if (this.checkUniqueUsed(objectId, uid, table)) {
			return {passed: false,message: 'There is already a relation to the selected element!'};
		} else {
			return {passed: true};
		}
	},

		// Checks if a record was used and should be unique:
	checkUniqueUsed: function(objectId, uid, table) {
		if (this.data.unique && this.data.unique[objectId]) {
			var unique = this.data.unique[objectId];
			var values = $H(unique.used).values();

				// for select: only the uid is stored
			if (unique['type'] == 'select') {
				if (values.indexOf(uid) != -1) {
					return true;
				}

				// for group/db: table and uid is stored in a assoc array
			} else if (unique.type == 'groupdb') {
				for (var i=values.length-1; i>=0; i--) {
						// if the pair table:uid is already used:
					if (values[i].table==table && values[i].uid==uid) {
						return true;
					}
				}
			}
		}
		return false;
	},

	setUniqueElement: function(objectId, table, uid, type, elName) {
		var recordUid = this.parseFormElementName('none', elName, 1, 1);
		// alert(objectId+'/'+table+'/'+uid+'/'+recordUid);
		this.setUnique(objectId, recordUid, uid);
	},
		// Remove all select items already used
		// from a newly retrieved/expanded record
	removeUsed: function(objectId, recordUid) {
		if (this.data.unique && this.data.unique[objectId]) {
			var unique = this.data.unique[objectId];
			if (unique.type == 'select') {
				var formName = this.prependFormFieldNames+this.parseObjectId('parts', objectId, 3, 1, true);
				var formObj = document.getElementsByName(formName);
				var recordObj = document.getElementsByName(this.prependFormFieldNames+'['+unique.table+']['+recordUid+']['+unique.field+']');
				var values = $H(unique.used).values();
				if (recordObj.length) {
					if (recordObj[0].hasOwnProperty('options')) {
						var selectedValue = recordObj[0].options[recordObj[0].selectedIndex].value;
						for (var i=0; i<values.length; i++) {
							if (values[i] != selectedValue) {
								this.removeSelectOption(recordObj[0], values[i]);
							}
						}
					}
				}
			}
		}
	},
		// this function is applied to a newly inserted record by AJAX
		// it removes the used select items, that should be unique
	setUnique: function(objectId, recordUid, selectedValue) {
		if (this.data.unique && this.data.unique[objectId]) {
			var unique = this.data.unique[objectId];
			if (unique.type == 'select') {
				if (!(unique.selector && unique.max == -1)) {
					var formName = this.prependFormFieldNames+this.parseObjectId('parts', objectId, 3, 1, true);
					var formObj = document.getElementsByName(formName);
					var recordObj = document.getElementsByName(this.prependFormFieldNames+'['+unique.table+']['+recordUid+']['+unique.field+']');
					var values = $H(unique.used).values();
					var selector = $(objectId+'_selector');
					if (selector.length) {
							// remove all items from the new select-item which are already used in other children
						if (recordObj.length) {
							for (var i=0; i<values.length; i++) {
								this.removeSelectOption(recordObj[0], values[i]);
							}
								// set the selected item automatically to the first of the remaining items if no selector is used
							if (!unique.selector) {
								selectedValue = recordObj[0].options[0].value;
								recordObj[0].options[0].selected = true;
								this.updateUnique(recordObj[0], objectId, formName, recordUid);
								this.handleChangedField(recordObj[0], objectId+'['+recordUid+']');
							}
						}
						for (var i=0; i<values.length; i++) {
							this.removeSelectOption(selector, values[i]);
						}
						if (typeof this.data.unique[objectId].used.length != 'undefined') {
							this.data.unique[objectId].used = {};
						}
						this.data.unique[objectId].used[recordUid] = selectedValue;
					}
						// remove the newly used item from each select-field of the child records
					if (formObj.length && selectedValue) {
						var records = formObj[0].value.split(',');
						for (var i=0; i<records.length; i++) {
							recordObj = document.getElementsByName(this.prependFormFieldNames+'['+unique.table+']['+records[i]+']['+unique.field+']');
							if (recordObj.length && records[i] != recordUid) {
								this.removeSelectOption(recordObj[0], selectedValue);
							}
						}
					}
				}
			} else if (unique.type == 'groupdb') {
					// add the new record to the used items:
				this.data.unique[objectId].used[recordUid] = {'table':unique.elTable, 'uid':selectedValue};
			}

				// remove used items from a selector-box
			if (unique.selector == 'select' && selectedValue) {
				var selector = $(objectId+'_selector');
				this.removeSelectOption(selector, selectedValue);
				this.data.unique[objectId]['used'][recordUid] = selectedValue;
			}
		}
	},

	domAddNewRecord: function(method, insertObject, objectPrefix, htmlData) {
		if (this.isBelowMax(objectPrefix)) {
			if (method == 'bottom') {
				new Insertion.Bottom(insertObject, htmlData);
			} else if (method == 'after') {
				new Insertion.After(insertObject, htmlData);
			}
		}
	},
	domAddRecordDetails: function(objectId, objectPrefix, expandSingle, htmlData) {
		var hiddenValue, formObj, valueObj;
		var objectDiv = $(objectId + '_fields');
		if (!objectDiv || objectDiv.innerHTML.substr(0,16) != '<!--notloaded-->') {
			return;
		}

		var elName = this.parseObjectId('full', objectId, 2, 0, true);

		var escapeSelectorObjectId = this.escapeSelectorObjectId(objectId);

		formObj = $$('[name="' + elName + '[hidden]_0"]');
		valueObj = $$('[name="' + elName + '[hidden]"]');

			// It might be the case that a child record
			// cannot be hidden at all (no hidden field)
		if (formObj.length && valueObj.length) {
			hiddenValue = formObj[0].checked;
			formObj[0].remove();
			valueObj[0].remove();
		}

			// Update DOM
		objectDiv.update(htmlData);

		formObj = document.getElementsByName(elName + '[hidden]_0');
		valueObj = document.getElementsByName(elName + '[hidden]');

			// Set the hidden value again
		if (formObj.length && valueObj.length) {
			valueObj[0].value = hiddenValue ? 1 : 0;
			formObj[0].checked = hiddenValue;
		}

			// remove loading-indicator
		TYPO3.jQuery('#' + escapeSelectorObjectId + '_loadingbar').remove();

			// now that the content is loaded, set the expandState
		this.expandCollapseRecord(objectId, expandSingle);
	},

		// Get script and link elements from head tag:
	getDomHeadChildren: function(head) {
		var headTags = [];
		$$('head script', 'head link').each(function(tag) {
			headTags.push(tag);
		});
		return headTags;
	},

	getDomHeadTag: function() {
		if (document && document.head) {
			return document.head;
		} else {
			var head = $$('head');
			if (head.length) {
				return head[0];
			}
		}
		return false;
	},

		// Search whether elements exist in a given haystack:
	searchInDomTags: function(haystack, needle) {
		var result = false;
		$A(haystack).each(function(element) {
			if (element.nodeName.toUpperCase()==needle.name) {
				var attributesCount = $H(needle.attributes).keys().length;
				var attributesFound = 0;
				$H(needle.attributes).each(function(attribute) {
					if (element.getAttribute && element.getAttribute(attribute.key)==attribute.value) {
						attributesFound++;
					}
				});
				if (attributesFound==attributesCount) {
					result = true;
					return true;
				}
			}
		});
		return result;
	},

		// Create a new DOM element:
	createNewDomElement: function(addTag) {
		var element = document.createElement(addTag.name);
		if (addTag.attributes) {
			$H(addTag.attributes).each(function(attribute) {
				element[attribute.key] = attribute.value;
			});
		}
		return element;
	},

	changeSorting: function(objectId, direction) {
		var objectName = this.prependFormFieldNames+this.parseObjectId('parts', objectId, 3, 2, true);
		var objectPrefix = this.parseObjectId('full', objectId, 0, 1);
		var formObj = document.getElementsByName(objectName);

		if (formObj.length) {
				// the uid of the calling object (last part in objectId)
			var callingUid = this.parseObjectId('none', objectId, 1);
			var records = formObj[0].value.split(',');
			var current = records.indexOf(callingUid);
			var changed = false;

				// move up
			if (direction > 0 && current > 0) {
				records[current] = records[current-1];
				records[current-1] = callingUid;
				changed = true;

				// move down
			} else if (direction < 0 && current < records.length-1) {
				records[current] = records[current+1];
				records[current+1] = callingUid;
				changed = true;
			}

			if (changed) {
				formObj[0].value = records.join(',');
				var cAdj = direction > 0 ? 1 : 0; // adjustment
				$(objectId+'_div').parentNode.insertBefore(
					$(objectPrefix + this.structureSeparator + records[current-cAdj] + '_div'),
					$(objectPrefix + this.structureSeparator + records[current+1-cAdj] + '_div')
				);
				this.redrawSortingButtons(objectPrefix, records);
			}
		}

		return false;
	},

	dragAndDropSorting: function(element) {
		require(['jquery'], function($) {
			var objectId = element.getAttribute('id').replace(/_records$/, '');
			var objectName = inline.prependFormFieldNames+inline.parseObjectId('parts', objectId, 3, 0, true);
			var formObj = document.getElementsByName(objectName);
			var $element = $( element );

			if (formObj.length) {
				var checked = new Array();
				var order = [];
				$element.find('.sortableHandle').each(function(i,e) {
					order.push($(e).data('id').toString());
				});
				var records = formObj[0].value.split(',');

					// check if ordered uid is really part of the records
					// virtually deleted items might still be there but ordering shouldn't saved at all on them
				for (var i=0; i<order.length; i++) {
					if (records.indexOf(order[i]) != -1) {
						checked.push(order[i]);
					}
				}

				formObj[0].value = checked.join(',');

				if (inline.data.config && inline.data.config[objectId]) {
					var table = inline.data.config[objectId].table;
					inline.redrawSortingButtons(objectId + inline.structureSeparator + table, checked);
				}
			}
		});
	},

	createDragAndDropSorting: function(objectId) {
		require(['jquery','jquery-ui/jquery-ui-1.10.4.custom.min'], function($) {
			var $sortingContainer = $('#' + inline.escapeObjectId(objectId));

			if ($sortingContainer.hasClass('ui-sortable')) {
				$sortingContainer.sortable('enable');
				return;
			}

			$sortingContainer.addClass('t3-form-field-container-wrap');
			$sortingContainer.sortable(
				{
					containment: 'body',
					handle: '.sortableHandle',
					zIndex: '4000',
					axis: 'y',
					tolerance: 'pointer',
					stop: function() {
						inline.dragAndDropSorting($sortingContainer[0]);
					}
				}
			);
		});
	},

	destroyDragAndDropSorting: function(objectId) {
		require(['jquery','jquery-ui/jquery-ui-1.10.4.custom.min'], function($) {
			var $sortingContainer = $('#' + inline.escapeObjectId(objectId));
			if (!$sortingContainer.hasClass('ui-sortable')) {
				return;
			}
			$('#' + inline.escapeObjectId(objectId)).sortable('disable');
		});
	},

	redrawSortingButtons: function(objectPrefix, records) {
		var i;
		var headerObj;
		var sortingObj = new Array();

			// if no records were passed, fetch them from form field
		if (typeof records == 'undefined') {
			records = new Array();
			var objectName = this.prependFormFieldNames+this.parseObjectId('parts', objectPrefix, 3, 1, true);
			var formObj = document.getElementsByName(objectName);
			if (formObj.length) {
				records = formObj[0].value.split(',');
			}
		}

		for (i=0; i<records.length; i++) {
			if (!records[i].length) {
				continue;
			}

			headerObj = $(objectPrefix + this.structureSeparator + records[i] + '_header');
			sortingObj[0] = Element.select(headerObj, '.sortingUp');
			sortingObj[1] = Element.select(headerObj, '.sortingDown');

			if (sortingObj[0].length) {
				sortingObj[0][0].style.visibility = (i == 0 ? 'hidden' : 'visible');
			}
			if (sortingObj[1].length) {
				sortingObj[1][0].style.visibility = (i == records.length-1 ? 'hidden' : 'visible');
			}
		}
	},

	memorizeAddRecord: function(objectPrefix, newUid, afterUid, selectedValue) {
		if (this.isBelowMax(objectPrefix)) {
			var objectName = this.prependFormFieldNames+this.parseObjectId('parts', objectPrefix, 3, 1, true);
			var formObj = document.getElementsByName(objectName);

			if (formObj.length) {
				var records = new Array();
				if (formObj[0].value.length) records = formObj[0].value.split(',');

				if (afterUid) {
					var newRecords = new Array();
					for (var i=0; i<records.length; i++) {
						if (records[i].length) {
							newRecords.push(records[i]);
						}
						if (afterUid == records[i]) {
							newRecords.push(newUid);
						}
					}
					records = newRecords;
				} else {
					records.push(newUid);
				}
				formObj[0].value = records.join(',');
			}

			this.redrawSortingButtons(objectPrefix, records);

			if (this.data.unique && this.data.unique[objectPrefix]) {
				var unique = this.data.unique[objectPrefix];
				this.setUnique(objectPrefix, newUid, selectedValue);
			}
		}

			// if we reached the maximum off possible records after this action, hide the new buttons
		if (!this.isBelowMax(objectPrefix)) {
			var objectParent = this.parseObjectId('full', objectPrefix, 0 , 1);
			var md5 = this.getObjectMD5(objectParent);
			this.hideElementsWithClassName('.inlineNewButton'+(md5 ? '.'+md5 : ''), objectParent);
		}

		if (TBE_EDITOR) {
			TBE_EDITOR.fieldChanged_fName(objectName, formObj);
		}
	},

	memorizeRemoveRecord: function(objectName, removeUid) {
		var formObj = document.getElementsByName(objectName);
		if (formObj.length) {
			var parts = new Array();
			if (formObj[0].value.length) {
				parts = formObj[0].value.split(',');
				parts = parts.without(removeUid);
				formObj[0].value = parts.join(',');
				if (TBE_EDITOR) {
					TBE_EDITOR.fieldChanged_fName(objectName, formObj);
				}
				return parts.length;
			}
		}
		return false;
	},

	updateUnique: function(srcElement, objectPrefix, formName, recordUid) {
		if (this.data.unique && this.data.unique[objectPrefix]) {
			var unique = this.data.unique[objectPrefix];
			var oldValue = unique.used[recordUid];

			if (unique.selector == 'select') {
				var selector = $(objectPrefix+'_selector');
				this.removeSelectOption(selector, srcElement.value);
				if (typeof oldValue != 'undefined') {
					this.readdSelectOption(selector, oldValue, unique);
				}
			}

			if (!(unique.selector && unique.max == -1)) {
				var formObj = document.getElementsByName(formName);
				if (unique && formObj.length) {
					var records = formObj[0].value.split(',');
					var recordObj;
					for (var i=0; i<records.length; i++) {
						recordObj = document.getElementsByName(this.prependFormFieldNames+'['+unique.table+']['+records[i]+']['+unique.field+']');
						if (recordObj.length && recordObj[0] != srcElement) {
							this.removeSelectOption(recordObj[0], srcElement.value);
							if (typeof oldValue != 'undefined') {
								this.readdSelectOption(recordObj[0], oldValue, unique);
							}
						}
					}
					this.data.unique[objectPrefix].used[recordUid] = srcElement.value;
				}
			}
		}
	},

	revertUnique: function(objectPrefix, elName, recordUid) {
		if (this.data.unique && this.data.unique[objectPrefix]) {
			var unique = this.data.unique[objectPrefix];
			var fieldObj = elName ? document.getElementsByName(elName+'['+unique.field+']') : null;

			if (unique.type == 'select') {
				if (fieldObj && fieldObj.length) {
					delete(this.data.unique[objectPrefix].used[recordUid])

					if (unique.selector == 'select') {
						if (!isNaN(fieldObj[0].value)) {
							var selector = $(objectPrefix+'_selector');
							this.readdSelectOption(selector, fieldObj[0].value, unique);
						}
					}

					if (!(unique.selector && unique.max == -1)) {
						var formName = this.prependFormFieldNames+this.parseObjectId('parts', objectPrefix, 3, 1, true);
						var formObj = document.getElementsByName(formName);
						if (formObj.length) {
							var records = formObj[0].value.split(',');
							var recordObj;
								// walk through all inline records on that level and get the select field
							for (var i=0; i<records.length; i++) {
								recordObj = document.getElementsByName(this.prependFormFieldNames+'['+unique.table+']['+records[i]+']['+unique.field+']');
								if (recordObj.length) {
									this.readdSelectOption(recordObj[0], fieldObj[0].value, unique);
								}
							}
						}
					}
				}
			} else if (unique.type == 'groupdb') {
				// alert(objectPrefix+'/'+recordUid);
				delete(this.data.unique[objectPrefix].used[recordUid])
			}
		}
	},

	enableDisableRecord: function(objectId) {
		var elName = this.parseObjectId('full', objectId, 2, 0, true) + '[hidden]';
		var formObj = document.getElementsByName(elName + '_0');
		var valueObj = document.getElementsByName(elName);
		var icon = $(objectId + '_disabled');

		var $container = TYPO3.jQuery('#' + inline.escapeObjectId(objectId) + '_div');

			// It might be the case that there's no hidden field
		if (formObj.length && valueObj.length) {
			formObj[0].click();
			valueObj[0].value = formObj[0].checked ? 1 : 0;
			TBE_EDITOR.fieldChanged_fName(elName, elName);
		}

		if (icon) {
			if (icon.hasClassName('t3-icon-edit-hide')) {
				icon.removeClassName ('t3-icon-edit-hide');
				icon.addClassName('t3-icon-edit-unhide');
				$container.addClass('t3-form-field-container-inline-hidden');
			} else {
				icon.removeClassName ('t3-icon-edit-unhide');
				icon.addClassName('t3-icon-edit-hide');
				$container.removeClass('t3-form-field-container-inline-hidden');
			}
		}

		return false;
	},

	deleteRecord: function(objectId, options) {
		var i, j, inlineRecords, records, childObjectId, childTable;
		var objectPrefix = this.parseObjectId('full', objectId, 0 , 1);
		var elName = this.parseObjectId('full', objectId, 2, 0, true);
		var shortName = this.parseObjectId('parts', objectId, 2, 0, true);
		var recordUid = this.parseObjectId('none', objectId, 1);
		var beforeDeleteIsBelowMax = this.isBelowMax(objectPrefix);

			// revert the unique settings if available
		this.revertUnique(objectPrefix, elName, recordUid);

			// Remove from TBE_EDITOR (required fields, required range, etc.):
		if (TBE_EDITOR && TBE_EDITOR.removeElement) {
			var removeStack = [];
			// Iterate over all child records:
			inlineRecords = Element.select(objectId+'_div', '.inlineRecord');
				// Remove nested child records from TBE_EDITOR required/range checks:
			for (i=inlineRecords.length-1; i>=0; i--) {
				if (inlineRecords[i].value.length) {
					records = inlineRecords[i].value.split(',');
					childObjectId = this.data.map[inlineRecords[i].name];
					childTable = this.data.config[childObjectId].table;
					for (j=records.length-1; j>=0; j--) {
						removeStack.push(this.prependFormFieldNames+'['+childTable+']['+records[j]+']');
					}
				}
			}
			removeStack.push(this.prependFormFieldNames+shortName);
			TBE_EDITOR.removeElementArray(removeStack);
		}

			// Mark this container as deleted
		var deletedRecordContainer = $(objectId + '_div');
		if (deletedRecordContainer) {
			deletedRecordContainer.addClassName('inlineIsDeletedRecord');
		}

			// If the record is new and was never saved before, just remove it from DOM:
		if (this.isNewRecord(objectId) || options && options.forceDirectRemoval) {
			this.fadeAndRemove(objectId+'_div');
			// If the record already exists in storage, mark it to be deleted on clicking the save button:
		} else {
			document.getElementsByName('cmd'+shortName+'[delete]')[0].disabled = false;
			new Effect.Fade(objectId+'_div');
		}

		var recordCount = this.memorizeRemoveRecord(
			this.prependFormFieldNames+this.parseObjectId('parts', objectId, 3, 2, true),
			recordUid
		);

		if (recordCount <= 1) {
			this.destroyDragAndDropSorting(this.parseObjectId('full', objectId, 0 , 2)+'_records');
		}
		this.redrawSortingButtons(objectPrefix);

			// if the NEW-button was hidden and now we can add again new children, show the button
		if (!beforeDeleteIsBelowMax && this.isBelowMax(objectPrefix)) {
			var objectParent = this.parseObjectId('full', objectPrefix, 0 , 1);
			var md5 = this.getObjectMD5(objectParent);
			this.showElementsWithClassName('.inlineNewButton'+(md5 ? '.'+md5 : ''), objectParent);
		}
		return false;
	},

	parsePath: function(path) {
		var backSlash = path.lastIndexOf('\\');
		var normalSlash = path.lastIndexOf('/');

		if (backSlash > 0) {
			path = path.substring(0,backSlash+1);
		} else if (normalSlash > 0) {
			path = path.substring(0,normalSlash+1);
		} else {
			path = '';
		}

		return path;
	},

	parseFormElementName: function(wrap, formElementName, rightCount, skipRight) {
		var idParts = this.splitFormElementName(formElementName);

		if (!wrap) {
			wrap = 'full';
		}
		if (!skipRight) {
			skipRight = 0;
		}

		var elParts = new Array();
		for (var i=0; i<skipRight; i++) {
			idParts.pop();
		}

		if (rightCount > 0) {
			for (var i=0; i<rightCount; i++) {
				elParts.unshift(idParts.pop());
			}
		} else {
			for (var i=0; i<-rightCount; i++) {
				idParts.shift();
			}
			elParts = idParts;
		}

		var elReturn = this.constructFormElementName(wrap, elParts);

		return elReturn;
	},

	splitFormElementName: function(formElementName) {
		// remove left and right side "data[...|...]" -> '...|...'
		formElementName = formElementName.substr(0, formElementName.lastIndexOf(']')).substr(formElementName.indexOf('[')+1);
		var parts = objectId.split('][');

		return parts;
	},

	splitObjectId: function(objectId) {
		objectId = objectId.substr(objectId.indexOf(this.structureSeparator)+1);
		objectId = objectId.split(this.flexFormSeparator).join(this.flexFormSubstitute);
		var parts = objectId.split(this.structureSeparator);

		return parts;
	},

	constructFormElementName: function(wrap, parts) {
		var elReturn;

		if (wrap == 'full') {
			elReturn = this.prependFormFieldNames+'['+parts.join('][')+']';
			elReturn = elReturn.split(this.flexFormSubstitute).join('][');
		} else if (wrap == 'parts') {
			elReturn = '['+parts.join('][')+']';
			elReturn = elReturn.split(this.flexFormSubstitute).join('][');
		} else if (wrap == 'none') {
			elReturn = parts.length > 1 ? parts : parts.join('');
		}

		return elReturn;
	},

	constructObjectId: function(wrap, parts) {
		var elReturn;

		if (wrap == 'full') {
			elReturn = this.prependFormFieldNames+this.structureSeparator+parts.join(this.structureSeparator);
			elReturn = elReturn.split(this.flexFormSubstitute).join(this.flexFormSeparator);
		} else if (wrap == 'parts') {
			elReturn = this.structureSeparator+parts.join(this.structureSeparator);
			elReturn = elReturn.split(this.flexFormSubstitute).join(this.flexFormSeparator);
		} else if (wrap == 'none') {
			elReturn = parts.length > 1 ? parts : parts.join('');
		}

		return elReturn;
	},

	parseObjectId: function(wrap, objectId, rightCount, skipRight, returnAsFormElementName) {
		var idParts = this.splitObjectId(objectId);

		if (!wrap) {
			wrap = 'full';
		}
		if (!skipRight) {
			skipRight = 0;
		}

		var elParts = new Array();
		for (var i=0; i<skipRight; i++) {
			idParts.pop();
		}

		if (rightCount > 0) {
			for (var i=0; i<rightCount; i++) {
				elParts.unshift(idParts.pop());
			}
		} else {
			for (var i=0; i<-rightCount; i++) {
				idParts.shift();
			}
			elParts = idParts;
		}

		if (returnAsFormElementName) {
			var elReturn = this.constructFormElementName(wrap, elParts);
		} else {
			var elReturn = this.constructObjectId(wrap, elParts);
		}

		return elReturn;
	},

	handleChangedField: function(formField, objectId) {
		var formObj;
		if (typeof formField == 'object') {
			formObj = formField;
		} else {
			formObj = document.getElementsByName(formField);
			if (formObj.length) {
				formObj = formObj[0];
			}
		}

		if (formObj != undefined) {
			var value;
			if (formObj.nodeName == 'SELECT') {
				value = formObj.options[formObj.selectedIndex].text;
			} else {
				value = formObj.value;
			}
			$(objectId+'_label').innerHTML = value.length ? value : this.noTitleString;
		}
		return true;
	},

	arrayAssocCount: function(object) {
		var count = 0;
		if (typeof object.length != 'undefined') {
			count = object.length;
		} else {
			for (var i in object) {
				count++;
			}
		}
		return count;
	},

	isBelowMax: function(objectPrefix) {
		var isBelowMax = true;
		var objectName = this.prependFormFieldNames+this.parseObjectId('parts', objectPrefix, 3, 1, true);
		var formObj = document.getElementsByName(objectName);

		if (this.data.config && this.data.config[objectPrefix] && formObj.length) {
			var recordCount = formObj[0].value ? formObj[0].value.split(',').length : 0;
			if (recordCount >= this.data.config[objectPrefix].max) {
				isBelowMax = false;
			}
		}
		if (isBelowMax && this.data.unique && this.data.unique[objectPrefix]) {
			var unique = this.data.unique[objectPrefix];
			if (this.arrayAssocCount(unique.used) >= unique.max && unique.max >= 0) {
				isBelowMax = false;
			}
		}
		return isBelowMax;
	},

	getOptionsHash: function(selectObj) {
		var optionsHash = {};
		for (var i=0; i<selectObj.options.length; i++) {
			optionsHash[selectObj.options[i].value] = i;
		}
		return optionsHash;
	},

	removeSelectOption: function(selectObj, value) {
		var optionsHash = this.getOptionsHash(selectObj);
		if (optionsHash[value] != undefined) {
			selectObj.options[optionsHash[value]] = null;
		}
	},

	readdSelectOption: function(selectObj, value, unique) {
		var index = null;
		var optionsHash = this.getOptionsHash(selectObj);
		var possibleValues = $H(unique.possible).keys();

		for (var possibleValue in unique.possible) {
			if (possibleValue == value) {
				break;
			}
			if (optionsHash[possibleValue] != undefined) {
				index = optionsHash[possibleValue];
			}
		}

		if (index == null) {
			index = 0;
		} else if (index < selectObj.options.length) {
			index++;
		}
			// recreate the <option> tag
		var readdOption = document.createElement('option');
		readdOption.text = unique.possible[value];
		readdOption.value = value;
			// add the <option> at the right position
		selectObj.add(readdOption, document.all ? index : selectObj.options[index]);
	},

	hideElementsWithClassName: function(selector, parentElement) {
		this.setVisibilityOfElementsWithClassName('hide', selector, parentElement);
	},

	showElementsWithClassName: function(selector, parentElement) {
		this.setVisibilityOfElementsWithClassName('show', selector, parentElement);
	},

	setVisibilityOfElementsWithClassName: function(action, selector, parentElement) {
		var domObjects = Selector.findChildElements($(parentElement), [selector]);
		if (action == 'hide') {
			$A(domObjects).each(function(domObject) { new Effect.Fade(domObject); });
		} else if (action == 'show') {
			$A(domObjects).each(function(domObject) { new Effect.Appear(domObject); });
		}
	},

	fadeOutFadeIn: function(objectId) {
		var optIn = { duration:0.5, transition:Effect.Transitions.linear, from:0.50, to:1.00 };
		var optOut = { duration:0.5, transition:Effect.Transitions.linear, from:1.00, to:0.50 };
		optOut.afterFinish = function() {
			new Effect.Opacity(objectId, optIn);
		};
		new Effect.Opacity(objectId, optOut);
	},

	isNewRecord: function(objectId) {
		return $(objectId+'_div') && $(objectId+'_div').hasClassName('inlineIsNewRecord')
			? true
			: false;
	},

		// Find and fix nested of inline and tab levels if a new element was created dynamically (it doesn't know about its nesting):
	findContinuedNestedLevel: function(nested, objectId) {
		if (this.data.nested && this.data.nested[objectId]) {
				// Remove the first element from the new nested stack, it's just a hint:
			nested.shift();
			nested = this.data.nested[objectId].concat(nested);
			return nested;
		} else {
			return nested;
		}
	},

	getNumberOfRTE: function() {
		var number = 0;
		if (typeof RTEarea != 'undefined' && RTEarea.length > 0) {
			number = RTEarea.length-1;
		}
		return number;
  	},

  	getObjectMD5: function(objectPrefix) {
  		var md5 = false;
  		if (this.data.config && this.data.config[objectPrefix] && this.data.config[objectPrefix].md5) {
  			md5 = this.data.config[objectPrefix].md5;
  		}
  		return md5
  	},

  	fadeAndRemove: function(element) {
  		if ($(element)) {
			new Effect.Fade(element, { afterFinish: function() { Element.remove(element); }	});
		}
  	},

	getContext: function(objectId) {
		var result = null;

		if (objectId !== '' && typeof this.data.config[objectId] !== 'undefined' && typeof this.data.config[objectId].context !== 'undefined') {
			result = this.data.config[objectId].context;
		}

		return result;
	},

	/**
	 * Escapes object identifiers to be used in jQuery.
	 *
	 * @param string objectId
	 * @return string
	 */
	escapeObjectId: function(objectId) {
		var escapedObjectId;
		escapedObjectId = objectId.replace(/:/g, '\\:');
		escapedObjectId = escapedObjectId.replace(/\./g, '\\.');
		return escapedObjectId;
	},

	/**
	 * Escapes object identifiers to be used as jQuery selector.
	 *
	 * @param string objectId
	 * @return string
	 */
	escapeSelectorObjectId: function(objectId) {
		var escapedSelectorObjectId;
		var escapedObjectId = this.escapeObjectId(objectId);
		escapedSelectorObjectId = escapedObjectId.replace(/\\:/g, '\\\\\\:');
		escapedSelectorObjectId = escapedSelectorObjectId.replace(/\\\./g, '\\\\\\.');
		return escapedSelectorObjectId;
	}
};

Object.extend(Array.prototype, {
	diff: function(current) {
		var diff = new Array();
		if (this.length == current.length) {
			for (var i=0; i<this.length; i++) {
				if (this[i] !== current[i]) diff.push(i);
			}
		}
		return diff;
	}
});

/*]]>*/
(function($) {
	$(function() {
		$(document).delegate('div.t3-form-field-header-inline', 'click', inline.toggleEvent);
	});
})(TYPO3.jQuery);

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Class for JS handling of suggest fields in TCEforms.
 *
 * @author  Andreas Wolf <andreas.wolf@ikt-werk.de>
 * @author  Benni Mack <benni@typo3.org>
 */
if (!TCEForms) {
	var TCEForms = {};
}

TCEForms.Suggest = Class.create({
	objectId: '',
	suggestField: '',
	suggestResultList: '',
	minimumCharacters: 2,
	defaultValue: '',
	fieldType: '',

	/**
	 * Wrapper for script.aculo.us' Autocompleter functionality: Assigns a new autocompletion object to
	 * the input field of a given Suggest selector.
	 *
	 * @param  string  objectId  The ID of the object to assign the completer to
	 * @param  string  table     The table of the record which is currently edited
	 * @param  string  field     The field which is currently edited
	 * @param  integer uid       The uid of the record which is currently edited
	 * @param  integer pid       The pid of the record which is currently edited
	 * @param  integer minimumCharacters the minimum characters that is need to trigger the initial search
	 * @param  string  fieldType The TCA type of the field (e.g. group, select, ...)
	 * @param string newRecordRow JSON encoded new content element. Only set when new record is inside flexform
	 */
	initialize: function(objectId, table, field, uid, pid, minimumCharacters, fieldType, newRecordRow) {
		var PATH_typo3 = top.TS.PATH_typo3 || window.opener.top.TS.PATH_typo3;
		this.objectId = objectId;
		this.suggestField = objectId + 'Suggest';
		this.suggestResultList = objectId + 'SuggestChoices';
		this.fieldType = fieldType;

		new Ajax.Autocompleter(this.suggestField, this.suggestResultList, PATH_typo3 + TYPO3.settings.ajaxUrls['t3lib_TCEforms_suggest::searchRecord'], {
				paramName: 'value',
				minChars: (minimumCharacters ? minimumCharacters : this.minimumCharacters),
				updateElement: this.addElementToList.bind(this),
				parameters: 'table=' + table + '&field=' + field + '&uid=' + uid + '&pid=' + pid + '&newRecordRow=' + newRecordRow,
				indicator: objectId + 'SuggestIndicator'
			}
		);

		$(this.suggestField).observe('focus', this.checkDefaultValue.bind(this));
		$(this.suggestField).observe('keydown', this.checkDefaultValue.bind(this));
	},

	/**
	 * check for default value of the input field and set it to empty once somebody wants to type something in
	 */
	checkDefaultValue: function() {
		if ($(this.suggestField).value == this.defaultValue) {
			$(this.suggestField).value = '';
		}
	},

	/**
	 * Adds an element to the list of items in a group selector.
	 *
	 * @param  object  item  The item to add to the list (usually an li element)
	 * @return void
	 */
	addElementToList: function(item) {
		if (item.className.indexOf('noresults') == -1) {
			var arr = item.id.split('-');
			var ins_table = arr[0];
			var ins_uid = arr[1];
			var ins_uid_string = (this.fieldType == 'select') ? ins_uid : (ins_table + '_' + ins_uid);
			var rec_table = arr[2];
			var rec_uid = arr[3];
			var rec_field = arr[4];

			var formEl = this.objectId;

			var suggestLabelNode = Element.select(item, '.suggest-label')[0];
			var label = suggestLabelNode.textContent ? suggestLabelNode.textContent : suggestLabelNode.innerText;
			var suggestLabelTitleNode = Element.select(suggestLabelNode, '[title]')[0];
			var title = suggestLabelTitleNode ? suggestLabelTitleNode.readAttribute('title') : '';

			setFormValueFromBrowseWin(formEl, ins_uid_string, label, title);
			TBE_EDITOR.fieldChanged(rec_table, rec_uid, rec_field, formEl);

			$(this.suggestField).value = this.defaultValue;
		}
	}
});

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Class for JS handling of selectbox filter in TCEforms.
 *
 * @author  Marc Bastian Heinrichs <mbh@mbh-software.de>
 */

if (!TCEForms) {
	var TCEForms = {};
}

TCEForms.SelectBoxFilter = Class.create({
	selectBox: '',
	selectBoxOriginal: '',
	selectBoxOriginalOptionsLength: 0,
	filterTextfield: false,
	filterDropDown: false,
	delayObject: '',

	/**
	 * Assigns a new filter object to the available items select box object.
	 *
	 * @param selectBoxId  The ID of the object to assign the filter to
	 */
	initialize: function(selectBoxId) {

		this.selectBox = $(selectBoxId);
		this.selectBoxOriginal = this.selectBox.cloneNode(true);
		this.selectBoxOriginalOptionsLength = this.selectBoxOriginal.options.length;

		if ($(selectBoxId + '_filtertextfield') != undefined) {
			this.filterTextfield = $(selectBoxId + '_filtertextfield');
		}
		if ($(selectBoxId + '_filterdropdown') != undefined) {
			this.filterDropDown = $(selectBoxId + '_filterdropdown');
		}

		// setting
		if (this.filterTextfield) {
			this.filterTextfield.observe('keyup', function(event) {
				this.delayObject = this.updateSelectOptions.bindAsEventListener(this).delay(0.5);
			}.bindAsEventListener(this));

			this.filterTextfield.observe('keydown', function(event) {
			if (this.delayObject != undefined)
				window.clearTimeout(this.delayId);
			}.bindAsEventListener(this));
		}

		if (this.filterDropDown) {
			this.filterDropDown.observe('change', function(event) {
				this.updateSelectOptions();
			}.bindAsEventListener(this));
		}
	},

	/**
	 * Updates the available items select box based the filter textfield or filter drop-down
	 */
	updateSelectOptions: function() {

		var filterTextFromTextfield = '';
		var filterTextFromDropDown = '';

		if (this.filterTextfield) {
			filterTextFromTextfield = this.filterTextfield.getValue();
		}

		if (this.filterDropDown) {
			filterTextFromDropDown = this.filterDropDown.getValue();
		}

		this.selectBox.innerHTML = '';

		if (filterTextFromTextfield.length > 0 || filterTextFromDropDown.length > 0) {
			var matchStringTextfield = new RegExp(filterTextFromTextfield, 'i');
			var matchStringDropDown = new RegExp(filterTextFromDropDown, 'i');
			for (var i = 0; i < this.selectBoxOriginalOptionsLength; i++) {
				if (this.selectBoxOriginal.options[i].firstChild.nodeValue.match(matchStringTextfield) != null &&
					this.selectBoxOriginal.options[i].firstChild.nodeValue.match(matchStringDropDown) != null) {
					var tempNode = this.selectBoxOriginal.options[i].cloneNode(true);
					this.selectBox.appendChild(tempNode);
				}
			}
		} else {
			// recopy original list
			for (var i = 0; i < this.selectBoxOriginalOptionsLength; i++) {
				var tempNode = this.selectBoxOriginal.options[i].cloneNode(true);
				this.selectBox.appendChild(tempNode);
			}
		}
	}
});


/*
 * This code has been copied from Project_CMS
 * Copyright (c) 2005 by Phillip Berndt (www.pberndt.com)
 *
 * Extended Textarea for IE and Firefox browsers
 * Features:
 *  - Possibility to place tabs in <textarea> elements using a simply <TAB> key
 *  - Auto-indenting of new lines
 *
 * License: GNU General Public License
 */

function checkBrowser() {
	browserName = navigator.appName;
	browserVer = parseInt(navigator.appVersion);

	ok = false;
	if (browserName == "Microsoft Internet Explorer" && browserVer >= 4) {
		ok = true;
	} else if (browserName == "Netscape" && browserVer >= 3) {
		ok = true;
	}

	return ok;
}

	// Automatically change all textarea elements
function changeTextareaElements() {
	if (!checkBrowser()) {
			// Stop unless we're using IE or Netscape (includes Mozilla family)
		return false;
	}

	document.textAreas = document.getElementsByTagName("textarea");

	for (i = 0; i < document.textAreas.length; i++) {
			// Only change if the class parameter contains "enable-tab"
		if (document.textAreas[i].className && document.textAreas[i].className.search(/(^| )enable-tab( |$)/) != -1) {
			document.textAreas[i].textAreaID = i;
			makeAdvancedTextArea(document.textAreas[i]);
		}
	}
}

	// Wait until the document is completely loaded.
	// Set a timeout instead of using the onLoad() event because it could be used by something else already.
window.setTimeout("changeTextareaElements();", 200);

	// Turn textarea elements into "better" ones. Actually this is just adding some lines of JavaScript...
function makeAdvancedTextArea(textArea) {
	if (textArea.tagName.toLowerCase() != "textarea") {
		return false;
	}

		// On attempt to leave the element:
		// Do not leave if this.dontLeave is true
	textArea.onblur = function(e) {
		if (!this.dontLeave) {
			return;
		}
		this.dontLeave = null;
		window.setTimeout("document.textAreas[" + this.textAreaID + "].restoreFocus()", 1);
		return false;
	}

		// Set the focus back to the element and move the cursor to the correct position.
	textArea.restoreFocus = function() {
		this.focus();

		if (this.caretPos) {
			this.caretPos.collapse(false);
			this.caretPos.select();
		}
	}

		// Determine the current cursor position
	textArea.getCursorPos = function() {
		if (this.selectionStart) {
			currPos = this.selectionStart;
		} else if (this.caretPos) {	// This is very tricky in IE :-(
			oldText = this.caretPos.text;
			finder = "--getcurrpos" + Math.round(Math.random() * 10000) + "--";
			this.caretPos.text += finder;
			currPos = this.value.indexOf(finder);

			this.caretPos.moveStart('character', -finder.length);
			this.caretPos.text = "";

			this.caretPos.scrollIntoView(true);
		} else {
			return;
		}

		return currPos;
	}

		// On tab, insert a tabulator. Otherwise, check if a slash (/) was pressed.
	textArea.onkeydown = function(e) {
		if (this.selectionStart == null &! this.createTextRange) {
			return;
		}
		if (!e) {
			e = window.event;
		}

			// Tabulator
		if (e.keyCode == 9) {
			this.dontLeave = true;
			this.textInsert(String.fromCharCode(9));
		}

			// Newline
		if (e.keyCode == 13) {
				// Get the cursor position
			currPos = this.getCursorPos();

				// Search the last line
			lastLine = "";
			for (i = currPos - 1; i >= 0; i--) {
				if(this.value.substring(i, i + 1) == '\n') {
					break;
				}
			}
			lastLine = this.value.substring(i + 1, currPos);

				// Search for whitespaces in the current line
			whiteSpace = "";
			for (i = 0; i < lastLine.length; i++) {
				if (lastLine.substring(i, i + 1) == '\t') {
					whiteSpace += "\t";
				} else if (lastLine.substring(i, i + 1) == ' ') {
					whiteSpace += " ";
				} else {
					break;
				}
			}

				// Another ugly IE hack
			if (navigator.appVersion.match(/MSIE/)) {
				whiteSpace = "\\n" + whiteSpace;
			}

				// Insert whitespaces
			window.setTimeout("document.textAreas["+this.textAreaID+"].textInsert(\""+whiteSpace+"\");", 1);
		}
	}

		// Save the current cursor position in IE
	textArea.onkeyup = textArea.onclick = textArea.onselect = function(e) {
		if (this.createTextRange) {
			this.caretPos = document.selection.createRange().duplicate();
		}
	}

		// Insert text at the current cursor position
	textArea.textInsert = function(insertText) {
		if (this.selectionStart != null) {
			var savedScrollTop = this.scrollTop;
			var begin = this.selectionStart;
			var end = this.selectionEnd;
			if (end > begin + 1) {
				this.value = this.value.substr(0, begin) + insertText + this.value.substr(end);
			} else {
				this.value = this.value.substr(0, begin) + insertText + this.value.substr(begin);
			}

			this.selectionStart = begin + insertText.length;
			this.selectionEnd = begin + insertText.length;
			this.scrollTop = savedScrollTop;
		} else if (this.caretPos) {
			this.caretPos.text = insertText;
			this.caretPos.scrollIntoView(true);
		} else {
			text.value += insertText;
		}

		this.focus();
	}
}
/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

Ext.ns('TYPO3', 'TYPO3.CSH.ExtDirect');

/**
 * Class to show tooltips for links that have the css t3-help-link
 * need the tags data-table and data-field (HTML5)
 */


TYPO3.ContextHelp = function() {

	/**
	 * Cache for CSH
	 * @type {Ext.util.MixedCollection}
	 */
	var cshHelp = new Ext.util.MixedCollection(true),
	tip;

	/**
	 * Shows the tooltip, triggered from mouseover event handler
	 *
	 */
	function showToolTipHelp() {
		var link = tip.triggerElement;
		if (!link) {
			return false;
		}
		var table = link.getAttribute('data-table');
		var field = link.getAttribute('data-field');
		var key = table + '.' + field;
		var response = cshHelp.key(key);
		tip.target = tip.triggerElement;
		if (response) {
			updateTip(response);
		} else {
				// If a table is defined, use ExtDirect call to get the tooltip's content
			if (table) {
				var description = '';
				if (typeof(top.TYPO3.LLL) !== 'undefined') {
					description = top.TYPO3.LLL.core.csh_tooltip_loading;
				} else if (opener && typeof(opener.top.TYPO3.LLL) !== 'undefined') {
					description = opener.top.TYPO3.LLL.core.csh_tooltip_loading;
				}

					// Clear old tooltip contents
				updateTip({
					description: description,
					cshLink: '',
					moreInfo: '',
					title: ''
				});
					// Load content
				TYPO3.CSH.ExtDirect.getTableContextHelp(table, function(response, options) {
					Ext.iterate(response, function(key, value){
						cshHelp.add(value);
						if (key === field) {
							updateTip(value);
								// Need to re-position because the height may have increased
							tip.show();
						}
					});
				}, this);

				// No table was given, use directly title and description
			} else {
				updateTip({
					description: link.getAttribute('data-description'),
					cshLink: '',
					moreInfo: '',
					title: link.getAttribute('data-title')
				});
			}
		}
	}

	/**
	 * Update tooltip message
	 *
	 * @param {Object} response
	 */
	function updateTip(response) {
		tip.body.dom.innerHTML = response.description;
		tip.cshLink = response.id;
		tip.moreInfo = response.moreInfo;
		if (tip.moreInfo) {
			tip.addClass('tipIsLinked');
		}
		tip.setTitle(response.title);
		tip.doAutoWidth();
	}

	return {
		/**
		 * Constructor
		 */
		init: function() {
			tip = new Ext.ToolTip({
				title: 'CSH', // needs a title for init because of the markup
				html: '',
					// The tooltip will appear above the label, if viewport allows
				anchor: 'bottom',
				minWidth: 160,
				maxWidth: 240,
				target: Ext.getBody(),
				delegate: 'span.t3-help-link',
				renderTo: Ext.getBody(),
				cls: 'typo3-csh-tooltip',
				shadow: false,
				dismissDelay: 0, // tooltip stays while mouse is over target
				autoHide: true,
				showDelay: 1000, // show after 1 second
				hideDelay: 300, // hide after 0.3 seconds
				closable: true,
				isMouseOver: false,
				listeners: {
					beforeshow: showToolTipHelp,
					render: function(tip) {
						tip.body.on({
							'click': {
								fn: function(event) {
									event.stopEvent();
									if (tip.moreInfo) {
										try {
											top.TYPO3.ContextHelpWindow.open(tip.cshLink);
										} catch(e) {
											// do nothing
										}
									}
									tip.hide();
								}
							}
						});
						tip.el.on({
							'mouseover': {
								fn: function() {
									if (tip.moreInfo) {
										tip.isMouseOver = true;
									}
								}
							},
							'mouseout': {
								fn: function() {
									if (tip.moreInfo) {
										tip.isMouseOver = false;
										tip.hide.defer(tip.hideDelay, tip, []);
									}
								}
							}
						});
					},
					hide: function(tip) {
						tip.setTitle('');
						tip.body.dom.innerHTML = '';
					},
					beforehide: function(tip) {
						return !tip.isMouseOver;
					},
					scope: this
				}
			});

			Ext.getBody().on({
				'keydown': {
					fn: function() {
						tip.hide();
					}
				},
				'click': {
					fn: function() {
						tip.hide();
					}
				}
			});

			/**
			 * Adds a sequence to Ext.TooltTip::showAt so as to increase vertical offset when anchor position is 'bottom'
			 * This positions the tip box closer to the target element when the anchor is on the bottom side of the box
			 * When anchor position is 'top' or 'bottom', the anchor is pushed slightly to the left in order to align with the help icon, if any
			 *
			 */
			Ext.ToolTip.prototype.showAt = Ext.ToolTip.prototype.showAt.createSequence(
				function() {
					var ap = this.getAnchorPosition().charAt(0);
					if (this.anchorToTarget && !this.trackMouse) {
						switch (ap) {
							case 'b':
								var xy = this.getPosition();
								this.setPagePosition(xy[0]-10, xy[1]+5);
								break;
							case 't':
								var xy = this.getPosition();
								this.setPagePosition(xy[0]-10, xy[1]);
								break;
						}
					}
				}
			);

		},

		/**
		 * Opens the help window, triggered from click event handler
		 *
		 * @param {Event} event
		 * @param {Node} link
		 */
		openHelpWindow: function(event, link) {
			var id = link.getAttribute('data-table') + '.' + link.getAttribute('data-field');
			event.stopEvent();
			top.TYPO3.ContextHelpWindow.open(id);
		}
	}
}();

/**
 * Calls the init on Ext.onReady
 */
Ext.onReady(TYPO3.ContextHelp.init, TYPO3.ContextHelp);

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Flashmessage rendered by ExtJS
 *
 *
 * @author Steffen Kamper <info@sk-typo3.de>
 */
Ext.ns('TYPO3');

/**
 * Object for named severities
 */
TYPO3.Severity = {
	notice: 0,
	information: 1,
	ok: 2,
	warning: 3,
	error: 4
};

/**
 * @class TYPO3.Flashmessage
 * Passive popup box singleton
 * @singleton
 *
 * Example (Information message):
 * TYPO3.Flashmessage.display(1, 'TYPO3 Backend - Version 4.4', 'Ready for take off', 3);
 */
TYPO3.Flashmessage = function() {
	var messageContainer;
	var severities = ['notice', 'information', 'ok', 'warning', 'error'];

	function createBox(severity, title, message) {
		return ['<div class="typo3-message message-', severity, '" style="width: 400px">',
				'<div class="t3-icon t3-icon-actions t3-icon-actions-message t3-icon-actions-message-close t3-icon-message-' + severity + '-close"></div>',
				'<div class="header-container">',
				'<div class="message-header">', title, '</div>',
				'</div>',
				'<div class="message-body">', message, '</div>',
				'</div>'].join('');
	}

	return {
		/**
		 * Shows popup
		 * @member TYPO3.Flashmessage
		 * @param int severity (0=notice, 1=information, 2=ok, 3=warning, 4=error)
		 * @param string title
		 * @param string message
		 * @param float duration in sec (default 5)
		 */
		display : function(severity, title, message, duration) {
			duration = duration || 5;
			if (!messageContainer) {
				messageContainer = Ext.DomHelper.insertFirst(document.body, {
					id   : 'msg-div',
					style: 'position:absolute;z-index:10000'
				}, true);
			}

			var box = Ext.DomHelper.append(messageContainer, {
				html: createBox(severities[severity], title, message)
			}, true);
			messageContainer.alignTo(document, 't-t');
			box.child('.t3-icon-actions-message-close').on('click',	function (e, t, o) {
				var node;
				node = new Ext.Element(Ext.get(t).findParent('div.typo3-message'));
				node.hide();
				Ext.removeNode(node.dom);
			}, box);
			box.slideIn('t').pause(duration).ghost('t', {remove: true});
		}
	};
}();
