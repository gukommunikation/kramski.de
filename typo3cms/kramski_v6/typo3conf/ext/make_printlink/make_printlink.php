<?php
/***************************************************************
* Copyright notice
*
* (c) 2003 Jens Ellerbrock <je@hades.org>
*     javascript stuff by Dominic Brander <dbrander@snowflake.ch>
* All rights reserved
*
* This script is part of the Typo3 project. The Typo3 project is
* free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* The GNU General Public License can be found at
* http://www.gnu.org/copyleft/gpl.html.
* A copy is found in the textfile GPL.txt and important notices to the license
* from the author is found in LICENSE.txt distributed with these scripts.
*
*
* This script is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
* @authors Jens Ellerbrock <je@hades.org>, Dominic Brander <dbrander@snowflake.ch>
*/

function add_vars2($vars,$path) {
	$res='';
	if(is_array($vars)) {
		reset ($vars);
		while (list ($key, $val) = each ($vars)) {
			if (!is_array($val)) {
				$res .= '&amp;'.$path.'['.rawurlencode($key).']'.'='.rawurlencode($val);
			} else {
	  		$res .= add_vars2($val, $path.'['.rawurlencode($key).']');
			}
		}
	}
	return $res;
}

function add_vars($vars) {
	$res='';
	if(is_array($vars)) {	// mmeyer, 20090522, fehler abgefangen
		reset ($vars);
		while (list ($key, $val) = each ($vars)) {
			if (is_array($val)) {
		  	$res .= add_vars2($val, rawurlencode($key));
			} else {
				if (($key != 'id') && ($key != 'type')) {
					$res .= '&amp;'.rawurlencode($key).'='.rawurlencode($val);
				}
			}
		}
	}
	return $res;
}

function tx_open_printlink($content, $conf) {
  $GLOBALS['TT']->push('open_printlink');
	$conf['type']= ($conf['type']) ? $conf['type'] : 98;

 	$params=add_vars($GLOBALS['HTTP_GET_VARS']);
	if ($conf['include_post_vars']) {
		$params.=add_vars($GLOBALS['HTTP_POST_VARS']);
	}

	$templ=new t3lib_TStemplate;
	$LD=$templ->linkData($GLOBALS['TSFE']->page,''.'','','','',$params,$conf['type']);
	$uri=$LD[totalURL];

	$GLOBALS['TT']->setTSLogMessage( 'link to URI: '.$uri, 0);

	$target = ($conf['target']) ? ' target="'.$conf['target'].'"' : '';
  $atags = $conf['aTagParams'] ? ' ' . $conf['aTagParams'] . ' ' : '';	

	$js = $conf['noBlur'] ? '' : ' onfocus="blurLink(this);"';

	if ($conf['popup']) {
		$conf['windowname'] = ($conf['windowname']) ? $conf['windowname'] : 'print';
		$conf['windowparams'] = ($conf['windowparams']) ? $conf['windowparams'] : 'resizable=yes,toolbar=yes,scrollbars=yes,menubar=yes,width=500,height=500';

		$js .= ' onclick="window.open(\''.$uri.'\',\''.$conf['windowname'].'\',\''.$conf['windowparams'].'\');"';
		$target = '';
		$uri='#';
	}
  $GLOBALS['TT']->pull();
	return '<a href="' . $uri . '" ' . $js . $target . $atags . '>' . $content;
}

function tx_make_printlink($content, $conf){
  $GLOBALS['TT']->push('tx_make_printlink');
	if (!$conf['no_user_int']) {
		$GLOBALS['TT']->setTSLogMessage( 'making USER_INT object', 0);
		$substKey = 'INT_SCRIPT.'.$GLOBALS['TSFE']->uniqueHash();
		$link='<!--'.$substKey.'-->';
		$conf['userFunc'] = 'tx_open_printlink';
		$GLOBALS['TSFE']->config['INTincScript'][$substKey] = array(
			'conf'=>$conf,
			'cObj'=>serialize(new tslib_cObj),
			'type'=>'FUNC'
		);
  } else {
	  $link = tx_open_printlink('',$conf);
	}

  $GLOBALS['TT']->pull();
	return $link . $content . '</a>';
}
?>