page.1000 >
page {
    # Inlude JavaScript files
    includeJSFooterlibs {
        powermailJQuery =
        powermailJQueryUi = 
        powermailJQueryFormValidationLanguage = 
        powermailJQueryFormValidation =
       powermailJQueryTabs =
       powermailJQueryUiDatepicker =
    }
    includeJSFooter {
        powermailForm = 
    }
}
plugin.tx_powermail.settings.setup {
    spamshield {
	_enable = 0
	factor = 75

	indicator {
		linkLimit = 2
		blacklistStringValues = viagra,sex,porno
	}
    }
}

plugin.tx_powermail_pi1._LOCAL_LANG.de {
	email_linklabel = Bitte klicken Sie zum bestätigen auf diesen Link:
	email_text1 = Wir danken Ihnen für das Interesse an unserem kostenlosen Newsletter. Um den Vorgang abzuschließen, müssen Sie das Newsletter-Abonnement noch mit untenstehenden Link bestätigen.
}
plugin.tx_powermail.view.templateRootPath = {$path.libs}/powermail/