tt_content.gridelements_pi1.20.10.setup {    
    # 2-Spalter (50%|50%)
    1 < lib.gridelements.defaultGridSetup
    1 {
        columns {
            22 < .default
            22.wrap = <div class="col50 fleft">|</div>
            23 < .default
            23.wrap = <div class="col50 fright">|</div>
        }
        wrap = <div class="col2 col100">|</div>
    }  
    # 3 Spalten (33%|33%|33%)
    2 < lib.gridelements.defaultGridSetup
    2 {
        columns {
            24 < .default
            24.wrap = <div class="col30 fleft">|</div>
            25 < .default
            25.wrap = <div class="col70 fright"><div class="col50 fleft">|</div>
            26 < .default
            26.wrap = <div class="col50 fright">|</div></div>
        }
        wrap = <div class="col3 col100">|</div>
    }
  # 2-Spalter (70%|30%)
    3 < lib.gridelements.defaultGridSetup
    3 {
        columns {
            27 < .default
            27.wrap = <div class="col70 fleft">|</div>
            28 < .default
            28.wrap = <div class="col30 fright">|</div>
        }
        wrap = <div class="col2 col100">|</div>
    } 
  # 2-Spalter (30%|70%)
    4 < lib.gridelements.defaultGridSetup
    4 {
        columns {
            29 < .default
            29.wrap = <div class="col30 fleft">|</div>
            30 < .default
            30.wrap = <div class="col70 fright">|</div>
        }
        wrap = <div class="col2 col100">|</div>
    } 
  # 4-Spalter (25%|25%|25%|25%)
    5 < lib.gridelements.defaultGridSetup
    5 {
        columns {
            31 < .default
            31.wrap = <div class="col50 fleft"><div class="col50 fleft">|</div>
      32 < .default
            32.wrap = <div class="col50 fright">|</div></div>
            33 < .default
            33.wrap = <div class="col50 fright"><div class="col50 fleft">|</div>
      34 < .default
            34.wrap = <div class="col50 fright">|</div></div>
        }
        wrap = <div class="col4 col100">|</div>
    } 
  # 2-Spalter (80%|20%)
    6 < lib.gridelements.defaultGridSetup
    6 {
        columns {
            27 < .default
            27.wrap = <div class="col80 fleft">|</div>
            28 < .default
            28.wrap = <div class="col20 fright">|</div>
        }
        wrap = <div class="col2 col100">|</div>
    } 
  # 2-Spalter (20%|80%)
    7 < lib.gridelements.defaultGridSetup
    7 {
        columns {
            29 < .default
            29.wrap = <div class="col20 fleft">|</div>
            30 < .default
            30.wrap = <div class="col80 fright">|</div>
        }
        wrap = <div class="col2 col100">|</div>
    }
  # Kontakt Container
    8 < lib.gridelements.defaultGridSetup
    8 {
        columns {
            21 < .default
            21.wrap = <div class="contact-el contact-deu">|</div>
      22 < .default
            22.wrap = <div class="contact-el contact-sri">|</div>
            23 < .default
            23.wrap = <div class="contact-el contact-usa">|</div>
      24 < .default
            24.wrap = <div class="contact-el contact-ind">|</div>
        }
        wrap = <div class="contact-container">|</div>
    } 
  # Slider fuer Header
    9 < lib.gridelements.defaultGridSetup
    9 {
        columns {
      31 < .default
            31.wrap = |
    }
        wrap = <div class="slide-header">|<div class="slide-navilr"><div class="slide-prev"></div><div class="slide-next"></div></div></div>
    } 
  # Blauer Container
    10 < lib.gridelements.defaultGridSetup
    10 {
        columns {
      21 < .default
            21.wrap = <div class="blue-container" id="c{field:uid}"><div class="page_margins_inner">|</div></div>
      21.wrap.insertData=1
    }
        wrap = |
    } 
  # Blauer Container mit Schatten
    16 < lib.gridelements.defaultGridSetup
    16 {
        columns {
      21 < .default
            21.wrap = <div class="blue-container container-withshadow" id="c{field:uid}"><div class="page_margins_inner">|</div></div>
      21.wrap.insertData=1
    }
        wrap = |
    } 
  # Grauer Container
    11 < lib.gridelements.defaultGridSetup
    11 {
        columns {
      21 < .default
            21.wrap = <div class="grey-container" id="c{field:uid}"><div class="page_margins_inner">|</div></div>
      21.wrap.insertData=1
    }
        wrap = |
    }
  # 6-Spalter (16%|16%|16%|16%|16%|16%)
    12 < lib.gridelements.defaultGridSetup
    12 {
        columns {
            21 < .default
            21.wrap = <div class="col6-1wrap"><div class="col6-wrap-inner"><div class="col6-1 col6-subcol">|</div>
      22 < .default
            22.wrap = <div class="col6-2 col6-subcol">|</div>
            23 < .default
            23.wrap = <div class="col6-3 col6-subcol">|</div></div></div>
      24 < .default
            24.wrap = <div class="col6-2wrap"><div class="col6-wrap-inner"><div class="col6-4 col6-subcol">|</div>
      25 < .default
            25.wrap = <div class="col6-5 col6-subcol">|</div>
      26 < .default
            26.wrap = <div class="col6-6 col6-subcol">|</div></div></div>
        }
        wrap = <div class="col6 col100"><div class="col6-slide-prev"><div class="col6-slide-prev-arrow"></div></div><div class="col6-inner">|</div><div class="col6-slide-next"><div class="col6-slide-next-arrow"></div></div></div>
    } 
  # 2-Spalter eingerueckt (50%|50%)
    13 < lib.gridelements.defaultGridSetup
    13 {
        columns {
            22 < .default
            22.wrap = <div class="col50 fleft">|</div>
            23 < .default
            23.wrap = <div class="col50 fright">|</div>
        }
        wrap = <div class="col2 col100 inheritcol">|</div>
    }  
  # Jobcontainer deutsch
    14 < lib.gridelements.defaultGridSetup
    14 {
        columns {
            21 < .default
            21.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Aufgaben</span></div><div class="col80 fright">|</div></div>
            22 < .default
            22.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Voraussetzungen</span></div><div class="col80 fright">|</div></div>
      23 < .default
            23.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Bewerbung</span></div><div class="col80 fright">|</div></div>
        }
        wrap = <div class="jobcontainer">|</div>
    }
  # Jobcontainer englisch
    15 < lib.gridelements.defaultGridSetup
    15 {
        columns {
            21 < .default
            21.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Tasks</span></div><div class="col80 fright">|</div></div>
            22 < .default
            22.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Demands</span></div><div class="col80 fright">|</div></div>
      23 < .default
            23.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Contact</span></div><div class="col80 fright">|</div></div>
        }
        wrap = <div class="jobcontainer">|</div>
    }
  # Ausbildungsstellen deutsch
    17 < lib.gridelements.defaultGridSetup
    17 {
        columns {
            21 < .default
            21.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Voraussetzungen</span></div><div class="col80 fright">|</div></div>
            22 < .default
            22.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Ausbildungsstart</span></div><div class="col80 fright">|</div></div>
      23 < .default
            23.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Bewerbung</span></div><div class="col80 fright">|</div></div>
        }
        wrap = <div class="jobcontainer">|</div>
    }
  # Ausbildungsstellen englisch
    18 < lib.gridelements.defaultGridSetup
    18 {
        columns {
            21 < .default
            21.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Demands</span></div><div class="col80 fright">|</div></div>
            22 < .default
            22.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Training start</span></div><div class="col80 fright">|</div></div>
      23 < .default
            23.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Contact</span></div><div class="col80 fright">|</div></div>
        }
        wrap = <div class="jobcontainer">|</div>
    }
  # Weisser Container
    19 < lib.gridelements.defaultGridSetup
    19 {
        columns {
      21 < .default
            21.wrap = <div class="white-container" id="c{field:uid}"><div class="page_margins_inner">|</div></div>
      21.wrap.insertData=1
    }
        wrap = |
    }
	 # Rezepte Container deutsch
    20 < lib.gridelements.defaultGridSetup
    20 {
        columns {
            21 < .default
            21.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Zutaten</span></div><div class="col80 fright">|</div></div>
            22 < .default
            22.wrap = <div class="col2 col100"><div class="col20 fleft"><span class="fontbgblack">Zubereitung</span></div><div class="col80 fright">|</div></div>
        }
        wrap = <div class="jobcontainer">|</div>
    }
}
tt_content.stdWrap.innerWrap.cObject.default.if {
equals.field = CType
value = gridelements_pi1
negate = 1
}