<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "contentelements".
 *
 * Auto generated 16-10-2013 16:49
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Content Elements',
	'description' => 'Displays content elements',
	'category' => 'plugin',
	'author' => 'Stephanie Noack',
	'author_email' => 'sno@gkmb.de',
	'author_company' => 'GKMB GmbH',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
			'extbase' => '1.3',
			'fluid' => '1.3',
			'typo3' => '4.7-0.0.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:16:{s:12:"ext_icon.gif";s:4:"e922";s:17:"ext_localconf.php";s:4:"d32d";s:14:"ext_tables.php";s:4:"ac67";s:14:"ext_tables.sql";s:4:"d41d";s:21:"ExtensionBuilder.json";s:4:"1504";s:41:"Classes/Controller/ElementsController.php";s:4:"35ac";s:38:"Classes/ViewHelpers/LinkViewHelper.php";s:4:"f4cd";s:44:"Configuration/ExtensionBuilder/settings.yaml";s:4:"2c2e";s:40:"Resources/Private/Language/locallang.xml";s:4:"f95f";s:43:"Resources/Private/Language/locallang_db.xml";s:4:"dc53";s:52:"Resources/Private/Templates/Elements/Contactbox.html";s:4:"2570";s:54:"Resources/Private/Templates/Elements/Highlightbox.html";s:4:"a662";s:50:"Resources/Private/Templates/Elements/Icontext.html";s:4:"c0b5";s:50:"Resources/Private/Templates/Elements/Linktext.html";s:4:"f981";s:35:"Resources/Public/Icons/relation.gif";s:4:"e615";s:14:"doc/manual.sxw";s:4:"8d2d";}',
);

?>