<?php
class FluidCache_Standalone_template_source_b73d432cf7b10c2a1182bc218fc3b2eedcf12797 extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments1 = array();
// Rendering Boolean node
$arguments1['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.imageright', $renderingContext));
$arguments1['then'] = NULL;
$arguments1['else'] = NULL;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
$output3 = '';

$output3 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments4 = array();
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
$output6 = '';

$output6 .= '
	<div class="imagetextbox imageright">
		<div class="imagetextbox-text">
			<div class="imagetextbox-text-inner">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments7 = array();
// Rendering Boolean node
$arguments7['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext));
$arguments7['then'] = NULL;
$arguments7['else'] = NULL;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
$output9 = '';

$output9 .= '
					<h2>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments10 = array();
$arguments10['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext);
$arguments10['keepQuotes'] = false;
$arguments10['encoding'] = NULL;
$arguments10['doubleEncode'] = true;
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
return NULL;
};
$value12 = ($arguments10['value'] !== NULL ? $arguments10['value'] : $renderChildrenClosure11());

$output9 .= (!is_string($value12) ? $value12 : htmlspecialchars($value12, ($arguments10['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments10['encoding'] !== NULL ? $arguments10['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments10['doubleEncode']));

$output9 .= '</h2>
				';
return $output9;
};
$viewHelper13 = $self->getViewHelper('$viewHelper13', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper13->setArguments($arguments7);
$viewHelper13->setRenderingContext($renderingContext);
$viewHelper13->setRenderChildrenClosure($renderChildrenClosure8);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper13->initializeArgumentsAndRender();

$output6 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments14 = array();
// Rendering Boolean node
$arguments14['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.subheadline', $renderingContext));
$arguments14['then'] = NULL;
$arguments14['else'] = NULL;
$renderChildrenClosure15 = function() use ($renderingContext, $self) {
$output16 = '';

$output16 .= '
					<span class="subheadline bluemiddle">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments17 = array();
$arguments17['parseFuncTSPath'] = 'lib.parseFunc';
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.subheadline', $renderingContext);
};
$viewHelper19 = $self->getViewHelper('$viewHelper19', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper19->setArguments($arguments17);
$viewHelper19->setRenderingContext($renderingContext);
$viewHelper19->setRenderChildrenClosure($renderChildrenClosure18);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output16 .= $viewHelper19->initializeArgumentsAndRender();

$output16 .= '</span>
				';
return $output16;
};
$viewHelper20 = $self->getViewHelper('$viewHelper20', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper20->setArguments($arguments14);
$viewHelper20->setRenderingContext($renderingContext);
$viewHelper20->setRenderChildrenClosure($renderChildrenClosure15);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper20->initializeArgumentsAndRender();

$output6 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments21 = array();
// Rendering Boolean node
$arguments21['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext));
$arguments21['then'] = NULL;
$arguments21['else'] = NULL;
$renderChildrenClosure22 = function() use ($renderingContext, $self) {
return '
					<div class="header-separator"></div>
				';
};
$viewHelper23 = $self->getViewHelper('$viewHelper23', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper23->setArguments($arguments21);
$viewHelper23->setRenderingContext($renderingContext);
$viewHelper23->setRenderChildrenClosure($renderChildrenClosure22);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper23->initializeArgumentsAndRender();

$output6 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments24 = array();
$arguments24['parseFuncTSPath'] = 'lib.parseFunc';
$renderChildrenClosure25 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.bodytext', $renderingContext);
};
$viewHelper26 = $self->getViewHelper('$viewHelper26', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper26->setArguments($arguments24);
$viewHelper26->setRenderingContext($renderingContext);
$viewHelper26->setRenderChildrenClosure($renderChildrenClosure25);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output6 .= $viewHelper26->initializeArgumentsAndRender();

$output6 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments27 = array();
// Rendering Boolean node
$arguments27['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinkid', $renderingContext));
$arguments27['then'] = NULL;
$arguments27['else'] = NULL;
$renderChildrenClosure28 = function() use ($renderingContext, $self) {
$output29 = '';

$output29 .= '<div class="imagetextbox-more"><a href="#" class="c';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments30 = array();
$arguments30['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinkid', $renderingContext);
$arguments30['keepQuotes'] = false;
$arguments30['encoding'] = NULL;
$arguments30['doubleEncode'] = true;
$renderChildrenClosure31 = function() use ($renderingContext, $self) {
return NULL;
};
$value32 = ($arguments30['value'] !== NULL ? $arguments30['value'] : $renderChildrenClosure31());

$output29 .= (!is_string($value32) ? $value32 : htmlspecialchars($value32, ($arguments30['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments30['encoding'] !== NULL ? $arguments30['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments30['doubleEncode']));

$output29 .= '">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments33 = array();
$arguments33['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinktitle', $renderingContext);
$arguments33['keepQuotes'] = false;
$arguments33['encoding'] = NULL;
$arguments33['doubleEncode'] = true;
$renderChildrenClosure34 = function() use ($renderingContext, $self) {
return NULL;
};
$value35 = ($arguments33['value'] !== NULL ? $arguments33['value'] : $renderChildrenClosure34());

$output29 .= (!is_string($value35) ? $value35 : htmlspecialchars($value35, ($arguments33['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments33['encoding'] !== NULL ? $arguments33['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments33['doubleEncode']));

$output29 .= '</a></div>';
return $output29;
};
$viewHelper36 = $self->getViewHelper('$viewHelper36', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper36->setArguments($arguments27);
$viewHelper36->setRenderingContext($renderingContext);
$viewHelper36->setRenderChildrenClosure($renderChildrenClosure28);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper36->initializeArgumentsAndRender();

$output6 .= '
			</div>
		</div>
		<div class="imagetextbox-images">
			<div class="imagetextbox-images-inner ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments37 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments38 = array();
$arguments38['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext);
$arguments38['keepQuotes'] = false;
$arguments38['encoding'] = NULL;
$arguments38['doubleEncode'] = true;
$renderChildrenClosure39 = function() use ($renderingContext, $self) {
return NULL;
};
$value40 = ($arguments38['value'] !== NULL ? $arguments38['value'] : $renderChildrenClosure39());
$arguments37['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean((!is_string($value40) ? $value40 : htmlspecialchars($value40, ($arguments38['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments38['encoding'] !== NULL ? $arguments38['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments38['doubleEncode'])));
$arguments37['then'] = 'slideimages';
$arguments37['else'] = 'noslideimages';
$renderChildrenClosure41 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper42 = $self->getViewHelper('$viewHelper42', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper42->setArguments($arguments37);
$viewHelper42->setRenderingContext($renderingContext);
$viewHelper42->setRenderChildrenClosure($renderChildrenClosure41);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper42->initializeArgumentsAndRender();

$output6 .= '">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments43 = array();
// Rendering ViewHelper ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper
$arguments44 = array();
$arguments44['delimiter'] = ',';
$arguments44['subject'] = NULL;
$arguments44['removeEmpty'] = true;
$renderChildrenClosure45 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.image', $renderingContext);
};
$viewHelper46 = $self->getViewHelper('$viewHelper46', $renderingContext, 'ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper');
$viewHelper46->setArguments($arguments44);
$viewHelper46->setRenderingContext($renderingContext);
$viewHelper46->setRenderChildrenClosure($renderChildrenClosure45);
// End of ViewHelper ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper
$arguments43['each'] = $viewHelper46->initializeArgumentsAndRender();
$arguments43['as'] = 'image';
$arguments43['key'] = '';
$arguments43['reverse'] = false;
$arguments43['iteration'] = NULL;
$renderChildrenClosure47 = function() use ($renderingContext, $self) {
$output48 = '';

$output48 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments49 = array();
// Rendering Boolean node
$arguments49['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext));
$arguments49['then'] = NULL;
$arguments49['else'] = NULL;
$renderChildrenClosure50 = function() use ($renderingContext, $self) {
$output51 = '';

$output51 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments52 = array();
$renderChildrenClosure53 = function() use ($renderingContext, $self) {
$output54 = '';

$output54 .= '
							<div class="slide-image">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments55 = array();
$output56 = '';

$output56 .= 'uploads/pics/';

$output56 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments55['src'] = $output56;
$arguments55['alt'] = '';
$arguments55['additionalAttributes'] = NULL;
$arguments55['width'] = NULL;
$arguments55['height'] = NULL;
$arguments55['minWidth'] = NULL;
$arguments55['minHeight'] = NULL;
$arguments55['maxWidth'] = NULL;
$arguments55['maxHeight'] = NULL;
$arguments55['treatIdAsReference'] = false;
$arguments55['image'] = NULL;
$arguments55['class'] = NULL;
$arguments55['dir'] = NULL;
$arguments55['id'] = NULL;
$arguments55['lang'] = NULL;
$arguments55['style'] = NULL;
$arguments55['title'] = NULL;
$arguments55['accesskey'] = NULL;
$arguments55['tabindex'] = NULL;
$arguments55['onclick'] = NULL;
$arguments55['ismap'] = NULL;
$arguments55['longdesc'] = NULL;
$arguments55['usemap'] = NULL;
$renderChildrenClosure57 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper58 = $self->getViewHelper('$viewHelper58', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper58->setArguments($arguments55);
$viewHelper58->setRenderingContext($renderingContext);
$viewHelper58->setRenderChildrenClosure($renderChildrenClosure57);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output54 .= $viewHelper58->initializeArgumentsAndRender();

$output54 .= '</div>
						';
return $output54;
};
$viewHelper59 = $self->getViewHelper('$viewHelper59', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper59->setArguments($arguments52);
$viewHelper59->setRenderingContext($renderingContext);
$viewHelper59->setRenderChildrenClosure($renderChildrenClosure53);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output51 .= $viewHelper59->initializeArgumentsAndRender();

$output51 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments60 = array();
$renderChildrenClosure61 = function() use ($renderingContext, $self) {
$output62 = '';

$output62 .= '
							<img src="uploads/pics/';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments63 = array();
$arguments63['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments63['keepQuotes'] = false;
$arguments63['encoding'] = NULL;
$arguments63['doubleEncode'] = true;
$renderChildrenClosure64 = function() use ($renderingContext, $self) {
return NULL;
};
$value65 = ($arguments63['value'] !== NULL ? $arguments63['value'] : $renderChildrenClosure64());

$output62 .= (!is_string($value65) ? $value65 : htmlspecialchars($value65, ($arguments63['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments63['encoding'] !== NULL ? $arguments63['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments63['doubleEncode']));

$output62 .= '" alt="" />
						';
return $output62;
};
$viewHelper66 = $self->getViewHelper('$viewHelper66', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper66->setArguments($arguments60);
$viewHelper66->setRenderingContext($renderingContext);
$viewHelper66->setRenderChildrenClosure($renderChildrenClosure61);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output51 .= $viewHelper66->initializeArgumentsAndRender();

$output51 .= '
					';
return $output51;
};
$arguments49['__thenClosure'] = function() use ($renderingContext, $self) {
$output67 = '';

$output67 .= '
							<div class="slide-image">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments68 = array();
$output69 = '';

$output69 .= 'uploads/pics/';

$output69 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments68['src'] = $output69;
$arguments68['alt'] = '';
$arguments68['additionalAttributes'] = NULL;
$arguments68['width'] = NULL;
$arguments68['height'] = NULL;
$arguments68['minWidth'] = NULL;
$arguments68['minHeight'] = NULL;
$arguments68['maxWidth'] = NULL;
$arguments68['maxHeight'] = NULL;
$arguments68['treatIdAsReference'] = false;
$arguments68['image'] = NULL;
$arguments68['class'] = NULL;
$arguments68['dir'] = NULL;
$arguments68['id'] = NULL;
$arguments68['lang'] = NULL;
$arguments68['style'] = NULL;
$arguments68['title'] = NULL;
$arguments68['accesskey'] = NULL;
$arguments68['tabindex'] = NULL;
$arguments68['onclick'] = NULL;
$arguments68['ismap'] = NULL;
$arguments68['longdesc'] = NULL;
$arguments68['usemap'] = NULL;
$renderChildrenClosure70 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper71 = $self->getViewHelper('$viewHelper71', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper71->setArguments($arguments68);
$viewHelper71->setRenderingContext($renderingContext);
$viewHelper71->setRenderChildrenClosure($renderChildrenClosure70);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output67 .= $viewHelper71->initializeArgumentsAndRender();

$output67 .= '</div>
						';
return $output67;
};
$arguments49['__elseClosure'] = function() use ($renderingContext, $self) {
$output72 = '';

$output72 .= '
							<img src="uploads/pics/';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments73 = array();
$arguments73['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments73['keepQuotes'] = false;
$arguments73['encoding'] = NULL;
$arguments73['doubleEncode'] = true;
$renderChildrenClosure74 = function() use ($renderingContext, $self) {
return NULL;
};
$value75 = ($arguments73['value'] !== NULL ? $arguments73['value'] : $renderChildrenClosure74());

$output72 .= (!is_string($value75) ? $value75 : htmlspecialchars($value75, ($arguments73['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments73['encoding'] !== NULL ? $arguments73['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments73['doubleEncode']));

$output72 .= '" alt="" />
						';
return $output72;
};
$viewHelper76 = $self->getViewHelper('$viewHelper76', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper76->setArguments($arguments49);
$viewHelper76->setRenderingContext($renderingContext);
$viewHelper76->setRenderChildrenClosure($renderChildrenClosure50);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output48 .= $viewHelper76->initializeArgumentsAndRender();

$output48 .= '
				';
return $output48;
};

$output6 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments43, $renderChildrenClosure47, $renderingContext);

$output6 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments77 = array();
// Rendering Boolean node
$arguments77['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext));
$arguments77['then'] = NULL;
$arguments77['else'] = NULL;
$renderChildrenClosure78 = function() use ($renderingContext, $self) {
return '<div class="slide-navilr"><div class="slide-prev"></div><div class="slide-next"></div></div>';
};
$viewHelper79 = $self->getViewHelper('$viewHelper79', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper79->setArguments($arguments77);
$viewHelper79->setRenderingContext($renderingContext);
$viewHelper79->setRenderChildrenClosure($renderChildrenClosure78);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper79->initializeArgumentsAndRender();

$output6 .= '
			</div>
		</div>
	</div>
	';
return $output6;
};
$viewHelper80 = $self->getViewHelper('$viewHelper80', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper80->setArguments($arguments4);
$viewHelper80->setRenderingContext($renderingContext);
$viewHelper80->setRenderChildrenClosure($renderChildrenClosure5);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output3 .= $viewHelper80->initializeArgumentsAndRender();

$output3 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments81 = array();
$renderChildrenClosure82 = function() use ($renderingContext, $self) {
$output83 = '';

$output83 .= '
	<div class="imagetextbox imageleft">	
		<div class="imagetextbox-images">
			<div class="imagetextbox-images-inner ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments84 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments85 = array();
$arguments85['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext);
$arguments85['keepQuotes'] = false;
$arguments85['encoding'] = NULL;
$arguments85['doubleEncode'] = true;
$renderChildrenClosure86 = function() use ($renderingContext, $self) {
return NULL;
};
$value87 = ($arguments85['value'] !== NULL ? $arguments85['value'] : $renderChildrenClosure86());
$arguments84['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean((!is_string($value87) ? $value87 : htmlspecialchars($value87, ($arguments85['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments85['encoding'] !== NULL ? $arguments85['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments85['doubleEncode'])));
$arguments84['then'] = 'slideimages';
$arguments84['else'] = 'noslideimages';
$renderChildrenClosure88 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper89 = $self->getViewHelper('$viewHelper89', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper89->setArguments($arguments84);
$viewHelper89->setRenderingContext($renderingContext);
$viewHelper89->setRenderChildrenClosure($renderChildrenClosure88);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output83 .= $viewHelper89->initializeArgumentsAndRender();

$output83 .= '">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments90 = array();
// Rendering ViewHelper ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper
$arguments91 = array();
$arguments91['delimiter'] = ',';
$arguments91['subject'] = NULL;
$arguments91['removeEmpty'] = true;
$renderChildrenClosure92 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.image', $renderingContext);
};
$viewHelper93 = $self->getViewHelper('$viewHelper93', $renderingContext, 'ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper');
$viewHelper93->setArguments($arguments91);
$viewHelper93->setRenderingContext($renderingContext);
$viewHelper93->setRenderChildrenClosure($renderChildrenClosure92);
// End of ViewHelper ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper
$arguments90['each'] = $viewHelper93->initializeArgumentsAndRender();
$arguments90['as'] = 'image';
$arguments90['key'] = '';
$arguments90['reverse'] = false;
$arguments90['iteration'] = NULL;
$renderChildrenClosure94 = function() use ($renderingContext, $self) {
$output95 = '';

$output95 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments96 = array();
// Rendering Boolean node
$arguments96['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext));
$arguments96['then'] = NULL;
$arguments96['else'] = NULL;
$renderChildrenClosure97 = function() use ($renderingContext, $self) {
$output98 = '';

$output98 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments99 = array();
$renderChildrenClosure100 = function() use ($renderingContext, $self) {
$output101 = '';

$output101 .= '
							<div class="slide-image">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments102 = array();
$output103 = '';

$output103 .= 'uploads/pics/';

$output103 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments102['src'] = $output103;
$arguments102['alt'] = '';
$arguments102['additionalAttributes'] = NULL;
$arguments102['width'] = NULL;
$arguments102['height'] = NULL;
$arguments102['minWidth'] = NULL;
$arguments102['minHeight'] = NULL;
$arguments102['maxWidth'] = NULL;
$arguments102['maxHeight'] = NULL;
$arguments102['treatIdAsReference'] = false;
$arguments102['image'] = NULL;
$arguments102['class'] = NULL;
$arguments102['dir'] = NULL;
$arguments102['id'] = NULL;
$arguments102['lang'] = NULL;
$arguments102['style'] = NULL;
$arguments102['title'] = NULL;
$arguments102['accesskey'] = NULL;
$arguments102['tabindex'] = NULL;
$arguments102['onclick'] = NULL;
$arguments102['ismap'] = NULL;
$arguments102['longdesc'] = NULL;
$arguments102['usemap'] = NULL;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper105 = $self->getViewHelper('$viewHelper105', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper105->setArguments($arguments102);
$viewHelper105->setRenderingContext($renderingContext);
$viewHelper105->setRenderChildrenClosure($renderChildrenClosure104);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output101 .= $viewHelper105->initializeArgumentsAndRender();

$output101 .= '</div>
						';
return $output101;
};
$viewHelper106 = $self->getViewHelper('$viewHelper106', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper106->setArguments($arguments99);
$viewHelper106->setRenderingContext($renderingContext);
$viewHelper106->setRenderChildrenClosure($renderChildrenClosure100);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output98 .= $viewHelper106->initializeArgumentsAndRender();

$output98 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments107 = array();
$renderChildrenClosure108 = function() use ($renderingContext, $self) {
$output109 = '';

$output109 .= '
							<img src="uploads/pics/';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments110 = array();
$arguments110['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments110['keepQuotes'] = false;
$arguments110['encoding'] = NULL;
$arguments110['doubleEncode'] = true;
$renderChildrenClosure111 = function() use ($renderingContext, $self) {
return NULL;
};
$value112 = ($arguments110['value'] !== NULL ? $arguments110['value'] : $renderChildrenClosure111());

$output109 .= (!is_string($value112) ? $value112 : htmlspecialchars($value112, ($arguments110['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments110['encoding'] !== NULL ? $arguments110['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments110['doubleEncode']));

$output109 .= '" alt="" />
						';
return $output109;
};
$viewHelper113 = $self->getViewHelper('$viewHelper113', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper113->setArguments($arguments107);
$viewHelper113->setRenderingContext($renderingContext);
$viewHelper113->setRenderChildrenClosure($renderChildrenClosure108);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output98 .= $viewHelper113->initializeArgumentsAndRender();

$output98 .= '
						
					';
return $output98;
};
$arguments96['__thenClosure'] = function() use ($renderingContext, $self) {
$output114 = '';

$output114 .= '
							<div class="slide-image">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments115 = array();
$output116 = '';

$output116 .= 'uploads/pics/';

$output116 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments115['src'] = $output116;
$arguments115['alt'] = '';
$arguments115['additionalAttributes'] = NULL;
$arguments115['width'] = NULL;
$arguments115['height'] = NULL;
$arguments115['minWidth'] = NULL;
$arguments115['minHeight'] = NULL;
$arguments115['maxWidth'] = NULL;
$arguments115['maxHeight'] = NULL;
$arguments115['treatIdAsReference'] = false;
$arguments115['image'] = NULL;
$arguments115['class'] = NULL;
$arguments115['dir'] = NULL;
$arguments115['id'] = NULL;
$arguments115['lang'] = NULL;
$arguments115['style'] = NULL;
$arguments115['title'] = NULL;
$arguments115['accesskey'] = NULL;
$arguments115['tabindex'] = NULL;
$arguments115['onclick'] = NULL;
$arguments115['ismap'] = NULL;
$arguments115['longdesc'] = NULL;
$arguments115['usemap'] = NULL;
$renderChildrenClosure117 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper118 = $self->getViewHelper('$viewHelper118', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper118->setArguments($arguments115);
$viewHelper118->setRenderingContext($renderingContext);
$viewHelper118->setRenderChildrenClosure($renderChildrenClosure117);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output114 .= $viewHelper118->initializeArgumentsAndRender();

$output114 .= '</div>
						';
return $output114;
};
$arguments96['__elseClosure'] = function() use ($renderingContext, $self) {
$output119 = '';

$output119 .= '
							<img src="uploads/pics/';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments120 = array();
$arguments120['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments120['keepQuotes'] = false;
$arguments120['encoding'] = NULL;
$arguments120['doubleEncode'] = true;
$renderChildrenClosure121 = function() use ($renderingContext, $self) {
return NULL;
};
$value122 = ($arguments120['value'] !== NULL ? $arguments120['value'] : $renderChildrenClosure121());

$output119 .= (!is_string($value122) ? $value122 : htmlspecialchars($value122, ($arguments120['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments120['encoding'] !== NULL ? $arguments120['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments120['doubleEncode']));

$output119 .= '" alt="" />
						';
return $output119;
};
$viewHelper123 = $self->getViewHelper('$viewHelper123', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper123->setArguments($arguments96);
$viewHelper123->setRenderingContext($renderingContext);
$viewHelper123->setRenderChildrenClosure($renderChildrenClosure97);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output95 .= $viewHelper123->initializeArgumentsAndRender();

$output95 .= '
				';
return $output95;
};

$output83 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments90, $renderChildrenClosure94, $renderingContext);

$output83 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments124 = array();
// Rendering Boolean node
$arguments124['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext));
$arguments124['then'] = NULL;
$arguments124['else'] = NULL;
$renderChildrenClosure125 = function() use ($renderingContext, $self) {
return '<div class="slide-navilr"><div class="slide-prev"></div><div class="slide-next"></div></div>';
};
$viewHelper126 = $self->getViewHelper('$viewHelper126', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper126->setArguments($arguments124);
$viewHelper126->setRenderingContext($renderingContext);
$viewHelper126->setRenderChildrenClosure($renderChildrenClosure125);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output83 .= $viewHelper126->initializeArgumentsAndRender();

$output83 .= '
			</div>
		</div>
		<div class="imagetextbox-text">
			<div class="imagetextbox-text-inner">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments127 = array();
// Rendering Boolean node
$arguments127['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext));
$arguments127['then'] = NULL;
$arguments127['else'] = NULL;
$renderChildrenClosure128 = function() use ($renderingContext, $self) {
$output129 = '';

$output129 .= '
					<h2>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments130 = array();
$arguments130['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext);
$arguments130['keepQuotes'] = false;
$arguments130['encoding'] = NULL;
$arguments130['doubleEncode'] = true;
$renderChildrenClosure131 = function() use ($renderingContext, $self) {
return NULL;
};
$value132 = ($arguments130['value'] !== NULL ? $arguments130['value'] : $renderChildrenClosure131());

$output129 .= (!is_string($value132) ? $value132 : htmlspecialchars($value132, ($arguments130['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments130['encoding'] !== NULL ? $arguments130['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments130['doubleEncode']));

$output129 .= '</h2>
				';
return $output129;
};
$viewHelper133 = $self->getViewHelper('$viewHelper133', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper133->setArguments($arguments127);
$viewHelper133->setRenderingContext($renderingContext);
$viewHelper133->setRenderChildrenClosure($renderChildrenClosure128);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output83 .= $viewHelper133->initializeArgumentsAndRender();

$output83 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments134 = array();
// Rendering Boolean node
$arguments134['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.subheadline', $renderingContext));
$arguments134['then'] = NULL;
$arguments134['else'] = NULL;
$renderChildrenClosure135 = function() use ($renderingContext, $self) {
$output136 = '';

$output136 .= '
					<span class="subheadline bluemiddle">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments137 = array();
$arguments137['parseFuncTSPath'] = 'lib.parseFunc';
$renderChildrenClosure138 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.subheadline', $renderingContext);
};
$viewHelper139 = $self->getViewHelper('$viewHelper139', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper139->setArguments($arguments137);
$viewHelper139->setRenderingContext($renderingContext);
$viewHelper139->setRenderChildrenClosure($renderChildrenClosure138);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output136 .= $viewHelper139->initializeArgumentsAndRender();

$output136 .= '</span>
				';
return $output136;
};
$viewHelper140 = $self->getViewHelper('$viewHelper140', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper140->setArguments($arguments134);
$viewHelper140->setRenderingContext($renderingContext);
$viewHelper140->setRenderChildrenClosure($renderChildrenClosure135);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output83 .= $viewHelper140->initializeArgumentsAndRender();

$output83 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments141 = array();
// Rendering Boolean node
$arguments141['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext));
$arguments141['then'] = NULL;
$arguments141['else'] = NULL;
$renderChildrenClosure142 = function() use ($renderingContext, $self) {
return '
					<div class="header-separator"></div>
				';
};
$viewHelper143 = $self->getViewHelper('$viewHelper143', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper143->setArguments($arguments141);
$viewHelper143->setRenderingContext($renderingContext);
$viewHelper143->setRenderChildrenClosure($renderChildrenClosure142);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output83 .= $viewHelper143->initializeArgumentsAndRender();

$output83 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments144 = array();
$arguments144['parseFuncTSPath'] = 'lib.parseFunc';
$renderChildrenClosure145 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.bodytext', $renderingContext);
};
$viewHelper146 = $self->getViewHelper('$viewHelper146', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper146->setArguments($arguments144);
$viewHelper146->setRenderingContext($renderingContext);
$viewHelper146->setRenderChildrenClosure($renderChildrenClosure145);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output83 .= $viewHelper146->initializeArgumentsAndRender();

$output83 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments147 = array();
// Rendering Boolean node
$arguments147['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinkid', $renderingContext));
$arguments147['then'] = NULL;
$arguments147['else'] = NULL;
$renderChildrenClosure148 = function() use ($renderingContext, $self) {
$output149 = '';

$output149 .= '<div class="imagetextbox-more"><a href="#" class="c';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments150 = array();
$arguments150['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinkid', $renderingContext);
$arguments150['keepQuotes'] = false;
$arguments150['encoding'] = NULL;
$arguments150['doubleEncode'] = true;
$renderChildrenClosure151 = function() use ($renderingContext, $self) {
return NULL;
};
$value152 = ($arguments150['value'] !== NULL ? $arguments150['value'] : $renderChildrenClosure151());

$output149 .= (!is_string($value152) ? $value152 : htmlspecialchars($value152, ($arguments150['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments150['encoding'] !== NULL ? $arguments150['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments150['doubleEncode']));

$output149 .= '">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments153 = array();
$arguments153['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinktitle', $renderingContext);
$arguments153['keepQuotes'] = false;
$arguments153['encoding'] = NULL;
$arguments153['doubleEncode'] = true;
$renderChildrenClosure154 = function() use ($renderingContext, $self) {
return NULL;
};
$value155 = ($arguments153['value'] !== NULL ? $arguments153['value'] : $renderChildrenClosure154());

$output149 .= (!is_string($value155) ? $value155 : htmlspecialchars($value155, ($arguments153['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments153['encoding'] !== NULL ? $arguments153['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments153['doubleEncode']));

$output149 .= '</a></div>';
return $output149;
};
$viewHelper156 = $self->getViewHelper('$viewHelper156', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper156->setArguments($arguments147);
$viewHelper156->setRenderingContext($renderingContext);
$viewHelper156->setRenderChildrenClosure($renderChildrenClosure148);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output83 .= $viewHelper156->initializeArgumentsAndRender();

$output83 .= '
			</div>
		</div>
	</div>	
	';
return $output83;
};
$viewHelper157 = $self->getViewHelper('$viewHelper157', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper157->setArguments($arguments81);
$viewHelper157->setRenderingContext($renderingContext);
$viewHelper157->setRenderChildrenClosure($renderChildrenClosure82);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output3 .= $viewHelper157->initializeArgumentsAndRender();

$output3 .= '
';
return $output3;
};
$arguments1['__thenClosure'] = function() use ($renderingContext, $self) {
$output158 = '';

$output158 .= '
	<div class="imagetextbox imageright">
		<div class="imagetextbox-text">
			<div class="imagetextbox-text-inner">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments159 = array();
// Rendering Boolean node
$arguments159['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext));
$arguments159['then'] = NULL;
$arguments159['else'] = NULL;
$renderChildrenClosure160 = function() use ($renderingContext, $self) {
$output161 = '';

$output161 .= '
					<h2>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments162 = array();
$arguments162['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext);
$arguments162['keepQuotes'] = false;
$arguments162['encoding'] = NULL;
$arguments162['doubleEncode'] = true;
$renderChildrenClosure163 = function() use ($renderingContext, $self) {
return NULL;
};
$value164 = ($arguments162['value'] !== NULL ? $arguments162['value'] : $renderChildrenClosure163());

$output161 .= (!is_string($value164) ? $value164 : htmlspecialchars($value164, ($arguments162['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments162['encoding'] !== NULL ? $arguments162['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments162['doubleEncode']));

$output161 .= '</h2>
				';
return $output161;
};
$viewHelper165 = $self->getViewHelper('$viewHelper165', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper165->setArguments($arguments159);
$viewHelper165->setRenderingContext($renderingContext);
$viewHelper165->setRenderChildrenClosure($renderChildrenClosure160);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output158 .= $viewHelper165->initializeArgumentsAndRender();

$output158 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments166 = array();
// Rendering Boolean node
$arguments166['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.subheadline', $renderingContext));
$arguments166['then'] = NULL;
$arguments166['else'] = NULL;
$renderChildrenClosure167 = function() use ($renderingContext, $self) {
$output168 = '';

$output168 .= '
					<span class="subheadline bluemiddle">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments169 = array();
$arguments169['parseFuncTSPath'] = 'lib.parseFunc';
$renderChildrenClosure170 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.subheadline', $renderingContext);
};
$viewHelper171 = $self->getViewHelper('$viewHelper171', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper171->setArguments($arguments169);
$viewHelper171->setRenderingContext($renderingContext);
$viewHelper171->setRenderChildrenClosure($renderChildrenClosure170);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output168 .= $viewHelper171->initializeArgumentsAndRender();

$output168 .= '</span>
				';
return $output168;
};
$viewHelper172 = $self->getViewHelper('$viewHelper172', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper172->setArguments($arguments166);
$viewHelper172->setRenderingContext($renderingContext);
$viewHelper172->setRenderChildrenClosure($renderChildrenClosure167);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output158 .= $viewHelper172->initializeArgumentsAndRender();

$output158 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments173 = array();
// Rendering Boolean node
$arguments173['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext));
$arguments173['then'] = NULL;
$arguments173['else'] = NULL;
$renderChildrenClosure174 = function() use ($renderingContext, $self) {
return '
					<div class="header-separator"></div>
				';
};
$viewHelper175 = $self->getViewHelper('$viewHelper175', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper175->setArguments($arguments173);
$viewHelper175->setRenderingContext($renderingContext);
$viewHelper175->setRenderChildrenClosure($renderChildrenClosure174);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output158 .= $viewHelper175->initializeArgumentsAndRender();

$output158 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments176 = array();
$arguments176['parseFuncTSPath'] = 'lib.parseFunc';
$renderChildrenClosure177 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.bodytext', $renderingContext);
};
$viewHelper178 = $self->getViewHelper('$viewHelper178', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper178->setArguments($arguments176);
$viewHelper178->setRenderingContext($renderingContext);
$viewHelper178->setRenderChildrenClosure($renderChildrenClosure177);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output158 .= $viewHelper178->initializeArgumentsAndRender();

$output158 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments179 = array();
// Rendering Boolean node
$arguments179['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinkid', $renderingContext));
$arguments179['then'] = NULL;
$arguments179['else'] = NULL;
$renderChildrenClosure180 = function() use ($renderingContext, $self) {
$output181 = '';

$output181 .= '<div class="imagetextbox-more"><a href="#" class="c';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments182 = array();
$arguments182['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinkid', $renderingContext);
$arguments182['keepQuotes'] = false;
$arguments182['encoding'] = NULL;
$arguments182['doubleEncode'] = true;
$renderChildrenClosure183 = function() use ($renderingContext, $self) {
return NULL;
};
$value184 = ($arguments182['value'] !== NULL ? $arguments182['value'] : $renderChildrenClosure183());

$output181 .= (!is_string($value184) ? $value184 : htmlspecialchars($value184, ($arguments182['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments182['encoding'] !== NULL ? $arguments182['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments182['doubleEncode']));

$output181 .= '">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments185 = array();
$arguments185['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinktitle', $renderingContext);
$arguments185['keepQuotes'] = false;
$arguments185['encoding'] = NULL;
$arguments185['doubleEncode'] = true;
$renderChildrenClosure186 = function() use ($renderingContext, $self) {
return NULL;
};
$value187 = ($arguments185['value'] !== NULL ? $arguments185['value'] : $renderChildrenClosure186());

$output181 .= (!is_string($value187) ? $value187 : htmlspecialchars($value187, ($arguments185['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments185['encoding'] !== NULL ? $arguments185['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments185['doubleEncode']));

$output181 .= '</a></div>';
return $output181;
};
$viewHelper188 = $self->getViewHelper('$viewHelper188', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper188->setArguments($arguments179);
$viewHelper188->setRenderingContext($renderingContext);
$viewHelper188->setRenderChildrenClosure($renderChildrenClosure180);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output158 .= $viewHelper188->initializeArgumentsAndRender();

$output158 .= '
			</div>
		</div>
		<div class="imagetextbox-images">
			<div class="imagetextbox-images-inner ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments189 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments190 = array();
$arguments190['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext);
$arguments190['keepQuotes'] = false;
$arguments190['encoding'] = NULL;
$arguments190['doubleEncode'] = true;
$renderChildrenClosure191 = function() use ($renderingContext, $self) {
return NULL;
};
$value192 = ($arguments190['value'] !== NULL ? $arguments190['value'] : $renderChildrenClosure191());
$arguments189['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean((!is_string($value192) ? $value192 : htmlspecialchars($value192, ($arguments190['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments190['encoding'] !== NULL ? $arguments190['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments190['doubleEncode'])));
$arguments189['then'] = 'slideimages';
$arguments189['else'] = 'noslideimages';
$renderChildrenClosure193 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper194 = $self->getViewHelper('$viewHelper194', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper194->setArguments($arguments189);
$viewHelper194->setRenderingContext($renderingContext);
$viewHelper194->setRenderChildrenClosure($renderChildrenClosure193);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output158 .= $viewHelper194->initializeArgumentsAndRender();

$output158 .= '">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments195 = array();
// Rendering ViewHelper ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper
$arguments196 = array();
$arguments196['delimiter'] = ',';
$arguments196['subject'] = NULL;
$arguments196['removeEmpty'] = true;
$renderChildrenClosure197 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.image', $renderingContext);
};
$viewHelper198 = $self->getViewHelper('$viewHelper198', $renderingContext, 'ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper');
$viewHelper198->setArguments($arguments196);
$viewHelper198->setRenderingContext($renderingContext);
$viewHelper198->setRenderChildrenClosure($renderChildrenClosure197);
// End of ViewHelper ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper
$arguments195['each'] = $viewHelper198->initializeArgumentsAndRender();
$arguments195['as'] = 'image';
$arguments195['key'] = '';
$arguments195['reverse'] = false;
$arguments195['iteration'] = NULL;
$renderChildrenClosure199 = function() use ($renderingContext, $self) {
$output200 = '';

$output200 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments201 = array();
// Rendering Boolean node
$arguments201['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext));
$arguments201['then'] = NULL;
$arguments201['else'] = NULL;
$renderChildrenClosure202 = function() use ($renderingContext, $self) {
$output203 = '';

$output203 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments204 = array();
$renderChildrenClosure205 = function() use ($renderingContext, $self) {
$output206 = '';

$output206 .= '
							<div class="slide-image">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments207 = array();
$output208 = '';

$output208 .= 'uploads/pics/';

$output208 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments207['src'] = $output208;
$arguments207['alt'] = '';
$arguments207['additionalAttributes'] = NULL;
$arguments207['width'] = NULL;
$arguments207['height'] = NULL;
$arguments207['minWidth'] = NULL;
$arguments207['minHeight'] = NULL;
$arguments207['maxWidth'] = NULL;
$arguments207['maxHeight'] = NULL;
$arguments207['treatIdAsReference'] = false;
$arguments207['image'] = NULL;
$arguments207['class'] = NULL;
$arguments207['dir'] = NULL;
$arguments207['id'] = NULL;
$arguments207['lang'] = NULL;
$arguments207['style'] = NULL;
$arguments207['title'] = NULL;
$arguments207['accesskey'] = NULL;
$arguments207['tabindex'] = NULL;
$arguments207['onclick'] = NULL;
$arguments207['ismap'] = NULL;
$arguments207['longdesc'] = NULL;
$arguments207['usemap'] = NULL;
$renderChildrenClosure209 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper210 = $self->getViewHelper('$viewHelper210', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper210->setArguments($arguments207);
$viewHelper210->setRenderingContext($renderingContext);
$viewHelper210->setRenderChildrenClosure($renderChildrenClosure209);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output206 .= $viewHelper210->initializeArgumentsAndRender();

$output206 .= '</div>
						';
return $output206;
};
$viewHelper211 = $self->getViewHelper('$viewHelper211', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper211->setArguments($arguments204);
$viewHelper211->setRenderingContext($renderingContext);
$viewHelper211->setRenderChildrenClosure($renderChildrenClosure205);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output203 .= $viewHelper211->initializeArgumentsAndRender();

$output203 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments212 = array();
$renderChildrenClosure213 = function() use ($renderingContext, $self) {
$output214 = '';

$output214 .= '
							<img src="uploads/pics/';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments215 = array();
$arguments215['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments215['keepQuotes'] = false;
$arguments215['encoding'] = NULL;
$arguments215['doubleEncode'] = true;
$renderChildrenClosure216 = function() use ($renderingContext, $self) {
return NULL;
};
$value217 = ($arguments215['value'] !== NULL ? $arguments215['value'] : $renderChildrenClosure216());

$output214 .= (!is_string($value217) ? $value217 : htmlspecialchars($value217, ($arguments215['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments215['encoding'] !== NULL ? $arguments215['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments215['doubleEncode']));

$output214 .= '" alt="" />
						';
return $output214;
};
$viewHelper218 = $self->getViewHelper('$viewHelper218', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper218->setArguments($arguments212);
$viewHelper218->setRenderingContext($renderingContext);
$viewHelper218->setRenderChildrenClosure($renderChildrenClosure213);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output203 .= $viewHelper218->initializeArgumentsAndRender();

$output203 .= '
					';
return $output203;
};
$arguments201['__thenClosure'] = function() use ($renderingContext, $self) {
$output219 = '';

$output219 .= '
							<div class="slide-image">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments220 = array();
$output221 = '';

$output221 .= 'uploads/pics/';

$output221 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments220['src'] = $output221;
$arguments220['alt'] = '';
$arguments220['additionalAttributes'] = NULL;
$arguments220['width'] = NULL;
$arguments220['height'] = NULL;
$arguments220['minWidth'] = NULL;
$arguments220['minHeight'] = NULL;
$arguments220['maxWidth'] = NULL;
$arguments220['maxHeight'] = NULL;
$arguments220['treatIdAsReference'] = false;
$arguments220['image'] = NULL;
$arguments220['class'] = NULL;
$arguments220['dir'] = NULL;
$arguments220['id'] = NULL;
$arguments220['lang'] = NULL;
$arguments220['style'] = NULL;
$arguments220['title'] = NULL;
$arguments220['accesskey'] = NULL;
$arguments220['tabindex'] = NULL;
$arguments220['onclick'] = NULL;
$arguments220['ismap'] = NULL;
$arguments220['longdesc'] = NULL;
$arguments220['usemap'] = NULL;
$renderChildrenClosure222 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper223 = $self->getViewHelper('$viewHelper223', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper223->setArguments($arguments220);
$viewHelper223->setRenderingContext($renderingContext);
$viewHelper223->setRenderChildrenClosure($renderChildrenClosure222);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output219 .= $viewHelper223->initializeArgumentsAndRender();

$output219 .= '</div>
						';
return $output219;
};
$arguments201['__elseClosure'] = function() use ($renderingContext, $self) {
$output224 = '';

$output224 .= '
							<img src="uploads/pics/';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments225 = array();
$arguments225['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments225['keepQuotes'] = false;
$arguments225['encoding'] = NULL;
$arguments225['doubleEncode'] = true;
$renderChildrenClosure226 = function() use ($renderingContext, $self) {
return NULL;
};
$value227 = ($arguments225['value'] !== NULL ? $arguments225['value'] : $renderChildrenClosure226());

$output224 .= (!is_string($value227) ? $value227 : htmlspecialchars($value227, ($arguments225['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments225['encoding'] !== NULL ? $arguments225['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments225['doubleEncode']));

$output224 .= '" alt="" />
						';
return $output224;
};
$viewHelper228 = $self->getViewHelper('$viewHelper228', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper228->setArguments($arguments201);
$viewHelper228->setRenderingContext($renderingContext);
$viewHelper228->setRenderChildrenClosure($renderChildrenClosure202);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output200 .= $viewHelper228->initializeArgumentsAndRender();

$output200 .= '
				';
return $output200;
};

$output158 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments195, $renderChildrenClosure199, $renderingContext);

$output158 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments229 = array();
// Rendering Boolean node
$arguments229['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext));
$arguments229['then'] = NULL;
$arguments229['else'] = NULL;
$renderChildrenClosure230 = function() use ($renderingContext, $self) {
return '<div class="slide-navilr"><div class="slide-prev"></div><div class="slide-next"></div></div>';
};
$viewHelper231 = $self->getViewHelper('$viewHelper231', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper231->setArguments($arguments229);
$viewHelper231->setRenderingContext($renderingContext);
$viewHelper231->setRenderChildrenClosure($renderChildrenClosure230);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output158 .= $viewHelper231->initializeArgumentsAndRender();

$output158 .= '
			</div>
		</div>
	</div>
	';
return $output158;
};
$arguments1['__elseClosure'] = function() use ($renderingContext, $self) {
$output232 = '';

$output232 .= '
	<div class="imagetextbox imageleft">	
		<div class="imagetextbox-images">
			<div class="imagetextbox-images-inner ';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments233 = array();
// Rendering Boolean node
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments234 = array();
$arguments234['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext);
$arguments234['keepQuotes'] = false;
$arguments234['encoding'] = NULL;
$arguments234['doubleEncode'] = true;
$renderChildrenClosure235 = function() use ($renderingContext, $self) {
return NULL;
};
$value236 = ($arguments234['value'] !== NULL ? $arguments234['value'] : $renderChildrenClosure235());
$arguments233['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean((!is_string($value236) ? $value236 : htmlspecialchars($value236, ($arguments234['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments234['encoding'] !== NULL ? $arguments234['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments234['doubleEncode'])));
$arguments233['then'] = 'slideimages';
$arguments233['else'] = 'noslideimages';
$renderChildrenClosure237 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper238 = $self->getViewHelper('$viewHelper238', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper238->setArguments($arguments233);
$viewHelper238->setRenderingContext($renderingContext);
$viewHelper238->setRenderChildrenClosure($renderChildrenClosure237);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output232 .= $viewHelper238->initializeArgumentsAndRender();

$output232 .= '">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments239 = array();
// Rendering ViewHelper ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper
$arguments240 = array();
$arguments240['delimiter'] = ',';
$arguments240['subject'] = NULL;
$arguments240['removeEmpty'] = true;
$renderChildrenClosure241 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.image', $renderingContext);
};
$viewHelper242 = $self->getViewHelper('$viewHelper242', $renderingContext, 'ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper');
$viewHelper242->setArguments($arguments240);
$viewHelper242->setRenderingContext($renderingContext);
$viewHelper242->setRenderChildrenClosure($renderChildrenClosure241);
// End of ViewHelper ArminVieweg\Dce\ViewHelpers\ExplodeViewHelper
$arguments239['each'] = $viewHelper242->initializeArgumentsAndRender();
$arguments239['as'] = 'image';
$arguments239['key'] = '';
$arguments239['reverse'] = false;
$arguments239['iteration'] = NULL;
$renderChildrenClosure243 = function() use ($renderingContext, $self) {
$output244 = '';

$output244 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments245 = array();
// Rendering Boolean node
$arguments245['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext));
$arguments245['then'] = NULL;
$arguments245['else'] = NULL;
$renderChildrenClosure246 = function() use ($renderingContext, $self) {
$output247 = '';

$output247 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments248 = array();
$renderChildrenClosure249 = function() use ($renderingContext, $self) {
$output250 = '';

$output250 .= '
							<div class="slide-image">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments251 = array();
$output252 = '';

$output252 .= 'uploads/pics/';

$output252 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments251['src'] = $output252;
$arguments251['alt'] = '';
$arguments251['additionalAttributes'] = NULL;
$arguments251['width'] = NULL;
$arguments251['height'] = NULL;
$arguments251['minWidth'] = NULL;
$arguments251['minHeight'] = NULL;
$arguments251['maxWidth'] = NULL;
$arguments251['maxHeight'] = NULL;
$arguments251['treatIdAsReference'] = false;
$arguments251['image'] = NULL;
$arguments251['class'] = NULL;
$arguments251['dir'] = NULL;
$arguments251['id'] = NULL;
$arguments251['lang'] = NULL;
$arguments251['style'] = NULL;
$arguments251['title'] = NULL;
$arguments251['accesskey'] = NULL;
$arguments251['tabindex'] = NULL;
$arguments251['onclick'] = NULL;
$arguments251['ismap'] = NULL;
$arguments251['longdesc'] = NULL;
$arguments251['usemap'] = NULL;
$renderChildrenClosure253 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper254 = $self->getViewHelper('$viewHelper254', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper254->setArguments($arguments251);
$viewHelper254->setRenderingContext($renderingContext);
$viewHelper254->setRenderChildrenClosure($renderChildrenClosure253);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output250 .= $viewHelper254->initializeArgumentsAndRender();

$output250 .= '</div>
						';
return $output250;
};
$viewHelper255 = $self->getViewHelper('$viewHelper255', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper255->setArguments($arguments248);
$viewHelper255->setRenderingContext($renderingContext);
$viewHelper255->setRenderChildrenClosure($renderChildrenClosure249);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output247 .= $viewHelper255->initializeArgumentsAndRender();

$output247 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments256 = array();
$renderChildrenClosure257 = function() use ($renderingContext, $self) {
$output258 = '';

$output258 .= '
							<img src="uploads/pics/';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments259 = array();
$arguments259['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments259['keepQuotes'] = false;
$arguments259['encoding'] = NULL;
$arguments259['doubleEncode'] = true;
$renderChildrenClosure260 = function() use ($renderingContext, $self) {
return NULL;
};
$value261 = ($arguments259['value'] !== NULL ? $arguments259['value'] : $renderChildrenClosure260());

$output258 .= (!is_string($value261) ? $value261 : htmlspecialchars($value261, ($arguments259['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments259['encoding'] !== NULL ? $arguments259['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments259['doubleEncode']));

$output258 .= '" alt="" />
						';
return $output258;
};
$viewHelper262 = $self->getViewHelper('$viewHelper262', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper262->setArguments($arguments256);
$viewHelper262->setRenderingContext($renderingContext);
$viewHelper262->setRenderChildrenClosure($renderChildrenClosure257);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output247 .= $viewHelper262->initializeArgumentsAndRender();

$output247 .= '
						
					';
return $output247;
};
$arguments245['__thenClosure'] = function() use ($renderingContext, $self) {
$output263 = '';

$output263 .= '
							<div class="slide-image">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments264 = array();
$output265 = '';

$output265 .= 'uploads/pics/';

$output265 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments264['src'] = $output265;
$arguments264['alt'] = '';
$arguments264['additionalAttributes'] = NULL;
$arguments264['width'] = NULL;
$arguments264['height'] = NULL;
$arguments264['minWidth'] = NULL;
$arguments264['minHeight'] = NULL;
$arguments264['maxWidth'] = NULL;
$arguments264['maxHeight'] = NULL;
$arguments264['treatIdAsReference'] = false;
$arguments264['image'] = NULL;
$arguments264['class'] = NULL;
$arguments264['dir'] = NULL;
$arguments264['id'] = NULL;
$arguments264['lang'] = NULL;
$arguments264['style'] = NULL;
$arguments264['title'] = NULL;
$arguments264['accesskey'] = NULL;
$arguments264['tabindex'] = NULL;
$arguments264['onclick'] = NULL;
$arguments264['ismap'] = NULL;
$arguments264['longdesc'] = NULL;
$arguments264['usemap'] = NULL;
$renderChildrenClosure266 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper267 = $self->getViewHelper('$viewHelper267', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper267->setArguments($arguments264);
$viewHelper267->setRenderingContext($renderingContext);
$viewHelper267->setRenderChildrenClosure($renderChildrenClosure266);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output263 .= $viewHelper267->initializeArgumentsAndRender();

$output263 .= '</div>
						';
return $output263;
};
$arguments245['__elseClosure'] = function() use ($renderingContext, $self) {
$output268 = '';

$output268 .= '
							<img src="uploads/pics/';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments269 = array();
$arguments269['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'image', $renderingContext);
$arguments269['keepQuotes'] = false;
$arguments269['encoding'] = NULL;
$arguments269['doubleEncode'] = true;
$renderChildrenClosure270 = function() use ($renderingContext, $self) {
return NULL;
};
$value271 = ($arguments269['value'] !== NULL ? $arguments269['value'] : $renderChildrenClosure270());

$output268 .= (!is_string($value271) ? $value271 : htmlspecialchars($value271, ($arguments269['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments269['encoding'] !== NULL ? $arguments269['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments269['doubleEncode']));

$output268 .= '" alt="" />
						';
return $output268;
};
$viewHelper272 = $self->getViewHelper('$viewHelper272', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper272->setArguments($arguments245);
$viewHelper272->setRenderingContext($renderingContext);
$viewHelper272->setRenderChildrenClosure($renderChildrenClosure246);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output244 .= $viewHelper272->initializeArgumentsAndRender();

$output244 .= '
				';
return $output244;
};

$output232 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments239, $renderChildrenClosure243, $renderingContext);

$output232 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments273 = array();
// Rendering Boolean node
$arguments273['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.slideshow', $renderingContext));
$arguments273['then'] = NULL;
$arguments273['else'] = NULL;
$renderChildrenClosure274 = function() use ($renderingContext, $self) {
return '<div class="slide-navilr"><div class="slide-prev"></div><div class="slide-next"></div></div>';
};
$viewHelper275 = $self->getViewHelper('$viewHelper275', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper275->setArguments($arguments273);
$viewHelper275->setRenderingContext($renderingContext);
$viewHelper275->setRenderChildrenClosure($renderChildrenClosure274);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output232 .= $viewHelper275->initializeArgumentsAndRender();

$output232 .= '
			</div>
		</div>
		<div class="imagetextbox-text">
			<div class="imagetextbox-text-inner">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments276 = array();
// Rendering Boolean node
$arguments276['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext));
$arguments276['then'] = NULL;
$arguments276['else'] = NULL;
$renderChildrenClosure277 = function() use ($renderingContext, $self) {
$output278 = '';

$output278 .= '
					<h2>';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments279 = array();
$arguments279['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext);
$arguments279['keepQuotes'] = false;
$arguments279['encoding'] = NULL;
$arguments279['doubleEncode'] = true;
$renderChildrenClosure280 = function() use ($renderingContext, $self) {
return NULL;
};
$value281 = ($arguments279['value'] !== NULL ? $arguments279['value'] : $renderChildrenClosure280());

$output278 .= (!is_string($value281) ? $value281 : htmlspecialchars($value281, ($arguments279['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments279['encoding'] !== NULL ? $arguments279['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments279['doubleEncode']));

$output278 .= '</h2>
				';
return $output278;
};
$viewHelper282 = $self->getViewHelper('$viewHelper282', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper282->setArguments($arguments276);
$viewHelper282->setRenderingContext($renderingContext);
$viewHelper282->setRenderChildrenClosure($renderChildrenClosure277);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output232 .= $viewHelper282->initializeArgumentsAndRender();

$output232 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments283 = array();
// Rendering Boolean node
$arguments283['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.subheadline', $renderingContext));
$arguments283['then'] = NULL;
$arguments283['else'] = NULL;
$renderChildrenClosure284 = function() use ($renderingContext, $self) {
$output285 = '';

$output285 .= '
					<span class="subheadline bluemiddle">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments286 = array();
$arguments286['parseFuncTSPath'] = 'lib.parseFunc';
$renderChildrenClosure287 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.subheadline', $renderingContext);
};
$viewHelper288 = $self->getViewHelper('$viewHelper288', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper288->setArguments($arguments286);
$viewHelper288->setRenderingContext($renderingContext);
$viewHelper288->setRenderChildrenClosure($renderChildrenClosure287);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output285 .= $viewHelper288->initializeArgumentsAndRender();

$output285 .= '</span>
				';
return $output285;
};
$viewHelper289 = $self->getViewHelper('$viewHelper289', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper289->setArguments($arguments283);
$viewHelper289->setRenderingContext($renderingContext);
$viewHelper289->setRenderChildrenClosure($renderChildrenClosure284);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output232 .= $viewHelper289->initializeArgumentsAndRender();

$output232 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments290 = array();
// Rendering Boolean node
$arguments290['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.headline', $renderingContext));
$arguments290['then'] = NULL;
$arguments290['else'] = NULL;
$renderChildrenClosure291 = function() use ($renderingContext, $self) {
return '
					<div class="header-separator"></div>
				';
};
$viewHelper292 = $self->getViewHelper('$viewHelper292', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper292->setArguments($arguments290);
$viewHelper292->setRenderingContext($renderingContext);
$viewHelper292->setRenderChildrenClosure($renderChildrenClosure291);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output232 .= $viewHelper292->initializeArgumentsAndRender();

$output232 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments293 = array();
$arguments293['parseFuncTSPath'] = 'lib.parseFunc';
$renderChildrenClosure294 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.bodytext', $renderingContext);
};
$viewHelper295 = $self->getViewHelper('$viewHelper295', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper295->setArguments($arguments293);
$viewHelper295->setRenderingContext($renderingContext);
$viewHelper295->setRenderChildrenClosure($renderChildrenClosure294);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output232 .= $viewHelper295->initializeArgumentsAndRender();

$output232 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments296 = array();
// Rendering Boolean node
$arguments296['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinkid', $renderingContext));
$arguments296['then'] = NULL;
$arguments296['else'] = NULL;
$renderChildrenClosure297 = function() use ($renderingContext, $self) {
$output298 = '';

$output298 .= '<div class="imagetextbox-more"><a href="#" class="c';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments299 = array();
$arguments299['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinkid', $renderingContext);
$arguments299['keepQuotes'] = false;
$arguments299['encoding'] = NULL;
$arguments299['doubleEncode'] = true;
$renderChildrenClosure300 = function() use ($renderingContext, $self) {
return NULL;
};
$value301 = ($arguments299['value'] !== NULL ? $arguments299['value'] : $renderChildrenClosure300());

$output298 .= (!is_string($value301) ? $value301 : htmlspecialchars($value301, ($arguments299['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments299['encoding'] !== NULL ? $arguments299['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments299['doubleEncode']));

$output298 .= '">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments302 = array();
$arguments302['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'field.morelinktitle', $renderingContext);
$arguments302['keepQuotes'] = false;
$arguments302['encoding'] = NULL;
$arguments302['doubleEncode'] = true;
$renderChildrenClosure303 = function() use ($renderingContext, $self) {
return NULL;
};
$value304 = ($arguments302['value'] !== NULL ? $arguments302['value'] : $renderChildrenClosure303());

$output298 .= (!is_string($value304) ? $value304 : htmlspecialchars($value304, ($arguments302['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments302['encoding'] !== NULL ? $arguments302['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments302['doubleEncode']));

$output298 .= '</a></div>';
return $output298;
};
$viewHelper305 = $self->getViewHelper('$viewHelper305', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper305->setArguments($arguments296);
$viewHelper305->setRenderingContext($renderingContext);
$viewHelper305->setRenderChildrenClosure($renderChildrenClosure297);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output232 .= $viewHelper305->initializeArgumentsAndRender();

$output232 .= '
			</div>
		</div>
	</div>	
	';
return $output232;
};
$viewHelper306 = $self->getViewHelper('$viewHelper306', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper306->setArguments($arguments1);
$viewHelper306->setRenderingContext($renderingContext);
$viewHelper306->setRenderChildrenClosure($renderChildrenClosure2);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper306->initializeArgumentsAndRender();

$output0 .= ' ';

return $output0;
}


}
#1489071177    86856     