<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "make_printlink".
 *
 * Auto generated 11-11-2014 13:22
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Make Print Link',
	'description' => 'Generates a link to the current page with all nescessary GET (and possibly POST) parameters given, but another type.',
	'category' => 'misc',
	'shy' => 0,
	'version' => '1.5.3',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 1,
	'lockType' => '',
	'author' => 'Jens Ellerbrock',
	'author_email' => 'je@hades.org',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
			'php' => '3.0.0-0.0.0',
			'typo3' => '3.5.0-0.0.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:4:{s:12:"ext_icon.gif";s:4:"6d67";s:24:"ext_typoscript_setup.txt";s:4:"1afb";s:18:"make_printlink.php";s:4:"13cd";s:14:"doc/manual.sxw";s:4:"8037";}',
);

?>